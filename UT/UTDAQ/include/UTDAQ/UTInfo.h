/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/UTLiteCluster.h"
#include "UTDet/DeUTDetector.h"

#include <string>

/** Define some numbers for the UT which are detector specific
 *
 *
 *  @author Michel De Cian
 *  @date   2019-05-31
 */

namespace UTInfo {

  enum class DetectorNumbers { Sectors = 98, Regions = 3, Layers = 2, Stations = 2, TotalLayers = 4 };
  enum class MasksBits { LayerMask = 0x180000, StationMask = 0x600000, LayerBits = 19, StationBits = 21 };
  enum class SectorNumbers {
    EffectiveSectorsPerColumn = 28,
    MaxSectorsPerRegion       = 98,
    NumSectorsUTa             = 896,
    NumSectorsUTb             = 1008,
    MaxSectorsPerBoard        = 6,
    MaxBoards                 = 240
  };

  //--- Final Hit Location
  const std::string HitLocation = "UT/UTHits";
  //--- Detector Location of UT
  const std::string DetLocation = DeUTDetLocation::location();
  // DeUTDetLocation::location("UT");
  //--- Cluster location
  const std::string ClusLocation = LHCb::UTLiteClusterLocation::UTClusters;

  const int kMinStation = 0;  ///< Minimum valid station number for UT
  const int kMaxStation = 1;  ///< Maximum valid station number for UT
  const int kNStations  = 2;  ///< Number of UT stations
  const int kMinLayer   = 0;  ///< Minimum valid layer number for a UT station
  const int kMaxLayer   = 1;  ///< Maximum valid layer number for a UT station
  const int kNLayers    = 2;  ///< Number of UT layers within a station
  const int kMinRegion  = 0;  ///< Minimum valid region number for a UT layer
  const int kMaxRegion  = 11; ///< Maximum valid region number for a UT layer
  const int kNRegions   = 12; ///< Number of UT regions within a layer

} // namespace UTInfo
