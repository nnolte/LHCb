/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/RawBank.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/SIMDWrapper.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTInfo.h"

#include <array>
#include <boost/container/small_vector.hpp>
#include <optional>

namespace LHCb::UTDAQ {

  using simd = SIMDWrapper::best::types;

  /**
   * counts number of UT clusters in the given raw banks
   * if count exceeds max, it gives up and returns no value
   */
  std::optional<unsigned int> nbUTClusters( LHCb::RawBank::View banks, unsigned int maxNbClusters );

  struct LayerInfo final {
    float z;
    int   nColsPerSide;
    int   nRowsPerSide;
    float invHalfSectorYSize;
    float invHalfSectorXSize;
    float dxDy;
  };

  constexpr inline int max_sectors1 = align_size( static_cast<int>( UTInfo::SectorNumbers::NumSectorsUTa ) );
  constexpr inline int max_sectors2 = align_size( static_cast<int>( UTInfo::SectorNumbers::NumSectorsUTb ) );

  struct LUT final {
    std::array<int, max_sectors1> Station1;
    std::array<int, max_sectors2> Station2;

    std::size_t size{0};
    SOA_ACCESSOR( station1, Station1.data() )
    SOA_ACCESSOR( station2, Station2.data() )
  };

  using SectorsInRegionZ  = std::array<float, static_cast<int>( UTInfo::DetectorNumbers::Sectors )>;
  using SectorsInLayerZ   = std::array<SectorsInRegionZ, static_cast<int>( UTInfo::DetectorNumbers::Regions )>;
  using SectorsInStationZ = std::array<SectorsInLayerZ, static_cast<int>( UTInfo::DetectorNumbers::Layers )>;

  // -- For the moment, this is assigned here and overwritten in "computeGeometry" in case a geometry
  // -- version with a "wrong" sector ordering is used

  constexpr inline const auto mapSectorToSector = std::array{
      1,  2,  3,  4,  5,  0, 0, 0, 0, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 0, 0, 0, 0, 36, 37, 38, 39, 40,
      41, 42, 43, 44, 45, 0, 0, 0, 0, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 0, 0, 0, 0, 76, 77, 78, 79, 80};

  struct GeomCache final {
    std::array<LayerInfo, static_cast<int>( UTInfo::DetectorNumbers::TotalLayers )>      layers;
    std::array<SectorsInStationZ, static_cast<int>( UTInfo::DetectorNumbers::Stations )> sectorsZ;
    LUT                                                                                  sectorLUT;
    std::array<int, 64> mapQuarterSectorToSectorCentralRegion;
    GeomCache( const DeUTDetector& );
    GeomCache() = default;

    /**
     * fills container of sector fullID with all sectors concerned by
     * a hit at given layer and coordinates and with given x tolerance
     */
    void findSectorsFullID( unsigned int layer, float x, float y, float xTol, float yTol,
                            boost::container::small_vector_base<int>& sectors ) const;
  };

  /**
   * Function to get from layer (0-3), region and sector to global sector numbering
   * using SIMDWrapper
   */
  [[maybe_unused]] inline simd::int_v sectorFullID( unsigned int layer, typename simd::int_v region,
                                                    typename simd::int_v sector ) {
    constexpr int                  regions     = static_cast<int>( UTInfo::DetectorNumbers::Regions );
    constexpr int                  sectors     = static_cast<int>( UTInfo::SectorNumbers::MaxSectorsPerRegion );
    [[maybe_unused]] constexpr int totalLayers = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );
    assert( layer < totalLayers && "layer out of bound" );
    assert( none( region == 0 ) && none( region > regions ) && "region out of bound" );
    assert( none( sector == 0 ) && none( sector > sectors ) && "sector out of bound" );
    return ( layer * regions + region - 1 ) * sectors + sector - 1;
  }

  /**
   * Function to get from layer (0-3), region and sector to global sector numbering
   */
  constexpr inline int sectorFullID( unsigned int layer, unsigned int region, unsigned int sector ) {
    constexpr int                  regions     = static_cast<int>( UTInfo::DetectorNumbers::Regions );
    constexpr int                  sectors     = static_cast<int>( UTInfo::DetectorNumbers::Sectors );
    [[maybe_unused]] constexpr int totalLayers = static_cast<int>( UTInfo::DetectorNumbers::TotalLayers );
    assert( layer < totalLayers && "layer out of bound" );
    assert( region != 0 && region <= regions && "region out of bound" );
    assert( sector != 0 && sector <= sectors && "sector out of bound" );
    return ( layer * regions + region - 1 ) * sectors + sector - 1;
  }

  /**
   * Function to get from station (1-2), layer (1-2), region and sector to global sector numbering
   */
  constexpr inline int sectorFullID( unsigned int station, unsigned int layer, unsigned int region,
                                     unsigned int sector ) {
    constexpr int                  regions  = static_cast<int>( UTInfo::DetectorNumbers::Regions );
    constexpr int                  sectors  = static_cast<int>( UTInfo::DetectorNumbers::Sectors );
    constexpr int                  layers   = static_cast<int>( UTInfo::DetectorNumbers::Layers );
    [[maybe_unused]] constexpr int stations = static_cast<int>( UTInfo::DetectorNumbers::Stations );
    assert( station != 0 && station <= stations && "station out of bound" );
    assert( layer != 0 && layer <= layers && "layer out of bound" );
    assert( region != 0 && region <= regions && "region out of bound" );
    assert( sector != 0 && sector <= sectors && "sector out of bound" );
    return ( ( ( station - 1 ) * layers + ( layer - 1 ) ) * regions + ( region - 1 ) ) * sectors + ( sector - 1 );
  }

  //=============================================================================
  // -- find the sectors - SIMD version
  //=============================================================================
  template <typename F, typename I, typename = std::enable_if_t<std::is_same_v<I, typename LHCb::type_map<F>::int_t>>>
  inline __attribute__( ( always_inline ) ) auto findSectors( unsigned int layerIndex, F x, F y, F xTol, F yTol,
                                                              const LayerInfo& info, I& subcolmin2, I& subcolmax2,
                                                              I& subrowmin2, I& subrowmax2 ) {

    static_assert( std::is_same_v<F, typename LHCb::type_map<I>::float_t> );
    static_assert( std::is_same_v<I, typename LHCb::type_map<F>::int_t> );
    using M = typename LHCb::type_map<F>::mask_t;

    F localX = x - info.dxDy * y;
    // deal with sector overlaps and geometry imprecision
    xTol        = xTol + F{1.0f}; // mm
    F localXmin = localX - xTol;
    F localXmax = localX + xTol;

    I subcolmin{( localXmin * info.invHalfSectorXSize - 0.5f ) + 2.0f * ( info.nColsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding
    I subcolmax{( localXmax * info.invHalfSectorXSize - 0.5f ) + 2.0f * ( info.nColsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding

    M outOfAcceptanceCol = ( subcolmax < 0 ) || ( subcolmin > I{4 * info.nColsPerSide - 1} );
    // if( all(outOfAcceptanceCol) ) return;

    // deal with sector shifts in tilted layers and overlaps in regular ones
    yTol        = yTol + ( ( layerIndex == 1 || layerIndex == 2 ) ? F{8.0f} : F{1.0f} ); //  mm
    F localYmin = y - yTol;
    F localYmax = y + yTol;
    I subrowmin{( localYmin * info.invHalfSectorYSize - 0.5f ) + 2.0f * ( info.nRowsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding
    I subrowmax{( localYmax * info.invHalfSectorYSize - 0.5f ) + 2.0f * ( info.nRowsPerSide ) + 0.5f}; // faking
                                                                                                       // rounding

    M outOfAcceptanceRow = ( subrowmax < 0 ) || ( subrowmin > I{4 * info.nRowsPerSide - 1} );
    // if( all(outOfAcceptanceRow) ) return;

    // on the acceptance limit
    subcolmin2 = max( subcolmin, I{0} );
    subcolmax2 = min( subcolmax, I{4 * info.nColsPerSide - 1} );

    subrowmin2 = max( subrowmin, I{0} );
    subrowmax2 = min( subrowmax, I{( 4 * info.nRowsPerSide ) - 1} );

    return !outOfAcceptanceCol && !outOfAcceptanceRow;
  }

} // namespace LHCb::UTDAQ
