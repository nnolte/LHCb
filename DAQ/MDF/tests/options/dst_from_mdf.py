###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Write a DST (ROOT file) from an MDF."""
from Configurables import ApplicationMgr, LHCbApp, OutputStream
from PRConfig.TestFileDB import test_file_db

output_file = "output.dst"
output_type = "ROOT"
writer = OutputStream(
    ItemList=["/Event/DAQ/RawEvent#1"],
    Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
        output_file),
)

test_file_db["MiniBrunel_2018_MinBias_FTv4_MDF"].run(
    configurable=LHCbApp(EvtMax=1000, ), )
ApplicationMgr(TopAlg=[writer])
