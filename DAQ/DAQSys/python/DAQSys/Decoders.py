###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Database of standard decoders, can be added to or manipulated in user code.

This database is normally eaten by the DAQSys configurables to organize which data will go where.
"""
from DAQSys.DecoderClass import Decoder

DecoderDB = {}

#===========ODIN===========
Decoder(
    "LHCb__UnpackRawEvent",
    properties={"BankTypes": None},
    inputs={"RawEventLocation": None},
    outputs={"RawBankLocations": None},
    active=True,
    conf=DecoderDB)

Decoder(
    "createODIN",
    active=True,
    banks=["ODIN"],
    inputs={"RawBanks": None},
    outputs={"ODIN": None},
    required=["LHCb__UnpackRawEvent"],
    publicTools=["OdinTimeDecoder/ToolSvc.OdinTimeDecoder"],
    conf=DecoderDB)

Decoder(
    "OdinTimeDecoder/ToolSvc.OdinTimeDecoder",
    active=False,
    privateTools=["ODINDecodeTool"],
    conf=DecoderDB)

Decoder(
    "ODINDecodeTool",
    active=False,  #tool handle??
    inputs={"RawEventLocations": None},
    outputs={"ODINLocation": None},
    conf=DecoderDB)

#===========RICH===========

Decoder(
    "Rich::Future::RawBankDecoder/RichFutureDecode",
    active=True,
    banks=['Rich'],
    outputs={"DecodedDataLocation": None},
    inputs={"RawEventLocation": None},
    #required=["createODIN"],
    conf=DecoderDB)

#===========ECAL and HCAL===========

#ECAL and HCAL use the same algorithms, where the name of the alg
#decides which detector is decoded!

dec_calo_zs = []
dec_calo_nzs = []

for cal in ["Ecal", "Hcal"]:
    name = "Future" + cal + "ZSup"
    algname = "CaloFutureRawToDigits/" + name
    Decoder(
        algname,
        active=True,
        banks=[cal + 'E'],
        inputs={"RawEventLocation": None},
        outputs={"OutputDigitData": "Raw/" + cal + "/CaloDigits"},
        conf=DecoderDB)

#===========MUON===========
tname = "MuonRawBuffer"

Decoder(
    "MuonRec",
    active=True,
    banks=["Muon"],
    inputs={"RawEventLocations": None},
    outputs={"OutputLocation": None},
    privateTools=[tname],  #used in TAE mode
    publicTools=[tname + "/ToolSvc." + tname],  #used in TAE mode
    conf=DecoderDB)

tool = Decoder("MuonRawBuffer", active=False, conf=DecoderDB)

tool2 = tool.clone(tname + "/ToolSvc." + tname)

#TRIGGER ==========HLT===========

#firstly the Dec/Sel/Vertex reports,
#these need separate versions for Hlt1/2 split scenario,
#decoding into different locations

#create them all in a similar way, since they have similar options
#for each decoder we have an HLT1, HLT2 and combined version.
#Decoders look like "Hlt(''/1/2)(Sel/Dec/Vertex)ReportsDecoder"

#report, the type of report
for report in ["Dec", "Sel"]:
    #which HLT to decode?
    for hltname in ["Hlt1", "Hlt2"]:
        #create the decoder
        dec = Decoder(
            #\/ e.g. HltSelReportsDecoder/Hlt1SelReportsDecoder
            "Hlt" + report + "ReportsDecoder/" + hltname + report +
            "ReportsDecoder",
            active=True,
            #\/ e.g. HltSelReports
            banks=["Hlt" + report + "Reports"],
            inputs={"RawEventLocations": None},
            #\/ e.g. OutputHltSelReportsLocation: Hlt1/SelReports
            outputs={
                "OutputHlt" + report + "ReportsLocation":
                hltname + "/" + report + "Reports"
            },
            properties={"SourceID": hltname},
            conf=DecoderDB)
        if report == "Sel":
            dec.Outputs[
                "OutputHltObjectSummariesLocation"] = hltname + "/SelReports/Candidates"

#Vertex Decoder
for hltname in ["Hlt1", "Hlt2"]:
    Decoder(
        "HltVertexReportsDecoder/" + hltname + "VertexReportsDecoder",
        active=True,
        banks=["HltVertexReports"],
        inputs={"RawEventLocations": None},
        outputs={
            "OutputHltVertexReportsLocations": [
                hltname + "/VertexReports/PV3D",
                hltname + "/VertexReports/ProtoPV3D"
            ]
        },
        properties={"SourceID": hltname},
        conf=DecoderDB)

#is a Routing bits filter really a decoder? it doesn't create output...
Decoder(
    "HltRoutingBitsFilter",
    active=False,
    banks=["HltRoutingBits"],
    inputs={"RawEventLocations": None},
    conf=DecoderDB)

Decoder(
    "HltLumiSummaryDecoder",
    active=True,
    banks=["HltLumiSummary"],
    inputs={"RawEventLocations": None},
    outputs={"OutputContainerName": None},
    conf=DecoderDB)

from GaudiConf.PersistRecoConf import PersistRecoPacking
# The following decoder depends on the data type. By default you
# get whatever is below, but "framework users" should override it!
__packing = PersistRecoPacking(data_type='Upgrade', stream='/Event/Hlt2')
Decoder(
    "HltPackedBufferDecoder/Hlt2PackedBufferDecoder",
    active=True,
    banks=["DstData"],
    inputs={"RawEventLocations": None},
    outputs=__packing.packedLocations(),
    conf=DecoderDB)
