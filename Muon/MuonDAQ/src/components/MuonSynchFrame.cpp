/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonSynchFrame.h"

#include "Event/MuonBankVersion.h"

//-----------------------------------------------------------------------------
// Implementation file for class : MuonSynchFrame
//
// 2021-12-14 : Alessia Satta
//-----------------------------------------------------------------------------

void MuonSynchFrame::setHitAndTime( unsigned int pos, unsigned int num ) {
  m_hitmap[47 - pos] = true;
  m_TDCdata[pos]     = (num)&MuonSynchFrameMask::MaskTime;
}

void MuonSynchFrame::getFullSynchFrame( unsigned int frame[8] ) {
  // first copy hit map
  for ( int i = 0; i < 32; i++ ) {
    if ( m_hitmap[i] ) { frame[0] |= 1UL << i; }
  }
  for ( int i = 0; i < 16; i++ ) {
    if ( m_hitmap[i + 32] ) { frame[1] |= 1UL << i; }
  }
  // now TDC
  for ( int i = 0; i < 48; i++ ) {
    unsigned int nframe = ( i * 4 ) / 32;
    unsigned int shift  = ( i * 4 ) % 32;

    frame[nframe] |= ( m_TDCdata[i] & MuonSynchFrameMask::MaskTime ) << shift;
  }
}

ShortType MuonSynchFrame::getSynchFrame( unsigned int frame[3], bool edac ) {
  // first copy hit map
  // remember frame start on MSB :-(((
  unsigned int count_hits = 0;
  unsigned int max_hits   = 12;
  unsigned int nframe     = 0;
  unsigned int shift      = 0;
  if ( edac ) max_hits = 10;
  for ( int i = 0; i < 48; i++ ) {
    if ( m_hitmap[i] ) {
      if ( i < 32 ) {
        frame[0] |= 1UL << ( 31 - i );
      } else {
        frame[1] |= 1UL << ( 31 - ( i - 32 ) );
      }
    }
    if ( m_hitmap[47 - i] ) {
      if ( count_hits < max_hits ) {
        nframe = ( 16 + count_hits * 4 ) / 32 + 1;
        shift  = ( 44 - count_hits * 4 ) % 32;
        frame[nframe] |= ( m_TDCdata[i] & MuonSynchFrameMask::MaskTime ) << shift;
      }
      count_hits++;
    }
  }
  return count_hits;
}
