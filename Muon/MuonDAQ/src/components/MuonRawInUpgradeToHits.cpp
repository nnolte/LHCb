/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonDet/MuonTilePositionUpgrade.h"

#include "LHCbAlgs/Transformer.h"

#include "GaudiKernel/ToolHandle.h"

#include <boost/numeric/conversion/cast.hpp>

#include <array>
#include <bitset>
#include <functional>
#include <optional>
#include <string>
#include <vector>

/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */
using namespace Muon::DAQ;
namespace LHCb::MuonUpgrade::DAQ {

  namespace {
    struct Digit {
      Detector::Muon::TileID tile;
      unsigned int           tdc;
      Digit( Detector::Muon::TileID tile, unsigned int tdc ) : tile{tile}, tdc{tdc} {} // C++20: please remove this line
    };
  } // namespace

  namespace {
    ByteType single_bit_position[8] = {0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01};
  }

  namespace EC::RawToHits {

    enum class ErrorCode : StatusCode::code_t {
      BAD_MAGIC = 10,
      BANK_TOO_SHORT,
      PADDING_TOO_LONG,
      TOO_MANY_HITS,
      INVALID_TELL1
    };
    struct ErrorCategory : StatusCode::Category {
      const char* name() const override { return "MuonRawBankDecoding"; }
      bool        isRecoverable( StatusCode::code_t ) const override { return false; }
      std::string message( StatusCode::code_t code ) const override {
        switch ( static_cast<ErrorCode>( code ) ) {
        case ErrorCode::BAD_MAGIC:
          return "Incorrect Magic pattern in raw bank";
        case ErrorCode::BANK_TOO_SHORT:
          return "Muon bank is too short";
        case ErrorCode::TOO_MANY_HITS:
          return "Muon bank has too many hits for its size";
        default:
          return StatusCode::default_category().message( code );
        }
      }
    };
  } // namespace EC::RawToHits
} // namespace LHCb::MuonUpgrade::DAQ

STATUSCODE_ENUM_DECL( LHCb::MuonUpgrade::DAQ::EC::RawToHits::ErrorCode )
STATUSCODE_ENUM_IMPL( LHCb::MuonUpgrade::DAQ::EC::RawToHits::ErrorCode,
                      LHCb::MuonUpgrade::DAQ::EC::RawToHits::ErrorCategory )

namespace LHCb::MuonUpgrade::DAQ {
  namespace {
    [[gnu::noreturn]] void throw_exception( EC::RawToHits::ErrorCode ec, const char* tag ) {
      auto sc = StatusCode( ec );
      throw GaudiException{sc.message(), tag, std::move( sc )};
    }
#define OOPS( x ) throw_exception( x, __PRETTY_FUNCTION__ )
    template <typename Iterator>
    Iterator addCoordsCrossingMap( Iterator first, Iterator last, CommonMuonHits& commonHits,
                                   const ComputeTilePosition& compute, size_t /* nStations */ ) {
      // need to calculate the shape of the horizontal and vertical logical strips

      // used flags
      assert( std::distance( first, last ) < 500 );
      std::bitset<500> used; // (to be updated with new readout) the maximum # of channels per quadrant is currently 384
                             // (from M2R2) the pads readout region have been ignored otherwise larger number is
                             // possible

      // find if region is already at pads
      bool alreadyPads = false;

      if ( first != last ) {
        auto s = first->tile.station();
        auto r = first->tile.region();
        if ( s == 0 && r > 1 ) alreadyPads = true;
        if ( s == 2 && r == 0 ) alreadyPads = true;
        if ( s == 3 && r == 0 ) alreadyPads = true;
        if ( s == 3 && r == 3 ) alreadyPads = true;
      }

      // partition into the two directions of digits
      // vertical and horizontal stripes
      Iterator mid;

      if ( !alreadyPads ) {
        mid = std::partition( first, last, []( const Digit& digit ) { return digit.tile.isHorizontal(); } );
      } else {
        // if already a pad region skip the crossing
        mid = last;
      }

      auto digitsOne = make_span( first, mid );
      auto digitsTwo = make_span( mid, last );

      // check how many cross
      if ( first != mid && mid != last ) {
        auto thisGridX = first->tile.layout().xGrid();
        auto thisGridY = first->tile.layout().yGrid();

        auto     otherGridX = mid->tile.layout().xGrid();
        auto     otherGridY = mid->tile.layout().yGrid();
        unsigned i          = 0;
        for ( const Digit& one : digitsOne ) {
          unsigned int calcX = one.tile.nX() * otherGridX / thisGridX;
          unsigned     j     = mid - first;
          for ( const Digit& two : digitsTwo ) {
            unsigned int calcY = two.tile.nY() * thisGridY / otherGridY;
            if ( calcX == two.tile.nX() && calcY == one.tile.nY() ) {
              Detector::Muon::TileID pad( one.tile );
              pad.setY( two.tile.nY() );
              pad.setLayout( {thisGridX, otherGridY} );
              auto&& [pos, dx, dy] = compute.tilePosition( pad );
              commonHits.emplace_back( std::move( pad ), one.tile, two.tile, pos.X(), dx, pos.Y(), dy, pos.Z(), 0,
                                       one.tdc, one.tdc - two.tdc, 0 );
              used[i] = used[j] = true;
            }
            ++j;
          }
          ++i;
        }
      }

      if ( alreadyPads ) {
        for ( const Digit& digit : digitsOne ) {
          auto pos = compute.tilePosition( digit.tile );

          commonHits.emplace_back( digit.tile, pos.p.X(), pos.dX, pos.p.Y(), pos.dY, pos.p.Z(), 0., 1, digit.tdc,
                                   digit.tdc );
        }
      } else {
        unsigned m = 0;
        for ( const Digit& digit : digitsOne ) {

          if ( !used[m++] ) {

            auto pos = compute.stripXPosition( digit.tile );
            commonHits.emplace_back( digit.tile, pos.p.X(), pos.dX, pos.p.Y(), pos.dY, pos.p.Z(), 0., 1, digit.tdc,
                                     digit.tdc );
          }
        }
        for ( const Digit& digit : digitsTwo ) {
          if ( !used[m++] ) {

            auto pos = compute.stripYPosition( digit.tile );
            commonHits.emplace_back( digit.tile, pos.p.X(), pos.dX, pos.p.Y(), pos.dY, pos.p.Z(), 0., 1, digit.tdc,
                                     digit.tdc );
          }
        }
      }

      return last;
    }
  } // namespace
  //-----------------------------------------------------------------------------
  // Implementation file for class : RawToHits
  //-----------------------------------------------------------------------------
  class RawToHits final
      : public Algorithm::Transformer<MuonHitContainer( const EventContext&, const RawEvent&, const DeMuonDetector&,
                                                        const ComputeTilePosition& ),
                                      DetDesc::usesConditions<DeMuonDetector, ComputeTilePosition>> {
  public:
    RawToHits( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode       initialize() override;
    StatusCode       finalize() override;
    MuonHitContainer operator()( const EventContext&, const RawEvent&, const DeMuonDetector&,
                                 const ComputeTilePosition& ) const override;

  private:
    mutable unsigned int                                local_hits_counters[44][16]       = {};
    mutable unsigned int                                local_wrong_hits_counters[44][16] = {};
    mutable unsigned int                                local_acquired_frame[44][16]      = {};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_invalid_add{this, "invalid add"};
    mutable Gaudi::Accumulators::Counter<>              m_Tell40Size[44]{
        {this, "Tell_001_0 size"}, {this, "Tell_001_1 size"}, {this, "Tell_002_0 size"}, {this, "Tell_002_1 size"},
        {this, "Tell_003_0 size"}, {this, "Tell_003_1 size"}, {this, "Tell_004_0 size"}, {this, "Tell_004_1 size"},
        {this, "Tell_005_0 size"}, {this, "Tell_005_1 size"}, {this, "Tell_006_0 size"}, {this, "Tell_006_1 size"},
        {this, "Tell_007_0 size"}, {this, "Tell_007_1 size"}, {this, "Tell_008_0 size"}, {this, "Tell_008_1 size"},
        {this, "Tell_009_0 size"}, {this, "Tell_009_1 size"}, {this, "Tell_010_0 size"}, {this, "Tell_010_1 size"},
        {this, "Tell_011_0 size"}, {this, "Tell_011_1 size"}, {this, "Tell_012_0 size"}, {this, "Tell_012_1 size"},
        {this, "Tell_013_0 size"}, {this, "Tell_013_1 size"}, {this, "Tell_014_0 size"}, {this, "Tell_014_1 size"},
        {this, "Tell_015_0 size"}, {this, "Tell_015_1 size"}, {this, "Tell_016_0 size"}, {this, "Tell_016_1 size"},
        {this, "Tell_017_0 size"}, {this, "Tell_017_1 size"}, {this, "Tell_018_0 size"}, {this, "Tell_018_1 size"},
        {this, "Tell_019_0 size"}, {this, "Tell_019_1 size"}, {this, "Tell_020_0 size"}, {this, "Tell_020_1 size"},
        {this, "Tell_021_0 size"}, {this, "Tell_021_1 size"}, {this, "Tell_022_0 size"}, {this, "Tell_022_1 size"}};
    Gaudi::Property<bool> m_print_stat{this, "PrintStat", false};
  };

  DECLARE_COMPONENT_WITH_ID( RawToHits, "MuonRawInUpgradeToHits" )

  //=============================================================================
  // Standard constructor
  //=============================================================================
  RawToHits::RawToHits( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"RawEventLocation", RawEventLocation::Default},
                      KeyValue{"MuonDetectorLocation", DeMuonLocation::Default},
                      KeyValue{"TilePositionCalculator", "AlgorithmSpecific-" + name + "-TilePositionCalculator"}},
                     KeyValue{"HitContainer", MuonHitContainerLocation::Default} ) {}

  //=============================================================================
  // Initialisation
  //=============================================================================
  StatusCode RawToHits::initialize() {
    return Transformer::initialize().andThen( [&]() {
      addConditionDerivation<ComputeTilePosition( const DeMuonDetector& )>( {DeMuonLocation::Default},
                                                                            inputLocation<ComputeTilePosition>() );
    } );
  }

  StatusCode RawToHits::finalize() {
    return Transformer::finalize().andThen( [&]() {
      if ( m_print_stat ) {
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 size " << i / 2 + 1 << " " << i % 2 << " " << m_Tell40Size[i] << endmsg;
        }
        info() << "----------------------------------------------------------------------------------------------------"
                  "--------------------------------------------------"
               << endmsg;
        info() << " Number of hits in each link " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            if ( local_acquired_frame[i][j] > 0 ) {
              info() << "  " << std::setw( 10 ) << local_hits_counters[i][j] << "  ";
            } else {
              info() << "  " << std::setw( 10 ) << "n/c"
                     << "  ";
            }

            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of bits in hits map not connected to FE chsannes in each link " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            if ( local_acquired_frame[i][j] > 0 ) {
              info() << "  " << std::setw( 10 ) << local_wrong_hits_counters[i][j] << "  ";
            } else {
              info() << "  " << std::setw( 10 ) << "n/c"
                     << "  ";
            }
            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
        info() << " Number of acquired events  in each link " << endmsg;
        for ( int i = 0; i < 44; i++ ) {
          info() << "Tell40 " << std::setw( 2 ) << i / 2 + 1 << " PCI number " << i % 2 << "-------"; // endmsg;
          for ( int j = 0; j < 16; j++ ) {
            info() << "  " << std::setw( 8 ) << local_acquired_frame[i][j] << "  ";
            if ( j == 7 ) {
              info() << endmsg;
              info() << std::setw( 29 ) << " ";
            }
          }
          info() << endmsg;
        }
      }
    } );
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  MuonHitContainer RawToHits::operator()( const EventContext& evtCtx, const RawEvent& raw, const DeMuonDetector& det,
                                          const ComputeTilePosition& compute ) const {

    if ( msgLevel( MSG::DEBUG ) ) { debug() << "==> Execute the decoding" << endmsg; }
    size_t nStations = boost::numeric_cast<size_t>( det.stations() );
    assert( nStations <= 4 );
    const auto& muon_banks  = raw.banks( RawBank::Muon );
    auto        memResource = LHCb::getMemResource( evtCtx );
    // Maybe not actually important to set up the memory resource properly here..?
    auto stations = LHCb::make_object_array<CommonMuonStation, 4>( memResource );
    if ( muon_banks.empty() ) return {std::move( stations )};

    // array of vectors of hits
    // each element of the array correspond to hits from a single station
    // this will ease the sorting after
    auto decoding = LHCb::make_object_array<std::vector<Digit, LHCb::Allocators::EventLocal<Digit>>, 64>( memResource );

    std::vector<Digit> digits_inTell40;
    digits_inTell40.reserve( 48 ); // 48 is the max number of hits in each Tell40 link
    // std::vector<LHCb::span<ByteType>> lista_link;
    // lista_link.reserve( 64 );
    for ( const auto& raw_bank : muon_banks ) {
      if ( RawBank::MagicPattern != raw_bank->magic() ) { OOPS( EC::RawToHits::ErrorCode::BAD_MAGIC ); }

      // the Tell40 number is hidden in source ID lower part
      unsigned int tell40PCI       = raw_bank->sourceID() & 0x00FF;
      unsigned int TNumber         = tell40PCI / 2 + 1;
      unsigned int PCINumber       = tell40PCI % 2;
      unsigned int stationOfTell40 = det.getUpgradeDAQInfo()->whichstationTell40( TNumber - 1 );
      unsigned int active_links    = det.getUpgradeDAQInfo()->getNumberOfActiveLinkInTell40PCI( TNumber, PCINumber );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " reading tell40 num " << TNumber << " PCI n " << PCINumber << " "
                << " station " << stationOfTell40 << " active link " << active_links << endmsg;
      if ( msgLevel( MSG::DEBUG ) )
        debug() << " reading tell40 num " << TNumber << " PCI n " << PCINumber << " "
                << " bank size" << (unsigned int)raw_bank->size() << endmsg;

      if ( (unsigned int)raw_bank->size() < 1 ) { OOPS( EC::RawToHits::ErrorCode::BANK_TOO_SHORT ); }
      auto         range8             = raw_bank->range<ByteType>();
      auto         range_data         = range8.subspan( 1 );
      unsigned int link_start_pointer = 0;
      unsigned int regionOfLink       = 99;
      unsigned int quarterOfLink      = 99;
      unsigned int ZS_applied         = ( range8[0] & 0x05 );
      unsigned int EDAC_applied       = ( range8[0] & 0x08 ) >> 3;
      unsigned int synch_evt          = ( range8[0] & 0x10 ) >> 4;
      unsigned int align_info         = ( range8[0] & 0x20 ) >> 5;
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "header of bank reports :" << endmsg;
        debug() << " ZS type " << ZS_applied << " "
                << " EDAC enabled " << EDAC_applied << " " << endmsg;
        debug() << " Synch event " << synch_evt << " aligned fiber info stored " << align_info << endmsg;
      }
      if ( synch_evt ) continue;
      if ( (unsigned int)raw_bank->size() < 1 + 3 * align_info ) { OOPS( EC::RawToHits::ErrorCode::BANK_TOO_SHORT ); }
      unsigned int number_of_readout_fibers = 0;
      unsigned int map_connected_fibers[24] = {};

      // need to determine the true enabled links, possible only if align_info is true
      // probably this part will be removed when info will be stored in DB
      if ( !align_info ) {
        for ( unsigned int i = 0; i < active_links; i++ ) { map_connected_fibers[i] = i; }
        number_of_readout_fibers = active_links;
        if ( (unsigned int)raw_bank->size() < active_links + 1 ) { OOPS( EC::RawToHits::ErrorCode::BANK_TOO_SHORT ); }

      } else {
        link_start_pointer = link_start_pointer + 3;
        auto range_fiber   = range8.subspan( 1, 3 );
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "connected fibers " << std::bitset<8>( range_fiber[0] ) << " " << std::bitset<8>( range_fiber[1] )
                  << " " << std::bitset<8>( range_fiber[2] ) << endmsg;
        bool         align_vector[24] = {};
        unsigned int readout_fibers   = 0;
        for ( int i = 0; i < 8; i++ ) {
          if ( ( range_fiber[0] >> i ) & 0x1 ) {
            align_vector[16 + i] = true;
            readout_fibers++;
          }
          if ( ( range_fiber[1] >> i ) & 0x1 ) {
            align_vector[8 + i] = true;
            readout_fibers++;
          }
          if ( ( range_fiber[2] >> i ) & 0x1 ) {
            align_vector[i] = true;
            readout_fibers++;
          }
        }
        if ( msgLevel( MSG::VERBOSE ) ) {
          for ( int i = 0; i < 24; i++ ) { verbose() << i << " " << align_vector[i] << endmsg; }
        }

        unsigned int fib_counter = 0;
        if ( msgLevel( MSG::DEBUG ) ) debug() << "how many active link " << active_links << endmsg;
        for ( unsigned int i = 0; i < active_links; i++ ) {
          if ( align_vector[i] ) {
            map_connected_fibers[fib_counter] = i;
            if ( msgLevel( MSG::DEBUG ) )
              debug() << "filling " << fib_counter << " " << map_connected_fibers[fib_counter] << endmsg;
            fib_counter++;
          }
        }
        if ( msgLevel( MSG::VERBOSE ) ) {
          for ( int i = 0; i < 24; i++ ) { verbose() << i << " " << align_vector[i] << endmsg; }
        }
        number_of_readout_fibers = readout_fibers;
        if ( (unsigned int)raw_bank->size() < number_of_readout_fibers + 1 + 3 )
          OOPS( EC::RawToHits::ErrorCode::BANK_TOO_SHORT );
      }
      if ( (unsigned int)raw_bank->size() > number_of_readout_fibers + 1 + 3 * align_info ) {
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << " link " << TNumber << " " << PCINumber << " " << number_of_readout_fibers << " "
                    << raw_bank->size() - number_of_readout_fibers + 1 + 3 * align_info << endmsg;
          m_Tell40Size[( TNumber - 1 ) * 2 + PCINumber] +=
              raw_bank->size() - number_of_readout_fibers - 1 - 3 * align_info;
        }
      }
      for ( unsigned int link = 0; link < number_of_readout_fibers; link++ ) {
        unsigned int hits_in_frame       = 0;
        unsigned int wrong_hits_in_frame = 0;

        unsigned int reroutered_link = map_connected_fibers[link];
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " number of active links " << number_of_readout_fibers << " " << link << " " << reroutered_link
                  << endmsg;
        }
        regionOfLink  = det.getUpgradeDAQInfo()->regionOfLink( TNumber, PCINumber, reroutered_link );
        quarterOfLink = det.getUpgradeDAQInfo()->quarterOfLink( TNumber, PCINumber, reroutered_link );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "region and quarter " << regionOfLink << " " << quarterOfLink << endmsg;
        }
        digits_inTell40.clear();
        digits_inTell40.reserve( 48 );
        /*unsigned int offsetDAQ = ( TNumber - 1 ) * MuonUpgradeDAQHelper_maxTell40PCINumber_linkNumber_ODEFrameSize +
                                 PCINumber * MuonUpgradeDAQHelper_linkNumber_ODEFrameSize +
                                 reroutered_link * MuonUpgradeDAQHelper_ODEFrameSize;*/
        ByteType     curr_byte    = range_data[link_start_pointer];
        unsigned int size_of_link = ( ( curr_byte & 0xF0 ) >> 4 ) + 1;

        // single link data
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " new link " << link << " " << size_of_link << " " << link_start_pointer << endmsg;
        if ( msgLevel( MSG::DEBUG ) ) debug() << " new link " << std::bitset<8>( curr_byte ) << endmsg;

        if ( size_of_link > 1 ) {
          // get two subpart (there is an overlap due to the 4 bits shift
          auto range_link_HitsMap = range_data.subspan( link_start_pointer, 7 );
          auto range_link_TDC     = range_data.subspan( link_start_pointer + 6, size_of_link - 6 );

          bool         first_hitmap_byte  = false;
          bool         last_hitmap_byte   = true;
          unsigned int count_byte         = 0;
          unsigned int pos_in_link        = 0;
          unsigned int nSynch_hits_number = 0;
          unsigned int TDC_counter        = 0;
          TDC_counter                     = range_link_TDC.size() * 2 - 1;
          unsigned int TDC_value          = 0;

          for ( auto r = range_link_HitsMap.rbegin(); r < range_link_HitsMap.rend(); r++ ) {
            // loop in reverse mode hits map is 47->0
            count_byte++;
            if ( count_byte == 7 ) first_hitmap_byte = true;
            if ( count_byte > 7 ) break; // should never happens
            ByteType data_copy = *r;
            for ( unsigned int bit_pos_1 = 8; bit_pos_1 > 0; --bit_pos_1 ) {
              unsigned int bit_pos = bit_pos_1 - 1;

              if ( first_hitmap_byte && bit_pos < 4 ) continue; // better put break;
              if ( last_hitmap_byte && bit_pos > 3 ) continue;  // better put bit_pos_1=4
              // if ( first_hitmap_byte && bit_pos < 4 ) break; //better put break;
              // if ( last_hitmap_byte && bit_pos > 3 ) {bit_pos_1=5; continue ;}
              if ( data_copy & single_bit_position[bit_pos] ) {
                if ( msgLevel( MSG::DEBUG ) ) debug() << "found a bit in pos " << bit_pos << endmsg;
                hits_in_frame++;

                LHCb::Detector::Muon::TileID hitTile =
                    ( det.getUpgradeDAQInfo() )
                        ->TileInTell40FrameNoHole( TNumber, PCINumber, reroutered_link, pos_in_link );

                if ( msgLevel( MSG::DEBUG ) )
                  debug() << " hit " << bit_pos << " " << pos_in_link << " " << reroutered_link << " " << hitTile
                          << endmsg;
                if ( hitTile.isDefined() ) {
                  if ( nSynch_hits_number < TDC_counter ) {
                    // TDC info associated to first hits are transimitted,let's pair Muontile and TDC

                    switch ( nSynch_hits_number ) {
                    case 0:
                      TDC_value = ( range_link_TDC[0] & 0x0F );
                      break;
                    case 1:
                      TDC_value = ( range_link_TDC[1] & 0xF0 ) >> 4;
                      break;
                    case 2:
                      TDC_value = ( range_link_TDC[1] & 0x0F );
                      break;
                    case 3:
                      TDC_value = ( range_link_TDC[2] & 0xF0 ) >> 4;
                      break;
                    case 4:
                      TDC_value = ( range_link_TDC[2] & 0x0F );
                      break;
                    case 5:
                      TDC_value = ( range_link_TDC[3] & 0xF0 ) >> 4;
                      break;
                    case 6:
                      TDC_value = ( range_link_TDC[3] & 0x0F );
                      break;
                    case 7:
                      TDC_value = ( range_link_TDC[4] & 0xF0 ) >> 4;
                      break;
                    case 8:
                      TDC_value = ( range_link_TDC[4] & 0x0F );
                      break;
                    case 9:
                      TDC_value = ( range_link_TDC[5] & 0xF0 ) >> 4;
                      break;
                    case 10:
                      TDC_value = ( range_link_TDC[5] & 0x0F );
                      break;
                    case 11:
                      TDC_value = ( range_link_TDC[6] & 0xF0 ) >> 4;
                      break;
                    default:
                      TDC_value = 0;
                      break;
                    }
                    struct Digit d = {hitTile, TDC_value};
                    if ( msgLevel( MSG::DEBUG ) )
                      debug() << "found a new hit " << hitTile << " time " << TDC_value << endmsg;
                    digits_inTell40.emplace_back( d );
                  } else {

                    // there are too much hits in this nSync, so some of them have not the associated TDC stored in data
                    struct Digit d = {hitTile, 0};

                    if ( msgLevel( MSG::DEBUG ) )
                      debug() << "found a new hit " << hitTile << " no time associated " << endmsg;
                    digits_inTell40.emplace_back( d );
                  }

                  nSynch_hits_number++;
                } else {
                  wrong_hits_in_frame++;
                  info() << TNumber << " " << PCINumber << " " << reroutered_link << " " << pos_in_link << " "
                         << stationOfTell40 << " " << regionOfLink << endmsg;
                  ++m_invalid_add;
                }

                // end here
              }

              pos_in_link++;
            }

            last_hitmap_byte = false;
          }
        }
        // fill stats counters;
        local_hits_counters[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] += hits_in_frame;
        local_wrong_hits_counters[( TNumber - 1 ) * 2 + PCINumber][reroutered_link] += wrong_hits_in_frame;
        local_acquired_frame[( TNumber - 1 ) * 2 + PCINumber][reroutered_link]++;
        // move the raw bank pointer to next link
        link_start_pointer = link_start_pointer + size_of_link;

        if ( stationOfTell40 * 16 + regionOfLink * 4 + quarterOfLink < 64 ) {

          decoding[stationOfTell40 * 16 + regionOfLink * 4 + quarterOfLink].insert(
              decoding[stationOfTell40 * 16 + regionOfLink * 4 + quarterOfLink].end(),
              std::make_move_iterator( digits_inTell40.begin() ), std::make_move_iterator( digits_inTell40.end() ) );
        }
      }
    }

    auto addCrossings = [&]( auto f, auto l, auto& dest ) {
      return addCoordsCrossingMap( f, l, dest, compute, nStations );
    };

    unsigned int where = 0;

    unsigned station = 0;

    CommonMuonHits commonHits_sta[4];
    CommonMuonHits commonHits_12{memResource};

    int count_sta = 0;

    for ( auto& decode : decoding ) {
      if ( where % 16 == 15 ) {
        commonHits_sta[station].reserve( count_sta );
        count_sta = 0;
        station++;
      }
      count_sta = count_sta + decode.size();

      where++;
    }
    where   = 0;
    station = 0;

    for ( auto& decode : decoding ) {

      for ( auto i = decode.begin(); i != decode.end(); i = addCrossings( i, decode.end(), commonHits_sta[station] ) )
        ; /* empty on purpose */

      if ( where % 16 == 15 ) {
        stations[station] = CommonMuonStation{det, station, std::move( commonHits_sta[station] )};
        station++;
      }

      where++;
    }

    return MuonHitContainer{std::move( stations )};
  }

} // namespace LHCb::MuonUpgrade::DAQ
