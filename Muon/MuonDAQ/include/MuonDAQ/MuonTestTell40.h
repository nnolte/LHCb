/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONTESTTELL40_H
#define MUONTESTTELL40_H 1

// from Gaudi
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"

// from FTDet
//#include "FTDet/DeFTDetector.h"

#include "Event/MuonCoord.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
/** @class MCFTDigitMonitor MCFTDigitMonitor.h
 *
 *
 *  @author Eric Cogneras, Luca Pescatore
 *  @date   2012-07-05
 */

class MuonTestTell40 : public Gaudi::Functional::Consumer<void( const MuonHitContainer& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  MuonTestTell40( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const MuonHitContainer& hits ) const override;

private:
  //  void fillHistograms( const LHCb::MCFTDigit* mcDigit, const std::set<const LHCb::MCHit*>& mcHits,
  //                   const std::string& hitType ) const;
};
#endif // MUONTESTTELL40_H
