###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/GenEvent
--------------
#]=======================================================================]

gaudi_add_library(GenEvent
    SOURCES
        src/BeamForInitialization.cpp
        src/CrossSectionsFSR.cpp
        src/GenCountersFSR.cpp
        src/HepMCEvent.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            HepMC::HepMC
            LHCb::EventBase
)

gaudi_add_dictionary(GenEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK
        HepMC::HepMC
        LHCb::GenEvent
)
