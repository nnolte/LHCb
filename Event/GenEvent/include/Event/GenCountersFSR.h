/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/VectorMap.h"
#include <algorithm>
#include <map>
#include <ostream>

namespace LHCb {

  /** @class GenCountersFSR GenCountersFSR.h
   *
   * Enum class for Generator counters
   *
   * @author Davide Fazzini
   *
   */

  class GenCountersFSR final {
  public:
    /// lookup table for counter keys
    enum CounterKey {
      AllEvt = 0, // Total number of events generated, including events with no interactions (empty events). One event
                  // can be made of several interactions
      ZeroInt      = 1,  // Events (amongst the AllEvt) with no interactions
      EvtGenerated = 2,  // Total number of generated events in general of Minimum Bias type (one event contains several
                         // pile-up interactions)
      IntGenerated  = 3, // Total number of generated interactions in the generated events
      EvtAccepted   = 4, // Number of accepted events amongst the generated events (event containing one signal B, ...)
      IntAccepted   = 5, // Number of interactions contained in the accepted events
      BeforeFullEvt = 6, // Number of events before the full event generator level cut
      AfterFullEvt  = 7, // Number of events after the full event generator level cut
      OnebGen       = 10, // Number of generated interactions containing at least 1 b quark
      ThreebGen     = 11, // Number of generated interactions containing at least 3 b quarks
      PromptBGen =
          12, // Number of generated interactions containing at least 1 prompt B (one B hadron without a b quark)
      OnecGen    = 13, // Number of generated interactions containing at least 1 c quark
      ThreecGen  = 14, // Number of generated interactions containing at least 3 c quarks
      PromptCGen = 15, // Number of generated interactions containing at least 1 prompt charmed hadron (one D hadron
                       // without a c quark)
      bAndcGen  = 16,  // Number of generated interactions containing at least 1 b quark and 1 c quark
      OnebAcc   = 17,  // Number of accepted  interactions containing at least 1 b quark
      ThreebAcc = 18,  // Number of accepted  interactions containing at least 3 b quarks
      PromptBAcc =
          19, // Number of accepted  interactions containing at least 1 prompt B (one B hadron without a b quark)
      OnecAcc    = 20,     // Number of accepted  interactions containing at least 1 c quark
      ThreecAcc  = 21,     // Number of accepted  interactions containing at least 3 c quarks
      PromptCAcc = 22,     // Number of accepted  interactions containing at least 1 prompt charmed hadron (one D hadron
                           // without a c quark)
      bAndcAcc       = 23, // Number of accepted  interactions containing at least 1 b quark and 1 c quark
      BeforeLevelCut = 26, // Number of inclusive events generated
      AfterLevelCut  = 27, // Number of inclusive events passing the generator level cut and where at least one selected
                           // particle has pz > 0.
      EvtInverted = 28,    // Number of z-inverted events
      B0Gen =
          30, // Number of generated B0 in events with at least one selected particle (contained in the inclusive list)
      antiB0Gen  = 31, // Number of generated anti-B0 in events with at least one selected particle
      BplusGen   = 32, // Number of generated B+ in events with at least one selected particle
      BminusGen  = 33, // Number of generated B- in events with at least one selected particle
      Bs0Gen     = 34, // Number of generated Bs0 in events with at least one selected particle
      antiBs0Gen = 35, // Number of generated anti-Bs0 in events with at least one selected particle
      bBaryonGen =
          36, // Number of generated b-Baryon (with negative PDG code) in events with at least one selected particle
      antibBaryonGen = 37, // Number of generated anti-b-Baryon (with positive PDG code) in events with at least one
                           // selected particle
      BcplusGen      = 38, // Number of generated Bc+ in events with at least one selected particle
      BcminusGen     = 39, // Number of generated Bc- in events with at least one selected particle
      bbGen          = 40, // Number of generated b anti-b states in events with at least one selected particle
      B0Acc          = 41, // Number of B0 in accepted events (passing the generator level cuts)
      antiB0Acc      = 42, // Number of anti-B0 in accepted events
      BplusAcc       = 43, // Number of B+ in accepted events
      BminusAcc      = 44, // Number of B- in accepted events
      Bs0Acc         = 45, // Number of Bs0 in accepted events
      antiBs0Acc     = 46, // Number of anti-Bs0 in accepted events
      bBaryonAcc     = 47, // Number of b-Baryon (with negative PDG code) in accepted events
      antibBaryonAcc = 48, // Number of anti-b-Baryon (with positive PDG code) in accepted events
      BcplusAcc      = 49, // Number of Bc+ in accepted events
      BcminusAcc     = 50, // Number of Bc- in accepted events
      bbAcc          = 51, // Number of b anti-b states in accepted events
      D0Gen          = 55, // Number of generated D0
      antiD0Gen      = 56, // Number of generated anti-D0
      DplusGen       = 57, // Number of generated D+
      DminusGen      = 58, // Number of generated D-
      DsplusGen      = 59, // Number of generated Ds+
      DsminusGen     = 60, // Number of generated Ds-
      cBaryonGen     = 61, // Number of generated c-Baryon (with positive PDG code)
      anticBaryonGen = 62, // Number of generated anti-c-Baryon (with negative PDG code)
      ccGen          = 63, // Number of generated c anti-c states
      D0Acc          = 64, // Number of D0 in accepted events
      antiD0Acc      = 65, // Number of anti-D0 in accepted events
      DplusAcc       = 66, // Number of D+ in accepted events
      DminusAcc      = 67, // Number of D- in accepted events
      DsplusAcc      = 68, // Number of Ds+ in accepted events
      DsminusAcc     = 69, // Number of Ds- in accepted events
      cBaryonAcc     = 70, // Number of c-Baryon (with positive PDG code) in accepted events
      anticBaryonAcc = 71, // Number of anti-c-Baryon (with negative PDG code) in accepted events
      ccAcc          = 72, // Number of c anti-c states in accepted events
      BGen           = 75, // Number of generated B (spin 0, angular momentum 0)
      BstarGen       = 76, // Number of generated B* (spin 0, angular momentum >= 1)
      B2starGen      = 77, // Number of generated B** (spin >= 1)
      BAcc           = 78, // Number of B (spin 0, angular momentum 0) in accepted events
      BstarAcc       = 79, // Number of B* (spin 0, angular momentum >= 1) in accepted events
      B2starAcc      = 80, // Number of B** (spin >= 1) in accepted events
      DGen           = 85, // Number of generated D (spin 0, angular momentum 0)
      DstarGen       = 86, // Number of generated D* (spin 0, angular momentum >= 1)
      D2starGen      = 87, // Number of generated D** (spin >= 1)
      DAcc           = 88, // Number of D (spin 0, angular momentum 0) in accepted events
      DstarAcc       = 89, // Number of D* (spin 0, angular momentum >= 1) in accepted events
      D2starAcc      = 90, // Number of D** (spin >= 1) in accepted events
      EvtSignal      = 91, // Number of generated signal events
      EvtantiSignal  = 92, // Number of generated anti-signal events
      BeforePCut     = 93, // Number of signal events containing a signal particle generated
      AfterPCut      = 94, // Number of signal particle passing the generator level cut with pz > 0
      BeforeantiPCut = 95, // Number of signal events containing a signal anti-particle generated
      AfterantiPCut  = 96, // Number of signal anti-particle passing the generator level cut with pz > 0
      Unknown        = 98, // Unknown value
      AllsubProcess  = 100 // Total cross-section of Minimum Bias events
    };

    friend std::ostream& operator<<( std::ostream& s, CounterKey e );

    /// Default Constructor
    GenCountersFSR() = delete;

    /// conversion from string to enum of type CounterKey
    static CounterKey CounterKeyToType( const std::string& aName ) {
      auto iter = s2ck.find( aName );
      return iter != s2ck.end() ? iter->second : CounterKey::Unknown;
    }

    /// conversion to string from enum type CounterKey
    static const std::string& CounterKeyToString( CounterKey aEnum ) {
      static const std::string s_Unknown = "Unknown";
      auto iter = std::find_if( s2ck.begin(), s2ck.end(), [&]( const auto& i ) { return i.second == aEnum; } );
      return iter != s2ck.end() ? iter->first : s_Unknown;
    }

    /// return a map with the complete names of generator counters
    static const std::map<CounterKey, std::string>& getFullNames() { return ck2s; }

  private:
    static const GaudiUtils::VectorMap<std::string, CounterKey> s2ck;
    static const std::map<CounterKey, std::string>              ck2s;

  }; // class GenCountersFSR

} // namespace LHCb
