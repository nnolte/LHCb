/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoAddCombineDLLs.cpp
 *
 * Implementation file for tool ChargedProtoAddCombineDLLs
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 15/11/2006
 */
//-----------------------------------------------------------------------------

#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"

namespace LHCb::Rec::ProtoParticle::Charged {
  namespace {
    //-----------------------------------------------------------------------------
    /** @class CombinedLL ChargedProtoPAlg.h
     *
     *  Utility class holding the combined LL values for a ProtoParticle
     *
     *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
     *  @date 29/03/2006
     */
    //-----------------------------------------------------------------------------
    class CombinedLL {
    public:
      /// Standard constructor with initialisation value
      CombinedLL( double init = 0 )
          : elDLL( init ), muDLL( init ), piDLL( init ), kaDLL( init ), prDLL( init ), deDLL( init ){};
      double elDLL; ///< Electron Log Likelihood
      double muDLL; ///< Muon Log Likelihood
      double piDLL; ///< Pion Log Likelihood
      double kaDLL; ///< Kaon Log Likelihood
      double prDLL; ///< Proton Log Likelihood
      double deDLL; ///< Deuteron Log Likelihood
    public:
      /// Implement ostream << method
      friend std::ostream& operator<<( std::ostream& s, const CombinedLL& dlls ) {
        return s << "[ " << dlls.elDLL << " " << dlls.muDLL << " " << dlls.piDLL << " " << dlls.kaDLL << " "
                 << dlls.prDLL << " " << dlls.deDLL << " ]";
      }
    };

    struct Cmp {
      using is_transparent = void;
      bool operator()( std::string_view lhs, std::string_view rhs ) const {
        return std::lexicographical_compare( lhs.begin(), lhs.end(), rhs.begin(), rhs.end(),
                                             []( char l, char r ) { return std::toupper( l ) < std::toupper( r ); } );
      }
    };
    using namespace std::string_view_literals;
    const auto m_maskTechnique = std::map<std::string_view, int, Cmp>{{{"RICH"sv, 0x1},
                                                                       {"MUON"sv, 0x2},
                                                                       {"ECAL"sv, 0x4},
                                                                       {"HCAL"sv, 0x8},
                                                                       {"PRS"sv, 0x10},
                                                                       {"SPD"sv, 0x20},
                                                                       {"BREM"sv, 0x40},
                                                                       {"CALO"sv, 0x7C}},
                                                                      Cmp{}};
  } // namespace

  class AddCombineDLLs final : public extends<GaudiTool, Interfaces::IProtoParticles> {

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode initialize() override;                                              ///< Algorithm initialization
    StatusCode operator()( ProtoParticles&, IGeometryInfo const& ) const override; ///< Algorithm execution

  private: // methods
    /// Add the Rich DLL information to the combined DLL
    bool addRich( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const;

    /// Add the Muon DLL information to the combined DLL
    bool addMuon( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const;

    /// Add the Calo DLL information to the combined DLL
    bool addCalo( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const;

  private: // data
    Gaudi::Property<std::vector<std::string>> m_elDisable{this, "ElectronDllDisable", {}};
    Gaudi::Property<std::vector<std::string>> m_muDisable{this, "MuonDllDisable", {}};
    Gaudi::Property<std::vector<std::string>> m_kaDisable{this, "KaonDllDisable", {}};
    Gaudi::Property<std::vector<std::string>> m_piDisable{this, "ProtonDllDisable", {}};
    Gaudi::Property<std::vector<std::string>> m_prDisable{this, "PionllDisable", {}};
    Gaudi::Property<std::vector<std::string>> m_deDisable{this, "DeuteronDllDisable", {}};

    int m_elCombDll{0xFFFF};
    int m_muCombDll{0xFFFF};
    int m_prCombDll{0xFFFF};
    int m_piCombDll{0xFFFF};
    int m_kaCombDll{0xFFFF};
    int m_deCombDll{0xFFFF};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( AddCombineDLLs, "ChargedProtoParticleAddCombineDLLs" )

  //=============================================================================
  // Initialization
  //=============================================================================
  StatusCode AddCombineDLLs::initialize() {
    StatusCode scc = extends::initialize();
    if ( scc.isFailure() ) return scc;

    for ( const auto& itech : m_elDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Electron PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_elCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_muDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Muon PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_muCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_prDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Proton PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_prCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_piDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Pion PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_piCombDll &= ~i->second;
      }
    }
    for ( const auto& itech : m_kaDisable ) {
      auto i = m_maskTechnique.find( itech );
      if ( i == m_maskTechnique.end() ) {
        error() << "Kaon PID technique " << itech << " unknown" << endmsg;
        scc = StatusCode::FAILURE;
      } else {
        m_kaCombDll &= ~i->second;
      }
    }

    info() << "Using retuned RICH el and mu DLL values in combined DLLs" << endmsg;
    if ( 0 == m_elCombDll )
      Warning( "Not creating Combined DLL for electron hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_muCombDll ) Warning( "Not creating Combined DLL for muon hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_piCombDll ) Warning( "Not creating Combined DLL for pion hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_kaCombDll ) Warning( "Not creating Combined DLL for kaon hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_prCombDll ) Warning( "Not creating Combined DLL for proton hypothesis", StatusCode::SUCCESS ).ignore();
    if ( 0 == m_deCombDll )
      Warning( "Not creating Combined DLL for deuteron hypothesis", StatusCode::SUCCESS ).ignore();

    return scc;
  }

  //=============================================================================
  // Main execution
  //=============================================================================
  StatusCode AddCombineDLLs::operator()( ProtoParticles& protos, IGeometryInfo const& ) const ///< Algorithm execution
  {

    // Loop over the protos
    for ( auto* proto : protos ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << "Creating Combined DLLs for ProtoParticle " << proto->key() << endmsg;

      // Remove any current Combined DLL information
      proto->removeCombinedInfo();

      bool hasPIDinfo( false );

      // Combined DLL data object for this proto
      CombinedLL combDLL( 0 );

      // Add any RICH info
      hasPIDinfo |= addRich( proto, combDLL );

      // Add any MUON info
      hasPIDinfo |= addMuon( proto, combDLL );

      // Add any CALO info
      hasPIDinfo |= addCalo( proto, combDLL );

      if ( hasPIDinfo ) {
        // Store the final combined DLL values into the ProtoParticle
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CombDLLe, combDLL.elDLL - combDLL.piDLL );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CombDLLmu, combDLL.muDLL - combDLL.piDLL );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CombDLLpi, 0 ); // by definition
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CombDLLk, combDLL.kaDLL - combDLL.piDLL );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CombDLLp, combDLL.prDLL - combDLL.piDLL );
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::CombDLLd, combDLL.deDLL - combDLL.piDLL );
      } else {
        proto->addInfo( LHCb::ProtoParticle::additionalInfo::NoPID, 1 );
      }

    } // loop over protos

    return StatusCode::SUCCESS;
  }

  bool AddCombineDLLs::addRich( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const {
    // Add RICH Dll information.
    bool ok = false;
    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::RichPIDStatus ) ) {
      ok = true;
      // Apply renormalisation of RICH el and mu DLL values
      // Eventually, should make these tunable job options ....
      const int rTechnique = m_maskTechnique.at( "RICH" );
      if ( 0 != ( m_elCombDll & rTechnique ) )
        combDLL.elDLL += 7.0 * tanh( proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLe, 0 ) / 40.0 );
      if ( 0 != ( m_muCombDll & rTechnique ) )
        combDLL.muDLL += 7.0 * tanh( proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLmu, 0 ) / 5.0 );
      if ( 0 != ( m_piCombDll & rTechnique ) )
        combDLL.piDLL += proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLpi, 0 );
      if ( 0 != ( m_kaCombDll & rTechnique ) )
        combDLL.kaDLL += proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLk, 0 );
      if ( 0 != ( m_prCombDll & rTechnique ) )
        combDLL.prDLL += proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLp, 0 );
      if ( 0 != ( m_deCombDll & rTechnique ) )
        combDLL.deDLL += proto->info( LHCb::ProtoParticle::additionalInfo::RichDLLd, 0 );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding RICH info " << combDLL << endmsg;
    }
    return ok;
  }

  bool AddCombineDLLs::addMuon( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const {

    bool ok = false;
    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::MuonPIDStatus ) ) {
      ok                   = true;
      const int mTechnique = m_maskTechnique.at( "MUON" );
      if ( 0 != ( m_elCombDll & mTechnique ) )
        combDLL.elDLL += proto->info( LHCb::ProtoParticle::additionalInfo::MuonBkgLL, 0 );
      if ( 0 != ( m_muCombDll & mTechnique ) )
        combDLL.muDLL += proto->info( LHCb::ProtoParticle::additionalInfo::MuonMuLL, 0 );
      if ( 0 != ( m_piCombDll & mTechnique ) )
        combDLL.piDLL += proto->info( LHCb::ProtoParticle::additionalInfo::MuonBkgLL, 0 );
      if ( 0 != ( m_kaCombDll & mTechnique ) )
        combDLL.kaDLL += proto->info( LHCb::ProtoParticle::additionalInfo::MuonBkgLL, 0 );
      if ( 0 != ( m_prCombDll & mTechnique ) )
        combDLL.prDLL += proto->info( LHCb::ProtoParticle::additionalInfo::MuonBkgLL, 0 );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding MUON info " << combDLL << endmsg;
    }
    return ok;
  }

  bool AddCombineDLLs::addCalo( LHCb::ProtoParticle* proto, CombinedLL& combDLL ) const {
    bool ok = false;
    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::EcalPIDe ) ||
         proto->hasInfo( LHCb::ProtoParticle::additionalInfo::EcalPIDmu ) ) {
      const int eTechnique = m_maskTechnique.at( "ECAL" );
      if ( 0 != ( m_elCombDll & eTechnique ) )
        combDLL.elDLL += proto->info( LHCb::ProtoParticle::additionalInfo::EcalPIDe, 0 );
      if ( 0 != ( m_muCombDll & eTechnique ) )
        combDLL.muDLL += proto->info( LHCb::ProtoParticle::additionalInfo::EcalPIDmu, 0 );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding ECAL info " << combDLL << endmsg;
      ok = true;
    }

    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::HcalPIDe ) ||
         proto->hasInfo( LHCb::ProtoParticle::additionalInfo::HcalPIDmu ) ) {
      const int hTechnique = m_maskTechnique.at( "HCAL" );
      if ( 0 != ( m_elCombDll & hTechnique ) )
        combDLL.elDLL += proto->info( LHCb::ProtoParticle::additionalInfo::HcalPIDe, 0 );
      if ( 0 != ( m_muCombDll & hTechnique ) )
        combDLL.muDLL += proto->info( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, 0 );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding HCAL info " << combDLL << endmsg;
      ok = true;
    }

    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::PrsPIDe ) ) {
      const int pTechnique = m_maskTechnique.at( "PRS" );
      if ( 0 != ( m_elCombDll & pTechnique ) )
        combDLL.elDLL += proto->info( LHCb::ProtoParticle::additionalInfo::PrsPIDe, 0 );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding PRS  info " << combDLL << endmsg;
      ok = true;
    }

    if ( proto->hasInfo( LHCb::ProtoParticle::additionalInfo::BremPIDe ) ) {
      const int bTechnique = m_maskTechnique.at( "BREM" );
      if ( 0 != ( m_elCombDll & bTechnique ) )
        combDLL.elDLL += proto->info( LHCb::ProtoParticle::additionalInfo::BremPIDe, 0 );
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> Adding BREM info " << combDLL << endmsg;
      ok = true;
    }
    return ok;
  }

} // namespace LHCb::Rec::ProtoParticle::Charged
//=============================================================================
