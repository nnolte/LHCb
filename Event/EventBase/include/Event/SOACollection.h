/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"
#include "LHCbMath/SIMDWrapper.h"
#include "SOAUtils.h"
#include "ZipUtils.h"

#include <boost/align/is_aligned.hpp>
#include <boost/integer/common_factor_rt.hpp>
#include <boost/mp11/set.hpp>

#include <cstdint>
#include <tuple>

#include "Proxy.h"
#include "SOAData.h"
#include "SOAZip.h"

// For example and documentation, see: Event/TrackEvent/Event/PrSeedTracks.h

namespace LHCb::Event {
  /** This is a utility that functions a bit like a struct full of std::vector
   *  members. The key differences are that:
   *  - all vectors are served by the same memory allocation
   *  - there is only one size -- all columns are the same length
   *  This is designed to be a useful tool for LHCb::Event::make_zip()-compatible
   *  event model classes. It was designed with LHCb::Composites in mind.
   *  SOACollection is a CRTP base class, i.e. the intended use is that you
   *  define e.g.
   *    struct Composites : public SOACollection<Composites, Tag1, Tag2, ...> {
   *      auto const* column1data() const { return data<Tag1>(); }
   *    };
   *  It is possible for a Tag to represent a runtime-variable (but
   *  class-instance-constant) number of columns.
   */
  template <typename Derived, typename... Tags>
  struct SOACollection {
    // Useful alias below.
    using TagsTuple = std::tuple<Tags...>;

    // Utility to check if this collection contains Tag
    template <typename Tag>
    using hasTag = boost::mp11::mp_set_contains<TagsTuple, Tag>;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = Proxy<simd, behaviour, ContainerType>;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using resolve_proxy_t =
        typename ContainerType::template proxy_type<resolve_instruction_set_v<simd>, behaviour, ContainerType>;

  private:
    // The implementation assumes that tag types are unique. Assert that.
    static_assert( boost::mp11::mp_is_set<TagsTuple>::value, "Tag types must be unique" );

    // Widest SIMD vector backend that is supported, in bytes.
    // SOACollection guarantees that it is valid to read/write a vector this
    // wide starting from any valid container entry, though the contained
    // values are undefined if they lie outside the validity of the container.
    constexpr static std::size_t max_vector_width = constants::max_vector_width;

    // The number of extra entries that we must guarantee are valid after the
    // notional last ("capacity() - 1") element.
    constexpr static std::size_t required_padding_entries = max_vector_width / sizeof( int );

  public:
    using data_tree_t    = SOADataStruct<Tags...>;
    using allocator_type = LHCb::Allocators::EventLocal<data_tree_t>;
    SOACollection( Zipping::ZipFamilyNumber zip_identifier = Zipping::generateZipIdentifier(),
                   allocator_type           alloc          = {} )
        : m_alloc{std::move( alloc )}, m_zip_identifier{zip_identifier} {}
    SOACollection( Zipping::ZipFamilyNumber zn, SOACollection const& old ) : SOACollection( zn, old.get_allocator() ) {}

    // Could define SOACollection( SOACollection& old ) : SOACollection() { swap( *this, old ); }
    // if we had a default constructor that didn't generate new zip identifiers
    SOACollection( SOACollection&& old ) noexcept
        : m_alloc{std::move( old.m_alloc )}
        , m_data_tree{std::exchange( old.m_data_tree, nullptr )}
        , m_size{std::exchange( old.m_size, 0 )}
        , m_capacity{std::exchange( old.m_capacity, 0 )}
        , m_zip_identifier{old.m_zip_identifier} {}
    ~SOACollection() {
      if ( m_data_tree ) {
        m_data_tree->deallocate( m_alloc.resource(), m_capacity );
        std::allocator_traits<allocator_type>::deallocate( m_alloc, m_data_tree, 1 );
      }
    }
    friend void swap( SOACollection& a, SOACollection& b ) noexcept {
      using std::swap;
      swap( a.m_alloc, b.m_alloc );
      swap( a.m_data_tree, b.m_data_tree );
      swap( a.m_size, b.m_size );
      swap( a.m_capacity, b.m_capacity );
      swap( a.m_zip_identifier, b.m_zip_identifier );
    }
    SOACollection& operator=( SOACollection&& old ) noexcept {
      swap( *this, old );
      return *this;
    }
    SOACollection( SOACollection const& ) = delete;
    SOACollection& operator=( SOACollection const& ) = delete;

    [[gnu::always_inline]] inline void resize( std::size_t size ) {
      reserve_exponential( size );
      if ( m_size < size ) { m_data_tree->construct( m_size, size ); }
      m_size = size;
    }

    void reserve( std::size_t required_capacity ) {
      required_capacity = safe_capacity( required_capacity );
      if ( required_capacity > capacity() ) { reallocate( required_capacity ); }
    }

    void                                   clear() { resize( 0 ); }
    [[nodiscard]] bool                     empty() const { return m_size == 0U; }
    [[nodiscard]] std::size_t              size() const { return m_size; }
    [[nodiscard]] std::size_t              capacity() const { return m_capacity; }
    [[nodiscard]] allocator_type           get_allocator() const { return m_alloc; }
    [[nodiscard]] Zipping::ZipFamilyNumber zipIdentifier() const { return m_zip_identifier; }

    template <SIMDWrapper::InstructionSet simd, typename ProxyType>
    [[gnu::always_inline]] inline void copy_back( ProxyType const& from ) {
      using simd_t = typename SIMDWrapper::type_map_t<simd>;
      reserve_exponential( m_size + simd_t::size );
      m_data_tree->template copy_back<simd_t>( m_size, *from.container()->m_data_tree, from.offset(),
                                               std::true_type{} );
      m_size += simd_t::size;
    }

    template <SIMDWrapper::InstructionSet simd, typename ProxyType>
    [[gnu::always_inline]] inline void copy_back( ProxyType const&                                      from,
                                                  typename SIMDWrapper::type_map_t<simd>::mask_v const& mask ) {
      using simd_t = typename SIMDWrapper::type_map_t<simd>;
      // Copy elements from 'from' into ourself according to 'mask'
      // First, make sure we have enough capacity
      auto const count = popcount( mask );
      if ( !count ) {
        // warning if you think about removing this: if m_size == count == 0
        // then this would lead to a segfault
        return;
      }
      reserve_exponential( m_size + count );
      m_data_tree->template copy_back<simd_t>( m_size, *from.container()->m_data_tree, from.offset(), mask );
      m_size += count;
    }

    template <typename simd_t>
    [[gnu::always_inline]] inline void copy_back( SOACollection const& from, int at ) {
      // Copy simd_t::size elements from 'from' into ourself
      // First, make sure we have enough capacity
      reserve_exponential( m_size + simd_t::size );
      m_data_tree->template copy_back<simd_t>( m_size, *from.m_data_tree, at, std::true_type{} );
      m_size += simd_t::size;
    }

    template <typename simd_t>
    [[gnu::always_inline]] inline void copy_back( SOACollection const& from, int at,
                                                  typename simd_t::mask_v const& mask ) {
      // Copy elements from 'from' into ourself according to 'mask'
      // First, make sure we have enough capacity
      auto const count = popcount( mask );
      if ( !count ) {
        // warning if you think about removing this: if m_size == count == 0
        // then this would lead to a segfault
        return;
      }
      reserve_exponential( m_size + count );
      m_data_tree->template copy_back<simd_t>( m_size, *from.m_data_tree, at, mask );
      m_size += count;
    }

    template <typename Tag, typename F, typename M = std::true_type>
    void store( int at, F const& value, M&& mask = {} ) {
      store_impl( std::next( this->data<Tag>(), at ), value, mask );
    }
    // -- make sure it accepts an int as an argument
    template <typename Tag, typename I, typename M = std::true_type>
    void store( int at, int i, I const& value, M&& mask = {} ) {
      store_impl( std::next( this->data<Tag>( i ), at ), value, mask );
    }

    // -- an enum as an argument
    template <typename Tag, typename I, typename L, typename M = std::true_type,
              typename std::enable_if_t<std::is_enum_v<L>>* = nullptr>
    void store( int at, L i, I const& value, M&& mask = {} ) {
      store_impl( std::next( this->data<Tag>( static_cast<unsigned>( i ) ), at ), value, mask );
    }
    // -- two ints as arguments
    template <typename Tag, typename I, typename M = std::true_type>
    void store( int at, int i, int j, I const& value, M&& mask = {} ) {
      store_impl( std::next( this->data<Tag>( i, j ), at ), value, mask );
    }

    // -- int and an enum as arguments
    template <typename Tag, typename I, typename L, typename M = std::true_type,
              typename std::enable_if_t<std::is_enum_v<L>>* = nullptr>
    void store( int at, int i, L j, I const& value, M&& mask = {} ) {
      store_impl( std::next( this->data<Tag>( i, static_cast<unsigned>( j ) ), at ), value, mask );
    }

  private:
    /**
     * @brief all public store() functions dispatch to this implementation for the actual store operation
     *
     * @tparam S type of storage
     * @tparam V type of the value that will be stored
     * @tparam M type of the mask, either std::true_type or a mask form SIMDWrapper
     * @param storage pointer that we store to
     * @param val value to be stored
     * @param mask perform masked store, only used if not of type std::true_type
     */
    template <typename S, typename V, typename M>
    void store_impl( S* storage, V const& val, M const& mask ) {
      if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
        val.store( storage );
      } else {
        val.compressstore( mask, storage );
      }
    }

    /** Helper for methods that want to trigger exponential capacity growth if
     *  more capacity is needed (copy_back, resize)
     */
    [[gnu::always_inline]] inline void reserve_exponential( std::size_t required_capacity ) {
      required_capacity          = safe_capacity( required_capacity );
      auto const usable_capacity = capacity();
      if ( required_capacity > usable_capacity ) { reallocate( std::max( required_capacity, 2 * usable_capacity ) ); }
    }

    /** Helper for reserve methods that assumes its argument is nonzero, has
     *  been rounded up by safe_capacity(), and is large enough that we should
     *  really reallocate.
     */
    void reallocate( std::size_t capacity ) {
      if ( !m_data_tree ) {
        m_data_tree = std::allocator_traits<allocator_type>::allocate( m_alloc, 1 );
        std::allocator_traits<allocator_type>::construct( m_alloc, m_data_tree, m_alloc.resource() );
      }
      m_data_tree->reallocate( m_alloc.resource(), m_size, m_capacity, capacity );
      m_capacity = capacity;
    }

    /** Helper to calculate what actual capacity we need to have allocated in
     *  order for all operations to be safe if the container is used as if it
     *  has the requested capacity. To expand a bit, the idea is that if you do
     *    reserve( n ); // might reallocate
     *    resize( n );  // will not reallocate
     *  then loading a SIMD vector starting from the last (n-1) element should
     *  be safe, even though for a 512bit register and 32bit floats then 15 out
     *  of 16 loaded elements are "past the end".
     *
     *  Note that this also guarantees that compressstore operations that
     *  permute and then write a full vector unit width (avx2) will not clobber
     *  anything that they shouldn't.
     */
    [[nodiscard]] std::size_t static safe_capacity( std::size_t capacity ) {
      return capacity + required_padding_entries;
    }

  public:
    // Add simd_t::size entries to the container and return a proxy object that can be used to write to them
    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best>
    [[gnu::always_inline]] inline auto emplace_back() {
      using simd_t        = SIMDWrapper::type_map_t<simd>;
      auto const old_size = size();
      resize( old_size + simd_t::size );
      return resolve_proxy_t<simd, LHCb::Pr::ProxyBehaviour::Contiguous, Derived>( static_cast<Derived*>( this ),
                                                                                   old_size );
    }

    // Add simd_t::size entries to the container and return a proxy object that can be used to write to them
    // only if it fits under max_size, otherwise ignore the last entries.
    // Can be used as a cheaper but safe alternative to compress_back if the mask is only a loop_mask
    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best>
    [[gnu::always_inline]] inline auto emplace_back( std::size_t max_size ) {
      using simd_t        = SIMDWrapper::type_map_t<simd>;
      auto const old_size = size();
      resize( std::min( old_size + simd_t::size, max_size ) );
      return resolve_proxy_t<simd, LHCb::Pr::ProxyBehaviour::Contiguous, Derived>( static_cast<Derived*>( this ),
                                                                                   old_size );
    }

    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best>
    [[gnu::always_inline]] inline auto compress_back( typename SIMDWrapper::type_map_t<simd>::mask_v const& mask ) {
      auto const old_size = size();
      resize( old_size + popcount( mask ) );
      return resolve_proxy_t<simd, LHCb::Pr::ProxyBehaviour::Compress, Derived>( static_cast<Derived*>( this ),
                                                                                 {old_size, mask} );
    }

    LHCb::Event::Zip<SIMDWrapper::InstructionSet::Scalar, Derived> scalar() { return {static_cast<Derived*>( this )}; }

    LHCb::Event::Zip<SIMDWrapper::InstructionSet::Scalar, const Derived> scalar() const {
      return {static_cast<const Derived*>( this )};
    }

    template <SIMDWrapper::InstructionSet InstSet = SIMDWrapper::InstructionSet::Best>
    LHCb::Event::Zip<InstSet, Derived> simd() {
      return {static_cast<Derived*>( this )};
    }

    template <SIMDWrapper::InstructionSet InstSet = SIMDWrapper::InstructionSet::Best>
    LHCb::Event::Zip<InstSet, const Derived> simd() const {
      return {static_cast<const Derived*>( this )};
    }

    /** e.g. float* to first element of column Tag
     */
    template <typename Tag, typename... Ts>
    [[nodiscard]] constexpr auto data( Ts... Is ) noexcept {
      assert( m_data_tree != nullptr );
      return data( SOAPath<Tag, Ts...>{std::make_tuple( Tag{}, Is... )} );
    }
    template <typename Tag, typename... Ts>
    [[nodiscard]] constexpr auto data( Ts... Is ) const noexcept {
      assert( m_data_tree != nullptr );
      return data( SOAPath<Tag, Ts...>{std::make_tuple( Tag{}, Is... )} );
    }

    template <typename Path>
    [[nodiscard]] constexpr auto data( const Path p ) const noexcept {
      assert( m_data_tree != nullptr );
      return m_data_tree->data( p );
    }

    constexpr auto dataTree() const noexcept {
      assert( m_data_tree != nullptr );
      return m_data_tree;
    }

    template <typename Buffer>
    void packHeader( Buffer& ) const {}
    template <typename Buffer>
    void unpackHeader( Buffer& ) {}

    bool checkHeader( const Derived& ) const { return true; }

    template <bool debug = false, typename Buffer>
    void pack( Buffer& buffer ) const {
      auto pos_before = buffer.pos();
      buffer.write( m_size );
      buffer.write( m_version );
      buffer.write( int32_t( m_zip_identifier ) );
      static_cast<const Derived*>( this )->packHeader( buffer );
      if constexpr ( debug ) {
        std::cout << "Packed header from " << pos_before << " to " << buffer.pos() << " ("
                  << ( buffer.pos() - pos_before ) << " bytes)" << std::endl;
      }
      SOAPackStruct<Tags...>::template pack<debug>( buffer, *this );
    }

    template <bool debug = false, typename Buffer>
    void unpack( Buffer& buffer ) {
      std::size_t size;
      int32_t     zip_id;
      auto        pos_before = buffer.pos();
      buffer.read( size );
      buffer.read( m_version );
      buffer.read( zip_id );
      m_zip_identifier = Zipping::ZipFamilyNumber{zip_id};
      static_cast<Derived*>( this )->unpackHeader( buffer );
      if constexpr ( debug ) {
        std::cout << "Unpacked header from " << pos_before << " to " << buffer.pos() << " ("
                  << ( buffer.pos() - pos_before ) << " bytes)" << std::endl;
      }
      resize( size );
      SOAPackStruct<Tags...>::template unpack<debug>( buffer, *this );
    }

    bool checkEqual( const Derived& other ) const {
      if ( m_size != other.m_size || m_version != other.m_version || m_zip_identifier != other.m_zip_identifier )
        return false;
      if ( !static_cast<const Derived*>( this )->checkHeader( other ) ) return false;
      return SOAPackStruct<Tags...>::checkEqual( *static_cast<const Derived*>( this ), other );
    }

    uint32_t version() const { return m_version; }

    void setVersion( uint32_t version ) { m_version = version; }

    template <typename Functor>
    auto sorted( Functor et ) const {
      Derived out{Zipping::generateZipIdentifier(), get_allocator().resource()};
      out.reserve( size() );
      std::vector<unsigned> permutation;
      permutation.resize( size() );
      for ( unsigned i = 0; i < size(); i++ ) permutation[i] = i;

      auto s = scalar();
      std::sort( permutation.begin(), permutation.end(),
                 [&]( const auto& lhs, const auto& rhs ) { return et( s[lhs] ) > et( s[rhs] ); } );

      for ( const auto i : permutation ) { out.template copy_back<SIMDWrapper::InstructionSet::Scalar>( s[i] ); }

      return out;
    }

  private:
    allocator_type           m_alloc;
    data_tree_t*             m_data_tree{nullptr};
    std::size_t              m_size{0}, m_capacity{0};
    Zipping::ZipFamilyNumber m_zip_identifier;
    uint32_t                 m_version{0};
  };
} // namespace LHCb::Event
