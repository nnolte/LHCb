/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/SerializeSTL.h"
#include <map>
#include <ostream>

namespace LHCb {

  // Class ID definition
  static const CLID CLID_IntLink = 500;

  /** @class IntLink IntLink.h
   *
   * Link to int
   *
   * @author Jose A. Hernando
   *
   */

  class IntLink final : public DataObject {
  public:
    /// Default Constructor
    IntLink() = default;

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override { return LHCb::IntLink::classID(); }
    static const CLID& classID() { return CLID_IntLink; }

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override {
      using GaudiUtils::operator<<;
      return s << "{ "
               << "link :	" << m_link << "\n }";
    }

    /// Sets the int value asociated to this key
    void setLink( int key, int value ) { m_link[key] = value; }

    /// Returns the link associated to this key
    int link( int key ) const {
      auto i = m_link.find( key );
      return i == m_link.end() ? 0 : i->second;
    }

    /// Returns the index of the key. True if key exist, else inserting position
    bool hasKey( int key ) const { return m_link.find( key ) != m_link.end(); }

    /// Retrieve const  list of linked ints
    const std::map<int, int>& link() const { return m_link; }

    /// Update  list of linked ints
    void setLink( const std::map<int, int>& value ) { m_link = value; }

    friend std::ostream& operator<<( std::ostream& str, const IntLink& obj ) { return obj.fillStream( str ); }

  private:
    std::map<int, int> m_link; ///< list of linked ints

  }; // class IntLink

} // namespace LHCb
