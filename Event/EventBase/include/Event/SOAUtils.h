/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <cstddef>
namespace LHCb::Event {
  namespace constants {
    /** Widest SIMD vector backend that is supported, in bytes.
     *  @todo It would be nicer to import this constant from elsewhere.
     */
    inline constexpr std::size_t max_vector_width = 64;
  } // namespace constants
  /** Helper for non-SOACollection-based containers that manually reserve
   *  the spare capacity needed for a compressstore at the end of the container
   *  to be safe.
   *  @tparam T Type of contained object (normally a 32-bit type)
   *  @todo Migrate to SOACollection and drop this helper.
   */
  template <typename T>
  constexpr std::size_t safe_simd_capacity( std::size_t capacity ) {
    return capacity + ( constants::max_vector_width / sizeof( T ) ) - 1;
  }
} // namespace LHCb::Event
