/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/detected.h"
#include "Kernel/Variant.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"

namespace LHCb::Event {
  namespace details {

    template <std::size_t N, SIMDWrapper::InstructionSet... is>
    constexpr SIMDWrapper::InstructionSet match_size() {
      for ( auto [i, n] : std::array{std::pair{is, SIMDWrapper::type_map<is>::type::size}...} ) {
        if ( n == N ) return i;
      }
      throw std::invalid_argument( "unable to map vectorwidth to instruction set" );
      return SIMDWrapper::InstructionSet( -1 );
    }

    template <std::size_t N>
    constexpr SIMDWrapper::InstructionSet
        instructionSet_for_ = match_size<N, SIMDWrapper::InstructionSet::AVX512, SIMDWrapper::InstructionSet::AVX256,
                                         SIMDWrapper::InstructionSet::AVX2, SIMDWrapper::InstructionSet::SSE>();

    template <typename Target, typename... Ts>
    struct contains : std::disjunction<std::is_same<Target, Ts>...> {};

  } // namespace details

  /* when we have different accessor names, we use ADL, with as default this indirection layer */

  template <typename T>
  auto trackState( T const& item ) -> decltype( item.trackState() ) {
    return item.trackState();
  }
  template <typename... Ts>
  auto trackState( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return trackState( i ); }, item );
  }

  template <typename T>
  auto slopes( T const& item ) -> decltype( item.slopes() ) {
    return item.slopes();
  }
  template <typename... Ts>
  auto slopes( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return slopes( i ); }, item );
  }

  template <typename T>
  auto threeMomentum( T const& item ) -> decltype( item.threeMomentum() ) {
    return item.threeMomentum();
  }
  template <typename... Ts>
  auto threeMomentum( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return threeMomentum( i ); }, item );
  }

  template <typename T>
  auto fourMomentum( T const& item ) -> decltype( item.momentum() ) {
    return item.momentum();
  }
  template <typename... Ts>
  auto fourMomentum( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return fourMomentum( i ); }, item );
  }

  template <typename T>
  auto mass2( T const& item ) -> decltype( item.mass2() ) {
    return item.mass2();
  }
  template <typename... Ts>
  auto mass2( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return mass2( i ); }, item );
  }

  template <typename T>
  auto endVertexPos( T const& item ) -> decltype( item.endVertex() ) {
    return item.endVertex();
  }
  template <typename... Ts>
  auto endVertexPos( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return endVertexPos( i ); }, item );
  }
  template <typename T, std::size_t N>
  auto endVertexPos( std::array<T const*, N> const& items ) {
    using float_v = typename SIMDWrapper::type_map<details::instructionSet_for_<N>>::type::float_v;
    return std::apply(
        []( const auto&... i ) {
          constexpr auto nan     = std::numeric_limits<float>::quiet_NaN();
          constexpr auto invalid = LHCb::LinAlg::Vec3<SIMDWrapper::scalar::float_v>{nan, nan, nan};
          using LHCb::LinAlg::gather;
          return gather<float_v>( std::array{( i ? endVertexPos( *i ) : invalid )...} );
        },
        items );
  }

  template <typename T>
  auto referencePoint( T const& item ) -> decltype( item.referencePoint() ) {
    return item.referencePoint();
  }
  template <typename... Ts>
  auto referencePoint( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return referencePoint( i ); }, item );
  }

  template <typename T>
  auto threeMomCovMatrix( T const& item ) -> decltype( item.threeMomCovMatrix() ) {
    return item.threeMomCovMatrix();
  }
  template <typename... Ts>
  auto threeMomCovMatrix( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return threeMomCovMatrix( i ); }, item );
  }

  template <typename T>
  auto threeMomPosCovMatrix( T const& item ) -> decltype( item.threeMomPosCovMatrix() ) {
    return item.threeMomPosCovMatrix();
  }

  template <typename T>
  auto momPosCovMatrix( T const& item ) -> decltype( item.momPosCovMatrix() ) {
    return item.momPosCovMatrix();
  }

  template <typename T>
  auto momCovMatrix( T const& item ) -> decltype( item.momCovMatrix() ) {
    return item.momCovMatrix();
  }

  template <typename T>
  auto posCovMatrix( T const& item ) -> decltype( item.posCovMatrix() ) {
    return item.posCovMatrix();
  }
  template <typename T, std::size_t N>
  auto posCovMatrix( std::array<T const*, N> const& items ) {
    using float_v = typename SIMDWrapper::type_map<details::instructionSet_for_<N>>::type::float_v;
    return std::apply(
        []( const auto&... i ) {
          constexpr auto nan     = std::numeric_limits<float>::quiet_NaN();
          constexpr auto invalid = LHCb::LinAlg::MatSym<SIMDWrapper::scalar::float_v, 3>{nan, nan, nan, nan, nan, nan};
          using LHCb::LinAlg::gather;
          return gather<float_v>( std::array{( i ? posCovMatrix( *i ) : invalid )...} );
        },
        items );
  }

  template <typename T>
  __attribute__( ( flatten ) ) auto covMatrix( T const& item ) {
    // LHCb::LinAlg::resize_t<decltype( posCovMatrix( obj ) ), 6> cov6{};
    // cov6 = cov6.template place_at<0, 0>( posCovMatrix( obj ) );
    // cov6 = cov6.template place_at<3, 0>( threeMomPosCovMatrix( obj ) );
    // cov6 = cov6.template place_at<3, 3>( threeMomCovMatrix( obj ) );
    // return cov6;
    auto                                              tempPosMat    = posCovMatrix( item );
    auto                                              tempMomMat    = threeMomCovMatrix( item );
    auto                                              tempMomPosMat = threeMomPosCovMatrix( item );
    LHCb::LinAlg::resize_t<decltype( tempPosMat ), 6> covMat;
    LHCb::Utils::unwind<0, 3>( [&]( auto i ) {
      LHCb::Utils::unwind<0, 3>( [&]( auto j ) {
        covMat( i, j )         = tempPosMat( i, j );
        covMat( i + 3, j + 3 ) = tempMomMat( i, j );
        covMat( i, j + 3 )     = tempMomPosMat( i, j );
        covMat( j, i + 3 )     = tempMomPosMat( j, i );
      } );
    } );
    return covMat;
  }

  template <typename T>
  auto pid( T const& item ) -> decltype( item.pid() ) {
    return item.pid();
  }

  template <typename T>
  auto decayProducts( T const& item ) -> decltype( item.decayProducts() ) {
    // return a 'range' of decay products, i.e. something
    // that one can ask for 'size', and loop over with a range-based loop,
    // and that supports random-access
    // i.e.
    //        auto num_children = decayProducts(parent).size();
    //        const auto & child = children[ i ]; // provided 0 <= i < num_children
    //
    //        for (const auto& child : decayProducts(parent) ) {
    //             ...
    //        }
    //
    // shall be valid statements.
    //
    return item.decayProducts();
  }

  template <typename... Ts>
  auto decayProducts( LHCb::variant<Ts...> const& item ) {
    return visit( []( auto const& i ) { return decayProducts( i ); }, item );
  }

  template <std::size_t... idx, typename T>
  auto subCombination( T const& item ) -> decltype( item.template subCombination<idx...>() ) {
    return item.template subCombination<idx...>();
  }
} // namespace LHCb::Event

// namespace for backwards compatibility...
namespace Sel::Utils {
  using namespace LHCb::Event;
}
