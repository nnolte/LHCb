/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/SOACollection.h"
#include "Event/SOAUtils.h"
#include "GaudiKernel/GaudiException.h"
#include "LHCbMath/SIMDWrapper.h"
#include <algorithm>
#include <numeric>

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utest_stl

#include <boost/test/unit_test.hpp>

using namespace LHCb::Event;

struct Int : int_field {};

struct loopable : public SOACollection<loopable, Int> {
  template <typename dType>
  typename dType::mask_v return_true( const std::size_t i ) const {
    if ( i + dType::size - 1 >= capacity() ) {
      throw GaudiException{"out of range access:\noffset = " + std::to_string( i ) + "\ncapacity = " +
                               std::to_string( capacity() ) + "\nsimd size = " + std::to_string( dType::size ),
                           "stl.cpp", StatusCode::FAILURE};
    }
    return dType::mask_true();
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  struct CustomProxy : public LHCb::Event::Proxy<simd, behaviour, ContainerType> {
    using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;
    using simd_t  = SIMDWrapper::type_map_t<simd>;
    using int_v   = typename simd_t::int_v;
    using float_v = typename simd_t::float_v;

    [[nodiscard]] auto return_true() const {
      static_assert( behaviour == LHCb::Pr::ProxyBehaviour::Contiguous );
      return this->container()->template return_true<simd_t>( this->offset() );
    }
  };
  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  using proxy_type = CustomProxy<simd, behaviour, ContainerType>;
};

BOOST_AUTO_TEST_CASE( test_stl_algorithms ) {

  loopable l;
  for ( int i = 0; i < 9; ++i ) {
    auto const& proxy = l.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    proxy.field<Int>().set( i );
  }
  auto zip = LHCb::Event::make_zip<SIMDWrapper::InstructionSet::SSE>( l );
  bool true_check;
  BOOST_CHECK_NO_THROW( true_check = std::all_of( zip.begin(), zip.end(), []( const auto chunk ) {
                          return all( ( !chunk.loop_mask() ) || chunk.return_true() );
                        } ) );
  BOOST_CHECK( true_check );

#if defined transform_reduce_ready
  std::size_t true_count;
  BOOST_CHECK_NO_THROW( true_count = std::transform_reduce(
                            std::execution::seq, zip.begin(), zip.end(), (std::size_t)0, std::plus<std::size_t>(),
                            []( const auto proxy ) { return popcount( proxy.loop_mask() && proxy.return_true() ); } ) );

  BOOST_CHECK_EQUAL( true_count, l.size() );
#endif

  decltype( zip.begin() ) firstfalse;
  BOOST_CHECK_NO_THROW( firstfalse = std::find_if_not(
                            // use !none instead of any, see https://github.com/root-project/root/issues/8370
                            zip.begin(), zip.end(), []( const auto proxy ) { return !none( proxy.return_true() ); } ) );

  // BOOST_CHECK_EQUAL( firstfalse, zip.end() );
  BOOST_CHECK( firstfalse == zip.end() );
}

struct FloatA : float_field {};
struct IntA : int_field {};

struct FloatB : float_field {};
struct IntB : int_field {};

struct TestA : public SOACollection<TestA, FloatA, IntA> {};
struct TestB : public SOACollection<TestB, FloatB, IntB> {};

BOOST_AUTO_TEST_CASE( test_growing_zip ) {
  // Make two compatible containers
  auto a = TestA{};
  auto b = TestB{a.zipIdentifier()};

  // Make some iterable a
  auto iterable_a = LHCb::Event::make_zip( a );

  // Check that calling make_zip again is a no-op
  auto iterable_a_2 = LHCb::Event::make_zip( iterable_a );
  static_assert( std::is_same_v<decltype( iterable_a ), decltype( iterable_a_2 )> );
  BOOST_CHECK( iterable_a == iterable_a_2 );

  // Extend this zip with b
  auto tracks_with_b = LHCb::Event::make_zip( iterable_a, b );

  // Make the same zip directly
  auto tracks_with_b_2 = LHCb::Event::make_zip( a, b );

  // And with the opposite argument order
  auto tracks_with_b_3 = LHCb::Event::make_zip( b, a );

  // Check that we get the same type via all three routes
  static_assert( std::is_same_v<decltype( tracks_with_b ), decltype( tracks_with_b_2 )> );
  static_assert( std::is_same_v<decltype( tracks_with_b ), decltype( tracks_with_b_3 )> );

  // Make zip from tuple
  auto tuple_a_b      = std::make_tuple( std::move( a ), std::move( b ) );
  auto iterable_tuple = LHCb::Event::make_zip( tuple_a_b );
  static_assert( std::is_same_v<decltype( iterable_tuple ), decltype( tracks_with_b )> );
}