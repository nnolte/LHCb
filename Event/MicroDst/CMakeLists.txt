###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/MicroDst
--------------
#]=======================================================================]

gaudi_add_library(MicroDstLib
    SOURCES
        src/Particle2LHCbIDs.cpp
        src/Particle2UnsignedInts.cpp
        src/Particle2VertexBase.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::PhysEvent
            LHCb::RecEvent
)

gaudi_add_dictionary(MicroDstDict
    HEADERFILES dict/MicroDstDict.h
    SELECTION dict/MicroDstDict.xml
    LINK
        LHCb::MicroDstLib
        LHCb::LHCbKernel
        LHCb::PhysEvent
)

gaudi_install(PYTHON)
