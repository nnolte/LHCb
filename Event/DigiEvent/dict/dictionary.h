/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// begin include files
#include "Event/BcmDigit.h"
#include "Event/CaloAdc.h"
#include "Event/CaloDigit.h"
#include "Event/CaloDigitStatus.h"
#include "Event/HCDigit.h"
#include "Event/MuonDigit.h"
#include "Event/PackMuonDigit.h"
#include "Event/RichDigit.h"
#include "Event/UTDigit.h"
#include "Event/VPCluster.h"
#include "Event/VPDigit.h"
#include "Event/VeloCluster.h" // PK-R3C kept from Lbcom UTAlgorithms
#include "Event/VeloLiteCluster.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/ObjectVector.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/SmartRefVector.h"
#include "Kernel/FastClusterContainer.h"
#include <vector>

// end include files

namespace {
  struct DigiEvent_Instantiations {
    // begin instantiations
    FastClusterContainer<LHCb::VeloLiteCluster, int> _i1;
    KeyedContainer<LHCb::BcmDigit>                   m_KeyedContainer_LHCb__BcmDigit;
    KeyedContainer<LHCb::CaloAdc>                    m_KeyedContainer_LHCb__CaloAdc;
    KeyedContainer<LHCb::CaloDigit>                  m_KeyedContainer_LHCb__CaloDigit;
    KeyedContainer<LHCb::HCDigit>                    m_KeyedContainer_LHCb__HCDigit;
    KeyedContainer<LHCb::MuonDigit>                  m_KeyedContainer_LHCb__MuonDigit;
    KeyedContainer<LHCb::RichDigit>                  m_KeyedContainer_LHCb__RichDigit;
    KeyedContainer<LHCb::UTDigit>                    m_KeyedContainer_LHCb__UTDigit;
    KeyedContainer<LHCb::VPCluster>                  m_KeyedContainer_LHCb__VPCluster;
    KeyedContainer<LHCb::VPDigit>                    m_KeyedContainer_LHCb__VPDigit;
    KeyedObject<LHCb::Detector::Calo::CellID>        m_KeyedObject_LHCb__Calo__CellID;
    KeyedObject<LHCb::HCCellID>                      m_KeyedObject_LHCb__HCCellID;
    KeyedObject<LHCb::Detector::Muon::TileID>        m_KeyedObject_LHCb__Detectori__Muon__TileID;
    KeyedObject<LHCb::RichSmartID>                   m_KeyedObject_LHCb__RichSmartID;
    KeyedObject<LHCb::Detector::UT::ChannelID>       m_KeyedObject_LHCb__Detector__UT__ChannelID;
    KeyedObject<LHCb::Detector::VPChannelID>         m_KeyedObject_LHCb__Detector__VPChannelID;
    KeyedContainer<LHCb::VeloCluster>                m_KeyedContainer_LHCb__VeloCluster;
    ObjectVector<LHCb::BcmDigit>                     m_ObjectVector_LHCb__BcmDigit;
    ObjectVector<LHCb::CaloAdc>                      m_ObjectVector_LHCb__CaloAdc;
    ObjectVector<LHCb::CaloDigit>                    m_ObjectVector_LHCb__CaloDigit;
    ObjectVector<LHCb::HCDigit>                      m_ObjectVector_LHCb__HCDigit;
    ObjectVector<LHCb::MuonDigit>                    m_ObjectVector_LHCb__MuonDigit;
    ObjectVector<LHCb::RichDigit>                    m_ObjectVector_LHCb__RichDigit;
    ObjectVector<LHCb::UTDigit>                      m_ObjectVector_LHCb__UTDigit;
    ObjectVector<LHCb::VPCluster>                    m_ObjectVector_LHCb__VPCluster;
    ObjectVector<LHCb::VPDigit>                      m_ObjectVector_LHCb__VPDigit;
    ObjectVector<LHCb::VeloCluster>                  m_ObjectVector_LHCb__VeloCluster;
    SmartRef<LHCb::BcmDigit>                         m_SmartRef_LHCb__BcmDigit;
    SmartRef<LHCb::CaloAdc>                          m_SmartRef_LHCb__CaloAdc;
    SmartRef<LHCb::CaloDigit>                        m_SmartRef_LHCb__CaloDigit;
    SmartRef<LHCb::HCDigit>                          m_SmartRef_LHCb__HCDigit;
    SmartRef<LHCb::MuonDigit>                        m_SmartRef_LHCb__MuonDigit;
    SmartRef<LHCb::RichDigit>                        m_SmartRef_LHCb__RichDigit;
    SmartRef<LHCb::UTDigit>                          m_SmartRef_LHCb__UTDigit;
    SmartRef<LHCb::VPCluster>                        m_SmartRef_LHCb__VPCluster;
    SmartRef<LHCb::VPDigit>                          m_SmartRef_LHCb__VPDigit;
    SmartRefVector<LHCb::BcmDigit>                   m_SmartRefVector_LHCb__BcmDigit;
    SmartRefVector<LHCb::CaloAdc>                    m_SmartRefVector_LHCb__CaloAdc;
    SmartRefVector<LHCb::CaloDigit>                  m_SmartRefVector_LHCb__CaloDigit;
    SmartRefVector<LHCb::HCDigit>                    m_SmartRefVector_LHCb__HCDigit;
    SmartRefVector<LHCb::MuonDigit>                  m_SmartRefVector_LHCb__MuonDigit;
    SmartRefVector<LHCb::RichDigit>                  m_SmartRefVector_LHCb__RichDigit;
    SmartRefVector<LHCb::UTDigit>                    m_SmartRefVector_LHCb__UTDigit;
    SmartRefVector<LHCb::VPCluster>                  m_SmartRefVector_LHCb__VPCluster;
    SmartRefVector<LHCb::VPDigit>                    m_SmartRefVector_LHCb__VPDigit;
    SmartRefVector<LHCb::VeloCluster>                m_SmartRefVector_LHCb__VeloCluster;
    std::vector<LHCb::BcmDigit*>                     m_std_vector_LHCb__BcmDigitp;
    std::vector<LHCb::CaloAdc*>                      m_std_vector_LHCb__CaloAdcp;
    std::vector<LHCb::CaloDigit*>                    m_std_vector_LHCb__CaloDigitp;
    std::vector<LHCb::HCDigit*>                      m_std_vector_LHCb__HCDigitp;
    std::vector<LHCb::MuonDigit*>                    m_std_vector_LHCb__MuonDigitp;
    std::vector<LHCb::RichDigit*>                    m_std_vector_LHCb__RichDigitp;
    std::vector<LHCb::UTDigit*>                      m_std_vector_LHCb__UTDigitp;
    std::vector<LHCb::VPCluster*>                    m_std_vector_LHCb__VPClusterp;
    std::vector<LHCb::VPDigit*>                      m_std_vector_LHCb__VPDigitp;
    std::vector<SmartRef<LHCb::BcmDigit>>            m_std_vector_SmartRef_LHCb__BcmDigit;
    std::vector<SmartRef<LHCb::CaloAdc>>             m_std_vector_SmartRef_LHCb__CaloAdc;
    std::vector<SmartRef<LHCb::CaloDigit>>           m_std_vector_SmartRef_LHCb__CaloDigit;
    std::vector<SmartRef<LHCb::HCDigit>>             m_std_vector_SmartRef_LHCb__HCDigit;
    std::vector<SmartRef<LHCb::MuonDigit>>           m_std_vector_SmartRef_LHCb__MuonDigit;
    std::vector<SmartRef<LHCb::RichDigit>>           m_std_vector_SmartRef_LHCb__RichDigit;
    std::vector<SmartRef<LHCb::UTDigit>>             m_std_vector_SmartRef_LHCb__UTDigit;
    std::vector<SmartRef<LHCb::VPCluster>>           m_std_vector_SmartRef_LHCb__VPCluster;
    std::vector<SmartRef<LHCb::VPDigit>>             m_std_vector_SmartRef_LHCb__VPDigit;
    // end instantiations
  };
} // namespace
