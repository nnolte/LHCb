/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/STLExtensions.h"
#include "LHCbMath/bit_cast.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/LinkManager.h"

#include <boost/pfr/core.hpp>

#include <cmath>
#include <type_traits>

namespace Gaudi {
  class Algorithm;
}

/**
 *  A set of functions to convert to int/short with standard factors various quantities.
 *
 *  @author Olivier Callot
 *  @date   2005-03-15
 */

namespace Packer {
  constexpr double ENERGY_SCALE   = 1.0e2; ///< .01 MeV steps
  constexpr double POSITION_SCALE = 1.0e4; ///< 0.1 micron steps
  constexpr double SLOPE_SCALE    = 1.0e8; ///< full scale +- 20 radians
  constexpr double FRACTION_SCALE = 3.0e4; ///< store in std::int16_t.
  constexpr double TIME_SCALE     = 1.0e5; ///< 0.0001 ns resolution
  constexpr double DELTALL_SCALE  = 1.0e4; ///< 0.0001 precision
  constexpr double MASS_SCALE     = 1.0e3; ///< 1 keV steps
  constexpr double MVA_SCALE      = 1.0e5; ///< Scale for MVAs

  template <typename Buffer, typename Obj>
  void io( Buffer& buffer, Obj& obj ) {
    static_assert( std::is_aggregate_v<Obj> );
    boost::pfr::for_each_field( obj, [&]( auto&& f ) { buffer.io( std::forward<decltype( f )>( f ) ); } );
  }

  template <typename Container>
  constexpr auto subrange( Container const& c, size_t first, size_t last ) {
    assert( last >= first );
    auto s = LHCb::span{c};
    return s.subspan( first, last - first );
  }

  class Carry {
    std::array<size_t, 2>   m_carry = {0u, 0u};
    std::array<uint16_t, 2> m_prev  = {0u, 0u};

    // NOTE: this assumes that a single protoparticle has less than 64K extrainfo entries
    // otherwise, this code will fail to detect the overflow...
    static size_t with_carry( uint16_t i, uint16_t& prev, size_t& carry ) {
      if ( i < std::exchange( prev, i ) ) carry += 0x10000u; // overflow detected -- increment carry
      return carry + i;                                      // return carry-corrected value
    }

  public:
    Carry() = default;

    template <typename Container>
    auto operator()( const Container& c, uint16_t first, uint16_t last ) {
      auto f = with_carry( first, m_prev[0], m_carry[0] );
      auto l = with_carry( last, m_prev[1], m_carry[1] );
      assert( f <= l );
      assert( l <= c.size() );
      return subrange( c, f, l );
    }
    explicit operator bool() const { return m_carry[0] != 0 || m_carry[1] != 0; }
  };

} // namespace Packer

namespace StandardPacker {

  namespace details {

    /// Pack a double to an int
    constexpr int packDouble( double val ) {
      return ( 2.e9 < val ? 2000000000 :              // saturate 31 bits
                   -2.e9 > val ? -2000000000 :        // idem
                       0 < val ? (int)( val + 0.5 ) : // proper rounding
                           (int)( val - 0.5 ) );
    }

    /// Pack a double to a short int
    constexpr std::int16_t shortPackDouble( const double val ) {
      return ( 3.e4 < val ? (std::int16_t)30000 :                // saturate 15 bits
                   -3.e4 > val ? (std::int16_t)-30000 :          // idem
                       0 < val ? ( std::int16_t )( val + 0.5 ) : // proper rounding
                           ( std::int16_t )( val - 0.5 ) );
    }

  } // namespace details

  /** returns an int for a double energy */
  constexpr int energy( const double e ) { return details::packDouble( e * Packer::ENERGY_SCALE ); }

  /** returns an int for a double position */
  constexpr int position( const double x ) { return details::packDouble( x * Packer::POSITION_SCALE ); }

  /** returns an int for a double slope */
  constexpr int slope( const double x ) { return details::packDouble( x * Packer::SLOPE_SCALE ); }

  /** returns an short int for a double fraction f */
  constexpr std::int16_t fraction( const double f ) { return details::shortPackDouble( f * Packer::FRACTION_SCALE ); }

  /** returns an short int for a double fraction top/bot */
  constexpr std::int16_t fraction( const double top, const double bot ) {
    return fraction( std::abs( bot ) > 0 ? top / bot : 0.0 );
  }

  /** returns an int for a double 'mva' x */
  constexpr int mva( const double x ) { return details::packDouble( x * Packer::MVA_SCALE ); }

  /** returns an int for a double time (TOF) value */
  constexpr int time( const double x ) { return details::packDouble( x * Packer::TIME_SCALE ); }

  /** returns an int for a double delta log likelihood value */
  constexpr int deltaLL( const double x ) { return details::packDouble( x * Packer::DELTALL_SCALE ); }

  /** returns an int for a double mass */
  constexpr int mass( const double mass ) { return details::packDouble( mass * Packer::MASS_SCALE ); }

  /** returns an int containing the float representation of the double */
  constexpr int fltPacked( double x ) { return bit_cast<int>( static_cast<float>( x ) ); }

  /// Returns the 'LinkID'
  inline std::int64_t linkID( DataObject* out, const DataObject* parent ) {
    auto* myLink = out->linkMgr()->link( parent );
    if ( !myLink ) {
      out->linkMgr()->addLink( parent->registry()->identifier(), parent );
      myLink = out->linkMgr()->link( parent );
    }
    return myLink ? myLink->ID() : 0;
  }

  /// Returns the 'LinkID'
  inline std::int64_t linkID( DataObject* out, const std::string& targetName ) {
    auto* myLink = out->linkMgr()->link( targetName );
    if ( !myLink ) {
      out->linkMgr()->addLink( targetName, 0 );
      myLink = out->linkMgr()->link( targetName );
    }
    return myLink ? myLink->ID() : 0;
  }

  /** returns an int for a Smart Ref.
   *  @arg  out : Output data object, to store the links
   *  @arg  parent : Pointer to the parent container of the SmartRef, method->parent()
   *  @arg  key    : returned by the method .linkID() of the SmartRef
   */
  int reference32( Gaudi::Algorithm const* alg, DataObject* out, const DataObject* parent, const int key );

  /** returns an int for a Smart Ref.
   *  @arg  out : Output data object, to store the links
   *  @arg  targetName : Name of the target
   *  @arg  key : returned by the method .linkID() of the SmartRef
   */
  int reference32( Gaudi::Algorithm const* alg, DataObject* out, const std::string& targetName, const int key );

  /// Extracts the key and index from a packed data word
  constexpr void indexAndKey32( const int data, int& indx, int& key ) {
    indx = data >> 28;
    key  = data & 0x0FFFFFFF;
  }

  /// Extracts the key and hint from a packed data word
  bool hintAndKey32( const int data, const DataObject* source, DataObject* target, int& hint, int& key );

  /** returns a std::int64_t for a Smart Ref, with small key and large links.
   *  @arg  out    : Output data object, to store the links
   *  @arg  parent : Pointer to the parent container of the SmartRef, method ->parent()
   *  @arg  key    : returned by the method .linkID() of the SmartRef
   */
  inline std::int64_t reference64( DataObject* out, const DataObject* parent, const int key ) {
    const std::int64_t ID( linkID( out, parent ) );
    const std::int64_t myLinkID = ( ID << 32 );
    return (std::int64_t)key + myLinkID;
  }

  /// Extracts the key and index from a packed 64-bit data word
  constexpr void indexAndKey64( std::int64_t data, int& indx, int& key ) {
    indx                        = data >> 32;
    constexpr std::int64_t mask = 0x00000000FFFFFFFF;
    key                         = data & mask;
  }

  /// Extracts the key and hint from a packed 64-bit data word
  bool hintAndKey64( const std::int64_t data, const DataObject* source, DataObject* target, int& hint, int& key );

  /** returns the energy as double from the int value */
  constexpr double energy( int k ) { return double( k ) / Packer::ENERGY_SCALE; }

  /** returns the position as double from the int value */
  constexpr double position( int k ) { return double( k ) / Packer::POSITION_SCALE; }

  /** returns the slope as double from the int value */
  constexpr double slope( int k ) { return double( k ) / Packer::SLOPE_SCALE; }

  /** returns the fraction as double from the short int value */
  constexpr double fraction( std::int16_t k ) { return double( k ) / Packer::FRACTION_SCALE; }

  /** returns the mva as double from the int value */
  constexpr double mva( int k ) { return double( k ) / Packer::MVA_SCALE; }

  /** returns the time as double from the int value */
  constexpr double time( int k ) { return double( k ) / Packer::TIME_SCALE; }

  /** returns the delta Log Likelihood as double from the int value */
  constexpr double deltaLL( int k ) { return double( k ) / Packer::DELTALL_SCALE; }

  /** returns the mass as double from the int value */
  constexpr double mass( int k ) { return double( k ) / Packer::MASS_SCALE; }

  /** returns an double from a int containing in fact the representation of a float */
  constexpr double fltPacked( int k ) { return bit_cast<float>( k ); }

} // namespace StandardPacker
