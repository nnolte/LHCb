/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <array>
#include <cstdint>
#include <limits>
#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Packed description of a track
   *
   *  Version 2: Added new data members to PackedTrack to follow the upgraded Track.
   *
   *  @author Olivier Callot
   *  @date   2009-08-26
   */
  struct PackedTrack {

    std::int64_t  key{0};
    std::int32_t  chi2PerDoF{0};
    std::int32_t  nDoF{0};
    std::uint32_t flags{0};
    std::int32_t  firstId{0};
    std::int32_t  lastId{0};
    std::int32_t  firstState{0};
    std::int32_t  lastState{0};
    std::int32_t  firstExtra{0};
    std::int32_t  lastExtra{0};
    //== Added for version 3, August 2009
    std::int32_t likelihood{0};
    std::int32_t ghostProba{0};

    //== Note that Nodes and Measurements on Track are transient only, and thus never stored.

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int version ) {
      if ( version == 4 ) {
        buf.io( key, chi2PerDoF, nDoF, flags );
        buf.template io<uint16_t>( firstId );
        buf.template io<uint16_t>( lastId );
        buf.template io<uint16_t>( firstState );
        buf.template io<uint16_t>( lastState );
        buf.template io<uint16_t>( firstExtra );
        buf.template io<uint16_t>( lastExtra );
        buf.io( likelihood, ghostProba );
      } else {
        Packer::io( buf, *this );
      }
    }
#endif
  };

  /**
   *  Describe a packed state
   *
   *  @author Olivier Callot
   *  @date   2008-11-07
   */
  struct PackedState {

    std::int32_t flags{0};

    std::int32_t x{0};
    std::int32_t y{0};
    std::int32_t z{0};
    std::int32_t tx{0};
    std::int32_t ty{0};
    std::int32_t p{0};

    std::int32_t cov_00{0};
    std::int32_t cov_11{0};
    std::int32_t cov_22{0};
    std::int32_t cov_33{0};
    std::int32_t cov_44{0};
    std::int16_t cov_10{0};
    std::int16_t cov_20{0};
    std::int16_t cov_21{0};
    std::int16_t cov_30{0};
    std::int16_t cov_31{0};
    std::int16_t cov_32{0};
    std::int16_t cov_40{0};
    std::int16_t cov_41{0};
    std::int16_t cov_42{0};
    std::int16_t cov_43{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedTracks = 1550;

  // Namespace for locations in TDS
  namespace PackedTrackLocation {
    inline const std::string Default  = "pRec/Track/Best";
    inline const std::string Muon     = "pRec/Track/Muon";
    inline const std::string InStream = "/pRec/Track/Custom";
  } // namespace PackedTrackLocation

  /**
   *  Container of packed Tracks
   *
   *  @author Olivier Callot
   *  @date   2009-08-26
   */

  class PackedTracks : public DataObject {

  public:
    /// Standard constructor
    PackedTracks() {
      m_vect.reserve( 500 );
      m_state.reserve( 2000 );
      m_ids.reserve( 2000 );
      m_extra.reserve( 5000 );
    }

    const CLID&        clID() const override { return PackedTracks::classID(); }
    static const CLID& classID() { return CLID_PackedTracks; }

    std::vector<PackedTrack>&       data() { return m_vect; }
    const std::vector<PackedTrack>& data() const { return m_vect; }

    std::vector<std::int32_t>&       ids() { return m_ids; }
    const std::vector<std::int32_t>& ids() const { return m_ids; }

    std::vector<PackedState>&       states() { return m_state; }
    const std::vector<PackedState>& states() const { return m_state; }

    std::vector<std::pair<std::int32_t, std::int32_t>>&       extras() { return m_extra; }
    const std::vector<std::pair<std::int32_t, std::int32_t>>& extras() const { return m_extra; }

    /// Default Packing Version
    static char defaultPackingVersion() { return 5; }
    /// Set the packing version
    void setPackingVersion( const char ver ) { m_packingVersion = ver; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_state );
      buf.save( m_ids );
      buf.save( m_extra );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      setVersion( buf.template load<uint8_t>() );
      if ( version() < 4 || version() > 5 ) {
        throw std::runtime_error( "PackedTracks packing version is not supported: " + std::to_string( version() ) );
      }
      buf.load( m_vect, version() );
      buf.load( m_state, version() );
      buf.load( m_ids );
      buf.load( m_extra, version() );
    }

  private:
    std::vector<PackedTrack>                           m_vect;
    std::vector<PackedState>                           m_state;
    std::vector<std::int32_t>                          m_ids;
    std::vector<std::pair<std::int32_t, std::int32_t>> m_extra;
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};
  };

  /**
   *  Utility class to handle the packing and unpacking of Tracks
   *
   *  @author Christopher Rob Jones
   *  @date   05/04/2012
   *
   */
  class TrackPacker : public PackerBase {

  public:
    typedef LHCb::Track        Data;
    typedef LHCb::PackedTrack  PackedData;
    typedef LHCb::Tracks       DataVector;
    typedef LHCb::PackedTracks PackedDataVector;
    static const std::string&  packedLocation() { return LHCb::PackedTrackLocation::Default; }
    static const std::string&  unpackedLocation() { return LHCb::TrackLocation::Default; }

    using PackerBase::PackerBase;

    /// Pack a Track
    void pack( const Data& track, PackedData& ptrack, PackedDataVector& ptracks ) const;

    /// Pack Tracks
    void pack( const DataVector& tracks, PackedDataVector& ptracks ) const;

    /// Unpack a single Track
    void unpack( const PackedData& ptrack, Data& track, const PackedDataVector& ptracks, DataVector& tracks ) const;

    /// Unpack Tracks
    void unpack( const PackedDataVector& ptracks, DataVector& tracks ) const;

    /// Compare two Track containers to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

    /// Compare two Tracks to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Convert a state to a packed state
    void convertState( const LHCb::State& state, PackedDataVector& ptracks ) const;

    /// Convert a packed state to a state in a track
    void convertState( const LHCb::PackedState& pSta, LHCb::Track& tra ) const;

    void compareStates( const LHCb::State& oSta, const LHCb::State& tSta ) const;

    /// Safe sqrt ...
    template <typename TYPE>
    static auto safe_sqrt( const TYPE x ) {
      return ( x > TYPE( 0 ) ? std::sqrt( x ) : TYPE( 0 ) );
    }

    /// Safe divide ...
    template <typename TYPE>
    static auto safe_divide( const TYPE a, const TYPE b ) {
      return ( b != TYPE( 0 ) ? a / b : 9e9 );
    }

    /// Check if the given packing version is supported
    bool isSupportedVer( const char ver ) const {
      const bool OK = ( 0 <= ver && ver <= 5 );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "TrackPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }

  public:
    /// Reset wraping bug counts
    void resetWrappingCounts() const {
      m_firstIdHigh     = 0;
      m_lastIdHigh      = 0;
      m_firstStateHigh  = 0;
      m_lastStateHigh   = 0;
      m_firstExtraHigh  = 0;
      m_lastExtraHigh   = 0;
      m_lastPackedDataV = nullptr;
    }

  private:
    // cached data to handle wrapped ID numbers ...
    mutable std::int32_t m_firstIdHigh{0};
    mutable std::int32_t m_lastIdHigh{0};
    mutable std::int32_t m_firstStateHigh{0};
    mutable std::int32_t m_lastStateHigh{0};
    mutable std::int32_t m_firstExtraHigh{0};
    mutable std::int32_t m_lastExtraHigh{0};

    // Cache the pointers to the last packed and unpacked containers
    mutable const PackedDataVector* m_lastPackedDataV = nullptr;
  };

} // namespace LHCb
