/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Compression.h"
#include "Event/PackedData.h"
#include "RVersion.h"
#include <algorithm>
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <vector>

namespace LHCb::Hlt::PackedData {

  // static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

  struct ObjectHeader {
    uint32_t             classID{0};
    int32_t              locationID{0};
    std::vector<int32_t> linkLocationIDs{0};
    uint32_t             storedSize{0};
  };

  /// Save a serializable object to buffer.
  template <typename Buffer, typename T>
  auto save( Buffer& buf, const T& object ) -> decltype( object.save( buf ), void() ) {
    object.save( buf );
  }

  /// Save a std::pair to buffer.
  template <typename Buffer, typename K, typename V>
  void save( Buffer& buf, const std::pair<K, V>& pair ) {
    buf.io( pair.first, pair.second );
  }

  /// Load a versioned serializable object from buffer.
  template <typename Buffer, typename T>
  auto load( Buffer& buf, T& object, unsigned int version ) -> decltype( object.save( buf ), void() ) {
    object.load( buf, version );
  }

  /// Load a non-versioned serializable object from buffer.
  template <typename Buffer, typename T>
  auto load( Buffer& buf, T& object ) -> decltype( object.load( buf ), void() ) {
    object.load( buf );
  }

  /// Load a std::pair from buffer.
  template <typename Buffer, typename K, typename V>
  void load( Buffer& buf, std::pair<K, V>& pair, unsigned int /*version*/ ) {
    buf.io( pair.first, pair.second );
  }

  /** @class ByteBuffer PackedDataBuffer.h
   *  Helper class that represents a byte buffer.
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  class ByteBuffer {
  public:
    using buffer_type = std::vector<std::byte>;

    /// Return the current position in the buffer.
    std::size_t pos() const { return m_pos; }
    /// Return whether the end of buffer was reached.
    bool eof() const { return m_pos >= m_buffer.size(); }
    /// Clear the buffer and reset position to zero.
    void clear() {
      m_buffer.clear();
      m_pos = 0;
    }
    /// Reset the position to zero without clearing the buffer
    void reset() { m_pos = 0; }
    /// Initialize from an existing buffer and reset position to zero.
    bool init( const buffer_type& data, bool compressed = false );
    /// Return the internal buffer.
    const buffer_type& buffer() const { return m_buffer; }
    /// Return the size of the internal buffer.
    int size() const { return m_buffer.size(); }
    /// Compress the buffer
    bool compress( Compression compression, int level, buffer_type& output ) const;
    /// Reserve size for the buffer
    void reserve( std::size_t size ) { m_buffer.reserve( size ); }
    /// Skip reading n bytes, and return the offset _prior_ to skipping ahead
    std::size_t readNull( std::size_t n ) {
      auto next_pos = m_pos + n;
      if ( next_pos > m_buffer.size() )
        throw std::runtime_error( "ByteBuffer(): requested a read beyond buffer size!" );
      return std::exchange( m_pos, next_pos );
    }
    /// Read an object from the current position.
    template <typename T>
    void read( T& x ) {
      static_assert( std::is_trivially_copyable_v<T> );
      if ( m_pos + sizeof( x ) > m_buffer.size() )
        throw std::runtime_error( "ByteBuffer(): requested a read beyond buffer size!" );
      std::memcpy( &x, m_buffer.data() + m_pos, sizeof( x ) );
      m_pos += sizeof( x );
    }
    /// Read an object from a given position.
    template <typename T>
    void read( T& x, std::size_t pos ) const {
      static_assert( std::is_trivially_copyable_v<T> );
      if ( pos + sizeof( x ) > m_buffer.size() )
        throw std::runtime_error( "ByteBuffer(): requested a read beyond buffer size!" );
      std::memcpy( &x, m_buffer.data() + pos, sizeof( x ) );
    }
    /// Skip writing n bytes.
    std::size_t writeNull( std::size_t n ) {
      auto next_pos = m_pos + n;
      m_buffer.resize( next_pos );
      return std::exchange( m_pos, next_pos );
    }
    /// Write an object at the current position.
    template <typename T>
    void write( const T& x ) {
      static_assert( std::is_trivially_copyable_v<T> );
      m_buffer.resize( m_pos + sizeof( x ) );
      std::memcpy( m_buffer.data() + m_pos, &x, sizeof( x ) );
      m_pos += sizeof( x );
    }
    /// Write an object at a given position.
    template <typename T>
    void write( const T& x, std::size_t pos ) {
      static_assert( std::is_trivially_copyable_v<T> );
      if ( pos + sizeof( x ) > m_buffer.size() )
        throw std::runtime_error( "ByteBuffer(): requested a write beyond buffer size!" );
      std::memcpy( m_buffer.data() + pos, &x, sizeof( x ) );
    }
    /// Write a buffer at current position. This adds the buffer x
    void writeBuffer( ByteBuffer const& x ) {
      m_buffer.resize( m_pos + x.size() );
      std::memcpy( m_buffer.data() + m_pos, x.m_buffer.data(), x.size() );
      m_pos += x.size();
    }
    /// Take requested part of the buffer
    LHCb::span<std::byte const> subspan( size_t offset, size_t size = gsl::dynamic_extent ) const {
      return LHCb::span{m_buffer}.subspan( offset, size );
    }
    /// Add sub span of a buffer to a new buffer
    void assign( LHCb::span<std::byte const> data ) {
      m_buffer.assign( data.begin(), data.end() );
      m_pos = 0;
    }

  private:
    buffer_type m_buffer; ///< Internal buffer
    std::size_t m_pos{0}; ///< Position in buffer
  };

  /** @class SerializationOffsets PackedDataBuffer.h
   *  Helper class for determining the offsets of serializable class fields.
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  template <typename POD>
  class SerializationOffsets {
  public:
    /// Construct offsets for saving (serialization)
    SerializationOffsets() {
      POD x;
      PackedData::save( *this, x );
    }
    /// Construct offsets for loading (de-serialization)
    SerializationOffsets( unsigned int version ) {
      POD x;
      PackedData::load( *this, x, version );
    }
    /// Return the offsets in bytes of the class fields.
    const std::vector<unsigned int>& offsets() const { return m_offsets; }
    /// Function called by serializable object save/load methods.
    template <typename... Args>
    void io( Args&&... args ) {
      ( m_offsets.push_back( m_offsets.back() + sizeof( args ) ), ... );
    }
    /// Function called by serializable object save/load methods.
    template <typename Persisted, typename Transient>
    void io( Transient& ) {
      m_offsets.push_back( m_offsets.back() + sizeof( Persisted ) );
    }

  private:
    /// Offsets in bytes of the class fields
    std::vector<unsigned int> m_offsets{0};
  };

  /** @class SOABuffer PackedDataBuffer.h
   *  Helper class for (de-)serializing a vector of serializable objects into a
   *  structure of arrays (column) format.
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  template <typename Buffer>
  class SOABuffer {
  public:
    /// Constructor
    template <typename POD>
    SOABuffer( Buffer* buffer, std::size_t pos, std::size_t length, const SerializationOffsets<POD>& offsets )
        : m_buffer{buffer}
        , m_pos{pos}
        , m_nmembers{offsets.offsets().size() - 1}
        , m_offsets{offsets.offsets()}
        , m_index{0} {
      for ( auto& x : m_offsets ) x *= length;
    }
    /// Function called by serializable object save/load methods.
    template <typename... Args>
    void io( Args&&... args ) {
      ( io_impl( args ), ... );
    }
    /// Function called by serializable object save/load methods.
    template <typename Persisted, typename Transient>
    void io( Transient& x ) {
      io_impl<Persisted, Transient>( x );
    }

  private:
    /// Get the offset of the next member to be saved/loaded.
    std::size_t offset( std::size_t size ) const {
      auto memberIndex = m_index % m_nmembers;
      auto arrayIndex  = m_index / m_nmembers;
      return m_offsets[memberIndex] + size * arrayIndex;
    }

    /// Implementation of the io() function for single argument.
    template <typename T>
    void io_impl( T& x ) {
      m_buffer->ioAt( x, m_pos + offset( sizeof( T ) ) );
      ++m_index;
    }

    /// Implementation of the io() function for single argument.
    template <typename Persisted, typename Transient>
    void io_impl( Transient& x ) {
      m_buffer->template ioAt<Persisted, Transient>( x, m_pos + offset( sizeof( Persisted ) ) );
      ++m_index;
    }

    Buffer*                   m_buffer;   ///< Main buffer
    std::size_t               m_pos;      ///< Position in main buffer where to save/load the SOA data
    std::size_t               m_nmembers; ///< Number of fields in the structure
    std::vector<unsigned int> m_offsets;  ///< Offsets of arrays in SOA data
    std::size_t               m_index;    ///< The sequential index of the next element to be save/loaded
  };

  /** @class PackedDataOutBuffer PackedDataBuffer.h
   *  Helper class for serializing data.
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  class PackedDataOutBuffer {
  public:
    /// Clear the internal byte buffer.
    void clear() { m_buffer.clear(); }

    /// Return a reference to the internal buffer.
    const std::vector<std::byte>& buffer() const { return m_buffer.buffer(); }
    /// Compress the buffer
    bool compress( Compression compression, int level, ByteBuffer::buffer_type& output ) const {
      return m_buffer.compress( compression, level, output );
    }
    /// Reserve size for the buffer
    void reserve( std::size_t size ) { m_buffer.reserve( size ); }
    /// Function called by serializable objects' save method.
    template <typename... Args>
    void io( Args&&... args ) {
      ( save( args ), ... );
    }
    /// Save an object at a given position.
    template <typename T>
    void ioAt( T x, std::size_t pos ) {
      saveAt( x, pos );
    }

    // ===========================================================================
    // Dealing with scalars
    // ===========================================================================

    /// Save a scalar.
    template <typename T>
    std::enable_if_t<std::is_scalar_v<T>, std::pair<std::size_t, std::size_t>> save( T x ) {
      auto index0 = m_buffer.pos();
      m_buffer.write( x );
      return {index0, sizeof( x )};
    }
    /// Save a scalar at a given position.
    template <typename T>
    void saveAt( T x, std::size_t pos ) {
      m_buffer.write( x, pos );
    }
    /// Add byte buffer of a packed data buffer to another one
    void addBuffer( PackedDataOutBuffer const& x ) { m_buffer.writeBuffer( x.m_buffer ); }
    /// Add a byte buffer to a packed data buffer
    void addBuffer( ByteBuffer const& x ) { m_buffer.writeBuffer( x ); }
    /// Save a size integer.
    std::pair<std::size_t, std::size_t> saveSize( std::size_t x ) { return save<uint32_t>( x ); }
    /// Save a vector of scalars.
    template <typename T, typename Allocator>
    std::enable_if_t<std::is_scalar_v<T>> save( const std::vector<T, Allocator>& v ) {
      saveSize( v.size() );
      for ( const auto& x : v ) save( x );
    }

    // ===========================================================================
    // Dealing with objects
    // ===========================================================================

    /// Save a serializable object.
    template <typename T>
    auto save( const T& object )
        -> decltype( PackedData::save( *this, object ), std::pair<std::size_t, std::size_t>() ) {
      auto index0 = m_buffer.pos();
      PackedData::save( *this, object );
      return {index0, m_buffer.pos() - index0};
    }
    /// Save a vector of serializable objects in the default format.
    template <typename T, typename Allocator>
    auto save( const std::vector<T, Allocator>& v ) -> decltype( PackedData::save( *this, v[0] ), void() ) {
      saveSize( v.size() );
      if ( v.empty() ) return;
      static const SerializationOffsets<T> offsets{};
      auto                                 totalSize = offsets.offsets().back() * v.size();
      auto                                 pos       = m_buffer.writeNull( totalSize ); // reserve the space
      SOABuffer<PackedDataOutBuffer>       soabuf{this, pos, v.size(), offsets};
      for ( const auto& object : v ) PackedData::save( soabuf, object );
    }

  private:
    ByteBuffer m_buffer; ///< Internal byte buffer
  };

  /** @class PackedDataInBuffer PackedDataBuffer.h
   *  Helper class for de-serializing data.
   *
   *  @author Rosen Matev
   *  @date   2016-01-03
   */
  class PackedDataInBuffer {
  public:
    /// Return whether the end of buffer was reached.
    bool eof() const { return m_buffer.eof(); }
    /// Skip a number of bytes from the buffer.
    void skip( std::size_t n ) { m_buffer.readNull( n ); }
    /// Initialize from an existing byte buffer.
    bool init( const ByteBuffer::buffer_type& data, bool compressed = false ) {
      return m_buffer.init( data, compressed );
    }
    bool init( const ByteBuffer& buffer, bool compressed = false ) {
      return m_buffer.init( buffer.buffer(), compressed );
    }
    /// Initialize from an existing header and byte buffer.
    void init( const ObjectHeader header, const ByteBuffer::buffer_type& data ) {
      m_buffer.clear();
      m_buffer.write( header.classID );
      m_buffer.write( header.locationID );
      for ( auto l : header.linkLocationIDs ) m_buffer.write( l );
      m_buffer.write( header.storedSize );
      for ( auto d : data ) m_buffer.write( d );
      m_buffer.reset();
    }
    /// Return a reference to the internal buffer.
    const ByteBuffer& buffer() const { return m_buffer; }
    /// Clear the internal byte buffer.
    void clear() { m_buffer.clear(); }
    /// Function called by serializable objects' load method.
    template <typename... Args>
    void io( Args&&... args ) {
      ( load( args ), ... );
    }
    /// Load an object from a given position.
    template <typename T>
    void ioAt( T& x, std::size_t i ) const {
      loadAt( x, i );
    }

    /// Function called by serializable objects' load method.
    template <typename Persisted, typename Transient>
    void io( Transient& x ) {
      x = load<Persisted>(); // conversion happens here!
    }
    /// Load an object from a given position and convert it.
    template <typename Persisted, typename Transient>
    void ioAt( Transient& x, std::size_t i ) {
      x = loadAt<Persisted>( i ); // conversion happens here!
    }

    // ===========================================================================
    // Dealing with scalars
    // ===========================================================================

    /// Load a scalar in place.
    template <typename T>
    std::enable_if_t<std::is_scalar_v<T>> load( T& x ) {
      m_buffer.read( x );
    }
    /// Load a scalar from a given position.
    template <typename T>
    void loadAt( T& x, std::size_t i ) const {
      m_buffer.read( x, i );
    }
    /// Load a scalar and return it.
    template <typename T>
    T load() {
      T x;
      m_buffer.read( x );
      return x;
    }

    /// Add a byte buffer of a packed data buffer to another one at a given position
    void addBuffer( PackedDataInBuffer const& x, std::uint32_t p ) {
      return m_buffer.assign( x.m_buffer.subspan( p, x.m_buffer.pos() - p ) );
    }
    /// Add a byte buffer of a packed data buffer to another one at a given position
    void addBuffer( ByteBuffer const& x, std::uint32_t p ) { return m_buffer.assign( x.subspan( p, x.pos() - p ) ); }
    /// Load a scalar from a given position and return it.
    template <typename T>
    T loadAt( std::size_t i ) const {
      T x;
      m_buffer.read( x, i );
      return x;
    }
    /// Load a size integer and return it.
    std::size_t loadSize() {
      uint32_t y;
      io( y );
      return y;
    }
    /// Load a vector of scalars.
    template <typename T, typename Allocator>
    typename std::enable_if_t<std::is_scalar_v<T>> load( std::vector<T, Allocator>& v ) {
      v.resize( loadSize() );
      for ( auto& x : v ) load( x );
    }

    // ===========================================================================
    // Dealing with objects
    // ===========================================================================
    /// Load a serializable object.
    template <typename T>
    auto load( T& object ) -> decltype( PackedData::load( *this, object ), std::size_t() ) {
      auto index0 = m_buffer.pos();
      PackedData::load( *this, object );
      return m_buffer.pos() - index0;
    }
    /// Load a vector of serializable objects
    template <typename T, typename Allocator>
    auto load( std::vector<T, Allocator>& v, unsigned int version )
        -> decltype( PackedData::load( *this, v[0], version ), void() ) {
      v.resize( loadSize() );
      if ( v.empty() ) return;
      const SerializationOffsets<T> offsets{version}; // TODO can we optimize this?
      auto                          totalSize = offsets.offsets().back() * v.size();
      auto                          pos       = m_buffer.readNull( totalSize ); // mark the data as read
      SOABuffer<PackedDataInBuffer> soabuf{this, pos, v.size(), offsets};
      for ( auto& object : v ) PackedData::load( soabuf, object, version );
    }

  private:
    ByteBuffer m_buffer; ///< Internal byte buffer
  };

} // namespace LHCb::Hlt::PackedData
