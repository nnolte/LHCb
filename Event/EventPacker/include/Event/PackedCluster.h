/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/UTCluster.h"
#include "GaudiKernel/DataObject.h"
#include "StandardPacker.h"
#include <string>
#include <vector>

namespace LHCb {

  /** @class PackedCluster PackedCluster.h Event/PackedCluster.h
   *
   *  Packed description of a cluster (Velo, ST) for mDST transmission
   *
   *  @author Olivier Callot
   *  @date   2012-03-05
   */
  struct PackedCluster {
    std::uint32_t id{0};
    std::uint32_t begin{0};
    std::uint32_t end{0};
    std::uint32_t sum{0};
    std::uint32_t sourceID{0};
    std::uint32_t tell1Channel{0};
    std::int32_t  spill{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedClusters = 1540;

  namespace PackedClusterLocation {
    inline const std::string Default = "pRec/Track/Clusters";
    inline const std::string UT      = "pRec/UT/Clusters";
  } // namespace PackedClusterLocation

  /** @class PackedClusters PackedCluster.h Event/PackedCluster.h
   *
   *  Container of packed Clusters
   *
   *  @author Olivier Callot
   *  @date   2012-03-05
   */

  class PackedClusters : public DataObject {

  public:
    /// Standard constructor
    PackedClusters() {
      m_clusters.reserve( 100 );
      m_strips.reserve( 1000 );
      m_adcs.reserve( 1000 );
    }

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

  public:
    const CLID&        clID() const override { return PackedClusters::classID(); }
    static const CLID& classID() { return CLID_PackedClusters; }

  public:
    void addUTCluster( const LHCb::UTCluster* sCl ) {
      addUTCluster( sCl, 0x50000000 ); // need to find the proper key (JC)
    }

    void addUTCluster( const LHCb::UTCluster* uCl, const std::uint32_t key );

  public:
    const std::vector<PackedCluster>& clusters() const { return m_clusters; }
    const std::vector<std::int32_t>&  strips() const { return m_strips; }
    const std::vector<std::uint32_t>& adcs() const { return m_adcs; }

  public:
    /// Set the packing version
    void setPackingVersion( const char ver ) { m_packingVersion = ver; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_clusters );
      buf.save( m_strips );
      buf.save( m_adcs );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      setPackingVersion( buf.template load<uint8_t>() );
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedClusters packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_clusters, m_packingVersion );
      buf.load( m_strips );
      buf.load( m_adcs );
    }

  private:
    std::vector<PackedCluster> m_clusters;
    std::vector<std::int32_t>  m_strips;
    std::vector<std::uint32_t> m_adcs;

    /** Data packing version
     *  Note the default packing version here must stay as zero, for compatibility
     *  with data written before the packing version was added, to implicitly
     *  define the version as 0 for this data  */
    char m_packingVersion{0};
  };

} // namespace LHCb
