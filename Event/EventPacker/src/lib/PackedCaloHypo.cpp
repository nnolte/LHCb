/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloHypo.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

enum struct ErrorCode {
  hypo,
  lh,
  pos_mismatch,
  pos_x,
  pos_y,
  pos_z,
  pos_e,
  center0,
  center1,
  digit_size,
  digit_ref,
  cluster_size,
  cluster_ref,
  hypo_size,
  hypo_ref,
  cov00,
  cov11,
  cov22,
  spread00,
  spread11,
  cov10,
  cov20,
  cov21,
  spread10
};

std::ostream& operator<<( std::ostream& os, ErrorCode ec ) {
  switch ( ec ) {
  case ErrorCode::hypo:
    return os << "Hypo";
  case ErrorCode::lh:
    return os << "lh";
  case ErrorCode::pos_mismatch:
    return os << "pos_mismatch";
  case ErrorCode::pos_x:
    return os << "pos_x";
  case ErrorCode::pos_y:
    return os << "pos_y";
  case ErrorCode::pos_z:
    return os << "pos_z";
  case ErrorCode::pos_e:
    return os << "pos_e";
  case ErrorCode::center0:
    return os << "center0";
  case ErrorCode::center1:
    return os << "center1";
  case ErrorCode::digit_size:
    return os << "digit_size";
  case ErrorCode::digit_ref:
    return os << "digit_ref";
  case ErrorCode::cluster_size:
    return os << "cluster_size";
  case ErrorCode::cluster_ref:
    return os << "cluster_ref";
  case ErrorCode::hypo_size:
    return os << "hypo_size";
  case ErrorCode::hypo_ref:
    return os << "hypo_ref";
  case ErrorCode::cov00:
    return os << "cov00";
  case ErrorCode::cov11:
    return os << "cov11";
  case ErrorCode::cov22:
    return os << "cov22";
  case ErrorCode::cov10:
    return os << "cov10";
  case ErrorCode::cov20:
    return os << "cov20";
  case ErrorCode::cov21:
    return os << "cov21";
  case ErrorCode::spread00:
    return os << "spread00";
  case ErrorCode::spread10:
    return os << "spread10";
  case ErrorCode::spread11:
    return os << "spread11";
  }
  throw std::runtime_error{"unknown CaloHypo unpacking ErrorCode"};
}

void CaloHypoPacker::pack( const DataVector& hypos, PackedDataVector& phypos ) const {

  // packing version
  phypos.setVersion( 1 );
  const auto ver = phypos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  phypos.data().reserve( hypos.size() );

  for ( const auto* H : hypos ) {
    // make new packed object
    auto& pH = phypos.data().emplace_back();

    // Save the data
    pH.key        = H->key();
    pH.hypothesis = H->hypothesis();
    pH.lh         = StandardPacker::fltPacked( H->lh() );
    if ( !H->position() ) {
      pH.z      = 0;
      pH.posX   = 0;
      pH.posY   = 0;
      pH.posE   = 0;
      pH.cov00  = 0;
      pH.cov10  = 0;
      pH.cov20  = 0;
      pH.cov11  = 0;
      pH.cov21  = 0;
      pH.cov22  = 0;
      pH.centX  = 0;
      pH.centY  = 0;
      pH.cerr00 = 0;
      pH.cerr10 = 0;
      pH.cerr11 = 0;
    } else {
      const auto* pos = H->position();
      pH.z            = StandardPacker::position( pos->z() );
      pH.posX         = StandardPacker::position( pos->x() );
      pH.posY         = StandardPacker::position( pos->y() );
      pH.posE         = StandardPacker::energy( pos->e() );

      // convariance Matrix
      const auto err0 = safe_sqrt( pos->covariance()( 0, 0 ) );
      const auto err1 = safe_sqrt( pos->covariance()( 1, 1 ) );
      const auto err2 = safe_sqrt( pos->covariance()( 2, 2 ) );
      pH.cov00        = StandardPacker::position( err0 );
      pH.cov11        = StandardPacker::position( err1 );
      pH.cov22        = StandardPacker::energy( err2 );
      pH.cov10        = StandardPacker::fraction( pos->covariance()( 1, 0 ), err1 * err0 );
      pH.cov20        = StandardPacker::fraction( pos->covariance()( 2, 0 ), err2 * err0 );
      pH.cov21        = StandardPacker::fraction( pos->covariance()( 2, 1 ), err2 * err1 );

      pH.centX = StandardPacker::position( pos->center()( 0 ) );
      pH.centY = StandardPacker::position( pos->center()( 1 ) );

      const auto serr0 = safe_sqrt( pos->spread()( 0, 0 ) );
      const auto serr1 = safe_sqrt( pos->spread()( 1, 1 ) );
      pH.cerr00        = StandardPacker::position( serr0 );
      pH.cerr11        = StandardPacker::position( serr1 );
      pH.cerr10        = StandardPacker::fraction( pos->spread()( 1, 0 ), serr1 * serr0 );
    }

    //== Store the CaloDigits
    pH.firstDigit = phypos.refs().size();
    for ( const auto& dig : H->digits() ) {
      if ( dig.target() ) {
        phypos.refs().push_back(
            0 == ver ? StandardPacker::reference32( &parent(), &phypos, dig->parent(), dig->key().all() )
                     : StandardPacker::reference64( &phypos, dig->parent(), dig->key().all() ) );
      } else {
        parent().warning() << "Null CaloDigit SmartRef found" << endmsg;
      }
    }
    pH.lastDigit = phypos.refs().size();

    //== Store the CaloClusters
    pH.firstCluster = phypos.refs().size();
    for ( const auto& clu : H->clusters() ) {
      if ( clu.target() ) {
        phypos.refs().push_back( 0 == ver ? StandardPacker::reference32( &parent(), &phypos, clu->parent(), clu->key() )
                                          : StandardPacker::reference64( &phypos, clu->parent(), clu->key() ) );
      } else {
        parent().warning() << "Null CaloCluster SmartRef found" << endmsg;
      }
    }
    pH.lastCluster = phypos.refs().size();

    //== Store the CaloHypos
    pH.firstHypo = phypos.refs().size();
    for ( const auto& iH : H->hypos() ) {
      if ( iH.target() ) {
        phypos.refs().push_back( 0 == ver ? StandardPacker::reference32( &parent(), &phypos, iH->parent(), iH->key() )
                                          : StandardPacker::reference64( &phypos, iH->parent(), iH->key() ) );
      } else {
        parent().warning() << "Null CaloCluster CaloHypo found" << endmsg;
      }
    }
    pH.lastHypo = phypos.refs().size();

  } // loop over hypos
}

void CaloHypoPacker::unpack( const PackedDataVector& phypos, DataVector& hypos ) const {

  // packing version
  const auto ver = phypos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  hypos.reserve( phypos.data().size() );

  for ( const auto& src : phypos.data() ) {

    // make new unpacked object
    auto* hypo = new LHCb::CaloHypo();
    hypos.insert( hypo, src.key );

    // fill data objects
    hypo->setHypothesis( (LHCb::CaloHypo::Hypothesis)src.hypothesis );
    hypo->setLh( StandardPacker::fltPacked( src.lh ) );
    if ( 0 != src.z ) {
      auto pos = std::make_unique<LHCb::CaloPosition>();
      pos->setZ( StandardPacker::position( src.z ) );
      pos->setParameters( LHCb::CaloPosition::Parameters( StandardPacker::position( src.posX ),
                                                          StandardPacker::position( src.posY ),
                                                          StandardPacker::energy( src.posE ) ) );

      auto&      cov  = pos->covariance();
      const auto err0 = StandardPacker::position( src.cov00 );
      const auto err1 = StandardPacker::position( src.cov11 );
      const auto err2 = StandardPacker::energy( src.cov22 );
      cov( 0, 0 )     = err0 * err0;
      cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( src.cov10 );
      cov( 1, 1 )     = err1 * err1;
      cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( src.cov20 );
      cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( src.cov21 );
      cov( 2, 2 )     = err2 * err2;

      pos->setCenter(
          LHCb::CaloPosition::Center( StandardPacker::position( src.centX ), StandardPacker::position( src.centY ) ) );

      auto&      spr   = pos->spread();
      const auto serr0 = StandardPacker::position( src.cerr00 );
      const auto serr1 = StandardPacker::position( src.cerr11 );
      spr( 0, 0 )      = serr0 * serr0;
      spr( 1, 0 )      = serr1 * serr0 * StandardPacker::fraction( src.cerr10 );
      spr( 1, 1 )      = serr1 * serr1;

      hypo->setPosition( std::move( pos ) );
    }

    int hintID( 0 ), key( 0 );
    for ( const auto reference : Packer::subrange( phypos.refs(), src.firstDigit, src.lastDigit ) ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &phypos, &hypos, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( reference, &phypos, &hypos, hintID, key ) ) ) {
        hypo->addToDigits( {&hypos, hintID, key} );
      } else {
        parent().error() << "Corrupt CaloHypo CaloDigit SmartRef detected." << endmsg;
      }
    }
    for ( const auto reference : Packer::subrange( phypos.refs(), src.firstCluster, src.lastCluster ) ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &phypos, &hypos, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( reference, &phypos, &hypos, hintID, key ) ) ) {
        hypo->addToClusters( {&hypos, hintID, key} );
      } else {
        parent().error() << "Corrupt CaloHypo CaloCluster SmartRef detected." << endmsg;
      }
    }
    for ( const auto reference : Packer::subrange( phypos.refs(), src.firstHypo, src.lastHypo ) ) {
      if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &phypos, &hypos, hintID, key ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( reference, &phypos, &hypos, hintID, key ) ) ) {
        hypo->addToHypos( {&hypos, hintID, key} );
      } else {
        parent().error() << "Corrupt CaloHypo CaloHypo SmartRef detected." << endmsg;
      }
    }

  } // loop over hypos
}

StatusCode CaloHypoPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  if ( dataA.size() != dataB.size() ) {
    parent().err() << "Old CaloHypo size " << dataA.size() << " differs form Test " << dataB.size() << endmsg;
    return StatusCode::FAILURE;
  }

  auto itOld  = dataA.begin();
  auto itTest = dataB.begin();
  while ( dataA.end() != itOld ) {
    LHCb::CaloHypo* oHypo = ( *itOld++ );
    LHCb::CaloHypo* tHypo = ( *itTest++ );
    if ( oHypo->key() != tHypo->key() ) {
      parent().warning() << "Wrong key : old " << oHypo->key() << " test " << tHypo->key() << endmsg;
    }

    std::vector<ErrorCode> errors;

    if ( oHypo->hypothesis() != tHypo->hypothesis() ) errors.push_back( ErrorCode::hypo );
    if ( 1.e-7 < std::abs( ( oHypo->lh() - tHypo->lh() ) / oHypo->lh() ) ) errors.push_back( ErrorCode::lh );
    const LHCb::CaloPosition* oPos = oHypo->position();
    const LHCb::CaloPosition* tPos = tHypo->position();
    if ( oPos == 0 && tPos != 0 ) errors.push_back( ErrorCode::pos_mismatch );
    if ( oPos != 0 && tPos == 0 ) errors.push_back( ErrorCode::pos_mismatch );
    if ( oPos != 0 && tPos != 0 ) {
      if ( 5.e-5 < std::abs( oPos->z() - tPos->z() ) ) errors.push_back( ErrorCode::pos_z );
      if ( 5.e-5 < std::abs( oPos->x() - tPos->x() ) ) errors.push_back( ErrorCode::pos_x );
      if ( 5.01e-5 < std::abs( oPos->y() - tPos->y() ) ) errors.push_back( ErrorCode::pos_y );
      if ( 5.01e-3 < std::abs( oPos->e() - tPos->e() ) ) errors.push_back( ErrorCode::pos_e );

      if ( 5.e-5 < std::abs( oPos->center()( 0 ) - tPos->center()( 0 ) ) ) errors.push_back( ErrorCode::center0 );
      if ( 5.e-5 < std::abs( oPos->center()( 1 ) - tPos->center()( 1 ) ) ) errors.push_back( ErrorCode::center1 );

      auto oDiag = std::array{safe_sqrt( oPos->covariance()( 0, 0 ) ), safe_sqrt( oPos->covariance()( 1, 1 ) ),
                              safe_sqrt( oPos->covariance()( 2, 2 ) ), safe_sqrt( oPos->spread()( 0, 0 ) ),
                              safe_sqrt( oPos->spread()( 1, 1 ) )};

      auto tDiag = std::array{safe_sqrt( tPos->covariance()( 0, 0 ) ), safe_sqrt( tPos->covariance()( 1, 1 ) ),
                              safe_sqrt( tPos->covariance()( 2, 2 ) ), safe_sqrt( tPos->spread()( 0, 0 ) ),
                              safe_sqrt( tPos->spread()( 1, 1 ) )};

      if ( 5.e-5 < std::abs( oDiag[0] - tDiag[0] ) ) errors.push_back( ErrorCode::cov00 );
      if ( 5.e-5 < std::abs( oDiag[1] - tDiag[1] ) ) errors.push_back( ErrorCode::cov11 );
      if ( 5.e-3 < std::abs( oDiag[2] - tDiag[2] ) ) errors.push_back( ErrorCode::cov22 );
      if ( 5.e-5 < std::abs( oDiag[3] - tDiag[3] ) ) errors.push_back( ErrorCode::spread00 );
      if ( 5.e-5 < std::abs( oDiag[4] - tDiag[4] ) ) errors.push_back( ErrorCode::spread11 );

      auto oFrac =
          std::array{oPos->covariance()( 1, 0 ) / oDiag[1] / oDiag[0], oPos->covariance()( 2, 0 ) / oDiag[2] / oDiag[0],
                     oPos->covariance()( 2, 1 ) / oDiag[2] / oDiag[1], oPos->spread()( 1, 0 ) / oDiag[3] / oDiag[4]};

      auto tFrac =
          std::array{tPos->covariance()( 1, 0 ) / tDiag[1] / tDiag[0], tPos->covariance()( 2, 0 ) / tDiag[2] / tDiag[0],
                     tPos->covariance()( 2, 1 ) / tDiag[2] / tDiag[1], tPos->spread()( 1, 0 ) / tDiag[3] / tDiag[4]};

      auto ec = std::array{ErrorCode::cov10, ErrorCode::cov20, ErrorCode::cov21, ErrorCode::spread10};

      for ( unsigned int kk = 0; oFrac.size() > kk; ++kk ) {
        if ( 2.e-5 < std::abs( oFrac[kk] - tFrac[kk] ) ) errors.push_back( ec[kk] );
      }
    }
    if ( oHypo->digits().size() != tHypo->digits().size() ) {
      errors.push_back( ErrorCode::digit_size );
    } else {
      for ( unsigned int kk = 0; oHypo->digits().size() > kk; kk++ ) {
        const LHCb::CaloDigit* dum  = oHypo->digits()[kk]; // convert smartref to pointers
        const LHCb::CaloDigit* dum1 = tHypo->digits()[kk];
        if ( dum != dum1 ) errors.push_back( ErrorCode::digit_ref );
      }
    }

    if ( oHypo->clusters().size() != tHypo->clusters().size() ) {
      errors.push_back( ErrorCode::cluster_size );
    } else {
      for ( unsigned int kk = 0; oHypo->clusters().size() > kk; kk++ ) {
        const LHCb::CaloCluster* dum  = oHypo->clusters()[kk]; // convert smartref to pointers
        const LHCb::CaloCluster* dum1 = tHypo->clusters()[kk];
        if ( dum != dum1 ) errors.push_back( ErrorCode::cluster_ref );
      }
    }

    if ( oHypo->hypos().size() != tHypo->hypos().size() ) {
      errors.push_back( ErrorCode::hypo_size );
    } else {
      for ( unsigned int kk = 0; oHypo->hypos().size() > kk; kk++ ) {
        const LHCb::CaloHypo* dum  = oHypo->hypos()[kk]; // convert smartref to pointers
        const LHCb::CaloHypo* dum1 = tHypo->hypos()[kk];
        if ( dum != dum1 ) errors.push_back( ErrorCode::hypo_ref );
      }
    }

    // Force for tests
    // isOK = false;

    if ( !errors.empty() || MSG::DEBUG >= parent().msgLevel() ) {
      const std::string loc = ( dataA.registry() ? dataA.registry()->identifier() : "Not in TES" );
      parent().warning() << "Problem with CaloHypo data packing :-" << endmsg;
      for ( auto e : errors ) parent().warning() << e << " ";
      parent().warning() << endmsg;
      parent().warning() << "  Original CaloHypo key=" << oHypo->key() << " in '" << loc << "'" << endmsg << *oHypo
                         << endmsg << "  Unpacked CaloHypo" << endmsg << *tHypo << endmsg;
    }
  }

  return sc;
}
