/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedWeightsVector.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void WeightsVectorPacker::pack( const DataVector& weightsV, PackedDataVector& pweightsV ) const {
  const auto pVer = pweightsV.packingVersion();
  if ( !isSupportedVer( pVer ) ) return;
  pweightsV.data().reserve( weightsV.size() );
  for ( const Data* weights : weightsV ) {
    // new packed data
    pweightsV.data().push_back( PackedData() );
    auto& pweights = pweightsV.data().back();

    // Save the PV key
    pweights.pvKey = weights->key();

    // fill packed data
    pweights.firstWeight = pweightsV.weights().size();
    pweightsV.weights().reserve( pweightsV.weights().size() + weights->weights().size() );
    for ( const auto& [k, v] : weights->weights() ) {
      pweightsV.weights().emplace_back( k, StandardPacker::fraction( v ) );
    }
    pweights.lastWeight = pweightsV.weights().size();
  }
}

void WeightsVectorPacker::unpack( const PackedDataVector& pweightsV, DataVector& weightsV ) const {
  const auto pVer = pweightsV.packingVersion();
  if ( !isSupportedVer( pVer ) ) return;
  weightsV.reserve( pweightsV.data().size() );
  for ( const PackedData& pweights : pweightsV.data() ) {

    // unpack the weights vector
    Data::WeightDataVector wWeights;
    wWeights.reserve( pweights.lastWeight - pweights.firstWeight );
    for ( const auto& pweight : Packer::subrange( pweightsV.weights(), pweights.firstWeight, pweights.lastWeight ) ) {
      wWeights.emplace_back( pweight.key, StandardPacker::fraction( pweight.weight ) );
    }

    // make and save new unpacked data
    auto* weights = new Data( std::move( wWeights ) );
    if ( 0 == pVer ) {
      weightsV.insert( weights );
    } else {
      weightsV.insert( weights, pweights.pvKey );
    }
  }
}

StatusCode WeightsVectorPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    // assume OK from the start
    bool ok = true;

    // loop over weights and test
    const bool sizeOK = ch.compareInts( "#Weights", ( *iA )->weights().size(), ( *iB )->weights().size() );
    ok &= sizeOK;
    if ( sizeOK ) {
      auto iWA( ( *iA )->weights().begin() ), iWB( ( *iB )->weights().begin() );
      for ( ; iWA != ( *iA )->weights().end() && iWB != ( *iB )->weights().end(); ++iWA, ++iWB ) {
        ok &= ( *iWA ).first == ( *iWB ).first;
        ok &= ch.compareDoubles( "Weight", ( *iWA ).second, ( *iWB ).second );
      }
    }

    // force printout for tests
    // ok = false;
    // If comparison not OK, print full information
    if ( !ok ) {
      parent().warning() << "Problem with WeightsVector data packing :-" << endmsg << "  Original Weight : " << *iA
                         << endmsg << "  Unpacked Weight : " << *iB << endmsg;
      sc = StatusCode::FAILURE;
    }
  }

  // Return final status
  return sc;
}
