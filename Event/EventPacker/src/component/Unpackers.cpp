/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Buffer1RelationUnpackerBaseAlg.h"
#include "Buffer2RelationUnpackerBaseAlg.h"
#include "BufferUnpackerBaseAlg.h"
#include "UnpackerBaseAlg.h"

#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecSummary.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedTwoProngVertex.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

#include "Event/PackedMCCaloHit.h"
#include "Event/PackedMCHit.h"
#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedMCRichHit.h"
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedMCRichSegment.h"
#include "Event/PackedMCRichTrack.h"

// MC Packers don't serialize data so we keep the ones taking packed objects
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichHitPacker>, "MCRichHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichOpticalPhotonPacker>, "MCRichOpticalPhotonUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichSegmentPacker>, "MCRichSegmentUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichTrackPacker>, "MCRichTrackUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCPrsHitPacker>, "MCPrsHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCSpdHitPacker>, "MCSpdHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCEcalHitPacker>, "MCEcalHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCHcalHitPacker>, "MCHcalHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCVeloHitPacker>, "MCVeloHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCVPHitPacker>, "MCVPHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCPuVetoHitPacker>, "MCPuVetoHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCTTHitPacker>, "MCTTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCUTHitPacker>, "MCUTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCITHitPacker>, "MCITHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCOTHitPacker>, "MCOTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCMuonHitPacker>, "MCMuonHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichDigitSummaryPacker>, "MCRichDigitSummaryUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCFTHitPacker>, "MCFTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCSLHitPacker>, "MCSLHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCHCHitPacker>, "MCHCHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCBcmHitPacker>, "MCBcmHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCBlsHitPacker>, "MCBlsHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCPlumeHitPacker>, "MCPlumeHitUnpacker" )

// These are needed for unpacking brunel output files, to be removed
// Used in RecoConf/data_from_file.py and  DaVinci/data_from_file.py
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::RichPIDPacker>, "UnpackRichPIDs" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MuonPIDPacker>, "UnpackMuonPIDs" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::WeightsVectorPacker>, "UnpackWeightsVector" )

// These packers take one data buffer location and produce unpacked object
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::RecVertexPacker>, "RecVertexUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::VertexPacker>, "VertexUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::TwoProngVertexPacker>, "TwoProngVertexUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::RichPIDPacker>, "RichPIDUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::MuonPIDPacker>, "MuonPIDUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::ParticlePacker>, "ParticleUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::TrackPacker>, "TrackUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::FlavourTagPacker>, "FlavourTagUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::CaloHypoPacker>, "CaloHypoUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::CaloClusterPacker>, "CaloClusterUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::CaloDigitPacker>, "CaloDigitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::CaloAdcPacker>, "CaloAdcUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::WeightsVectorPacker>, "WeightsVectorUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Unpack<LHCb::RecSummaryPacker>, "RecSummaryUnpacker" )
// ProtoParticleUnpacker is not templated, because it needs an additional function.

// Relation packers
using P2VRELATION   = DataPacking::Buffer::Rel2Unpack<LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase>,
                                                    LHCb::PackedRelations, LHCb::Particles, LHCb::RecVertices>;
using P2MCPRELATION = DataPacking::Buffer::Rel2Unpack<LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>,
                                                      LHCb::PackedRelations, LHCb::Particles, LHCb::MCParticles>;

using PP2MCPRELATION =
    DataPacking::Buffer::Rel2Unpack<LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>,
                                    LHCb::PackedWeightedRelations, LHCb::ProtoParticles, LHCb::MCParticles>;

using P2IntRELATION =
    DataPacking::Buffer::Rel1Unpack<LHCb::Relation1D<LHCb::Particle, int>, LHCb::PackedRelations, LHCb::Particles>;

using P2InfoRELATION = DataPacking::Buffer::Rel1Unpack<LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>,
                                                       LHCb::PackedRelatedInfoRelations, LHCb::Particles>;

DECLARE_COMPONENT_WITH_ID( P2VRELATION, "P2VRelationUnpacker" )
DECLARE_COMPONENT_WITH_ID( P2MCPRELATION, "P2MCPRelationUnpacker" )
DECLARE_COMPONENT_WITH_ID( PP2MCPRELATION, "PP2MCPRelationUnpacker" )

DECLARE_COMPONENT_WITH_ID( P2IntRELATION, "P2IntRelationUnpacker" )
DECLARE_COMPONENT_WITH_ID( P2InfoRELATION, "P2InfoRelationUnpacker" )
