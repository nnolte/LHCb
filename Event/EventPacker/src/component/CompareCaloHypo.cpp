/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloHypo.h"

#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   *  Compare two containers of CaloHypo
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  class CompareCaloHypo : public Algorithm::Consumer<void( const CaloHypos&, const CaloHypos& )> {

  public:
    /// Standard constructor
    CompareCaloHypo( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name,
                   pSvcLocator,
                   {KeyValue{"InputName", CaloHypoLocation::Electrons},
                    KeyValue{"TestName", CaloHypoLocation::Electrons + "Test"}}} {}

    void operator()( CaloHypos const& old, CaloHypos const& test ) const override {
      // compare and return
      const CaloHypoPacker packer( this );
      packer.check( old, test )
          .orThrow( "CompareCaloHypo failed", "CompareCaloHypo" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::CompareCaloHypo, "CompareCaloHypo" )
