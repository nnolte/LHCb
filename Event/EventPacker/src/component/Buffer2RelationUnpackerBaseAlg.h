/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/IANNSvc.h"
#include "LHCbAlgs/Consumer.h"

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"

#include "Event/PackedRelations.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

#include "Event/PackedDataBuffer.h"
#include "Event/StandardPacker.h"

#include "RegistryWrapper.h"

/**
 *  Templated base algorithm for
 *  (Proto)Particle  to MCParticle/PV relation unpacking algorithms
 *
 *  Note that the inheritance from Consumer and the void input are misleading.
 *  The algorithm is reading from and writing to TES, just via Handles so that
 *  it can deal with non existant input and with output failures, which is not
 *  authorized in the functional world.
 *  FIXME this should not be necessary, it's mainly due to misconfigurations in
 *  the tests
 *  Additionally packing requires the data objects to be in TES before one can add links to it
 *  https://gitlab.cern.ch/lhcb/LHCb/-/issues/180
 **/

namespace DataPacking::Buffer {

  template <typename RELATION, typename PRELATION, typename FROM, typename TO>
  class Rel2Unpack : public LHCb::Algorithm::Consumer<void( const LHCb::Hlt::PackedData::PackedDataInBuffer& )> {

    using Buffer = LHCb::Hlt::PackedData::PackedDataInBuffer;

  public:
    Rel2Unpack( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::Algorithm::Consumer<void( const Buffer& )>( name, pSvcLocator,
                                                            KeyValue{"InputName", "/Event/PackedRelations"} ) {}

    void operator()( const Buffer& buffer ) const override {

      auto* rels = m_rels.put( std::make_unique<RELATION>() );

      if ( !buffer.buffer().size() ) return;

      Buffer readBuffer;
      readBuffer.init( buffer.buffer(), false );

      // Sadly the pack structure expects an object with a valid RegEntry. To be improved
      auto prels = RegistryWrapper<PRELATION>( m_rels.fullKey().key() + "_Packed" );

      // Do the actual loading of the objects
      LHCb::Hlt::PackedData::ObjectHeader header;
      while ( !readBuffer.eof() ) {

        // Packer::io( readBuffer, header );
        readBuffer.load( header.classID );
        readBuffer.load( header.locationID );
        readBuffer.load( header.linkLocationIDs );
        readBuffer.load( header.storedSize );

        auto nBytesRead = readBuffer.load( *prels );

        if ( nBytesRead != header.storedSize ) {
          this->fatal() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                        << " consumed " << nBytesRead << " bytes, "
                        << " but " << header.storedSize << " were stored!" << endmsg;
        }

        if ( this->msgLevel( MSG::DEBUG ) ) {
          this->debug() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                        << " consumed " << nBytesRead << " bytes, "
                        << " packing version  " << prels->version() << " and " << header.storedSize << " were stored!"
                        << endmsg;
        }
      }

      static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

      for ( auto id : header.linkLocationIDs ) {
        auto location = m_hltANNSvc->value( PackedObjectLocations, id );
        if ( location ) {
          prels->linkMgr()->addLink( location.value().first, nullptr );
        } else {
          m_missingLinks++;
        }
      }

      if ( prels->data().size() ) {
        rels->setVersion( prels->version() );
        unpack( *rels, *prels );
      }

      // Count packed output
      m_unpackedData += rels->relations().size();
    }

    void unpack( RELATION& rels, const PRELATION& prels ) const {

      for ( const auto& prel : prels.data() ) {
        for ( int kk = prel.start; prel.end > kk; ++kk ) {
          int srcLink( 0 );
          int srcKey( 0 );
          int destLink( 0 );
          int destKey( 0 );

          StandardPacker::indexAndKey64( prels.sources()[kk], srcLink, srcKey );
          StandardPacker::indexAndKey64( prels.dests()[kk], destLink, destKey );

          DataObject* fp = nullptr;
          this->evtSvc()->retrieveObject( prels.linkMgr()->link( srcLink )->path(), fp ).ignore();
          auto* from = static_cast<FROM*>( fp );

          if ( !from ) {
            this->warning() << "Source location " << prels.linkMgr()->link( srcLink )->path()
                            << " not persisted, skip the relation." << endmsg;
            continue;
          }

          DataObject* tp = nullptr;
          this->evtSvc()->retrieveObject( prels.linkMgr()->link( destLink )->path(), tp ).ignore();
          auto* to = static_cast<TO*>( tp );

          if ( !to ) {
            this->warning() << "Destination location " << prels.linkMgr()->link( destLink )->path()
                            << " not persisted, skip the relation." << endmsg;
            continue;
          }

          typename RELATION::From f = from->object( srcKey );
          typename RELATION::To   t = to->object( destKey );

          StatusCode sc;

          // only weighted relations have weights
          if constexpr ( std::is_same_v<PRELATION, LHCb::PackedWeightedRelations> ) {
            typename RELATION::Weight wgt = prels.weights()[kk];
            sc                            = rels.relate( f, t, wgt );
          } else {
            sc = rels.relate( f, t );
          }

          if ( !sc ) {
            this->warning() << "Something went wrong with relation unpacking "
                            << " sourceKey " << srcKey << " sourceLink " << srcLink << " destKey " << destKey
                            << " destLink " << destLink << "\n"
                            << " from " << prels.linkMgr()->link( srcLink )->path() << "\n"
                            << " to " << prels.linkMgr()->link( destLink )->path() << endmsg;
          }

          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << "Relation build between "
                          << " sourceKey " << srcKey << " sourceLink " << srcLink << " destKey " << destKey
                          << " destLink " << destLink << "\n"
                          << " from " << prels.linkMgr()->link( srcLink )->path() << "\n"
                          << " to " << prels.linkMgr()->link( destLink )->path() << " rels size "
                          << rels.relations().size() << endmsg;
          }
        }
      }
    }

  private:
    ServiceHandle<IANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs"};

    DataObjectWriteHandle<RELATION> m_rels{this, "OutputName", ""};

    mutable Gaudi::Accumulators::StatCounter<> m_unpackedData{this, "# PackedData"};

    mutable Gaudi::Accumulators::StatCounter<> m_missingLinks{this, "# Missing Link Locations"};
  };

} // namespace DataPacking::Buffer
