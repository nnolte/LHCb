/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Interfaces/IProtoParticleTool.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "Event/PackedProtoParticle.h"
#include "Event/ProtoParticle.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/DataObjectHandle.h"

#include "Event/PackedDataBuffer.h"
#include "Kernel/IANNSvc.h"
#include "RegistryWrapper.h"

namespace DataPacking::Buffer {

  /**
   *  Unpack a protoparticle container
   *
   *  Note that the inheritance from Consumer is misleading. The algorithm is
   *  writing to TES, just via a Handle so that it can do it at the begining of
   *  the operator(), as cross pointers are used in the TES and requires this.
   */

  static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

  struct ProtoParticleUnpacker
      : LHCb::Algorithm::Consumer<void( DetectorElement const& ),
                                  LHCb::DetDesc::usesBaseAndConditions<FixTESPath<Gaudi::Algorithm>, DetectorElement>> {

    ProtoParticleUnpacker( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}}} {}
    void operator()( DetectorElement const& ) const override;

    ServiceHandle<IANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs"};

    DataObjectWriteHandle<LHCb::ProtoParticles> m_protos{this, "OutputName", LHCb::ProtoParticleLocation::Charged};
    DataObjectReadHandle<LHCb::Hlt::PackedData::PackedDataInBuffer> m_buffer{
        this, "InputName", LHCb::PackedProtoParticleLocation::Charged};

    ToolHandleArray<LHCb::Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
  };

  void ProtoParticleUnpacker::operator()( DetectorElement const& lhcb ) const {

    auto* data = m_protos.put( std::make_unique<LHCb::ProtoParticles>() );

    auto const buffer = m_buffer.get();
    if ( !buffer or !buffer->buffer().size() ) return;

    // Sadly the pack structure expects an object with a valid RegEntry. To be improved
    auto pdata = RegistryWrapper<LHCb::PackedProtoParticles>( m_protos.fullKey().key() + "_Packed" );

    if ( msgLevel( MSG::DEBUG ) )
      this->debug() << "Unpacking ProtoParticles from " << m_buffer << " to " << m_protos << endmsg;

    std::vector<int32_t>                      linkLocationIDs;
    LHCb::Hlt::PackedData::PackedDataInBuffer readBuffer = *buffer;
    const LHCb::ProtoParticlePacker           packer( this );

    // Do the actual loading of the objects
    LHCb::Hlt::PackedData::ObjectHeader header;
    while ( !readBuffer.eof() ) {
      readBuffer.load( header.classID );
      readBuffer.load( header.locationID );
      readBuffer.load( header.linkLocationIDs );
      readBuffer.load( header.storedSize );

      auto nBytesRead = readBuffer.load( *pdata );
      if ( nBytesRead != header.storedSize ) {
        this->fatal() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                      << "consumed " << nBytesRead << " bytes, "
                      << "but " << header.storedSize << " were stored!" << endmsg;
      }
      if ( msgLevel( MSG::DEBUG ) )
        this->debug() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                      << "consumed " << nBytesRead << " bytes, "
                      << "and " << header.linkLocationIDs.size() << " links were stored!" << endmsg;
    }

    for ( auto id : header.linkLocationIDs ) {
      auto location = m_hltANNSvc->value( PackedObjectLocations, id );
      if ( location ) {
        pdata->linkMgr()->addLink( location.value().first, nullptr );
      } else {
        this->warning() << " could not find the location for link id " << id << endmsg;
      }
    }

    // unpack
    data->setVersion( pdata->version() );
    packer.unpack( *pdata, *data );

    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Created " << data->size() << " ProtoParticles at " << m_protos.fullKey() << endmsg;

    for ( auto& addInfo : m_addInfo ) ( *addInfo )( *data, *lhcb.geometry() ).ignore();
  }

  DECLARE_COMPONENT_WITH_ID( ProtoParticleUnpacker, "ProtoParticleUnpacker" )

} // namespace DataPacking::Buffer
