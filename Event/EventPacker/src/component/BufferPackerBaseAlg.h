/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackedDataBuffer.h"
#include "Kernel/IANNSvc.h"
#include "LHCbAlgs/Producer.h"
#include "RegistryWrapper.h"

/**
 *  Templated base algorithm for all packing algorithms
 *  except MC objects
 *
 *  Note that the inheritance from Producer and the void input are misleading.
 *  The algorithm is reading from and writing to TES, just via Handles so that
 *  it can deal with non existant input and with output failures, which is not
 *  authorized in the functional world.
 *  FIXME this should not be necessary, it's mainly due to misconfigurations in
 *  the tests
 *  Additionally packing requires the data objects to be in TES before one can add links to it
 *  https://gitlab.cern.ch/lhcb/LHCb/-/issues/180
 **/

namespace DataPacking::Buffer {

  using Producer = LHCb::Algorithm::Producer<LHCb::Hlt::PackedData::PackedDataOutBuffer()>;

  template <class PACKER>
  class Pack final : public Producer {

    using Buffer = LHCb::Hlt::PackedData::PackedDataOutBuffer;

  public:
    // Standard constructor
    Pack( const std::string& name, ISvcLocator* pSvcLocator )
        : Producer( name, pSvcLocator, KeyValue{"OutputName", PACKER::packedLocation()} ) {}

    Buffer operator()() const override {

      if ( !m_data.exist() ) {
        if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Input location " << m_data << " doesn't exist." << endmsg;
        return {};
      }
      auto const* data = m_data.get();
      if ( data->size() == 0 ) {
        if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Input location " << m_data << " empty." << endmsg;
        return {};
      }

      // Sadly the pack structure expects an object with a valid registry entry. To be improved
      auto pdata = RegistryWrapper<typename PACKER::PackedDataVector>( m_data.fullKey().key() + "_Packed" );

      const PACKER packer( this );
      pdata->setPackingVersion( m_packingVersion );
      saveVersion( *data, *pdata );

      pdata->data().reserve( pdata->data().size() + data->size() );
      packer.pack( *data, *pdata );

      // Count packed output
      m_nbPackedData += pdata->data().size();

      // reserve some space for data, this should be tuned
      Buffer buffer;
      buffer.reserve( pdata->data().size() );
      static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};
      auto                          locationID = m_hltANNSvc->value( PackedObjectLocations, outputLocation() );
      auto                          classID    = pdata->clID();

      buffer.save<uint32_t>( classID );
      buffer.save<int32_t>( locationID->second );
      auto*        linkMgr = pdata->linkMgr();
      unsigned int nlinks  = linkMgr->size();

      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Packed version " << (unsigned int)pdata->version() << endmsg;
        this->debug() << "packed data type " << System::typeinfoName( typeid( *pdata ) ) << endmsg;
        this->debug() << "data location " << locationID->first << endmsg;
        this->debug() << "packed data size " << pdata->data().size() << endmsg;
        this->debug() << "classID " << classID << " locationID " << locationID->second << " nlinks " << nlinks
                      << endmsg;
      }

      buffer.saveSize( nlinks );
      for ( unsigned int id = 0; id < nlinks; ++id ) {
        auto location = linkMgr->link( id )->path();
        if ( location[0] != '/' ) { location = "/Event/" + location; }

        auto packedLocation = m_containerMap.find( location );
        if ( packedLocation != end( m_containerMap ) ) { location = packedLocation->second; }
        auto linkID = m_hltANNSvc->value( PackedObjectLocations, location );

        buffer.save<int32_t>( linkID->second );
      }

      // Reserve bytes for the size of the object
      auto posObjectSize = buffer.saveSize( 0 ).first;

      // Save the object actual object and see how many bytes were written
      auto objectSize = buffer.save( *pdata ).second;

      // Save the object's size in the correct position
      buffer.saveAt<uint32_t>( objectSize, posObjectSize );

      // Count packed output
      m_nbPackedData += pdata->data().size();

      // Packing checks
      if ( m_enableCheck ) {
        auto* unpacked     = new typename PACKER::DataVector();
        auto  testlocation = m_data.fullKey().key() + "_PackingCheck";
        this->evtSvc()->registerObject( testlocation, unpacked ).ignore();
        unpacked->setVersion( pdata->version() );
        packer.unpack( *pdata, *unpacked );
        packer.check( *data, *unpacked ).ignore();
        this->evtSvc()
            ->unregisterObject( unpacked )
            .andThen( [&unpacked] { delete unpacked; } )
            .orElse( [&] { ++m_unregisterError; } )
            .ignore();
      }

      return buffer;
    }

    /// Copy data object version
    template <typename INPUT, typename OUTPUT>
    void saveVersion( const INPUT& in, OUTPUT& out ) const {

      const int i_ver = in.version();
      const int o_ver = out.version();
      // sanity check
      if ( o_ver != 0 && o_ver != i_ver ) {
        std::string loc = in.registry() ? in.registry()->identifier() : std::string{};
        this->warning() << loc << "  input version " << i_ver << " != current packed version " << o_ver << endmsg;
      }
      out.setVersion( i_ver );
    }

  private:
    ServiceHandle<IANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve PackedObjectLocations"};

    DataObjectReadHandle<typename PACKER::DataVector> m_data{this, "InputName", PACKER::unpackedLocation()};

    Gaudi::Property<unsigned short int> m_packingVersion{
        this, "PackingVersion", (unsigned short int)PACKER::PackedDataVector::defaultPackingVersion(),
        "Packing version number"};

    Gaudi::Property<bool> m_enableCheck{this, "EnableCheck", false};

    // Mapping of reconstruction object to their location after packing
    Gaudi::Property<std::map<std::string, std::string>> m_containerMap{this, "ContainerMap"};

    mutable Gaudi::Accumulators::StatCounter<>          m_nbPackedData{this, "# PackedData"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unregisterError{
        this, "Problem unregistering data in PackerBaseAlg", 10};
  };

} // namespace DataPacking::Buffer
