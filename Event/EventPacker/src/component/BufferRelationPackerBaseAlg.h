/*****************************************************************************\
   * (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration      *
   *                                                                             *
   * This software is distributed under the terms of the GNU General Public      *
   * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
   *                                                                             *
   * In applying this licence, CERN does not waive the privileges and immunities *
   * granted to it by virtue of its status as an Intergovernmental Organization  *
   * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/IANNSvc.h"
#include "LHCbAlgs/Producer.h"

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"

#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedRelations.h"
#include "Event/RelatedInfoMap.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

#include "Event/PackedDataBuffer.h"
#include "Event/StandardPacker.h"

#include "RegistryWrapper.h"

/**
 *  Templated base algorithm for all relation packing algorithms
 *
 *  Note that the inheritance from Producer and the void input are misleading.
 *  The algorithm is reading from and writing to TES, just via Handles so that
 *  it can deal with non existant input and with output failures, which is not
 *  authorized in the functional world.
 *  FIXME this should not be necessary, it's mainly due to misconfigurations in
 *  the tests
 *  Additionally packing requires the data objects to be in TES before one can add links to it
 *  https://gitlab.cern.ch/lhcb/LHCb/-/issues/180
 **/

namespace {

  // Relation types
  using P2VRELATION    = LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase>;
  using P2MCPRELATION  = LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>;
  using P2IntRELATION  = LHCb::Relation1D<LHCb::Particle, int>;
  using P2InfoRELATION = LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>;
  using PP2MCPRELATION = LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>;
} // namespace

namespace DataPacking::Buffer {

  template <typename RELATION, typename PRELATION>
  class RelPack : public LHCb::Algorithm::Producer<LHCb::Hlt::PackedData::PackedDataOutBuffer()> {

    using Buffer   = LHCb::Hlt::PackedData::PackedDataOutBuffer;
    using KeyValue = typename LHCb::Algorithm::Producer<Buffer()>::KeyValue;

  public:
    RelPack( const std::string& name, ISvcLocator* pSvcLocator )
        : LHCb::Algorithm::Producer<Buffer()>( name, pSvcLocator, KeyValue{"OutputName", "/Event/PackedRelations"} ) {}

    Buffer operator()() const override {

      if ( !m_rels.exist() ) {
        if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Input location " << m_rels << " doesn't exist." << endmsg;
        return {};
      }

      auto const* rels = m_rels.get();
      if ( !rels->relations().size() ) {
        if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Relations empty " << endmsg;
        return {};
      }

      // FIXME This (temporary) TES usage should not be needed
      // Sadly the pack structure expects an object with a valid RegistryEntry. To be improved
      auto prels = RegistryWrapper<PRELATION>( m_rels.fullKey().key() + "_Packed" );

      saveVersion( *rels, *prels );
      prels->data().reserve( prels->data().size() + rels->relations().size() );
      pack( *rels, *prels );

      // Count packed output
      m_nbPackedData += prels->data().size();

      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Relations size " << rels->relations().size() << endmsg;
        this->debug() << "PackedRelations size " << prels->data().size() << endmsg;
        this->debug() << "Relations version " << (int)rels->version() << endmsg;
        this->debug() << "PackedRelations version " << (int)prels->version() << endmsg;
      }

      // reserve some space for data, this should be tuned
      Buffer buffer;
      buffer.reserve( prels->data().size() );

      static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};
      auto                          locationID = m_hltANNSvc->value( PackedObjectLocations, outputLocation() );
      auto                          classID    = prels->clID();
      buffer.save<uint32_t>( classID );
      buffer.save<int32_t>( locationID->second );
      auto*        linkMgr = prels->linkMgr();
      unsigned int nlinks  = linkMgr->size();

      buffer.saveSize( nlinks );

      for ( unsigned int id = 0; id < nlinks; ++id ) {
        auto location = linkMgr->link( id )->path();
        if ( location[0] != '/' ) { location = "/Event/" + location; }

        auto packedLocation = m_containerMap.find( location );
        if ( packedLocation != end( m_containerMap ) ) { location = packedLocation->second; }
        auto linkID = m_hltANNSvc->value( PackedObjectLocations, location );
        buffer.save<int32_t>( linkID->second );
      }

      // Reserve bytes for the size of the object
      auto posObjectSize = buffer.saveSize( 0 ).first;

      // Save the object actual object and see how many bytes were written
      auto objectSize = buffer.save( *prels ).second;

      // Save the object's size in the correct position
      buffer.saveAt<uint32_t>( objectSize, posObjectSize );

      return buffer;
    }

    // Pack P2VRelations
    void pack( const P2VRELATION& rels, LHCb::PackedRelations& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container and key
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
        prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to()->parent(), R.to()->key() ) );
      }

      // last object
      prel.end = prels.sources().size();
    }
    // Pack P2MCPRelation
    void pack( const P2MCPRELATION& rels, LHCb::PackedRelations& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container and key
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
        prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to()->parent(), R.to()->key() ) );
      }

      // last object
      prel.end = prels.sources().size();
    }

    // Pack Proto particle 2 MC particle Relation
    void pack( const PP2MCPRELATION& rels, LHCb::PackedWeightedRelations& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container and key
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );
      prels.weights().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
        prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to()->parent(), R.to()->key() ) );
        prels.weights().emplace_back( R.weight() );
      }

      // last object
      prel.end = prels.sources().size();
    }

    // pack P2IntRELATION
    void pack( const P2IntRELATION& rels, LHCb::PackedRelations& prels ) const {

      // Make a new packed data object and save
      auto& prel = prels.data().emplace_back();

      // reference to original container and key
      prel.container = StandardPacker::reference64( &prels, &rels, 0 );

      // First object
      prel.start = prels.sources().size();

      // reserve size
      const auto newSize = prels.sources().size() + rels.relations().size();
      prels.sources().reserve( newSize );
      prels.dests().reserve( newSize );

      // Loop over relations
      for ( const auto& R : rels.relations() ) {
        prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
        prels.dests().emplace_back( R.to() );
      }

      // last object
      prel.end = prels.sources().size();
    }

    // particle 2 info relations
    void pack( const P2InfoRELATION& rels, LHCb::PackedRelatedInfoRelations& prels ) const {

      // Make a entry in the containers vector, for this TES location
      prels.containers().emplace_back();
      auto& pcont = prels.containers().back();

      // reference to original container and key
      pcont.reference = StandardPacker::reference64( &prels, &rels, 0 );

      // First entry in the relations vector
      pcont.first = prels.data().size();

      // Loop over the relations and fill
      prels.data().reserve( prels.data().size() + rels.relations().size() );

      // Use the packer to pack this location ...
      m_rInfoPacker.pack( rels, prels );

      // last entry in the relations vector
      pcont.last = prels.data().size();
    }

    /// Copy data object version
    template <typename INPUT, typename OUTPUT>
    void saveVersion( const INPUT& in, OUTPUT& out ) const {
      const int i_ver = in.version();
      const int o_ver = out.version();

      // sanity check
      if ( o_ver != 0 && o_ver != i_ver ) {
        std::string loc = in.registry() ? in.registry()->identifier() : std::string{};
        this->warning() << loc << "  input version " << i_ver << " != current packed version " << o_ver << endmsg;
      }
      out.setVersion( i_ver );
    }

  private:
    DataObjectReadHandle<RELATION> m_rels{this, "InputName", "/Event/PackedRelations"};

    ServiceHandle<IANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve PackdObjectLocations"};

    /// Related Info Packer
    const LHCb::RelatedInfoRelationsPacker m_rInfoPacker{this};

    // Should add some packing checks, this is here only to make configuration happy for now
    Gaudi::Property<bool> m_enableCheck{this, "EnableCheck", false};

    // Mapping of reconstruction object to their location after packing
    Gaudi::Property<std::map<std::string, std::string>> m_containerMap{this, "ContainerMap"};

    mutable Gaudi::Accumulators::StatCounter<> m_nbPackedData{this, "# PackedData"};
  };

} // namespace DataPacking::Buffer
