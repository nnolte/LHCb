/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/PackedDataBuffer.h"
#include "Kernel/IANNSvc.h"
#include "LHCbAlgs/Consumer.h"
#include "RegistryWrapper.h"

/**
 *  Templated base algorithm for all unpacking algorithms
 *  except relations and MC objects
 *
 *  Note that the inheritance from Consumer and the void input are misleading.
 *  The algorithm is reading from and writing to TES, just via Handles so that
 *  it can deal with non existant input and with output failures, which is not
 *  authorized in the functional world.
 *  FIXME this should not be necessary, it's mainly due to misconfigurations in
 *  the tests
 *  Additionally packing requires the data objects to be in TES before one can add links to it
 *  https://gitlab.cern.ch/lhcb/LHCb/-/issues/180
 **/

namespace DataPacking::Buffer {

  template <class PACKER>
  class Unpack final : public LHCb::Algorithm::Consumer<void()> {

    using Buffer = LHCb::Hlt::PackedData::PackedDataInBuffer;

  public:
    using Consumer::Consumer;
    StatusCode initialize() override;
    void       operator()() const override;

  private:
    ServiceHandle<IANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs"};

    DataObjectReadHandle<Buffer> m_buffer{this, "InputName", PACKER::packedLocation()};

    DataObjectWriteHandle<typename PACKER::DataVector> m_data{this, "OutputName", PACKER::unpackedLocation()};

    mutable Gaudi::Accumulators::StatCounter<> m_unpackedData{this, "# PackedData"};
    mutable Gaudi::Accumulators::StatCounter<> m_missingLinks{this, "# Missing Link Locations"};
  };

  template <class PACKER>
  StatusCode Unpack<PACKER>::initialize() {
    return Consumer::initialize().andThen( [&] {
      if ( this->msgLevel( MSG::DEBUG ) )
        this->debug() << "Input " << m_buffer.fullKey() << " Output " << m_data.fullKey() << "" << endmsg;
    } );
  }

  template <class PACKER>
  void Unpack<PACKER>::operator()() const {

    auto* data = m_data.put( std::make_unique<typename PACKER::DataVector>() );

    const PACKER packer( this );
    const auto   buffer = m_buffer.get();
    if ( !buffer or !buffer->buffer().size() ) return;

    // Sadly the pack structure expects data with valid Registry. To be improved
    auto pdata = RegistryWrapper<typename PACKER::PackedDataVector>( m_data.fullKey().key() + "_Packed" );

    LHCb::Hlt::PackedData::ObjectHeader header;
    Buffer                              readBuffer;
    readBuffer.init( buffer->buffer(), false );

    while ( !readBuffer.eof() ) {
      readBuffer.load( header.classID );
      readBuffer.load( header.locationID );
      readBuffer.load( header.linkLocationIDs );
      readBuffer.load( header.storedSize );

      auto nBytesRead = readBuffer.load( *pdata );

      if ( nBytesRead != header.storedSize ) {
        this->fatal() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                      << " consumed " << nBytesRead << " bytes, "
                      << " but " << header.storedSize << " were stored!" << endmsg;
      }

      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Loading of object (CLID=" << header.classID << " locationID=" << header.locationID << ") "
                      << " consumed " << nBytesRead << " bytes, "
                      << " and " << header.linkLocationIDs.size() << " links were stored!" << endmsg;
      }
    }

    static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};
    for ( auto id : header.linkLocationIDs ) {
      auto location = m_hltANNSvc->value( PackedObjectLocations, id );

      if ( location ) {
        pdata->linkMgr()->addLink( location.value().first, nullptr );
      } else {
        m_missingLinks++;
      }
    }
    // Fill unpacked data if there is anything in packed data
    if ( pdata->data().size() > 0 ) {
      data->setVersion( pdata->version() );
      packer.unpack( *pdata, *data );
      if ( this->msgLevel( MSG::DEBUG ) ) {
        this->debug() << "Created " << data->size() << " data objects from " << m_buffer << endmsg;
        this->debug() << "data type " << System::typeinfoName( typeid( data ) ) << endmsg;
        this->debug() << "packed data type " << System::typeinfoName( typeid( pdata ) ) << endmsg;
        this->debug() << " Packed Data Version    = " << (unsigned int)pdata->version() << endmsg;
        this->debug() << " Packed Packing Version = " << (unsigned int)pdata->packingVersion() << endmsg;
        this->debug() << " Unpacked Data Version  = " << (unsigned int)data->version() << endmsg;
      }
    }

    // Count packed output
    m_unpackedData += data->size();
  }

} // namespace DataPacking::Buffer
