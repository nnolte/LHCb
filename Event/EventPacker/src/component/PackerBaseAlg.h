/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/StandardPacker.h"
#include "LHCbAlgs/Consumer.h"

/**
 *  Templated base algorithm for all packing algorithms
 *
 *  Note that the inheritance from Consumer and the void input are misleading.
 *  The algorithm is reading from and writing to TES, just via Handles so that
 *  it can deal with non existant input and with output failures, which is not
 *  authorized in the functional world.
 *  FIXME this should not be necessary, it's mainly due to misconfigurations in
 *  the tests
 *  Additionally packing requires the data objects to be in TES before one can add links to it
 *  https://gitlab.cern.ch/lhcb/LHCb/-/issues/180
 *  @author Christopher Rob Jones
 *  @date   2009-10-14
 */

namespace DataPacking {

  template <class PACKER>
  class Pack : public LHCb::Algorithm::Consumer<void()> {

  public:
    using Consumer::Consumer;
    StatusCode initialize() override;
    void       operator()() const override;

  private:
    DataObjectReadHandle<typename PACKER::DataVector>        m_data{this, "InputName", PACKER::unpackedLocation()};
    DataObjectWriteHandle<typename PACKER::PackedDataVector> m_pdata{this, "OutputName", PACKER::packedLocation()};
    Gaudi::Property<unsigned short int>                      m_version{this, "PackingVersion",
                                                  (unsigned short int)PACKER::PackedDataVector::defaultPackingVersion(),
                                                  "Packing version number"};
    Gaudi::Property<bool>                                    m_enableCheck{this, "EnableCheck", false,
                                        "Flag to turn on automatic unpacking and checking of the output post-packing"};
    Gaudi::Property<bool>                                    m_clearRegistry{this, "ClearRegistry", true,
                                          "Flag to turn on the clearing of the registry if the input data is not kept"};

    mutable Gaudi::Accumulators::StatCounter<>          m_nbPackedData{this, "# PackedData"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unregisterError{
        this, "Problem unregistering data in PackerBaseAlg", 10};

    const PACKER m_packer{this};
  };

  template <class PACKER>
  StatusCode Pack<PACKER>::initialize() {
    return Consumer::initialize().andThen( [&] {
      if ( this->msgLevel( MSG::DEBUG ) )
        this->debug() << "Input '" << m_data.fullKey() << "' Output '" << m_pdata.fullKey() << "'" << endmsg;
    } );
  }

  template <class PACKER>
  void Pack<PACKER>::operator()() const {
    // deal with missing input or output failure
    if ( m_pdata.exist() ) return;
    auto* pdata = m_pdata.put( std::make_unique<typename PACKER::PackedDataVector>() );
    if ( !m_data.exist() ) return;

    auto const* data = m_data.get();
    pdata->setVersion( data->version() );
    pdata->setPackingVersion( m_version );
    // Fill packed data
    m_packer.pack( *data, *pdata );

    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "Created " << pdata->data().size() << " data objects at '" << m_pdata.fullKey() << endmsg;
      this->debug() << " Unpacked Data Version  = " << (unsigned int)data->version() << endmsg;
      this->debug() << " Packed Data Version    = " << (unsigned int)pdata->version() << endmsg;
      this->debug() << " Packed Packing Version = " << (unsigned int)pdata->packingVersion() << endmsg;
    }

    // Packing checks
    if ( m_enableCheck.value() ) {
      // make new unpacked output data object
      auto unpacked = std::make_unique<typename PACKER::DataVector>();
      auto location = m_data.fullKey().key() + "_PackingCheck";
      /// FIXME This (temporary) TES usage should not be needed
      /// Sadly the pack structure expects it. To be improved
      this->evtSvc()->registerObject( location, unpacked.get() ).ignore();
      // give same version as original
      unpacked->setVersion( data->version() );
      // unpack
      m_packer.unpack( *pdata, *unpacked );
      // run checks
      m_packer.check( *data, *unpacked ).ignore();
      // clean up after checks
      this->evtSvc()->unregisterObject( unpacked.get() ).ignore();
    }

    // Clear the registry address of the unpacked container, to prevent reloading
    if ( m_clearRegistry.value() ) {
      auto* pReg = data->registry();
      if ( pReg ) pReg->setAddress( nullptr );
    }

    // Count packed output
    m_nbPackedData += pdata->data().size();
  }

} // namespace DataPacking
