/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "BufferPackerBaseAlg.h"
#include "BufferRelationPackerBaseAlg.h"
#include "PackerBaseAlg.h"

#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecSummary.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedTwoProngVertex.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

#include "Event/PackedMCCaloHit.h"
#include "Event/PackedMCHit.h"
#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedMCRichHit.h"
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedMCRichSegment.h"
#include "Event/PackedMCRichTrack.h"

// MC Packers don't serialize data so we keep the ones producing packed objects
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichHitPacker>, "MCRichHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichOpticalPhotonPacker>, "MCRichOpticalPhotonPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichSegmentPacker>, "MCRichSegmentPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichTrackPacker>, "MCRichTrackPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCPrsHitPacker>, "MCPrsHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCSpdHitPacker>, "MCSpdHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCEcalHitPacker>, "MCEcalHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCHcalHitPacker>, "MCHcalHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCVeloHitPacker>, "MCVeloHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCVPHitPacker>, "MCVPHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCPuVetoHitPacker>, "MCPuVetoHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCTTHitPacker>, "MCTTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCUTHitPacker>, "MCUTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCITHitPacker>, "MCITHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCOTHitPacker>, "MCOTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCMuonHitPacker>, "MCMuonHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichDigitSummaryPacker>, "MCRichDigitSummaryPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCFTHitPacker>, "MCFTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCSLHitPacker>, "MCSLHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCHCHitPacker>, "MCHCHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCBcmHitPacker>, "MCBcmHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCBlsHitPacker>, "MCBlsHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCPlumeHitPacker>, "MCPlumeHitPacker" )

// These packers take one object location and produce packed data buffer
// To switch back to packers producing packed objects, just replace namespace with DataPacking::Pack
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::RecVertexPacker>, "RecVertexPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::VertexPacker>, "VertexPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::TwoProngVertexPacker>, "TwoProngVertexPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::RichPIDPacker>, "RichPIDPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::MuonPIDPacker>, "MuonPIDPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::ProtoParticlePacker>, "ProtoParticlePacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::ParticlePacker>, "ParticlePacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::TrackPacker>, "TrackPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::FlavourTagPacker>, "FlavourTagPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::CaloHypoPacker>, "CaloHypoPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::CaloClusterPacker>, "CaloClusterPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::CaloDigitPacker>, "CaloDigitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::CaloAdcPacker>, "CaloAdcPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::WeightsVectorPacker>, "WeightsVectorPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Buffer::Pack<LHCb::RecSummaryPacker>, "RecSummaryPacker" )

// Relation packers
using P2VRELATIONPacker =
    DataPacking::Buffer::RelPack<LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase>, LHCb::PackedRelations>;

using P2MCPRELATIONPacker =
    DataPacking::Buffer::RelPack<LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>, LHCb::PackedRelations>;

using PP2MCPRELATIONPacker =
    DataPacking::Buffer::RelPack<LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>,
                                 LHCb::PackedWeightedRelations>;
using P2IntRELATIONPacker = DataPacking::Buffer::RelPack<LHCb::Relation1D<LHCb::Particle, int>, LHCb::PackedRelations>;

using P2InfoRELATIONPacker = DataPacking::Buffer::RelPack<LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>,
                                                          LHCb::PackedRelatedInfoRelations>;

DECLARE_COMPONENT_WITH_ID( P2VRELATIONPacker, "P2VRelationPacker" )
DECLARE_COMPONENT_WITH_ID( P2MCPRELATIONPacker, "P2MCPRelationPacker" )
DECLARE_COMPONENT_WITH_ID( PP2MCPRELATIONPacker, "PP2MCPRelationPacker" )

DECLARE_COMPONENT_WITH_ID( P2IntRELATIONPacker, "P2IntRelationPacker" )
DECLARE_COMPONENT_WITH_ID( P2InfoRELATIONPacker, "P2InfoRelationPacker" )
