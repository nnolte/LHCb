/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOACollection.h"
#include "Kernel/meta_enum.h"

/** @file  CaloChargedInfo.h
 *  @brief Definition of object containing all charged calo PID info for ChargedBasic.
 */
namespace LHCb::Event::Calo {

  inline namespace v2 {

    // bremsstrahlung reco methos
    meta_enum_class( BremMethod, int, Unknown = 0, Cluster = 1, TrackBased = 2, Mixed = 3, LooseTrackBased = 4, Last );

    // definition and encoding of booleans into one int
    enum AccMasks { InEcal = 0x1, InHcal = 0x2 };
    enum BremMasks { InBrem = 0x1, HasBrem = 0x2 };

    inline int getAccEncoding( bool inecal, bool inhcal ) {
      return inecal * AccMasks::InEcal + inhcal * AccMasks::InHcal;
    }
    inline int getBremEncoding( bool inbrem, bool hasbrem ) {
      return inbrem * BremMasks::InBrem + hasbrem * BremMasks::HasBrem;
    }

    // lists / defines all to-be-saved variables
    namespace ChargedInfoTag {
      // acceptances ( calo ones encoded into one int )
      struct CaloAcceptance : Event::int_field {};
      struct BremAcceptance : Event::int_field {};
      // base variables
      struct ClusterMatch : Event::float_field {};
      struct ElectronMatch : Event::float_field {};
      struct ElectronEnergy : Event::float_field {};
      struct ElectronShowerEoP : Event::float_field {};
      struct ElectronShowerDLL : Event::float_field {};
      struct HcalEoP : Event::float_field {};
      // bremsstrahlung info
      struct BremHypoMatch : Event::float_field {};
      struct BremHypoEnergy : Event::float_field {};
      struct BremHypoDeltaX : Event::float_field {};
      struct BremTrackBasedEnergy : Event::float_field {};
      struct BremBendingCorrection : Event::float_field {};
      struct BremEnergy : Event::float_field {};
      // cluster IDs
      struct ClusterIndex : Event::int_field {};
      struct ElectronIndex : Event::int_field {};
      struct BremHypoIndex : Event::int_field {};
      // DLLs
      struct EcalPIDe : Event::float_field {};
      struct HcalPIDe : Event::float_field {};
      struct BremPIDe : Event::float_field {};
      struct EcalPIDmu : Event::float_field {};
      struct HcalPIDmu : Event::float_field {};

      // charged calo info
      template <typename T>
      using pid_t = Event::SOACollection<T, CaloAcceptance, ClusterMatch, ElectronMatch, ElectronEnergy,
                                         ElectronShowerEoP, ElectronShowerDLL, HcalEoP, ClusterIndex, ElectronIndex,
                                         EcalPIDe, HcalPIDe, EcalPIDmu, HcalPIDmu>;

      // brem info
      template <typename T>
      using brem_t =
          Event::SOACollection<T, BremAcceptance, BremHypoMatch, BremHypoEnergy, BremHypoDeltaX, BremTrackBasedEnergy,
                               BremBendingCorrection, BremEnergy, BremHypoIndex, BremPIDe>;

    } // namespace ChargedInfoTag

    // charged calo info; e.g. for Long, Downstream and T tracks
    struct ChargedPID : ChargedInfoTag::pid_t<ChargedPID> {
      using base_t = typename ChargedInfoTag::pid_t<ChargedPID>;
      using base_t::base_t;

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct ChargedPIDProxy : Event::Proxy<simd, behaviour, ContainerType> {
        using Event::Proxy<simd, behaviour, ContainerType>::Proxy;
        using simd_t = SIMDWrapper::type_map_t<simd>;
        using int_v  = typename simd_t::int_v;

        // acceptances
        [[nodiscard]] auto InEcal() const {
          return !( ( this->template get<ChargedInfoTag::CaloAcceptance>() & int_v{AccMasks::InEcal} ) == int_v{0} );
        }
        [[nodiscard]] auto InHcal() const {
          return !( ( this->template get<ChargedInfoTag::CaloAcceptance>() & int_v{AccMasks::InHcal} ) == int_v{0} );
        }

        // base variables
        [[nodiscard]] auto ClusterIndex() const { return this->template get<ChargedInfoTag::ClusterIndex>(); }
        [[nodiscard]] auto ClusterMatch() const { return this->template get<ChargedInfoTag::ClusterMatch>(); }
        [[nodiscard]] auto ElectronIndex() const { return this->template get<ChargedInfoTag::ElectronIndex>(); }
        [[nodiscard]] auto ElectronMatch() const { return this->template get<ChargedInfoTag::ElectronMatch>(); }
        [[nodiscard]] auto ElectronEnergy() const { return this->template get<ChargedInfoTag::ElectronEnergy>(); }
        [[nodiscard]] auto ElectronShowerEoP() const { return this->template get<ChargedInfoTag::ElectronShowerEoP>(); }
        [[nodiscard]] auto ElectronShowerDLL() const { return this->template get<ChargedInfoTag::ElectronShowerDLL>(); }
        [[nodiscard]] auto HcalEoP() const { return this->template get<ChargedInfoTag::HcalEoP>(); }

        // PID DLLs
        [[nodiscard]] auto EcalPIDe() const { return this->template get<ChargedInfoTag::EcalPIDe>(); }
        [[nodiscard]] auto HcalPIDe() const { return this->template get<ChargedInfoTag::HcalPIDe>(); }
        [[nodiscard]] auto EcalPIDmu() const { return this->template get<ChargedInfoTag::EcalPIDmu>(); }
        [[nodiscard]] auto HcalPIDmu() const { return this->template get<ChargedInfoTag::HcalPIDmu>(); }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = ChargedPIDProxy<simd, behaviour, ContainerType>;
    };

    // bremsstrahlung only information; e.g. for Upstream and VELO tracks
    struct BremInfo : ChargedInfoTag::brem_t<BremInfo> {
      using base_t = typename ChargedInfoTag::brem_t<BremInfo>;
      using base_t::base_t;

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct BremInfoProxy : Event::Proxy<simd, behaviour, ContainerType> {
        using Event::Proxy<simd, behaviour, ContainerType>::Proxy;
        using simd_t = SIMDWrapper::type_map_t<simd>;
        using int_v  = typename simd_t::int_v;

        // acceptances
        [[nodiscard]] auto InBrem() const {
          return !( ( this->template get<ChargedInfoTag::BremAcceptance>() & int_v{BremMasks::InBrem} ) == int_v{0} );
        }

        // base variables
        [[nodiscard]] auto BremHypoIndex() const { return this->template get<ChargedInfoTag::BremHypoIndex>(); }
        [[nodiscard]] auto BremHypoMatch() const { return this->template get<ChargedInfoTag::BremHypoMatch>(); }
        [[nodiscard]] auto BremHypoEnergy() const { return this->template get<ChargedInfoTag::BremHypoEnergy>(); }
        [[nodiscard]] auto BremHypoDeltaX() const { return this->template get<ChargedInfoTag::BremHypoDeltaX>(); }
        [[nodiscard]] auto BremBendingCorrection() const {
          return this->template get<ChargedInfoTag::BremBendingCorrection>();
        }
        [[nodiscard]] auto BremTrackBasedEnergy() const {
          return this->template get<ChargedInfoTag::BremTrackBasedEnergy>();
        }

        // momentum recovery info
        [[nodiscard]] auto HasBrem() const {
          return !( ( this->template get<ChargedInfoTag::BremAcceptance>() & int_v{BremMasks::HasBrem} ) == int_v{0} );
        }
        [[nodiscard]] auto BremEnergy() const { return this->template get<ChargedInfoTag::BremEnergy>(); }

        // PID DLLs
        [[nodiscard]] auto BremPIDe() const { return this->template get<ChargedInfoTag::BremPIDe>(); }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = BremInfoProxy<simd, behaviour, ContainerType>;
    };

  } // namespace v2

} // namespace LHCb::Event::Calo
