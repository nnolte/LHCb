/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SIMDEventTypes.h"
#include "Event/SOACollection.h"

/** @file  MuonPIDs_v2.h
 *  @brief Definition of object for MuonPID.
 */
namespace LHCb::Event::v2::Muon {
  enum struct StatusMasks { IsMuon = 0, InAcceptance, PreSelMomentum, IsMuonTight };

  namespace Tag {
    struct Status : Event::flag_field<StatusMasks> {};
    struct Chi2Corr : Event::float_field {};
    struct LLMu : Event::float_field {};
    struct LLBg : Event::float_field {};
    struct CatBoost : Event::float_field {};

    template <typename T>
    using pids_t = Event::SOACollection<T, Status, Chi2Corr, LLMu, LLBg, CatBoost>;
  } // namespace Tag

  struct PIDs : Tag::pids_t<PIDs> {
    using base_t = typename Tag::pids_t<PIDs>;
    using base_t::base_t;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PIDProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using Event::Proxy<simd, behaviour, ContainerType>::Proxy;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;
      using mask_v  = typename simd_t::mask_v;

      [[nodiscard]] auto IsMuon() const {
        return this->template get<Tag::Status>().template test<StatusMasks::IsMuon>();
      }
      [[nodiscard]] auto IsMuonTight() const {
        return this->template get<Tag::Status>().template test<StatusMasks::IsMuonTight>();
      }
      [[nodiscard]] auto InAcceptance() const {
        return this->template get<Tag::Status>().template test<StatusMasks::InAcceptance>();
      }
      [[nodiscard]] auto PreSelMomentum() const {
        return this->template get<Tag::Status>().template test<StatusMasks::PreSelMomentum>();
      }
      [[nodiscard]] auto Chi2Corr() const { return this->template get<Tag::Chi2Corr>(); }
      [[nodiscard]] auto CatBoost() const { return this->template get<Tag::CatBoost>(); }
      [[nodiscard]] auto LLMu() const { return this->template get<Tag::LLMu>(); }
      [[nodiscard]] auto LLBg() const { return this->template get<Tag::LLBg>(); }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PIDProxy<simd, behaviour, ContainerType>;
  };
} // namespace LHCb::Event::v2::Muon
