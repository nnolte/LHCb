/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "PrTracksTag.h"

/** @class States States.h
 *  SoA implementation of states class
 */

namespace LHCb::Event::v3 {

  namespace StatesTag {

    struct State : Event::state_field {};
    struct Covariance : Event::state_cov_field {};
    struct Success : Event::int_field {};

    template <typename T>
    using states_t = Event::SOACollection<T, State, Covariance, Success>;
  } // namespace StatesTag

  struct States : StatesTag::states_t<States> {
    using base_t = typename StatesTag::states_t<States>;
    using base_t::base_t;
  };

} // namespace LHCb::Event::v3
