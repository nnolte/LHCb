/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTRAWBANKDECODERBASE_H
#define HLTRAWBANKDECODERBASE_H 1

// Include files
// from Gaudi
#include "DAQKernel/DecoderAlgBase.h"
#include "GaudiAlg/SplittingTransformer.h"
#include "GaudiAlg/Transformer.h"

#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/VectorMap.h"
#include "HltSourceID.h"
#include "Kernel/IANNSvc.h"
#include "Kernel/IIndexedANNSvc.h"

#include <mutex>

class HltRawBankDecoderBase : public Decoder::AlgBase {
public:
  using Decoder::AlgBase::AlgBase;

  StatusCode initialize() override;

  std::vector<const LHCb::RawBank*> selectRawBanks( LHCb::RawBank::View banks ) const;

  class element_t final {
    Gaudi::StringKey m_key;
    bool             m_decode;

  public:
    element_t( Gaudi::StringKey key, bool decode ) : m_key{std::move( key )}, m_decode{decode} {}
                       operator const Gaudi::StringKey&() const { return m_key; }
    const std::string& str() const { return m_key.str(); }
                       operator const std::string&() const { return str(); }
    bool               operator!() const { return !m_decode; }
    explicit           operator bool() const { return m_decode; }
  };

  const GaudiUtils::VectorMap<int, element_t>& id2string( unsigned int tck ) const {
    auto lock = std::lock_guard{m_mtx};
    auto itbl = m_idTable.find( tck );
    if ( itbl == std::end( m_idTable ) ) { return fetch_id2string( tck )->second; }
    return itbl->second;
  };
  const GaudiUtils::VectorMap<int, Gaudi::StringKey>& info2string( unsigned int tck ) const {
    auto                          lock = std::lock_guard{m_mtx};
    auto                          itbl = m_infoTable.find( tck );
    static const Gaudi::StringKey InfoID{"InfoID"};
    if ( itbl == std::end( m_infoTable ) ) { return fetch_info2string( tck, InfoID, m_infoTable )->second; }
    return itbl->second;
  }
  const GaudiUtils::VectorMap<int, Gaudi::StringKey>& packedObjectLocation2string( unsigned int tck ) const {
    auto                          lock = std::lock_guard{m_mtx};
    auto                          itbl = m_packedObjectLocationsTable.find( tck );
    static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};
    if ( itbl == std::end( m_packedObjectLocationsTable ) ) {
      return fetch_info2string( tck, PackedObjectLocations, m_packedObjectLocationsTable )->second;
    }
    return itbl->second;
  }
  unsigned int tck( const LHCb::RawEvent& event ) const;

private:
  ServiceHandle<IANNSvc>  m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs"};
  SmartIF<IIndexedANNSvc> m_TCKANNSvc;

  // FIXME/TODO: replace with stable containers so that returned values are stable
  //            and then do lookup / insert under a lock...
  using IdTable_t = std::map<unsigned int, GaudiUtils::VectorMap<int, element_t>>;
  using Table_t   = std::map<unsigned int, GaudiUtils::VectorMap<int, Gaudi::StringKey>>;
  mutable IdTable_t         m_idTable;
  IdTable_t::const_iterator fetch_id2string( unsigned int tck ) const;
  mutable Table_t           m_infoTable;
  mutable Table_t           m_packedObjectLocationsTable;
  mutable std::mutex        m_mtx;
  Table_t::const_iterator   fetch_info2string( unsigned int tck, const IANNSvc::major_key_type& major,
                                               Table_t& table ) const;

  Gaudi::Property<SourceIDs> m_sourceID{this, "SourceID", SourceIDs::Dummy};
};

using HltRawBankDecoder_Setup = Gaudi::Functional::Traits::BaseClass_t<HltRawBankDecoderBase>;

template <typename Out>
using HltRawBankDecoder = Gaudi::Functional::Transformer<Out( const LHCb::RawEvent& ), HltRawBankDecoder_Setup>;

template <typename... Out>
using HltRawBankMultiDecoder =
    Gaudi::Functional::MultiTransformer<std::tuple<Out...>( const LHCb::RawEvent& ), HltRawBankDecoder_Setup>;

template <typename Out>
using HltRawBankSplittingDecoder =
    Gaudi::Functional::SplittingTransformer<Gaudi::Functional::vector_of_optional_<Out>( const LHCb::RawEvent& ),
                                            HltRawBankDecoder_Setup>;
#endif
