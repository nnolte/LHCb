/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/StatusCode.h"
#include <iomanip>
#include <iostream>
#include <string>

namespace LHCb::Hlt::DAQ {

  enum SourceID : unsigned int {
    Dummy     = 0,
    Hlt       = Dummy,
    Hlt1      = 1,
    Hlt2      = 2,
    Spruce    = 3,
    Max       = 7,
    BitShift  = 13,
    MinorMask = 0x1FFF,
    MajorMask = 0xE000
  };

  StatusCode  parse( SourceID& id, const std::string& s );
  std::string toString( SourceID );

  inline std::ostream& toStream( SourceID id, std::ostream& os ) { return os << std::quoted( toString( id ), '\'' ); }
  inline std::ostream& operator<<( std::ostream& s, SourceID e ) { return toStream( e, s ); }

} // namespace LHCb::Hlt::DAQ

using SourceIDs /* [[deprecated]] */ = LHCb::Hlt::DAQ::SourceID; // TODO: deprecate and remove
