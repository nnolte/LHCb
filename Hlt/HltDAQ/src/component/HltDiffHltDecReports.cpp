/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "LHCbAlgs/Consumer.h"
//-----------------------------------------------------------------------------
// Implementation file for class : HltDiffHltDecReports
//
// 2009-05-26 : Gerhard Raven
//-----------------------------------------------------------------------------

struct HltDiffHltDecReports
    : LHCb::Algorithm::Consumer<void( LHCb::HltDecReports const&, LHCb::HltDecReports const& )> {

  HltDiffHltDecReports( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {KeyValue{"HltDecReportsLHS", ""}, KeyValue{"HltDecReportsRHS", ""}}} {}

  void operator()( LHCb::HltDecReports const& lhs, LHCb::HltDecReports const& rhs ) const override {
    if ( lhs.configuredTCK() != rhs.configuredTCK() ) {
      always() << " configuredTCK: " << inputLocation<0>() << " : " << lhs.configuredTCK() << inputLocation<1>()
               << " : " << rhs.configuredTCK() << endmsg;
    }
    auto l = lhs.begin(), r = rhs.begin();
    while ( l != lhs.end() || r != rhs.end() ) {
      auto rend = ( r == rhs.end() );
      auto lend = ( l == lhs.end() );

      if ( !lend && ( rend || l->first < r->first ) ) {
        always() << " only in " << inputLocation<0>() << " : " << l->first << " : " << l->second << endmsg;
        ++l;
      } else if ( !rend && ( lend || r->first < l->first ) ) {
        always() << " only in " << inputLocation<1>() << " : " << r->first << " : " << r->second << endmsg;
        ++r;
      } else {
        if ( l->second.decReport() != r->second.decReport() ) {
          always() << " diff: " << l->first << " : " << l->second << "  <-> " << r->second << endmsg;
        } else {
          // always() << " match: " << l->first << " : " << l->second << endmsg;
        }
        ++l;
        ++r;
      }
    }
  }
};

DECLARE_COMPONENT( HltDiffHltDecReports )
