/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/RawEvent.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "RZip.h"
#include <optional>

/** @class ...
 * Put packed data buffers into a single raw event
 *
 *  @author Sevda Esen
 *  @date   2021-07-15
 */

namespace LHCb::Hlt::PackedData {

  using VOC = Gaudi::Functional::vector_of_const_<PackedDataOutBuffer*>;

  class HltPackedBufferWriter final : public Algorithm::MergingTransformer<RawBank::View( VOC const& )> {

  public:
    // Standard constructor
    HltPackedBufferWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingTransformer( name, pSvcLocator, KeyValues{"PackedContainers", {}}, KeyValue{"OutputView", {}} ) {}

    // Functional operator
    RawBank::View operator()( const VOC& buffers ) const override {

      auto* rawEvent = m_outputRawEvent.getOrCreate();

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        this->debug() << "Requested to write " << buffers.size() << " buffers" << endmsg;
        this->debug() << "to " << m_outputRawEvent << endmsg;
        this->debug() << "to " << outputLocation() << endmsg;
      }

      /// Maximum bank payload size = 65535 (max uint16) - 8 (header) - 3 (alignment)
      constexpr size_t    MAX_PAYLOAD_SIZE{65524};
      unsigned int        nbuf_uncompressed = 0;
      PackedDataOutBuffer bigBuffer;

      for ( const auto& [nbuf, buffer] : range::enumerate( buffers ) ) {
        if ( !buffer || buffer->buffer().size() == 0 ) continue;
        bigBuffer.addBuffer( *buffer );
        if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
          debug() << " writing buffer for location " << inputLocation( nbuf ) << " buffer size "
                  << buffer->buffer().size() << endmsg;
        }
      }

      std::vector<std::byte> compressedBuffer;
      compressedBuffer.reserve( 0.5 * bigBuffer.buffer().size() );

      auto compressed = ( m_compression != Compression::NoCompression ) &&
                        bigBuffer.compress( m_compression, m_compressionLevel, compressedBuffer );

      Compression compression    = compressed ? m_compression.value() : Compression::NoCompression;
      uint16_t    sourceIDCommon = shift<SourceIDMasks::Compression>( static_cast<uint16_t>( compression ) );

      const auto& output = compressed ? compressedBuffer : bigBuffer.buffer();

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        this->debug() << " writing source id " << sourceIDCommon << " buffer size " << bigBuffer.buffer().size()
                      << " output size " << output.size() << endmsg;
      }

      auto chunks = range::chunk( output, MAX_PAYLOAD_SIZE );
      if ( chunks.size() > extract<SourceIDMasks::PartID>( ~uint16_t{0} ) ) {
        ++m_too_large;
        return rawEvent->banks( RawBank::DstData );
      }

      for ( auto [ibank, chunk] : range::enumerate( chunks ) ) {
        uint16_t sourceID = sourceIDCommon | shift<SourceIDMasks::PartID>( ibank );
        rawEvent->addBank( sourceID, RawBank::DstData, kVersionNumber, chunk );
      }

      m_serializedDataSize += bigBuffer.buffer().size();
      m_compressedDataSize += output.size();

      if ( !compressed ) nbuf_uncompressed++;

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        this->debug() << "Compressed " << buffers.size() - nbuf_uncompressed << " buffers" << endmsg;
        this->debug() << "Uncompressed " << nbuf_uncompressed << " buffers" << endmsg;
      }

      return rawEvent->banks( RawBank::DstData );
    }

  private:
    /// Property setting the compression algorithm
    Gaudi::Property<Compression> m_compression{this, "Compression", Compression::ZSTD};

    /// Property setting the compression level
    Gaudi::Property<int> m_compressionLevel{this, "CompressionLevel", 5};

    DataObjectWriteHandle<RawEvent> m_outputRawEvent{this, "OutputRawEvent", RawEventLocation::Default};

    mutable Gaudi::Accumulators::StatCounter<> m_serializedDataSize{this, "Size of serialized data"};

    mutable Gaudi::Accumulators::StatCounter<> m_compressedDataSize{this, "Size of compressed data"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_too_large{this, "Packed objects too large to save", 50};

    /// Property enabling calculation and print of checksums
    Gaudi::Property<bool> m_enableChecksum{this, "EnableChecksum", false};
  };

  DECLARE_COMPONENT_WITH_ID( HltPackedBufferWriter, "HltPackedBufferWriter" )

} // namespace LHCb::Hlt::PackedData
