/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "Event/RawEvent.h"
#include "HltSourceID.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltDecReportsWriter
//
// 2008-07-26 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

/** @class DecReportsWriter HltDecReportsWriter.h
 *
 *
 *  @author Tomasz Skwarnicki
 *  @date   2008-07-26
 *
 *  Algorithm to convert HltDecReports container on TES to *new* RawEvent and Raw Bank View
 *
 */

namespace LHCb::Hlt::DAQ {

  class DecReportsWriter : public Algorithm::MultiTransformer<std::tuple<LHCb::RawEvent, LHCb::RawBank::View>(
                               LHCb::HltDecReports const& )> {

    /// SourceID to insert in the bank header
    Gaudi::Property<SourceID> m_sourceID{this, "SourceID", SourceID::Dummy};

  public:
    enum HeaderIDs { kVersionNumber = 2 };

    DecReportsWriter( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer{name,
                           pSvcLocator,
                           // Input
                           KeyValue{"InputHltDecReportsLocation", LHCb::HltDecReportsLocation::Default},
                           // Outputs
                           {KeyValue{"OutputRawEvent", "/Event/DAQ/HltDecEvent"},
                            KeyValue{"OutputView", "/Event/DAQ/HltDec/View"}}} {};

    std::tuple<LHCb::RawEvent, LHCb::RawBank::View>
    operator()( LHCb::HltDecReports const& inputSummary ) const override {

      // Create *new* RawEvent
      LHCb::RawEvent outputrawevent;

      assert( m_sourceID == SourceIDs::Hlt1 || m_sourceID == SourceIDs::Hlt2 || m_sourceID == SourceIDs::Spruce );
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Input: ";
        for ( const auto& rep : inputSummary ) {
          auto decRep = HltDecReport{rep.second.decReport()};
          verbose() << decRep.intDecisionID() << "-" << decRep.decision() << " ";
        }
        verbose() << endmsg;
      }

      // compose the bank body
      std::vector<unsigned int> bankBody;
      bankBody.reserve( inputSummary.size() + 2 );
      bankBody.push_back( inputSummary.configuredTCK() );
      bankBody.push_back( inputSummary.taskID() );
      std::transform( std::begin( inputSummary ), std::end( inputSummary ), std::back_inserter( bankBody ),
                      []( const auto& r ) { return r.second.decReport(); } );

      // order according to the values, essentially orders by intDecisionID
      // this is important since it will put "*Global" reports at the beginning of the bank
      // NOTE: we must skip the first two words (configuredTCK, taskID)
      std::sort( std::next( std::begin( bankBody ), 2 ), std::end( bankBody ) );

      // shift bits in sourceID for the same convention as in HltSelReports
      outputrawevent.addBank( int( m_sourceID << SourceID::BitShift ), RawBank::HltDecReports, kVersionNumber,
                              bankBody );

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Output:  ";
        verbose() << " VersionNumber= " << kVersionNumber;
        verbose() << " SourceID= " << m_sourceID.value();
        auto i = std::begin( bankBody );
        verbose() << " configuredTCK = " << *i++ << " ";
        verbose() << " taskID = " << *i++ << " ";
        while ( i != std::end( bankBody ) ) {
          auto rep = HltDecReport{*i++};
          verbose() << rep.intDecisionID() << "-" << rep.decision() << " ";
        }
        verbose() << endmsg;
      }

      // FIXME this is _not_ save when a _subsequent_ algorithm calls
      // addBank,adoptBank or removeBank on the underlying RawEvent...
      // TODO: `writeViewFor` (see https://gitlab.cern.ch/gaudi/Gaudi/-/merge_requests/1151 )
      //       should be extended to allow it to be used for this case as well, as that would
      //       make the underlying `RawEvent` inaccessible, and thus _ensure_ it will not be
      //       modified (not even by code that doesn't play by the rules, provided it is not
      //       explicitly/maliciously tailored to do this)
      return std::tuple{std::move( outputrawevent ), outputrawevent.banks( RawBank::HltDecReports )};
      // without std::move here the RawEvent gets copied which would invalidate the view
      // View creation must be after RawEvent is made
    };
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( DecReportsWriter, "HltDecReportsWriter" )

} // namespace LHCb::Hlt::DAQ
