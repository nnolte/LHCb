/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HltRawBankDecoderBase.h"

#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackerBase.h"
#include "Event/RawEvent.h"
#include "Event/StandardPacker.h"

#include "GaudiAlg/SplittingTransformer.h"

#include "Kernel/IANNSvc.h"
#include "RZip.h"
#include <optional>

/** @class ...
 *
 * Decoding of PackedObjects from raw event
 * output locations are determined from
 *
 *  @author Sevda Esen
 *  @date   2021-07-15
 */

namespace LHCb::Hlt::PackedData {

  static const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};

  using VOC = Gaudi::Functional::vector_of_optional_<PackedDataInBuffer>;

  class HltPackedBufferDecoder final : public HltRawBankSplittingDecoder<PackedDataInBuffer> {

  public:
    // Standard constructor
    HltPackedBufferDecoder( const std::string& name, ISvcLocator* pSvcLocator )
        : HltRawBankSplittingDecoder<PackedDataInBuffer>(
              name, pSvcLocator, {"RawEventLocations", LHCb::RawEventLocation::Default}, {"OutputBuffers", {}} ) {}

    // Functional operator
    VOC operator()( const LHCb::RawEvent& rawEvent ) const override {

      VOC         buffers{outputLocationSize(), PackedDataInBuffer{}};
      const auto& locationsMap = packedObjectLocation2string( tck( rawEvent ) );

      auto const* rawBanksConst = m_rawBanks.get();
      if ( rawBanksConst->empty() ) {
        ++m_no_packed_bank;
        return buffers;
      }

      auto rawBanks = std::vector<const LHCb::RawBank*>{begin( *rawBanksConst ), end( *rawBanksConst )};

      std::sort( begin( rawBanks ), end( rawBanks ),
                 []( const LHCb::RawBank* lhs, const LHCb::RawBank* rhs ) { return partID( *lhs ) < partID( *rhs ); } );

      const auto* rawBank0 = rawBanks.front();

      // Check we know how to decode this version
      if ( rawBank0->version() < 2 || rawBank0->version() > kVersionNumber ) {
        ++m_bad_version;
        return buffers;
      }

      std::vector<std::byte> payload;
      payload.reserve( rawBank0->size() * rawBanks.size() );

      for ( auto [i, rawBank] : LHCb::range::enumerate( rawBanks ) ) {
        if ( partID( *rawBank ) != i ) {
          ++m_not_sequential;
          return buffers;
        }

        // Collect the data into a contiguous buffer
        auto r = rawBank->range<std::byte>();
        payload.insert( end( payload ), begin( r ), end( r ) );
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Rawbank size " << rawBank->size() << endmsg;
          debug() << "Payload size " << payload.size() << endmsg;
          debug() << "Compression " << compression( *rawBank0 ) << endmsg;
        }
      }

      PackedDataInBuffer buffer;
      // Decompress the payload and load into a one big buffer
      switch ( compression( *rawBank0 ) ) {
      case Compression::NoCompression: {
        buffer.init( payload );
        break;
      }
      case Compression::ZLIB:
      case Compression::LZMA:
      case Compression::LZ4:
      case Compression::ZSTD: {
        if ( !buffer.init( payload, true ) ) { ++m_decompression_failure; }
        break;
      }
      default: {
        ++m_unknown_compression;
        break;
      }
      }

      // keep track of how many buffers found
      int read = 0;

      while ( !buffer.eof() ) {
        // keep track of where this buffer starts are
        auto pos = buffer.buffer().pos();

        LHCb::Hlt::PackedData::ObjectHeader header;
        buffer.load( header.classID );
        buffer.load( header.locationID );
        buffer.load( header.linkLocationIDs );
        buffer.load( header.storedSize );

        // as we kept track of where we are, we skip over the payload to the next item
        buffer.skip( header.storedSize );

        if ( msgLevel( MSG::DEBUG ) ) printHeader( debug(), locationsMap, header );

        auto b = inputIndex( locationsMap, header );
        if ( b == -1 ) continue;
        // found a requested buffer
        // create a new buffer, and copy content of the big buffer
        // starting from (current position - pos ) = size of the buffer
        PackedDataInBuffer bufferIn;
        bufferIn.addBuffer( buffer, pos );

        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << " buffer number " << b << " with size " << bufferIn.buffer().size() << " to location "
                  << outputLocation( b ) << endmsg;
        }
        buffers[b] = std::move( bufferIn );
        ++read;
      }

      if ( UNLIKELY( msgLevel( MSG::DEBUG ) ) ) {
        debug() << "Found buffers " << read << " / " << outputLocationSize() << endmsg;
        int i = 0;
        for ( auto& buf : buffers ) {
          if ( buf && buf->buffer().size() > 0 )
            debug() << "Location " << i << ":  " << outputLocation( i ) << " Buffer " << buf->buffer().size() << endmsg;
          i++;
        }
      }

      return buffers;
    }

  private:
    DataObjectReadHandle<LHCb::RawBank::View> m_rawBanks{this, "RawBanks", "/Event/DAQ/RawBanks/DstData"};
    DataObjectReadHandle<LHCb::RawBank::View> m_decReports{this, "DecReports", "/Event/DAQ/RawBanks/HltDecReports"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_version{
        this, "HltPackedData raw bank version is not supported."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_sequential{
        this, "Part IDs for HltPackedData banks are not sequential."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_decompression_failure{this,
                                                                                "Failed to decompress HltPackedData."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unknown_compression{this, "Unknown compression method."};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unknown_packed_location{
        this, "Packed object location not found in ANNSvc. Skipping this link, unpacking may fail"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_packed_bank{
        this, "No HltPackedData raw bank (the DstData bank) in raw event."};

    int inputIndex( GaudiUtils::VectorMap<int, Gaudi::StringKey> const& map,
                    LHCb::Hlt::PackedData::ObjectHeader const&          header ) const {
      auto loc = map.find( header.locationID );
      if ( loc == end( map ) ) {
        warning() << "Packed object location not found in ANNSvc "
                  << "for id=" << header.locationID << " class ID " << header.classID
                  << ". Skipping reading the container!" << endmsg;
        ++m_unknown_packed_location;
        return -1;
      }
      for ( size_t i = 0; i < outputLocationSize(); ++i )
        if ( loc->second == outputLocation( i ) ) return i;
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " skipping location not requested in output list:" << loc->second << endmsg;
      }
      return -1;
    }

    static MsgStream& printHeader( MsgStream& os, GaudiUtils::VectorMap<int, Gaudi::StringKey> const& map,
                                   LHCb::Hlt::PackedData::ObjectHeader const& header ) {
      os << "class id=" << header.classID << endmsg;
      os << "location id=" << header.locationID << endmsg;
      os << "nlinks=" << header.linkLocationIDs.size() << endmsg;
      for ( auto j : header.linkLocationIDs ) {
        os << "link id=" << j;
        auto locationIt = map.find( j );
        if ( locationIt != end( map ) ) os << " location " << locationIt->second;
        os << " " << endmsg;
      }
      os << "stored size=" << header.storedSize << endmsg;
      return os;
    }
  };

} // namespace LHCb::Hlt::PackedData

DECLARE_COMPONENT_WITH_ID( LHCb::Hlt::PackedData::HltPackedBufferDecoder, "HltPackedBufferDecoder" )
