/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "DetDesc/Condition.h"
#include "Event/HltDecReports.h"
#include "Event/ODIN.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "HltDAQ/HltEvaluator.h"
#include "Kernel/IHltMonitorSvc.h"
#include "Kernel/RateCounter.h"
#include "LoKi/HLTTypes.h"
#include "LoKi/OdinTypes.h"
#include "boost/algorithm/string/join.hpp"
#include <array>
#include <boost/format.hpp>

//-----------------------------------------------------------------------------
// Implementation file for class : HltRoutingBitsWriter
//
// 2008-07-29 : Gerhard Raven
//-----------------------------------------------------------------------------

/** @class HltRoutingBitsWriter HltRoutingBitsWriter.h
 *
 *
 *  @author Gerhard Raven
 *  @date   2008-07-29
 */
class HltRoutingBitsWriter : public HltEvaluator {
public:
  /// Standard constructor
  using HltEvaluator::HltEvaluator;

  StatusCode execute() override; ///< Algorithm execution

private:
  /// Decode
  StatusCode decode() override;

  void zeroEvaluators();

  std::unordered_map<unsigned int, EvalVariant> m_evaluators;
  Gaudi::Property<bool>                         m_updateBank{this, "UpdateExistingRawBank", false};
  DataObjectWriteHandle<LHCb::RawEvent> m_outputRawEvent{this, "RawEventLocation", LHCb::RawEventLocation::Default};
  Gaudi::Property<std::map<unsigned int, std::string>> m_bits{this, "RoutingBits", {}, [=]( auto& ) {
                                                                /// mark as "to-be-updated"
                                                                m_evals_updated = true;
                                                                // no action if not yet initialized
                                                                if ( Gaudi::StateMachine::INITIALIZED > FSMState() )
                                                                  return;
                                                                // postpone the action
                                                                if ( !m_preambulo_updated ) { return; }
                                                                // perform the actual immediate decoding
                                                                decode().orThrow(
                                                                    "Error from HltRoutingBitsWriter::decode()" );
                                                              }};

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_multiple_banks{
      this, " Multiple RoutingBits RawBanks -- don't know which to update. Skipping... ", 20};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unexpected_size{
      this, " RoutingBits RawBanks has unexpected size.. Skipping", 20};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unexpected_two{
      this, " RoutingBits RawBanks: requested to update bank, but first two entries not the same", 20};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unexpected_three{
      this, " RoutingBits RawBanks: requested to update bank, but non-zero third entry", 20};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_unexpected_presence{
      this, " Pre-existing RoutingBits bank in the event..."};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltRoutingBitsWriter )

//=============================================================================
StatusCode HltRoutingBitsWriter::decode() {
  zeroEvaluators();

  // Create the right type of evaluator and build it
  auto build = [this]( const unsigned int bit, const std::string expr ) -> StatusCode {
    if ( expr.empty() ) return StatusCode::SUCCESS;

    std::string title  = boost::str( boost::format( "%02d:%s" ) % bit % expr );
    std::string htitle = boost::str( boost::format( "RoutingBit%02d" ) % bit );

    decltype( m_evaluators )::iterator it;
    bool                               placed{false};
    if ( bit < 32 ) {
      std::tie( it, placed ) = m_evaluators.emplace( bit, ODINEval{m_odin_location} );
    } else if ( bit < 64 ) {
      std::tie( it, placed ) = m_evaluators.emplace( bit, HltEval{m_hlt_location[0]} );
    } else {
      std::tie( it, placed ) = m_evaluators.emplace( bit, HltEval{m_hlt_location[1]} );
    }
    assert( placed && it->first == bit );
    return std::visit( Builder{this, expr, title, htitle}, it->second );
  };

  // Build the routing bits
  for ( const auto& i : m_bits ) {
    if ( i.first > nBits ) return StatusCode::FAILURE;
    auto sc = build( i.first, i.second );
    if ( !sc.isSuccess() ) return sc;
  }

  m_evals_updated     = false;
  m_preambulo_updated = false;
  return StatusCode::SUCCESS;
}

//=============================================================================
void HltRoutingBitsWriter::zeroEvaluators() {
  Deleter deleter;
  for ( auto& entry : m_evaluators ) { std::visit( deleter, entry.second ); }
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HltRoutingBitsWriter::execute() {
  // TODO/FIXME: take lock if update needed -- and move to `initialize` to avoid doing it here more often than needed...
  if ( m_evals_updated || m_preambulo_updated ) decode().orThrow( "HltRoutingBitsWriter: Unable to decode" );

  // Get the fill time, weight and event time
  double     t = 0, weight = 0, evt_time = 0;
  StatusCode sc = times( t, weight, evt_time );
  if ( !sc.isSuccess() ) return sc;

  // Create the evaluator
  Evaluator evaluator{this, t, weight, evt_time};

  // The routing bits
  std::array<unsigned int, 3> bits{0, 0, 0};

  // Evaluate the routing bits
  for ( auto& entry : m_evaluators ) {
    auto result = std::visit( evaluator, entry.second );
    auto bit    = entry.first;
    int  word   = bit / 32;
    if ( result ) bits[word] |= ( 0x01UL << ( bit - 32 * word ) );
  }

  // Get the raw event and update or add the routing bits bank.
  auto rawEvent = m_outputRawEvent.getOrCreate();

  if ( m_updateBank ) {
    const auto& banks = rawEvent->banks( LHCb::RawBank::HltRoutingBits );
    if ( banks.size() != 1 ) {
      ++m_multiple_banks;
      return StatusCode::SUCCESS;
    }
    const LHCb::RawBank* bank = banks[0];
    if ( bank->size() != 3 * sizeof( unsigned int ) ) {
      ++m_unexpected_size;
      return StatusCode::SUCCESS;
    }
    const unsigned int* data = bank->data();
    if ( data[0] != bits[0] || data[1] != bits[1] ) ++m_unexpected_two;
    if ( data[2] != 0 ) ++m_unexpected_three;
    const_cast<unsigned int*>( data )[2] = bits[2];
  } else {
    if ( !rawEvent->banks( LHCb::RawBank::HltRoutingBits ).empty() ) ++m_unexpected_presence;
    rawEvent->addBank( 0, LHCb::RawBank::HltRoutingBits, 0, bits );
  }

  return StatusCode::SUCCESS;
}
