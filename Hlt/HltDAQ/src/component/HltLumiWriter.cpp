/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltLumiSummary.h"
#include "Event/LumiCounters.h"
#include "Event/RawEvent.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltLumiWriter
//
//-----------------------------------------------------------------------------

/** @class HltLumiWriter HltLumiWriter.h
 *  Fills the Raw Buffer banks for the LumiSummary
 */
class HltLumiWriter : public LHCb::Algorithm::Transformer<LHCb::RawBank::View( const LHCb::HltLumiSummary& )> {

  DataObjectWriteHandle<LHCb::RawEvent> m_outputRawEvent{this, "RawEventLocation", LHCb::RawEventLocation::Default};
  mutable Gaudi::Accumulators::AveragingCounter<> m_totDataSize{this, "Average event size / 32-bit words"};

public:
  HltLumiWriter( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{name, pSvcLocator, KeyValue{"InputBank", LHCb::HltLumiSummaryLocation::Default},
                    KeyValue{"OutputBank", "DAQ/RawBanks/HltLumiSummary"}} {}

  //=============================================================================
  // Main execution
  //  Fill the data bank, structure: Key (upper 16 bits) + value
  //=============================================================================
  LHCb::RawBank::View operator()( const LHCb::HltLumiSummary& summary ) const override {

    std::vector<unsigned int> bank;
    bank.reserve( 20 );
    for ( const auto& info : summary.extraInfo() ) {
      bank.push_back( ( std::min( info.first, 0xFFFF ) << 16 ) | ( std::min( info.second, 0xFFFF ) & 0xFFFF ) );
      if ( msgLevel( MSG::VERBOSE ) ) {
        auto word = bank.back();
        verbose() << format( " %8x %11d %11d %11d ", word, word, word >> 16, word & 0xFFFF ) << endmsg;
      }
    }

    auto rawEvent = m_outputRawEvent.getOrCreate();
    if ( !rawEvent->banks( LHCb::RawBank::HltLumiSummary ).empty() ) {
      throw std::runtime_error( "RawEvent already contains HltLumiSummary raw bank" );
    }

    // set source, type, version
    rawEvent->addBank( 0, LHCb::RawBank::HltLumiSummary, 0, bank );

    m_totDataSize += bank.size();

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Bank size: " << format( "%4d ", bank.size() ) << "Total Data bank size " << bank.size() << endmsg;
    }

    if ( msgLevel( MSG::VERBOSE ) ) {
      verbose() << "DATA bank : " << endmsg;
      for ( const auto& [k, w] : LHCb::range::enumerate( bank, 1 ) ) {
        verbose() << format( " %8x %11d   ", w, w );
        if ( k % 4 == 0 ) verbose() << endmsg;
      }
      verbose() << endmsg;
    }

    return rawEvent->banks( LHCb::RawBank::HltLumiSummary );
  }
};

DECLARE_COMPONENT( HltLumiWriter )
