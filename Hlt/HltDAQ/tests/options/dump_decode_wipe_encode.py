from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import ApplicationMgr
from Configurables import RawEventDump, GaudiSequencer
from Configurables import LHCbApp, HltSelReportsWriter, HltDecReportsWriter

NEW_RAW_EVENT_LOC = "/Event/DAQ/NewRawEvent"

import os
if os.path.exists("rewritten.mdf"):
    print("EXISTS")
    os.system("rm rewritten.mdf")
else:
    print("NOTHING!")

LHCbApp().EvtMax = 10
dump = RawEventDump(RawBanks=[], DumpData=True)
decoders = GaudiSequencer("Decode", Members=[])
encoders = GaudiSequencer("Encode", Members=[])

from Configurables import IODataManager
IODataManager().DisablePFNWarning = True

# FIXME: we first add HltSelReportsWriter, which is Functional and
# creates a new RawEvent. HltDecReportsWriter is not yet functional so
# it can modify the new RawEvent in place.

dump.RawBanks.append("HltSelReports")
decoders.Members.append("HltSelReportsDecoder")
encoders.Members.append(
    HltSelReportsWriter(RawEvent=NEW_RAW_EVENT_LOC, SourceID='Hlt1'))

dump.RawBanks.append("HltDecReports")
decoders.Members.append("HltDecReportsDecoder")
encoders.Members.append(
    HltDecReportsWriter(
        OutputRawEventLocation=NEW_RAW_EVENT_LOC, SourceID='Hlt1'))

from Configurables import HltANNSvc
HltANNSvc().Hlt1SelectionID = {'PV3D': 10103, 'ProtoPV3D': 10117}

ApplicationMgr().TopAlg += [dump, decoders, encoders]

#writer
from GaudiConf import IOHelper
from Configurables import LHCb__MDFWriter
IOHelper('MDF', 'MDF').outStream(
    "rewritten.mdf",
    LHCb__MDFWriter("writer", Compress=0, BankLocation=NEW_RAW_EVENT_LOC),
    writeFSR=False)

#inputfile
from PRConfig import TestFileDB
tf = TestFileDB.test_file_db["2015_raw_full"]
tf.run()
from GaudiConf import IOExtension
IOExtension().inputFiles(tf.filenames, clear=True)
