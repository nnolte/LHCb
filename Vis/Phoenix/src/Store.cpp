/*****************************************************************************\
 * * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 * *                                                                             *
 * * This software is distributed under the terms of the GNU General Public      *
 * * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 * *                                                                             *
 * * In applying this licence, CERN does not waive the privileges and immunities *
 * * granted to it by virtue of its status as an Intergovernmental Organization  *
 * * or submit itself to any jurisdiction.                                       *
 * \*****************************************************************************/

// Icludes
#include "Phoenix/Store.h"

void LHCb::Phoenix::Store::storeEventData( nlohmann::json data ) const {

  // thread safe, can be called concurrently from the DumpEvent Algorithms
  auto l = std::scoped_lock{m_event_data_lock};
  m_event_data.push_back( std::move( data ) );
}

nlohmann::json LHCb::Phoenix::Store::toJSON() const {
  auto l = std::scoped_lock{m_event_data_lock};
  return m_event_data;
}

void LHCb::Phoenix::Store::reset() const {
  /// one should not be able to call reset while some other thread is still accessing it.
  auto l = std::scoped_lock{m_event_data_lock};
  m_event_data.clear();
}