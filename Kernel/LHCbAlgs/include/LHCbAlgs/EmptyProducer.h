/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/Producer.h"

template <typename Out>
class EmptyProducer final : public LHCb::Algorithm::Producer<Out()> {
public:
  EmptyProducer( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::Producer<Out()>{name, pSvcLocator, {"Output", ""}} {}

  Out operator()() const override { return {}; }
};
