/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IEventTimeDecoder.h" // Interface

/** Simple implementation of IEventTimeDecoder forwarding the calls to a list of
 *  other implementations.
 *
 *  @author Marco Clemencic
 *  @date   2010-09-23
 */

class TimeDecoderList final : public extends<GaudiTool, IEventTimeDecoder> {

public:
  /// Standard constructor
  using extends::extends;

  /// Initialize the tool
  StatusCode initialize() override;

  // --- implementation of IEventTimeDecoder ---
  /// Loop over all the used decoders and return the first non-zero event time.
  /// @return The time of current event.
  Gaudi::Time getTime() const override;

private:
  /// List of IEventTimeDecoder instance names.
  Gaudi::Property<std::vector<std::string>> m_decoderNames{
      this, "Decoders", {}, "List of IEventTimeDecoder tools to use"};

  /// List of IEventTimeDecoder instances
  std::list<IEventTimeDecoder*> m_decoders;
};

StatusCode TimeDecoderList::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  m_decoders.clear();
  for ( const auto& name : m_decoderNames ) {
    IEventTimeDecoder* p = tool<IEventTimeDecoder>( name, this );
    if ( !p ) return Error( "Cannot retrieve " + name );
    m_decoders.push_back( p );
  }

  return sc;
}

Gaudi::Time TimeDecoderList::getTime() const {
  for ( const auto& d : m_decoders ) {
    if ( auto t = d->getTime(); t.ns() ) { return t; }
  }
  return Gaudi::Time::epoch();
}

DECLARE_COMPONENT( TimeDecoderList )
