/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <Event/ODIN.h>
#include <Gaudi/Accumulators.h>
#include <Gaudi/Property.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/StatusCode.h>
#include <LHCbAlgs/FilterPredicate.h>
#include <algorithm>
#include <atomic>
#include <boost/lexical_cast.hpp>
#include <cctype>
#include <cstring>
#include <fmt/core.h>
#include <iomanip>
#include <iterator>
#include <map>
#include <ostream>
#include <range/v3/action/join.hpp>
#include <range/v3/action/sort.hpp>
#include <range/v3/algorithm/any_of.hpp>
#include <range/v3/numeric/accumulate.hpp>
#include <range/v3/range/conversion.hpp>
#include <range/v3/view/filter.hpp>
#include <range/v3/view/intersperse.hpp>
#include <range/v3/view/join.hpp>
#include <range/v3/view/map.hpp>
#include <range/v3/view/split.hpp>
#include <range/v3/view/transform.hpp>
#include <range/v3/view/trim.hpp>
#include <sstream>
#include <string>
#include <string_view>
#include <utility>
#include <vector>

namespace {
  /// Helper class to model an inclusive range of runs (`std::uint32_t` values).
  class RunRange {
  public:
    /// Construct a range of consisting of a single run.
    constexpr RunRange( std::uint32_t n ) : RunRange( n, n ) {}
    /// Construct a range from run `a` to run `b` inclusive.
    constexpr RunRange( std::uint32_t a, std::uint32_t b ) : m_limits{a, b} {}
    /// Return true if a given run number is included in the range.
    constexpr bool contains( std::uint32_t n ) const { return n >= m_limits.first && n <= m_limits.second; }
    /// Ordering comparison operator between two ranges (needed to sort a list of ranges).
    constexpr bool operator<( const RunRange& other ) const { return m_limits < other.m_limits; }
    /// Equality comparison operator between two ranges (mostly needed in unit tests).
    constexpr bool operator==( const RunRange& other ) const { return m_limits == other.m_limits; }
    /// Return true if two ranges overlap (not empty intersection).
    constexpr bool overlaps( const RunRange& other ) const {
      return std::max( m_limits.first, other.m_limits.first ) <= std::min( m_limits.second, other.m_limits.second );
    }
    /// Return true if the current range directly follows the other (they are adjacent).
    constexpr bool follows( const RunRange& other ) const { return ( other.m_limits.second + 1 ) == m_limits.first; }
    /// Try to extend the current range with another and return true if it is possible.
    ///
    /// Only overlapping or adjacent ranges can be used to overlap each other, for example
    /// ```cpp
    /// RunRange a{2, 5};
    /// a.extend_with(RunRange{4, 7});
    /// assert(a == RunRange{2, 7});
    /// ```
    constexpr bool extend_with( const RunRange& other ) {
      const bool can_extend = overlaps( other ) || follows( other ) || other.follows( *this );
      if ( can_extend ) {
        m_limits.first  = std::min( m_limits.first, other.m_limits.first );
        m_limits.second = std::max( m_limits.second, other.m_limits.second );
      }
      return can_extend;
    }

    /// Allow pretty printing or RunRange instances.
    friend std::ostream& operator<<( std::ostream& str, const RunRange& range ) {
      str << range.m_limits.first;
      if ( range.m_limits.first != range.m_limits.second ) { str << '-' << range.m_limits.second; }
      return str;
    }

  private:
    std::pair<std::uint32_t, std::uint32_t> m_limits;
  };

  /// Class representing a collection of RunRange instances.
  struct RunRangeColl {
    std::vector<RunRange> ranges;

    /// Return true if any of the RunRange instances in the collection
    /// contains the specified run number.
    bool contains( std::uint32_t run ) const {
      auto contains_run = [run]( const RunRange& rng ) { return rng.contains( run ); };
      return ranges::any_of( ranges, contains_run );
    }

    /// Helper to parse a string like `"1, 5-10"` into a `RunRangeColl{{{1}, {1,5}}}`.
    static RunRangeColl from_str( std::string_view desc ) {
      using namespace ranges;
      using boost::bad_lexical_cast;
      using boost::lexical_cast;
      try {
        // helper to parse a single range (e.g. `2-5` into `RunRange{2,5}`)
        auto to_runrange = views::transform( [desc]( auto rng_desc ) {
          auto v = rng_desc | views::split( '-' ) | views::transform( []( auto&& rng ) {
                     return std::string_view( &*rng.begin(), ranges::distance( rng ) );
                   } ) |
                   views::transform( []( std::string_view sv ) {
                     auto rng     = sv | views::trim( []( auto c ) { return std::isblank( c ); } );
                     auto trimmed = std::string_view( &*rng.begin(), ranges::distance( rng ) );
                     return lexical_cast<std::uint32_t>( trimmed );
                   } ) |
                   to<std::vector>;
          if ( v.size() == 1 ) return RunRange{v[0]};
          if ( v.size() == 2 ) return RunRange{v[0], v[1]};
          throw GaudiException( fmt::format( "invalid run ranges specification '{}'", desc ), "RunRangeColl::from_str",
                                StatusCode::FAILURE );
        } );
        // parse all ranges in the input string
        return RunRangeColl{desc | views::split( ',' ) | to_runrange | to<std::vector> |
                            actions::sort( std::less<RunRange>() )};
      } catch ( const bad_lexical_cast& ) {
        throw GaudiException( fmt::format( "invalid run ranges specification '{}': invalid std::uint32_t", desc ),
                              "RunRangeColl::from_str", StatusCode::FAILURE );
      }
    }
    /// Reduce the collection of RunRange instances to a minimal
    /// equivalent set, merging overlapping or contiguous ones.
    void normalize() {
      if ( ranges.empty() ) return;           // nothing to do for empty collections
      sort( begin( ranges ), end( ranges ) ); // we need a sorted list

      // Here we update the vector in place, squashing
      // run ranges while we iterate over it.
      // A first iterator (norm) points to the RR we are
      // updating and that will be part of the output, while
      // a second iterator (orig) will scan the whole vector.
      // For every RR observed by "orig" we try to extend "norm".
      // If that works we look for another "orig" to merge into
      // "norm". If it does not work we make a new "norm" from
      // the current "orig".
      // At the end of the loop we delete all the entries after
      // the current "norm".
      auto orig = ranges.begin();
      auto norm = orig++;
      auto end  = ranges.end();
      while ( orig != end ) {
        if ( !norm->extend_with( *orig ) ) { *( ++norm ) = *orig; }
        ++orig;
      }
      ranges.erase( ++norm, end );
    }
    bool empty() const { return ranges.empty(); }
    bool operator==( const RunRangeColl& rhs ) const { return ranges == rhs.ranges; }

    /// Concatenate collections of RunRange instances (it does not imply normalize()).
    friend RunRangeColl operator+( RunRangeColl lhs, const RunRangeColl& rhs ) {
      std::copy( begin( rhs.ranges ), end( rhs.ranges ), std::back_inserter( lhs.ranges ) );
      return lhs;
    }

    /// Allow pretty printing or RunRangeColl instances.
    friend std::ostream& operator<<( std::ostream& str, const RunRangeColl& coll ) {
      using namespace ranges;
      // convert a value to a string
      auto to_str = []( const auto& x ) {
        std::stringstream s;
        s << x;
        return s.str();
      };
      return str << ( coll.ranges | views::transform( to_str ) | views::intersperse( "," ) | actions::join );
    }
  };

  /// Class representing a selection of Data Quality Flags to take into account.
  ///
  /// The DQ flags are recorded in a map from _reason_ (std::string) to RunRangeColl.
  class DQFlags {
  public:
    /// Alias for the internal storage type.
    using FlagsStore = std::map<std::string, RunRangeColl>;
    DQFlags()        = default;
    template <class T>
    DQFlags( T&& flags ) : m_flags{std::forward<T>( flags )} {
      using namespace ranges;
      m_effectiveBadRuns = accumulate( m_flags | views::values, RunRangeColl{} );
      m_effectiveBadRuns.normalize();
    }

    /// Return true is there is no reason to exclude a run.
    bool is_good( std::uint32_t run ) const { return !m_effectiveBadRuns.contains( run ); }

    /// Return the list of reasons why a run has to be discarded.
    std::vector<std::string> why_bad( std::uint32_t run ) const {
      using namespace ranges;
      return m_flags | views::filter( [run]( const auto& item ) { return item.second.contains( run ); } ) |
             views::keys | to<std::vector> | actions::sort( std::less<std::string>() );
    }

    /// Helper to tell if no run will be excluded.
    bool empty() const { return m_effectiveBadRuns.empty(); }

    bool operator==( const DQFlags& rhs ) const { return m_flags == rhs.m_flags; }

    /// Convert the DQFlag instance to a Python dict representation of the form
    /// `{"a":"1,2","b":"1-5,10-20"}`
    std::string toString() const {
      using namespace ranges;
      // convert a value to a string
      auto to_str = []( const auto& x ) {
        std::stringstream s;
        s << x;
        return s.str();
      };
      // converts items from DQBadRunsMap to string like `"key":"1,3,5-10"`
      auto to_kv_str = [to_str]( const FlagsStore::value_type& item ) {
        std::stringstream s;
        s << std::quoted( item.first ) << ':' << std::quoted( to_str( item.second ) );
        return s.str();
      };
      // put together the final string
      std::stringstream str;
      str << '{' << ( m_flags | views::transform( to_kv_str ) | views::intersperse( "," ) | actions::join ) << '}';
      return str.str();
    }
    /// Decode a Python dict string like `{"a":"1,2","b":"1-5,10-20"}` into a `DQFlags` instance.
    static DQFlags from_str( const std::string& s ) {
      FlagsStore flags;
      // We leverage from existing Gaudi parser for `map<string,string>`.
      std::map<std::string, std::string> tmp;
      Gaudi::Parsers::parse( tmp, s )
          .andThen( [&]() {
            using namespace ranges;
            flags = tmp | views::transform( []( auto&& item ) -> FlagsStore::value_type {
                      return {std::move( item.first ), RunRangeColl::from_str( item.second )};
                    } ) |
                    to<FlagsStore>;
          } )
          .orThrow();
      return DQFlags{std::move( flags )};
    }

  private:
    FlagsStore   m_flags;
    RunRangeColl m_effectiveBadRuns;

    /// Allow pretty printing or DQFlags instances.
    friend std::ostream& operator<<( std::ostream& str, const DQFlags& dqflags ) { return str << dqflags.toString(); }
  };
} // namespace

// Implement custom property type support
namespace Gaudi::Details::Property {
  /// Template specialization to convert `DQFlags` to/from string.
  template <>
  struct StringConverter<DQFlags> {
    std::string toString( const DQFlags& v ) { return v.toString(); }
    DQFlags     fromString( const DQFlags&, const std::string& input ) { return DQFlags::from_str( input ); }
  };
} // namespace Gaudi::Details::Property

/** @class DQFilter
 *  Small algorithm to filter events according to the Data Quality flags specified in the
 *  property `Flags`.
 *
 *  `DQFilter.Flags` property is defined as a mapping string to string, where the key is a
 *  helper to identify the context (for example a subdetector name) and the value is the
 *  set of runs that should be excluded for the given context, expressed as a comma-separated
 *  list of ranges, for example `"1,3,5-7"`.
 *
 *  @author Marco Clemencic
 *  @date   2022-03-17
 */
class DQFilter final : public LHCb::Algorithm::FilterPredicate<bool( const LHCb::ODIN& )> {
public:
  DQFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : FilterPredicate{name, pSvcLocator, {"ODINLocation", LHCb::ODINLocation::Default}} {}

  bool operator()( const LHCb::ODIN& ) const override;

private:
  Gaudi::Property<DQFlags> m_dqflags{
      this,
      "Flags",
      {},
      "map 'subdetector -> bad runs' (where `bad runs` is formatted as a comma-separated "
      "list of run numbers or ranges, e.g. {'subdet': '1,3,6-9'})",
      // specify semantic for GaudiConfig2
      "map<string,string>"};

  mutable Gaudi::Accumulators::BinomialCounter<> m_accepted{this, "accepted"};

  /// Helper class to pack/unpack the result of the latest check.
  struct RunCheckType {
    RunCheckType() = default;
    RunCheckType( std::uint64_t state ) { std::memcpy( this, &state, sizeof( RunCheckType ) ); }
    operator std::uint64_t() const {
      std::uint64_t tmp;
      std::memcpy( &tmp, this, sizeof( RunCheckType ) );
      return tmp;
    }
    // IMPORTANT: the order of these two field has been chosen to produce better optimized code
    //            see https://godbolt.org/z/Mnz7WTecc
    bool          good{false};
    std::uint32_t run{0};
  };
  // the RunCheckType trick works only if it fits in 64 bits.
  static_assert( sizeof( RunCheckType ) <= sizeof( std::uint64_t ) );
  // internal storage for the result of the latest check.
  mutable std::atomic<std::uint64_t> m_lastRunCheckState{0};
};

bool DQFilter::operator()( const LHCb::ODIN& odin ) const {
  // get the latest recorded state
  RunCheckType state{m_lastRunCheckState.load()};
  // if different from current run, we check the list of bad runs, otherwise we use the previous result
  if ( const auto run = odin.runNumber(); run != state.run ) {
    const auto good = m_dqflags.value().is_good( run );
    state.run       = run;
    state.good      = good;
    m_lastRunCheckState.store( state );
    if ( msgLevel() <= MSG::DEBUG && !good ) {
      debug() << "run " << run << " ignored because of " << m_dqflags.value().why_bad( run ) << endmsg;
    }
  }
  if ( msgLevel() <= MSG::VERBOSE ) {
    verbose() << "Filter event: " << ( ( state.good ) ? "good" : "bad" ) << " event" << endmsg;
  }
  m_accepted += state.good;
  return state.good;
}

DECLARE_COMPONENT( DQFilter )

// From here on, we have unit tests.
#ifdef UNIT_TESTS

#  define BOOST_TEST_MODULE test_DQFilter
#  define BOOST_TEST_DYN_LINK
#  include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE( test_RunRange ) {
  RunRange rng1{100};
  RunRange rng2{110, 200};

  BOOST_TEST( !rng1.contains( 99 ) );
  BOOST_TEST( !rng1.contains( 101 ) );
  BOOST_TEST( rng1.contains( 100 ) );

  BOOST_TEST( !rng2.contains( 100 ) );
  BOOST_TEST( rng2.contains( 110 ) );
  BOOST_TEST( rng2.contains( 150 ) );
  BOOST_TEST( rng2.contains( 200 ) );
  BOOST_TEST( !rng2.contains( 210 ) );

  BOOST_TEST( rng1 < rng2 );
  BOOST_TEST( rng1 == rng1 );
  BOOST_TEST( rng2 == rng2 );

  BOOST_TEST( !rng1.overlaps( rng2 ) );
  BOOST_TEST( !rng2.overlaps( rng1 ) );
  BOOST_TEST( rng1.overlaps( rng1 ) );
  BOOST_TEST( rng2.overlaps( rng2 ) );
}

BOOST_AUTO_TEST_CASE( test_RunRange_parser ) {
  {
    RunRangeColl bad_runs = RunRangeColl::from_str( "  1, 2 ,3,4  " );
    RunRangeColl simple_list{{{1}, {2}, {3}, {4}}};
    BOOST_TEST( bad_runs == simple_list );
  }
  {
    RunRangeColl bad_runs = RunRangeColl::from_str( "  1, 5-10 ," );
    RunRangeColl mixed_list{{{1}, {5, 10}}};
    BOOST_TEST( bad_runs == mixed_list );
  }
  {
    RunRangeColl bad_runs = RunRangeColl::from_str( "2,4,3,1" );
    RunRangeColl sorted{{{1}, {2}, {3}, {4}}};
    BOOST_TEST( bad_runs == sorted );
  }
}

BOOST_AUTO_TEST_CASE( test_Flags_property ) {
  Gaudi::Property<DQFlags> property;

  BOOST_TEST_REQUIRE( property.fromString( "{}" ) );
  BOOST_TEST( property.empty() );
  BOOST_TEST( property.toString() == "{}" );

  BOOST_TEST_REQUIRE( property.fromString( "{ 'a': '1' } " ) );
  {
    DQFlags::FlagsStore expected{{"a", {{{1}}}}};
    BOOST_TEST( property.value() == DQFlags{expected} );
  }
  BOOST_TEST( property.toString() == "{\"a\":\"1\"}" );

  BOOST_TEST_REQUIRE( property.fromString( "{\"a\":\"1,2\",\"b\":\"1-5,10-20,30-33\"}" ) );
  {
    DQFlags::FlagsStore expected{{"a", {{{1}, {2}}}}, {"b", {{{1, 5}, {10, 20}, {30, 33}}}}};
    BOOST_TEST( property.value() == DQFlags{expected} );
  }
  BOOST_TEST( property.toString() == "{\"a\":\"1,2\",\"b\":\"1-5,10-20,30-33\"}" );

  std::stringstream s;
  property.toStream( s );
  BOOST_TEST( s.str() == "{\"a\":\"1,2\",\"b\":\"1-5,10-20,30-33\"}" );
}

BOOST_AUTO_TEST_CASE( test_normalize ) {
  auto check = []( std::string_view before, std::string_view after ) {
    auto run_ranges = RunRangeColl::from_str( before );
    run_ranges.normalize();
    BOOST_TEST( run_ranges.ranges == RunRangeColl::from_str( after ).ranges,
                "before: " << before << " after: " << ranges::views::all( run_ranges.ranges )
                           << " expected: " << after );
  };

  check( "  1, 1-5 , 2, 10-20  ", "1-5,10-20" );
  check( "5,1-3,7-10,3-5", "1-5,7-10" );
  check( "1,2,3,4", "1-4" );
}

BOOST_AUTO_TEST_CASE( test_parsing_errors ) {
  auto message_contains = []( std::string_view msg ) {
    return [msg]( const auto& exc ) { return std::string_view( exc.what() ).find( msg ) != std::string_view::npos; };
  };

  BOOST_CHECK_EXCEPTION( RunRangeColl::from_str( "  1 2  " ), GaudiException,
                         message_contains( "invalid run ranges specification '  1 2  '" ) );
  BOOST_CHECK_EXCEPTION( RunRangeColl::from_str( "1,a,3" ), GaudiException,
                         message_contains( "invalid run ranges specification '1,a,3'" ) );
  BOOST_CHECK_EXCEPTION( RunRangeColl::from_str( "1-2-3" ), GaudiException,
                         message_contains( "invalid run ranges specification '1-2-3'" ) );
  BOOST_CHECK_EXCEPTION( RunRangeColl::from_str( "1+3" ), GaudiException,
                         message_contains( "invalid run ranges specification '1+3'" ) );
  // it does not fit uint32_t bits
  BOOST_CHECK_EXCEPTION( RunRangeColl::from_str( "4294967296" ), GaudiException,
                         message_contains( "invalid run ranges specification" ) );
  // it does not fit 64 bits
  BOOST_CHECK_EXCEPTION( RunRangeColl::from_str( "18446744073709551616" ), GaudiException,
                         message_contains( "invalid run ranges specification" ) );
}

BOOST_AUTO_TEST_CASE( test_property_parsing_errors ) {
  auto message_contains = []( std::string_view msg ) {
    return [msg]( const auto& exc ) { return std::string_view( exc.what() ).find( msg ) != std::string_view::npos; };
  };

  Gaudi::Property<DQFlags> property{"DQFlags", {}};

  for ( std::string_view s : {"abc", "{", "{]", "{'det': 'bad'}", "{'det': 123}"} ) {
    BOOST_CHECK_EXCEPTION( property.fromString( std::string{s} ).ignore(), GaudiException,
                           message_contains( fmt::format( "Cannot convert '{}' for property 'DQFlags'", s ) ) );
  }
}

BOOST_AUTO_TEST_CASE( test_good_run ) {
  auto dq       = DQFlags::from_str( "{'DetA': '1-10,40', 'DetB': '10,50-100'}" );
  using reasons = std::vector<std::string>;

  BOOST_TEST( dq.is_good( 20 ) );
  BOOST_TEST( dq.is_good( 45 ) );
  BOOST_TEST( dq.is_good( 1000 ) );

  BOOST_TEST( !dq.is_good( 5 ) );
  BOOST_TEST( dq.why_bad( 5 ) == reasons{{"DetA"}} );
  BOOST_TEST( !dq.is_good( 10 ) );
  {
    auto expected = reasons{{"DetA"}, {"DetB"}};
    BOOST_TEST( dq.why_bad( 10 ) == expected );
  }
  BOOST_TEST( !dq.is_good( 80 ) );
  BOOST_TEST( dq.why_bad( 80 ) == reasons{{"DetB"}} );

  BOOST_TEST( dq.why_bad( 200 ) == reasons{} );
}
#endif
