/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ====================================================================
//  AddressKillerAlg.cpp
// --------------------------------------------------------------------
//
// Author    : Markus Frank
//
// ====================================================================
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IOpaqueAddress.h"
#include "GaudiKernel/IRegistry.h"

/**@class AddressKillerAlg
 *
 * Small algorithm to make the entire datastore anonymous.
 * All persistent info of all addresses is entirely removed.
 *
 * @author:  M.Frank
 * @version: 1.0
 */
class AddressKillerAlg : public Gaudi::Algorithm {

  /// Reference to data provider service
  ServiceHandle<IDataProviderSvc> m_dataSvc{this, "DataSvc", "EventDataSvc"};

public:
  /// Standard algorithm constructor
  using Algorithm::Algorithm;

  StatusCode initialize() override {
    return Algorithm::initialize().andThen( [&] { return m_dataSvc.retrieve(); } );
  }

  StatusCode finalize() override {
    return m_dataSvc.release().andThen( [&] { return Algorithm::finalize(); } );
  }

  StatusCode execute( const EventContext& ) const override {
    SmartIF<IDataManagerSvc> mgr( m_dataSvc.get() );
    return mgr->traverseTree( [&, id = mgr->rootName()]( IRegistry* reg, int depth ) {
      if ( const IOpaqueAddress* addr = reg->address(); addr ) {
        // NOT for MDF top level address!!!!
        if ( !( addr->svcType() == RAWDATA_StorageType && reg->identifier() == id ) ) {
          debug() << "Remove store address \"" << reg->identifier() << "\"." << endmsg;
          reg->setAddress( nullptr );
        }
      }
      return depth < 9999999; // infinite recursion protection..
    } );
  }
};

DECLARE_COMPONENT( AddressKillerAlg )
