/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_MODULE SIMDWrapperTest
#include "LHCbMath/SIMDWrapper.h"
#include <boost/test/included/unit_test.hpp>

using simd = SIMDWrapper::best::types;

BOOST_AUTO_TEST_CASE( test_exp_simd ) {
  for ( int x = -88; x <= 88; x++ ) {
    auto y     = SIMDWrapper::to_array( exp( simd::float_v{x} ) )[0];
    auto truth = std::exp( x );
    auto err   = std::abs( truth - y );
    BOOST_CHECK( ( err / truth ) < 0.01 );
    // std::cout << "exp(" << x << ") = "<< y << " err: " << (err * 100.f / truth) << std::endl;
  }
}