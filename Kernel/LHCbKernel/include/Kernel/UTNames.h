/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/UT/ChannelID.h"
#include "boost/container/flat_map.hpp"
#include <algorithm>
#include <ostream>
#include <vector>

namespace LHCb {

  // Forward declarations

  /** @class UTNames UTNames.h
   * @author Jianchun Wang
   *
   */

  class UTNames final {
  public:
    /// Station names
    enum Station { UnknownStation = 0, UTa = 1, UTb = 2 };
    /// Region names
    enum detRegion { UnknownRegion = 0, RegionC = 1, RegionB = 2, RegionA = 3 };

    /// FIXME: No constructor needed, as all methods are static -- so change this t0 =delete
    UTNames() = default;

    /// conversion of string to enum for type Station
    static LHCb::UTNames::Station StationToType( std::string_view aName );

    /// conversion to string for enum type Station
    static const std::string& StationToString( int aEnum );
    /// conversion of string to enum for type detRegion
    static LHCb::UTNames::detRegion detRegionToType( std::string_view aName );

    /// conversion to string for enum type detRegion
    static const std::string& detRegionToString( int aEnum );

    /// detector name
    static std::string detector() { return "UT"; }

    /// station string from id
    static std::string StationToString( const LHCb::Detector::UT::ChannelID& chan ) {
      return StationToString( chan.station() );
    }

    /// channel to string from id
    static std::string channelToString( const LHCb::Detector::UT::ChannelID& chan );

    /// unique box string from id
    static std::string UniqueRegionToString( LHCb::Detector::UT::ChannelID chan ) {
      return UniqueLayerToString( chan ) + detRegionToString( chan.detRegion() );
    }

    /// unique layer string from layer and station
    static std::string UniqueLayerToString( unsigned int layer, unsigned int station );

    /// unique layer string from id
    static std::string UniqueLayerToString( const LHCb::Detector::UT::ChannelID& chan ) {
      return UniqueLayerToString( chan.layer(), chan.station() );
    }

    /// unique sector string from id
    static std::string UniqueSectorToString( const LHCb::Detector::UT::ChannelID& chan );

    /// sector string from id
    static std::string SectorToString( const LHCb::Detector::UT::ChannelID& chan );

    /// vector of string names for all stations
    static std::vector<std::string> allStations();

    /// vector of string names for all regions
    static std::vector<std::string> allDetRegions();

    /// vector of string names for all layers
    static std::vector<std::string> allLayers();

    /// vector of string names for stations
    static std::vector<std::string> stations();

    /// vector of string names for regions
    static std::vector<std::string> detRegions();

    /// vector of string names for layers
    static const std::vector<std::string>& layers();

    /// convert string to channel
    static Detector::UT::ChannelID stringToChannel( std::string_view name );

  private:
    /// find type in vector
    static unsigned int findStationType( std::string_view testName, const std::vector<std::string>& names );

    /// find type in vector
    static unsigned int findRegionType( std::string_view testName, const std::vector<std::string>& names );

    /// convert string to int
    static unsigned int toInt( std::string_view str );

    static const auto& s_StationTypMap() {
      static const boost::container::flat_map<std::string, Station, std::less<>> m = {
          {"UnknownStation", UnknownStation}, {"UTa", UTa}, {"UTb", UTb}};
      return m;
    }
    static const auto& s_detRegionTypMap() {
      static const boost::container::flat_map<std::string, detRegion, std::less<>> m = {
          {"UnknownRegion", UnknownRegion}, {"RegionC", RegionC}, {"RegionB", RegionB}, {"RegionA", RegionA}};
      return m;
    }

  }; // class UTNames

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::Station e ) {
    switch ( e ) {
    case LHCb::UTNames::UnknownStation:
      return s << "UnknownStation";
    case LHCb::UTNames::UTa:
      return s << "UTa";
    case LHCb::UTNames::UTb:
      return s << "UTb";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::Station";
    }
  }

  inline std::ostream& operator<<( std::ostream& s, LHCb::UTNames::detRegion e ) {
    switch ( e ) {
    case LHCb::UTNames::UnknownRegion:
      return s << "UnknownRegion";
    case LHCb::UTNames::RegionC:
      return s << "RegionC";
    case LHCb::UTNames::RegionB:
      return s << "RegionB";
    case LHCb::UTNames::RegionA:
      return s << "RegionA";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::UTNames::detRegion";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
