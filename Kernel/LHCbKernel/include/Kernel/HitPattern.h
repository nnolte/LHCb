/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Kernel/LHCbID.h"
#include "Kernel/STLExtensions.h"
#include <bitset>
#include <ostream>
#include <vector>

// Forward declarations

namespace LHCb {

  // Forward declarations

  /** @class HitPattern HitPattern.h
   *
   * pattern of hits used on tracks
   *
   * @author Stephanie Hansmann-Menzemer and Wouter Hulsbergen (2009)
   *
   */

  class HitPattern {
  public:
    /// VP layer pattern
    typedef std::bitset<26> VPPattern;
    /// UT layer pattern
    typedef std::bitset<4> UTPattern;
    /// FT-layer hit pattern
    typedef std::bitset<12> FTPattern;
    /// Muon layer pattern
    typedef std::bitset<5> MuonPattern;

    /// number of detector layers/regions
    enum Number { NumVelo = 26, NumUT = 4, NumFT = 12, NumMuon = 5 };

    /// Constructor from LHCbIDs
    HitPattern( span<const LHCbID> ids );

    /// Default Constructor
    HitPattern() = default;

    /// Print this HitPattern
    std::ostream& fillStream( std::ostream& s ) const;

    /// velo layer pattern
    auto velo() const { return m_veloA | m_veloC; }

    /// velo layer pattern A-side
    auto veloA() const { return m_veloA; }

    /// velo layer pattern C-side
    auto veloC() const { return m_veloC; }

    /// ut layer pattern
    auto ut() const { return m_ut; }

    /// ft layer pattern
    auto ft() const { return m_ft; }

    /// ft layer pattern
    auto muon() const { return m_muon; }

    /// number of velo A-side layers
    auto numVeloA() const { return m_veloA.count(); }

    /// number of velo C-side layers
    auto numVeloC() const { return m_veloC.count(); }

    /// number Velo layers
    auto numVelo() const { return numVeloA() + numVeloC(); }

    /// number of FT layers
    auto numFT() const { return m_ft.count(); }

    /// number of UT layers
    auto numUT() const { return m_ut.count(); }

    /// number of Muon layers
    auto numMuon() const { return m_muon.count(); }

    /// number of velo stations with hit on both A and C side
    auto numVeloStationsOverlap() const { return ( m_veloA & m_veloC ).count(); }

    /// number of holes in velo (layer) pattern
    size_t numVeloHoles() const;

    /// number of holes in T (layer) pattern
    size_t numFTHoles() const;

    /// number Velo layers (kept for backward compatibility)
    auto numVeloStations() const { return numVelo(); }

    /// number of T-station layers (kept for backward compatibility)
    auto numTLayers() const { return numFT(); }

    /// number of T-station holes (kept for backward compatibility)
    auto numTHoles() const { return numFTHoles(); }

    friend std::ostream& operator<<( std::ostream& str, const HitPattern& obj ) { return obj.fillStream( str ); }

  private:
    VPPattern   m_veloA; ///< VP hit pattern on detector A side
    VPPattern   m_veloC; ///< VP hit pattern on detector C side
    FTPattern   m_ft;    ///< FT hit pattern
    UTPattern   m_ut;    ///< ut bit pattern
    MuonPattern m_muon;  ///< muon bit pattern

  }; // class HitPattern

  inline std::ostream& operator<<( std::ostream& s, LHCb::HitPattern::Number e ) {
    switch ( e ) {
    case LHCb::HitPattern::NumVelo:
      return s << "NumVelo";
    case LHCb::HitPattern::NumUT:
      return s << "NumUT";
    case LHCb::HitPattern::NumFT:
      return s << "NumFT";
    case LHCb::HitPattern::NumMuon:
      return s << "NumMuon";
    default:
      return s << "ERROR wrong value " << int( e ) << " for enum LHCb::HitPattern::Number";
    }
  }

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
