/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/HitPattern.h"
#include "Detector/FT/FTChannelID.h"

namespace LHCb {

  HitPattern::HitPattern( span<const LHCbID> ids ) {

    for ( const auto& id : ids ) {
      switch ( id.detectorType() ) {
      case LHCbID::channelIDtype::VP: {
        // FIXME: WH: the problem is that we have some
        // interesting logic to cpmpute velo holes, which
        // doesn't work for VP. this is not very neat, but
        // it is a solution for now. I may also have swapped
        // A and C side.
        LHCb::Detector::VPChannelID vpid    = id.vpID();
        unsigned int                station = vpid.station();
        switch ( vpid.sidepos() ) {
        case 0:
          m_veloA.set( station );
          break;
        case 1:
          m_veloC.set( station );
          break;
        }
      } break;
      case LHCbID::channelIDtype::FT: {
        // FIXME: would like to use ftid.uniqueLayer(), but that numbers the layers from 4 - 15 !
        auto         ftid        = id.ftID();
        unsigned int uniquelayer = ( to_unsigned( ftid.station() ) - 1 ) * 4 + to_unsigned( ftid.layer() );
        m_ft.set( uniquelayer );
      } break;
      case LHCbID::channelIDtype::Muon:
        m_muon.set( id.muonID().station() );
        break;
      case LHCbID::channelIDtype::UT: {
        // FIXME: would like to use utid.uniqueLayer(), but that currently returns {5,6,9,10}.
        const auto   utid        = id.utID();
        unsigned int uniquelayer = ( utid.station() - 1 ) * 2 + ( utid.layer() - 1 );
        m_ut.set( uniquelayer );
      } break;
      default:
        throw std::invalid_argument{"unsupported detectortype in HitPattern"};
      }
    }
  }

  std::ostream& HitPattern::fillStream( std::ostream& s ) const {
    s << "veloA:    " << m_veloA << std::endl
      << "veloC:    " << m_veloC << std::endl
      << "UT:       " << m_ut << std::endl
      << "FT:       " << m_ft << std::endl
      << "Muon:     " << m_muon << std::endl;
    return s;
  }

  namespace {
    /// Count the total number of missing bits in between the first and last active bits
    template <size_t N>
    int numholes( std::bitset<N> layers ) {
      bool   active                = false;
      size_t nummissingsincelastup = 0;
      size_t rc{0};
      for ( size_t n = 0; n < N; ++n ) {
        auto found = layers.test( n );
        if ( found ) {
          rc += nummissingsincelastup;
          nummissingsincelastup = 0;
          active                = true;
        } else if ( active )
          nummissingsincelastup += 1;
      }
      return rc;
    }
  } // namespace

  size_t HitPattern::numVeloHoles() const { return numholes( veloA() ) + numholes( veloC() ); }

  size_t HitPattern::numFTHoles() const { return numholes( ft() ); }

} // namespace LHCb
