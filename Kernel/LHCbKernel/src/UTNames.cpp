/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/UTNames.h"
#include "Detector/UT/ChannelID.h"
#include <charconv>
#include <iostream>
#include <string>

std::string LHCb::UTNames::UniqueSectorToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueRegionToString( chan ) + SectorToString( chan );
}

std::string LHCb::UTNames::SectorToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return "Sector" + std::to_string( chan.sector() );
  ;
}

std::vector<std::string> LHCb::UTNames::stations() {
  std::vector<std::string> stations;
  stations.reserve( s_StationTypMap().size() );
  for ( const auto& i : s_StationTypMap() ) {
    if ( i.first != "Unknownstation" ) stations.emplace_back( i.first );
  }
  return stations;
}

std::vector<std::string> LHCb::UTNames::detRegions() {
  std::vector<std::string> regions;
  regions.reserve( s_detRegionTypMap().size() );
  for ( const auto& i : s_detRegionTypMap() ) {
    if ( i.first != "UnknownRegion" ) regions.emplace_back( i.first );
  }
  return regions;
}

const std::vector<std::string>& LHCb::UTNames::layers() {
  // messy
  static const std::vector<std::string> layers = {"X", "U", "V"};
  return layers;
}

std::vector<std::string> LHCb::UTNames::allStations() { return stations(); }

std::vector<std::string> LHCb::UTNames::allDetRegions() {
  auto                     l = allLayers();
  auto                     r = detRegions();
  std::vector<std::string> tVector;
  tVector.reserve( r.size() * l.size() );
  for ( const auto& iL : l ) {
    for ( const auto& iR : r ) { tVector.emplace_back( iL + iR ); }
  }
  return tVector;
}

std::vector<std::string> LHCb::UTNames::allLayers() {

  auto stationVec = stations();
  return {stationVec[0] + "X", stationVec[0] + "U", stationVec[1] + "V", stationVec[1] + "X"};
}

std::string LHCb::UTNames::UniqueLayerToString( unsigned int layer, unsigned int station ) {
  std::string res = "UnknownLayer";
  if ( station == 1 ) {
    if ( layer == 1 ) {
      res = "X";
    } else if ( layer == 2 ) {
      res = "U";
    }
  } else if ( station == 2 ) {
    if ( layer == 1 ) {
      res = "V";
    } else if ( layer == 2 ) {
      res = "X";
    }
  } else {
    // nothing
  }
  return StationToString( station ) + res;
}

std::string LHCb::UTNames::channelToString( const LHCb::Detector::UT::ChannelID& chan ) {
  return UniqueSectorToString( chan ) + "Strip" + std::to_string( chan.strip() );
}

LHCb::Detector::UT::ChannelID LHCb::UTNames::stringToChannel( std::string_view name ) {

  // convert string to channel

  // get the station, layer and box
  const std::vector<std::string> thestations = stations();
  const unsigned int             station     = findStationType( name, thestations );

  //  const std::vector<std::string> thelayers = layers();
  unsigned int layer = 0;
  // station 1, layers U and X
  if ( station == 1 ) {
    if ( name.find( "X" ) != std::string_view::npos ) {
      layer = 1;
    } else if ( name.find( "U" ) != std::string_view::npos ) {
      layer = 2;
    } else {
      return Detector::UT::ChannelID( LHCb::Detector::UT::ChannelID::detType::typeUT, station, 0u, 0u, 0u, 0u );
    }
  }

  if ( station == 2 ) {
    if ( name.find( "X" ) != std::string_view::npos ) {
      layer = 2;
    } else if ( name.find( "V" ) != std::string_view::npos ) {
      layer = 1;
    } else {
      return Detector::UT::ChannelID( LHCb::Detector::UT::ChannelID::detType::typeUT, station, 0u, 0u, 0u, 0u );
    }
  }

  const std::vector<std::string> theregions = detRegions();
  const unsigned int             region     = findRegionType( name, theregions );

  // sector and strip is different
  unsigned int strip;
  unsigned int sector;
  auto         startSector = name.find( "Sector" );
  auto         startStrip  = name.find( "Strip" );

  if ( startSector == std::string_view::npos ) {
    sector = 0;
    strip  = 0;
  } else if ( startStrip == std::string_view::npos ) {
    strip  = 0;
    sector = toInt( name.substr( startSector + 6 ) );
    if ( sector == 0 ) return Detector::UT::ChannelID(); // invalid sector
  } else {
    strip  = toInt( name.substr( startStrip + 5 ) );
    sector = toInt( name.substr( startSector + 6, startStrip - startSector - 6 ) );
  }

  return LHCb::Detector::UT::ChannelID( LHCb::Detector::UT::ChannelID::detType::typeUT, station, layer, region, sector,
                                        strip );
}

unsigned int LHCb::UTNames::findStationType( std::string_view testname, const std::vector<std::string>& names ) {
  auto n = std::find_if( std::begin( names ), std::end( names ),
                         [&]( std::string_view s ) { return testname.find( s ) != std::string_view::npos; } );
  return n != std::end( names ) ? (unsigned int)StationToType( *n ) : 0;
}

unsigned int LHCb::UTNames::findRegionType( std::string_view testname, const std::vector<std::string>& names ) {
  auto n = std::find_if( std::begin( names ), std::end( names ),
                         [&]( std::string_view s ) { return testname.find( s ) != std::string_view::npos; } );
  return n != names.end() ? (unsigned int)detRegionToType( *n ) : 0;
}

unsigned int LHCb::UTNames::toInt( std::string_view str ) {
  unsigned int outValue = 0;
  auto [_, ec]          = std::from_chars( str.begin(), str.end(), outValue );
  if ( ec != std::errc{} ) {
    std::cerr << "ERROR " << make_error_code( ec ).message() << "** " << str << " **" << std::endl;
  }
  return outValue;
}

LHCb::UTNames::Station LHCb::UTNames::StationToType( std::string_view aName ) {
  auto iter = s_StationTypMap().find( aName );
  return iter != s_StationTypMap().end() ? iter->second : UnknownStation;
}

const std::string& LHCb::UTNames::StationToString( int aEnum ) {
  static const std::string s_UnknownStation = "UnknownStation";
  auto                     iter             = std::find_if( s_StationTypMap().begin(), s_StationTypMap().end(),
                            [&]( const auto& i ) { return i.second == aEnum; } );
  return iter != s_StationTypMap().end() ? iter->first : s_UnknownStation;
}

LHCb::UTNames::detRegion LHCb::UTNames::detRegionToType( std::string_view aName ) {
  auto iter = s_detRegionTypMap().find( aName );
  return iter != s_detRegionTypMap().end() ? iter->second : UnknownRegion;
}

const std::string& LHCb::UTNames::detRegionToString( int aEnum ) {
  static const std::string s_UnknownRegion = "UnknownRegion";
  auto                     iter            = std::find_if( s_detRegionTypMap().begin(), s_detRegionTypMap().end(),
                            [&]( const std::pair<const std::string, detRegion>& i ) { return i.second == aEnum; } );
  return iter != s_detRegionTypMap().end() ? iter->first : s_UnknownRegion;
}
