/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Kernel/PlumeChannelID.h"
#include <fmt/format.h>

namespace LHCb::Detector::Plume {

  std::ostream& ChannelID::fillStream( std::ostream& os ) const { return os << toString(); }

  std::string ChannelID::toString() const {
    std::string type;
    switch ( channelType() ) {
    case ChannelType::LUMI:
      type = "LUMI";
      break;
    case ChannelType::PIN:
      type = "PIN";
      break;
    case ChannelType::MON:
      type = "MON";
      break;
    case ChannelType::TIME:
      type = "TIME";
      break;
    case ChannelType::TIME_T:
      type = "TIME";
      break;
    default:
      type = "ERR";
    }

    if ( channelType() == ChannelType::TIME ) {
      return fmt::format( "{}_{:02}-{:02}", type, channelID(), channelSubID() );
    } else {
      return fmt::format( "{}_{:02}", type, channelID() );
    }
  }

} // namespace LHCb::Detector::Plume