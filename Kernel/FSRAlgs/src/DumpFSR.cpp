/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "FSRAlgs/IFSRNavigator.h"

#include "Event/EventCountFSR.h"
#include "Event/LumiFSR.h"
#include "Event/ODIN.h"
#include "Event/RawEvent.h"
#include "Event/TimeSpanFSR.h"

#include "Gaudi/Accumulators.h"
#include "Gaudi/Algorithm.h"
#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/IOpaqueAddress.h"

#include <cstdlib>
#include <fstream>
#include <iostream>
#include <mutex>
#include <string>

/**
 *  @author Jaap Panman
 *  @date   2009-02-27
 */
class DumpFSR : public extends<FixTESPath<Gaudi::Algorithm>, IIncidentListener> {
public:
  using extends::extends;

  StatusCode initialize() override;
  StatusCode execute( const EventContext& ) const override;
  StatusCode finalize() override;

  // IIncindentListener interface
  void handle( const Incident& ) override;

private:
  template <typename FSRType, typename CounterType, typename StreamType>
  void dump_FSR( std::string const& fsrName, std::string const& shortName, CounterType& errorCount, bool writeLog,
                 StreamType& stream, bool low = false ) const;
  template <typename StreamType>
  void dump_file( bool writelog, StreamType& stream ) const; ///< print the FSRs of one input file
  void write_file();                                         ///< write the FSRs as ascii to a file

  mutable std::once_flag m_eventDumpFlag;

  mutable Gaudi::Accumulators::Counter<> m_count_files{this, "Number of files seen"};   /// number of files read
  mutable Gaudi::Accumulators::Counter<> m_count_events{this, "Number of events seen"}; /// number of events read
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_missingLumi{this, "A lumi record was not found", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_missingEventCount{this, "An EventCount Record was not found",
                                                                            10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_missingTimeSpan{this, "A TimeSpan Record was not found", 10};

  Gaudi::Property<std::string> m_rawEventLocation{this, "RawEventLocation", LHCb::RawEventLocation::Default,
                                                  "Location where we get the RawEvent"};
  Gaudi::Property<std::string> m_FileRecordName{this, "FileRecordLocation", "/FileRecords", "location of FileRecords"};
  Gaudi::Property<std::string> m_FSRName{this, "FSRName", "/LumiFSR", "specific tag of summary data in FSR"};
  Gaudi::Property<std::string> m_LowFSRName{this, "LowFSRName", "/LumiLowFSR",
                                            "specific tag of low lumi summary data in FSR"};
  Gaudi::Property<std::string> m_EventCountFSRName{this, "EventCountFSRName", "/EventCountFSR",
                                                   "specific tag of event summary data in FSR"};
  Gaudi::Property<std::string> m_TimeSpanFSRName{this, "TimeSpanFSRName", "/TimeSpanFSR",
                                                 "specific tag of event summary data in FSR"};
  Gaudi::Property<std::string> m_ascii_fname{this, "AsciiFileName", "", "name of ascii file to write FSR data to"};
  Gaudi::Property<std::string> m_dumprequests{this, "DumpRequests", "F", "job: E:event F:fini, files: B:begin C:close"};

private:
  mutable ToolHandle<IFSRNavigator> m_navigatorTool{this, "FSRNavigator", "FSRNavigator", "tool to navigate FSR"};
  mutable std::mutex                m_navigatorToolLock;
  ServiceHandle<IDataProviderSvc>   m_fileRecordSvc{this, "FileRecordDataSvc", "FileRecordDataSvc",
                                                  "Reference to run records data service"};
  ServiceHandle<IIncidentSvc>       m_incSvc{this, "IncidentSvc", "IncidentSvc", "the incident service"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DumpFSR )

StatusCode DumpFSR::initialize() {
  return FixTESPath::initialize().andThen( [&]() {
  // check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
    m_incSvc->addListener( this, IncidentType::BeginInputFile );
    m_incSvc->addListener( this, IncidentType::EndInputFile );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "registered with incSvc" << endmsg;
      // if not then the counting is not reliable
#else
    warn() << "cannot register with incSvc" << endmsg;
#endif // GAUDI_FILE_INCIDENTS
    return StatusCode::SUCCESS;
  } );
}

StatusCode DumpFSR::execute( const EventContext& ) const {
  ++m_count_events;
  std::call_once( m_eventDumpFlag, [&]() {
    if ( m_dumprequests.value().find( "E" ) != std::string::npos ) dump_file( true, info() );
  } );
  return StatusCode::SUCCESS;
}

StatusCode DumpFSR::finalize() {
  if ( m_dumprequests.value().find( "F" ) != std::string::npos ) dump_file( true, info() );
  if ( m_ascii_fname.value() != "" ) write_file();
  return FixTESPath::finalize(); // must be called after all other actions
}

// IIncindentListener interface
void DumpFSR::handle( const Incident& incident ) {
  // check extended file incidents are defined
#ifdef GAUDI_FILE_INCIDENTS
  if ( incident.type() == IncidentType::BeginInputFile ) {
    ++m_count_files;
    if ( m_dumprequests.value().find( "B" ) != std::string::npos ) dump_file( true, info() );
  }
  if ( incident.type() == IncidentType::EndInputFile ) {
    if ( m_dumprequests.value().find( "C" ) != std::string::npos ) dump_file( true, info() );
  }
#endif
}

// Logging methods for different types, used in dump_FSR and write_file
namespace {
  // support of endmsg in standard streams, only locally to make the code generic
  template <typename C, typename T>
  std::basic_ostream<C, T>& operator<<( std::basic_ostream<C, T>& stream, MsgStream& (*)(MsgStream&)) {
    return stream << std::endl;
  }
  template <typename StreamType>
  void log( StreamType& stream, std::string const& address, LHCb::LumiFSRs const& FSRs, bool low ) {
    for ( auto& fsr : FSRs ) { stream << address << ": Lumi" << ( low ? "Low" : "" ) << "FSR: " << *fsr << endmsg; }
  }
  template <typename StreamType>
  void log( StreamType& stream, std::string const& address, LHCb::EventCountFSR const& FSR, bool ) {
    stream << address << ": EventCountFSR: " << FSR << endmsg;
  }
  template <typename StreamType>
  void log( StreamType& stream, std::string const& address, LHCb::TimeSpanFSRs const& FSRs, bool ) {
    for ( auto& tsfsr : FSRs ) { stream << address << ": TimeSpanFSR: " << *tsfsr << endmsg; }
  }
} // namespace

template <typename FSRType, typename CounterType, typename StreamType>
void DumpFSR::dump_FSR( std::string const& fsrName, std::string const& shortName, CounterType& errorCount,
                        bool writeLog, StreamType& stream, bool low ) const {
  // m_navigatorTool is not thread safe, neither writing to a stream,
  // thus let's serialize this method
  std::lock_guard guard( m_navigatorToolLock );
  // make an inventory of the FileRecord store
  std::vector<std::string> addresses = m_navigatorTool->navigate( m_FileRecordName, fsrName );
  for ( auto& address : addresses ) {
    if ( writeLog ) info() << shortName << " address: " << address << endmsg;
    DataObject* obj  = nullptr;
    FSRType*    FSRs = m_fileRecordSvc->retrieveObject( fullTESLocation( address, true ), obj ).isSuccess()
                        ? dynamic_cast<FSRType*>( obj )
                        : nullptr;
    if ( !FSRs ) {
      ++errorCount;
    } else {
      log( stream, address, *FSRs, low );
    }
  }
}

template <typename StreamType>
void DumpFSR::dump_file( bool writelog, StreamType& stream ) const {
  if ( msgLevel( MSG::INFO ) ) {
    dump_FSR<LHCb::LumiFSRs>( m_FSRName, "lu", m_missingLumi, writelog, stream );
    dump_FSR<LHCb::LumiFSRs>( m_LowFSRName, "lo", m_missingLumi, writelog, stream, true );
    dump_FSR<LHCb::EventCountFSR>( m_EventCountFSRName, "ev", m_missingEventCount, writelog, stream );
    dump_FSR<LHCb::TimeSpanFSRs>( m_TimeSpanFSRName, "ts", m_missingTimeSpan, writelog, stream );
  }
}

void DumpFSR::write_file() {
  if ( m_ascii_fname.value() != "" ) {
    std::ofstream outfile( m_ascii_fname.value().c_str() );
    if ( outfile ) {
      always() << "asciifile: " << m_ascii_fname.value() << " - opened" << endmsg;
      dump_file( false, outfile );
      // close output file
      outfile.close();
      always() << "asciifile: " << m_ascii_fname.value() << " - closed" << endmsg;

    } else {
      always() << "asciifile: " << m_ascii_fname.value() << " - not opened" << endmsg;
    }
  }
}
