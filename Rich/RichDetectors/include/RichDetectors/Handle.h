/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <cassert>
#include <optional>
#include <ostream>
#include <string>
#include <type_traits>
#include <utility>

#include "DetDesc/DetectorElement.h"

class ParamValidDataObject;

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class Handle handle.h
   *
   *  Rich helper class encapsulating differences between DetDesc and DD4HEP
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  template <typename T>
  class Handle {

  private:
    // Test of object type (DD4HEP or DetDesc)
    using B = std::disjunction<std::is_base_of<DetDesc::DetectorElement, T>, std::is_base_of<ParamValidDataObject, T>>;
    // internal handle type
    using H = std::conditional_t<B::value, const T*, std::optional<T>>;
    // the handle
    H h;

  public:
    /// DD4HEP needs default constructor :(
    Handle() = default;
    /// Constructor from object
    Handle( const T& t ) {
      if constexpr ( B::value ) {
        h = &t;
      } else {
        h = t;
      }
    }
    /// Access to handled object
    const T* access() const noexcept {
      if constexpr ( B::value ) {
        return h;
      } else {
        return ( h.has_value() ? &( h.value() ) : nullptr );
      }
    }
    /// Overload arrow operator
    const T* operator->() const noexcept { return access(); }
    /// print to ostream
    friend inline std::ostream& operator<<( std::ostream& s, const Handle<T>& h ) { return s << h.access(); }
  };

} // namespace Rich::Detector
