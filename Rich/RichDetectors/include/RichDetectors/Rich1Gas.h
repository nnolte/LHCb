/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// local
#include "RichDetectors/Rich1.h"
#include "RichDetectors/RichRadiator.h"

// detector element
#ifdef USE_DD4HEP
#  include "Detector/Rich1/DetElemAccess/DeRich1.h"
#  include "Detector/Rich1/DetElemAccess/DeRich1RadiatorGas.h"
#else
#  include "RichDet/DeRichRadiator.h"
#endif

namespace Rich::Detector {

#ifdef USE_DD4HEP
  using Rich1Gas = details::RichXGas<Rich::Rich1Gas,                 //
                                     Rich::Detector::Rich1::DetElem, //
                                     LHCb::Detector::DeRich1Gas,     //
                                     LHCb::Detector::detail::DeRichRadiatorObject>;
#else
  using Rich1Gas = details::RichXGas<Rich::Rich1Gas,                 //
                                     Rich::Detector::Rich1::DetElem, //
                                     DeRichRadiator,                 //
                                     DeRichRadiator>;
#endif

} // namespace Rich::Detector
