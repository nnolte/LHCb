/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#ifdef USE_DD4HEP
#  include <yaml-cpp/yaml.h>
#else
#  include "DetDesc/ParamValidDataObject.h"
#endif

#include <cassert>

namespace Rich::Detector {

#ifdef USE_DD4HEP
  using Condition = YAML::Node;
#else
  using Condition = ParamValidDataObject;
#endif

  template <typename T>
  inline auto condition_param( const Condition& cond, const std::string& paramName ) {
#ifdef USE_DD4HEP
    // tutorial on how to use yaml-cpp can be found at
    // https://github.com/jbeder/yaml-cpp/wiki/Tutorial
    return cond[paramName].as<T>();
#else
    return cond.param<T>( paramName );
#endif
  }

  template <typename T>
  inline auto condition_param( const Condition* cond, const std::string& paramName ) {
    assert( cond );
    return condition_param<T>( *cond, paramName );
  }

} // namespace Rich::Detector
