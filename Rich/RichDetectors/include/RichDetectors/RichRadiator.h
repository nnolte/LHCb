/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SystemOfUnits.h"

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichRadiator.h"
#else
#  include "RichDet/DeRichRadiator.h"
#endif

// local
#include "RichDetectors/Utilities.h"

// STL
#include <array>
#include <cassert>
#include <memory>
#include <string>

namespace Rich::Detector {

  namespace details {

    /// base class for all radiators
    template <typename DETELEM>
    class alignas( LHCb::SIMD::VectorAlignment ) Radiator {

    public:
      // types

      // expose underlying detector element type
      using DetElem = DETELEM;

    public:
      // constructors

      /// Default Constructor
      Radiator() = default;

#ifdef USE_DD4HEP
      /// Constructor from DD4HEP
      template <typename DET>
      Radiator( const DET& rad )
          : m_radiatorID( rad.radiatorID() ) //
          , m_rich( rad.rich() )             //
          , m_radiator( rad.access() )
          , m_refIndex( std::make_shared<const Rich::TabulatedFunction1D>( rad.GasRefIndex() ) ) {}
#else
      /// Constructor from DetDesc
      template <typename DET>
      Radiator( const DET& rad )
          : m_radiatorID( rad.radiatorID() ) //
          , m_rich( rad.rich() )             //
          , m_radiator( &rad )               //
          , m_refIndex( rad.refIndex() ) {}
#endif

    private:
      /// Get access to the underlying object
      inline auto get() const noexcept { return m_radiator; }

    public:
      // accessors

      /// The radiator ID
      inline auto radiatorID() const noexcept { return m_radiatorID; }

      /// The RICH type
      inline auto rich() const noexcept { return m_rich; }

      /// refractive index interpolator
      inline auto refIndex() const noexcept { return m_refIndex.get(); }

      /// refractive index for given photon energy
      template <typename TYPE>
      inline auto refractiveIndex( const TYPE energy ) const noexcept {
        assert( refIndex() );
        return refIndex()->value( energy * Gaudi::Units::eV );
      }

    public:
      // methods forwarded to the underlying Det Elem

      FORWARD_TO_DET_OBJ( intersectionPoints )
      FORWARD_TO_DET_OBJ( isInside )

    protected:
      /// messaging
      void fillStream( std::ostream& s ) const {
        s << "[ " << rich() << " " << radiatorID() //
          << " refIndex=" << *refIndex()           //
          << " ]";
      }

    private:
      // data

      /// The Radiator ID
      Rich::RadiatorType m_radiatorID = Rich::InvalidRadiator;

      /// The Rich detector of this radiator
      Rich::DetectorType m_rich = Rich::InvalidDetector;

      /// DetDesc panel object
      const DETELEM* m_radiator = nullptr;

      /// Refractive index
      std::shared_ptr<const Rich::TabulatedFunction1D> m_refIndex;
    };

    /// templated class for each top level Rich Gas object
    template <Rich::RadiatorType GAS, typename RICH, typename DETELEM, typename BASEDETELEM>
    class RichXGas : public Radiator<BASEDETELEM> {

    public:
      // types

      // expose base class types
      using DetElem     = DETELEM;
      using BaseDetElem = BASEDETELEM;
      using Base        = Radiator<BASEDETELEM>;

    public:
      // constructors

      /// Default Constructor
      RichXGas() = default;

      /// Constructor from Det Elem
      template <typename RAD>
      RichXGas( const RAD& rad ) : Base( rad ) {}

    private:
      // messaging

      /// name string
      static constexpr auto MyName() noexcept {
        return ( GAS == Rich::Rich1Gas ? "Rich::Detector::Rich1Gas" : //
                     GAS == Rich::Rich2Gas ? "Rich::Detector::Rich2Gas" : "UNDEFINED" );
      }

    public:
      // messaging

      /// Overload ostream operator
      friend inline std::ostream& operator<<( std::ostream& s, const RichXGas<GAS, RICH, DETELEM, BASEDETELEM>& r ) {
        s << MyName() << " ";
        r.fillStream( s );
        return s;
      }

    public:
      // conditions handling

      /// Construct conditions name
      inline static auto conditionKey( std::string tag = "DerivedRadiator" ) noexcept {
        return DeRichLocations::derivedCondition<RICH>( std::move( tag ) );
      }

      /// Default conditions name
      inline static const std::string DefaultConditionKey = conditionKey();

      /// static generator function for gas detector element
      static auto generate( const DETELEM& g ) {
        auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
        assert( msgSvc );
        MsgStream log( msgSvc, MyName() );
        log << MSG::DEBUG << "Creating instance from " << (void*)&g << " name='" << g.name() << "' loc='"
            << DeRichLocations::location<DETELEM>( GAS ) << "' access()=" << (void*)g.access() << endmsg;
        return RichXGas<GAS, RICH, DETELEM, BASEDETELEM>{g};
      }

#ifdef USE_DD4HEP
      /// static generator from RICH object
      static auto generate_from_rich( const RICH& r ) {
        auto g = r.radiatorGas();
        return generate( g );
      }
#endif

      /// Creates a condition derivation for the given key
      template <typename PARENT>
      static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key = conditionKey() ) {
        assert( parent );
        assert( !key.empty() );
        if ( parent->msgLevel( MSG::DEBUG ) ) {
          parent->debug() << MyName() << "::addConditionDerivation : Key='" << key << "'" << endmsg;
        }
#ifdef USE_DD4HEP
        // FIXME Cannot yet directly instanciate dd4hep gas volumes, for some reason ...
        // so for now access indirectly via the main RICH object
        return parent->addSharedConditionDerivation( //
            {DeRichLocations::location<RICH>()},     // input condition location
            std::move( key ),                        // output derived condition location
            &generate_from_rich );
#else
        return parent->addSharedConditionDerivation(     //
            {DeRichLocations::location<DETELEM>( GAS )}, // input condition location
            std::move( key ),                            // output derived condition location
            &generate );
#endif
      }
    };

  } // namespace details

#ifdef USE_DD4HEP
  using Radiator = details::Radiator<LHCb::Detector::detail::DeRichRadiatorObject>;
#else
  using Radiator = details::Radiator<DeRichRadiator>;
#endif

} // namespace Rich::Detector
