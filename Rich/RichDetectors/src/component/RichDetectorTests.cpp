/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Rich Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Detectors
#include "RichDet/DeRichLocations.h"
#include "RichDetectors/Condition.h"
#include "RichDetectors/Handle.h"
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich1Gas.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/Rich2Gas.h"

// DAQ
#include "RichFutureDAQ/RichTel40CableMapping.h"

// Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichFutureUtils/RichSmartIDs.h"

// Boost
#include "boost/format.hpp"

// STL
#include <string>
#include <utility>
#include <vector>

using namespace Rich::Future::DAQ;
using namespace Rich::Detector;

namespace Rich::Future {

  // Use the functional framework
  using namespace Gaudi::Functional;

#ifdef USE_DD4HEP
  using DR1 = LHCb::Detector::DeRich1;
  using DR2 = LHCb::Detector::DeRich2;
  using DG1 = LHCb::Detector::DeRich1Gas;
  using DG2 = LHCb::Detector::DeRich2Gas;
#else
  using DR1 = DeRich1;
  using DR2 = DeRich2;
  using DG1 = DeRichRadiator;
  using DG2 = DeRichRadiator;
#endif

  class TestDBAccess final : public Consumer<void( const DR1&, const DR2&, const DG1&, const DG2& ),
                                             LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, DR1, DR2, DG1, DG2>> {
  public:
    TestDBAccess( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,                                                    //
                    {KeyValue{"R1Loc", DeRichLocations::location<DR1>()},                 //
                     KeyValue{"R2Loc", DeRichLocations::location<DR2>()},                 //
                     KeyValue{"G1Loc", DeRichLocations::location<DG1>( Rich::Rich1Gas )}, //
                     KeyValue{"G2Loc", DeRichLocations::location<DG2>( Rich::Rich2Gas )}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { return setProperty( "OutputLevel", MSG::VERBOSE ); } );
    }

    void operator()( const DR1& r1, const DR2& r2, const DG1& g1, const DG2& g2 ) const override {
      debug() << DeRichLocations::location<DG1>( Rich::Rich1Gas ) << endmsg;
      debug() << DeRichLocations::location<DG2>( Rich::Rich2Gas ) << endmsg;
      debug() << "R1 " << &r1 << " name='" << r1.name() << "' access()=" << r1.access() << endmsg;
      debug() << "R2 " << &r2 << " name='" << r2.name() << "' access()=" << r2.access() << endmsg;
      debug() << "G1 " << &g1 << " name='" << g1.name() << "' access()=" << g1.access() << endmsg;
      debug() << "G2 " << &g2 << " name='" << g2.name() << "' access()=" << g2.access() << endmsg;
    }
  };

  namespace {
    template <typename R>
    struct TestDervCond {
      inline static const std::string R1Key = DeRichLocations::derivedCondition<DR1>( "TestDervCond" );
      inline static const std::string R2Key = DeRichLocations::derivedCondition<DR2>( "TestDervCond" );
      TestDervCond()                        = default;
      TestDervCond( const R& d ) : r( d ) {}
      Rich::Detector::Handle<R> r;
    };
  } // namespace

  class TestDerivedElem final
      : public Consumer<void( const TestDervCond<DR1>&, const TestDervCond<DR2>& ),
                        LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, TestDervCond<DR1>, TestDervCond<DR2>>> {
  public:
    TestDerivedElem( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,                                //
                    {KeyValue{"R1DervLoc", TestDervCond<DR1>::R1Key}, //
                     KeyValue{"R2DervLoc", TestDervCond<DR2>::R2Key}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        auto sc = setProperty( "OutputLevel", MSG::VERBOSE );
        addConditionDerivation( {DeRichLocations::location<DR1>()}, //
                                TestDervCond<DR1>::R1Key,           //
                                [p = this]( const DR1& d1 ) {
                                  p->debug() << "Creating DR1 derived object from " << &d1 << " name='" << d1.name()
                                             << "' loc='" << DeRichLocations::location<DR1>()
                                             << "' access()=" << d1.access() << endmsg;
                                  return TestDervCond<DR1>{d1};
                                } );
        addConditionDerivation( {DeRichLocations::location<DR2>()}, //
                                TestDervCond<DR2>::R2Key,           //
                                [p = this]( const DR2& d2 ) {
                                  p->debug() << "Creating DR2 derived object from " << &d2 << " name='" << d2.name()
                                             << "' loc='" << DeRichLocations::location<DR2>()
                                             << "' access()=" << d2.access() << endmsg;
                                  return TestDervCond<DR2>{d2};
                                } );
        return sc;
      } );
    }

    void operator()( const TestDervCond<DR1>& d1, const TestDervCond<DR2>& d2 ) const override {
      debug() << "R1 " << d1.r << " name='" << d1.r->name() << "' access()=" << d1.r->access() << endmsg;
      debug() << "R2 " << d2.r << " name='" << d2.r->name() << "' access()=" << d2.r->access() << endmsg;
    }
  };

  class TestDerivedDetObjects final
      : public Consumer<void( const Detector::Rich1&, const Detector::Rich2&, //
                              const Detector::Rich1Gas&, const Detector::Rich2Gas& ),
                        LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,                        //
                                                             Detector::Rich1, Detector::Rich2, //
                                                             Detector::Rich1Gas, Detector::Rich2Gas>> {
  public:
    /// Standard constructor
    TestDerivedDetObjects( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"Rich1", Detector::Rich1::conditionKey()},
                     KeyValue{"Rich2", Detector::Rich2::conditionKey()},
                     KeyValue{"Rich1Gas", Detector::Rich1Gas::conditionKey()},
                     KeyValue{"Rich2Gas", Detector::Rich2Gas::conditionKey()}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        auto sc = setProperty( "OutputLevel", MSG::VERBOSE );
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
        Detector::Rich1Gas::addConditionDerivation( this );
        Detector::Rich2Gas::addConditionDerivation( this );
        return sc;
      } );
    }

    void operator()( const Detector::Rich1& r1, const Detector::Rich2& r2, //
                     const Detector::Rich1Gas& g1, const Detector::Rich2Gas& g2 ) const override {
      debug() << r1 << endmsg;
      debug() << r2 << endmsg;
      debug() << g1 << endmsg;
      debug() << g2 << endmsg;
    }
  };

  class TestRadiatorIntersections final
      : public Consumer<void( const Detector::Rich1Gas&, const Detector::Rich2Gas& ),
                        LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, //
                                                             Detector::Rich1Gas, Detector::Rich2Gas>> {
  public:
    /// Standard constructor
    TestRadiatorIntersections( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"Rich1Gas", Detector::Rich1Gas::DefaultConditionKey},
                     KeyValue{"Rich2Gas", Detector::Rich2Gas::DefaultConditionKey}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        auto sc = setProperty( "OutputLevel", MSG::VERBOSE );
        Detector::Rich1Gas::addConditionDerivation( this );
        Detector::Rich2Gas::addConditionDerivation( this );
        return sc;
      } );
    }

    void operator()( const Detector::Rich1Gas& g1, const Detector::Rich2Gas& g2 ) const override {

      using TrajData                     = std::pair<Gaudi::XYZPoint, Gaudi::XYZVector>;
      const std::vector<TrajData> r1Traj = {{{4.8339, -29.1795, 990}, {0.00492716, -0.0340466, 1}},
                                            {{-98.3335, 17.5605, 990}, {-0.160749, 0.0358936, 1}},
                                            {{2.9279, -9.9783, 990}, {0.00322774, -0.0103524, 1}},
                                            {{-40.8416, 14.2797, 990}, {-0.0587384, 0.00969204, 1}},
                                            {{197.9, -182.212, 990}, {0.204637, -0.188133, 1}}};
      const std::vector<TrajData> r2Traj = {{{-211.711, -52.4182, 9450}, {-0.0398052, -0.00558938, 1}},
                                            {{877.05, 91.0958, 9500}, {0.285066, 0.00889497, 1}},
                                            {{2933.82, -1818.83, 9450}, {0.44724, -0.189439, 1}},
                                            {{1567.06, -230.439, 9500}, {0.280539, -0.0236485, 1}},
                                            {{-671.092, -96.1549, 9450}, {-0.133261, -0.0101545, 1}}};

      auto testIntersects = [&]( const auto& trajs, const auto& rad ) {
        for ( const auto& t : trajs ) {
          Gaudi::XYZPoint entry, exit;
          const auto      ok = rad.intersectionPoints( t.first, t.second, entry, exit );
          info() << endmsg;
          info() << "Trajectory " << t.first << " " << t.second << endmsg;
          info() << " -> intersects = " << ok << endmsg;
          info() << " -> entry      = " << entry << endmsg;
          info() << " -> exit       = " << exit << endmsg;
        }
      };

      testIntersects( r1Traj, g1 );
      testIntersects( r2Traj, g2 );
    }
  };

  // Example of algorithm accessing conditions
  struct TestConds
      : Gaudi::Functional::Consumer<void( const Rich::Detector::Condition& ),
                                    LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Detector::Condition>> {
    // constructor
    TestConds( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"CondPath", Tel40CableMapping::ConditionPaths[0]}}} {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { return setProperty( "OutputLevel", MSG::VERBOSE ); } );
    }

    void operator()( const Rich::Detector::Condition& cond ) const override {
      debug() << "NumberOfLinks=" << condition_param<int>( cond, "NumberOfLinks" ) << endmsg;
    }
  };

  // Test RICH decoding and SmartIDs
  struct TestDecodeAndIDs
      : Gaudi::Functional::Consumer<void( const DAQ::DecodedData&, //
                                          const Rich::Utils::RichSmartIDs& ),
                                    LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {
    // Standard constructor
    TestDecodeAndIDs( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default},
                     // input conditions data
                     KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { Rich::Utils::RichSmartIDs::addConditionDerivation( this ); } );
    }

    void operator()( const DAQ::DecodedData& data, const Rich::Utils::RichSmartIDs& smartIDs ) const override {
      // RICHes loop
      for ( const auto& rD : data ) {
        // RICH panels
        for ( const auto& pD : rD ) {
          // PD modules
          for ( const auto& mD : pD ) {
            // PDs per module
            for ( const auto& PD : mD ) {
              // PD ID
              const auto pdID = PD.pdID();
              if ( pdID.isValid() ) {
                // loop over IDs
                for ( const auto& id : PD.smartIDs() ) {
                  const auto gPos = smartIDs.globalPosition( id );
                  const auto lPos = smartIDs.globalToPDPanel( gPos );
                  // print to 2 d.p. only to avoid small diffs between DetDesc and dd4hep
                  info() << id
                         << boost::format( " GPos=( %.2f, %.2f, %.2f ) LPos=( %.2f, %.2f, %.2f )" ) //
                                % gPos.X() % gPos.Y() % gPos.Z()                                    //
                                % lPos.X() % lPos.Y() % lPos.Z()
                         << endmsg;
                }
              } else {
                warning() << "INVALID PD ID " << pdID << endmsg;
              }
            }
          }
        }
      }
    }
  };

  DECLARE_COMPONENT( TestDBAccess )
  DECLARE_COMPONENT( TestDerivedElem )
  DECLARE_COMPONENT( TestDerivedDetObjects )
  DECLARE_COMPONENT( TestRadiatorIntersections )
  DECLARE_COMPONENT( TestConds )
  DECLARE_COMPONENT( TestDecodeAndIDs )

} // namespace Rich::Future
