
/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <algorithm>
#include <array>
#include <vector>

#include "RichUtils/RichException.h"

namespace Rich {

  /// utility method to convert a vector to an array of the same size
  template <typename OUTTYPE, std::size_t N, typename INTYPE = OUTTYPE>
  inline decltype( auto ) toArray( const std::vector<INTYPE>& v ) {
    if ( v.size() != N ) { throw Rich::Exception( "Vector to Array Size Error" ); }
    std::array<OUTTYPE, N> a;
    std::copy( v.begin(), v.end(), a.begin() );
    return a;
  }

} // namespace Rich
