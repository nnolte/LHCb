/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichPDMDBEncodeMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

// Gaudi
#include "Gaudi/Algorithm.h"

// Messaging
#include "RichFutureUtils/RichMessaging.h"
#define debug( ... )                                                                                                   \
  if ( messenger() ) { ri_debug( __VA_ARGS__ ); }
#define verbo( ... )                                                                                                   \
  if ( messenger() ) { ri_verbo( __VA_ARGS__ ); }

// STL
#include <vector>

using namespace Rich::Future::DAQ;
using namespace Rich::Detector;

bool PDMDBEncodeMapping::fillRType( const EncodingConds& C ) {

  using namespace Rich::DAQ;

  bool OK = true;

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() ) {

    // load the condition
    const auto cond = C.rTypeConds[rich];

    // get the active ECs
    const auto active_ecs = condition_param<std::vector<int>>( cond, "ActiveECs" );
    debug( " -> Found ", active_ecs.size(), " active ECs", endmsg );

    // Loop over active ECs
    for ( const auto ec : active_ecs ) {

      // Load the active PMTs for this EC
      const auto ecS         = "EC" + std::to_string( ec ) + "_";
      const auto active_pmts = condition_param<std::vector<int>>( cond, ecS + "ActivePMTs" );
      debug( "  -> Loading ", active_pmts.size(), "ActivePMTs ", endmsg );

      // data shortcuts
      assert( rich < Rich::NRiches );
      auto& rich_data = m_rTypeData[rich];
      assert( (std::size_t)ec < rich_data.size() );
      auto& ec_data = rich_data[ec];

      // loop over PMTs ..
      for ( const auto pmt : active_pmts ) {
        verbo( "   -> PMT ", pmt, endmsg );

        // Load the PMT data
        const std::string pmtS = ecS + "PMT" + std::to_string( pmt ) + "_";
        const auto        pdmdbs =
            Rich::toArray<PDMDBID::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "PDMDB" ) );
        const auto frames =
            Rich::toArray<PDMDBFrame::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Frame" ) );
        const auto bits =
            Rich::toArray<FrameBitIndex::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Bit" ) );

        // Fill data structure for each anode
        assert( (std::size_t)pmt < ec_data.size() );
        auto&         pmt_data = ec_data[pmt];
        std::uint16_t anode{0};
        for ( const auto&& [pdmdb, frame, bit] : Ranges::ConstZip( pdmdbs, frames, bits ) ) {
          assert( (std::size_t)anode < pmt_data.size() ); // check anode in range
          assert( !pmt_data[anode].isValid() );           // check not yet initialised
          pmt_data[anode] = AnodeData( PDMDBID( pdmdb ), PDMDBFrame( frame ), FrameBitIndex( bit ) );
          verbo( "    -> Anode ", anode, " ", pmt_data[anode], endmsg );
          ++anode;
        } // conditions data loop

      } // active PMT loop

    } // ECs loop

  } // RICH loop

  return OK;
}

bool PDMDBEncodeMapping::fillHType( const EncodingConds& C ) {

  using namespace Rich::DAQ;

  bool OK = true;

  // Load the H Type encoding condition for this RICH
  const auto cond = C.hTypeCond;

  // get the active ECs
  const auto active_ecs = condition_param<std::vector<int>>( cond, "ActiveECs" );
  debug( " -> Found ", active_ecs.size(), " active ECs", endmsg );

  // Loop over active ECs
  for ( const auto ec : active_ecs ) {

    // Load the active PMTs for this EC
    const auto ecS         = "EC" + std::to_string( ec ) + "_";
    const auto active_pmts = condition_param<std::vector<int>>( cond, ecS + "ActivePMTs" );
    debug( "  -> Loading ", active_pmts.size(), "ActivePMTs ", endmsg );

    // data shortcuts
    assert( (std::size_t)ec < m_hTypeData.size() );
    auto& ec_data = m_hTypeData[ec];

    // loop over PMTs ..
    for ( const auto pmt : active_pmts ) {
      verbo( "   -> PMT ", pmt, endmsg );

      // Load the PMT data
      const std::string pmtS = ecS + "PMT" + std::to_string( pmt ) + "_";
      const auto        pdmdbs =
          Rich::toArray<PDMDBID::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "PDMDB" ) );
      const auto frames =
          Rich::toArray<PDMDBFrame::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Frame" ) );
      const auto bits =
          Rich::toArray<FrameBitIndex::Type, NumAnodes>( condition_param<std::vector<int>>( cond, pmtS + "Bit" ) );

      // Fill data structure for each anode
      assert( (std::size_t)pmt < ec_data.size() );
      auto&         pmt_data = ec_data[pmt];
      std::uint16_t anode{0};
      for ( const auto&& [pdmdb, frame, bit] : Ranges::ConstZip( pdmdbs, frames, bits ) ) {
        assert( (std::size_t)anode < pmt_data.size() ); // check anode in range
        assert( !pmt_data[anode].isValid() );           // check not yet initialised
        pmt_data[anode] = AnodeData( PDMDBID( pdmdb ), PDMDBFrame( frame ), FrameBitIndex( bit ) );
        verbo( "    -> Anode ", anode, " ", pmt_data[anode], endmsg );
        ++anode;
      } // conditions data loop

    } // active PMT loop

  } // ECs loop

  return OK;
}
