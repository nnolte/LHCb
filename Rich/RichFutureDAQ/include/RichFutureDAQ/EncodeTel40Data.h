
/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <array>
#include <bitset>
#include <cstddef>
#include <cstdint>
#include <map>
#include <utility>
#include <vector>

// RICH Utils
#include "RichFutureUtils/RichMessaging.h"
#include "RichUtils/RichDAQDefinitions.h"

// local
#include "RichFutureDAQ/RichTel40CableMapping.h"

// Event
#include "Event/RawEvent.h"

// Boost
#include "boost/container/static_vector.hpp"
#include "boost/format.hpp"
#include "boost/format/group.hpp"
#include "boost/range/adaptor/reversed.hpp"

namespace Gaudi {
  class Algorithm;
}

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PMT data format encoding
  class EncodeTel40 final {

  public:
    /// Constructor from RICH detector elements
    EncodeTel40( const Tel40CableMapping& tel40Maps, //
                 const Gaudi::Algorithm*  parent = nullptr );

  public:
    /// 8-bit data words for the two halves of each Tel40 data frame
    class DataHalves final {
    public:
      /// Basic data type
      using DataType = std::uint8_t;
      /// Bits in data words
      static constexpr const DataType BitsPerWord = 8u;
      /// maximum bit index
      static constexpr const DataType MaxBitIndex = BitsPerWord - 1u;
      /// Bit offset for second half of data payload
      static constexpr const std::array<DataType, 2> PayloadBitOffsets{0u, 39u};
      /// Maximum ZS data sizes for each payload half
      static constexpr const std::array<DataType, 2> MaxZSByteSize{4u, 5u};
      /// NZS data size for each payload half
      static constexpr const std::array<DataType, 2> NZSByteSize{5u, 6u};

    public:
      /// Add a frame bit to the data
      void add( const Rich::DAQ::FrameBitIndex bit );

    public:
      /// Append the data to the given byte vector
      template <typename BANK>
      inline void appendTo( BANK& data ) const {
        // note adding 'backwards' so that words appear in Rawbank in the expected order
        for ( const std::size_t half : {1, 0} ) {
          for ( const auto w : boost::adaptors::reverse( m_data[half] ) ) { data.emplace_back( w ); }
        }
      }

      /// Access the number of words (bytes) in this data frame
      auto nBytes() const noexcept { return m_data[0].size() + m_data[1].size(); }

      /// Is this link active
      auto isActive() const noexcept { return m_isActive; }

      /// Set link active flag
      void setActive( const bool active ) noexcept { m_isActive = active; }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const DataHalves& d ) {
        os << "{";
        for ( const int half : {1, 0} ) {
          os << " Half:" << half << " ZS:" << d.m_zs[half] << " [";
          for ( const auto w : boost::adaptors::reverse( d.m_data[half] ) ) {
            os << boost::format( " %02X" ) % (int)w;
            if ( d.m_zs[half] ) {
              os << "(" << (int)w << ")";
            } else {
              os << "(" << std::bitset<BitsPerWord>( w ) << ")";
            }
          }
          os << " ]";
        }
        return os << " }";
      }

    private:
      // types

      /// Container type for words in one half of a link payload
      using HalfPayload = boost::container::static_vector<DataHalves::DataType, 6>;

      /// array type for payload halves
      template <typename TYPE>
      using HalfArray = std::array<TYPE, 2>;

      /// Type for overall link payload
      using LinkPayload = HalfArray<HalfPayload>;

    private:
      // data

      /// data storage
      LinkPayload m_data;

      /// Is ZS or not
      HalfArray<bool> m_zs{true, true};

      /// Is link active or inactive in the DB
      bool m_isActive{true};
    };

    /// Data frame for each connection in a Tel40
    using Tel40Data = std::map<Rich::DAQ::Tel40Connector, DataHalves>;

    /// Data for all Tel40 source IDs
    using AllTel40 = std::map<Rich::DAQ::SourceID, Tel40Data>;

  public:
    /// Add data to the storage
    void add( const Rich::DAQ::SourceID       sID,  //
              const Rich::DAQ::Tel40Connector conn, //
              const Rich::DAQ::FrameBitIndex  bit );

    /// Fill RawBanks into a RawEvent
    void fill( LHCb::RawEvent& rawEv, const std::uint8_t version );

  private:
    /// Define the messenger entity
    inline auto messenger() const noexcept {
      assert( m_parent );
      return m_parent;
    }

  private:
    // data

    /// pointer to parent
    const Gaudi::Algorithm* m_parent = nullptr;

    /// AllTel40 data storage
    AllTel40 m_tel40Data;
  };

} // namespace Rich::Future::DAQ
