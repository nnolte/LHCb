/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Kernel
#include "Kernel/RichSmartID.h"

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Det Desc
#include "DetDesc/ConditionKey.h"

// RICH DAQ
#include "RichFutureDAQ/RichTel40CableMapping.h"

// Temporary. To check if conditions exist (see below)
#include "GaudiAlg/GetData.h"
#include "RichDetectors/Rich1.h"

// RICH Utils
#include "RichUtils/RichDAQDefinitions.h"

// Detectors
#include "RichDet/DeRichLocations.h"
#include "RichDetectors/Condition.h"
#include "RichDetectors/Utilities.h"

// STL
#include <array>
#include <cassert>
#include <cstdint>
#include <map>
#include <ostream>
#include <set>
#include <vector>

namespace Gaudi {
  class Algorithm;
}

namespace Rich::Future::DAQ {

  // overloads for vectors etc.
  using GaudiUtils::operator<<;

  /// Helper class for RICH PDMDB readout mapping
  class PDMDBDecodeMapping final {

  private:
    // types

    /// Local struct to hold conditions
    struct DecodingConds {
      DetectorArray<const Rich::Detector::Condition*> rTypeConds{{}};
      const Rich::Detector::Condition*                hTypeCond{nullptr};
    };

  public:
    /// Default constructor
    PDMDBDecodeMapping() = default;

    /// Null constructor
    explicit PDMDBDecodeMapping( const Gaudi::Algorithm* parent ) : m_parent( parent ) {}

    /// Constructor from decoding map conditions
    PDMDBDecodeMapping( const DecodingConds     C, //
                        const Gaudi::Algorithm* parent = nullptr )
        : m_parent( parent ) {
      // load the mapping conditions needed for encoding
      m_isInitialised = fillRType( C ) && fillHType( C );
    }

  public:
    // data types

    /// The data for each anode
    class BitData final {
    public:
      /// The EC number (0-3)
      Rich::DAQ::ElementaryCell ec;
      /// The PMT number in EC
      Rich::DAQ::PMTInEC pmtInEC;
      /// The Anode index (0-63)
      Rich::DAQ::AnodeIndex anode;

    public:
      /// Default constructor
      BitData() = default;
      /// Constructor from values
      BitData( const Rich::DAQ::ElementaryCell _ec,   ///< ec
               const Rich::DAQ::PMTInEC        _pmt,  ///< pmt
               const Rich::DAQ::AnodeIndex     _anode ///< anode
               )
          : ec( _ec ), pmtInEC( _pmt ), anode( _anode ) {}

    public:
      /// Check if data is valid
      inline constexpr bool isValid() const noexcept {
        return ( ec.isValid() && pmtInEC.isValid() && anode.isValid() );
      }

    public:
      /// ostream operator
      friend std::ostream& operator<<( std::ostream& os, const BitData& bd ) {
        return os << "{ EC=" << bd.ec << " PMTInEC=" << bd.pmtInEC << " Anode=" << bd.anode << " }";
      }
    };

  private:
    // defines

    /// Max Number of frames per PDMDB
    static constexpr const std::size_t FramesPerPDMDB = 6;

    /// Number of PDMDBs per module
    static constexpr const std::size_t PDMDBPerModule = 2;

    ///  Max Number of frames per PDM
    static constexpr const std::size_t FramesPerPDM = PDMDBPerModule * FramesPerPDMDB;

    /// Number of bits per data frame
    static constexpr const std::size_t BitsPerFrame = 86;

    /// Array of Bit Data structs per frame
    using FrameData = std::array<BitData, BitsPerFrame>;

    /// Data for each PDMDB
    using PDMDBData = std::array<FrameData, FramesPerPDMDB>;

    /// Data for each PDM
    using PDMData = std::array<PDMDBData, PDMDBPerModule>;

    ///  R-Type Module data for each RICH
    using RTypeRichData = DetectorArray<PDMData>;

  private:
    // methods

    /// fill R Type PMT anode map data
    bool fillRType( const DecodingConds& C );

    /// fill H Type PMT anode map data
    bool fillHType( const DecodingConds& C );

  public:
    // accessors

    /// Access the initialisation state
    inline bool isInitialised() const noexcept { return m_isInitialised; }

    /// Get the PDMDB data for given RICH, PDMDB and frame
    inline const auto& getFrameData( const Rich::DetectorType    rich,  //
                                     const Rich::DAQ::PDMDBID    pdmdb, //
                                     const Rich::DAQ::PDMDBFrame link,  //
                                     const bool                  isHType ) const noexcept {
      if ( !isHType ) {
        // R type PMT
        assert( (std::size_t)rich < m_pdmDataR.size() );
        assert( (std::size_t)pdmdb.data() < m_pdmDataR[rich].size() );
        assert( (std::size_t)link.data() < m_pdmDataR[rich][pdmdb.data()].size() );
        return m_pdmDataR[rich][pdmdb.data()][link.data()];
      } else {
        assert( (std::size_t)pdmdb.data() < m_pdmDataH.size() );
        assert( (std::size_t)link.data() < m_pdmDataH[pdmdb.data()].size() );
        return m_pdmDataH[pdmdb.data()][link.data()];
      }
    }

    /// Get PDMDB data for given Tel40 data
    inline const auto& getFrameData( const Tel40CableMapping::Tel40LinkData& cData ) const noexcept {
      return getFrameData( cData.smartID.rich(), cData.pdmdbNum, cData.linkNum, cData.isHType );
    }

  public:
    // conditions handling

    /// Default conditions name
    inline static const std::string DefaultConditionKey =
        DeRichLocations::derivedCondition( "PDMDBDecodeMapping-Handler" );

    /// Creates a condition derivation
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent ) {
      // Assume parent algorithm has one and only one input of the correct type...
      return addConditionDerivation( parent, parent->template inputLocation<PDMDBDecodeMapping>() );
    }

    /// Creates a condition derivation for the given key
    template <typename PARENT>
    static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
      if ( parent->msgLevel( MSG::DEBUG ) ) {
        parent->debug() << "PDMDBDecodeMapping::addConditionDerivation : Key=" << key << endmsg;
      }
      std::array<std::string, 3> cond_paths{
#ifdef USE_DD4HEP
          "/world/BeforeMagnetRegion/Rich1:PDMDB_R_DecodePixelMap",
          "/world/AfterMagnetRegion/Rich2:PDMDB_R_DecodePixelMap",
          "/world/AfterMagnetRegion/Rich2:PDMDB_H_DecodePixelMap"
#else
          "/dd/Conditions/ReadoutConf/Rich1/PDMDB_R_DecodePixelMap",
          "/dd/Conditions/ReadoutConf/Rich2/PDMDB_R_DecodePixelMap",
          "/dd/Conditions/ReadoutConf/Rich2/PDMDB_H_DecodePixelMap"
#endif
      };
      // NOTE: CheckData test only needed here to deal with fact
      // not all DB tags currently in use have the required mapping conditions.
      // We detect this here and just return a default uninitialised object.
      // downstream users always check if the object is initialised before using
      // the object, which is only done when the DB tags require it.
      // Once support for the old DB tags is no longer required the test can be removed.
#ifdef USE_DD4HEP
      // dd4hep will always have the tag, so skip the test
      const bool hasCond = true;
#else
      const bool hasCond = Gaudi::Utils::CheckData<Rich::Detector::Condition>()( parent->detSvc(), cond_paths[0] );
#endif
      if ( hasCond ) {
        return parent->addConditionDerivation( std::move( cond_paths ),                             // input
                                               std::move( key ),                                    // output
                                               [p = parent]( const Rich::Detector::Condition& r1Cr, //
                                                             const Rich::Detector::Condition& r2Cr, //
                                                             const Rich::Detector::Condition& r2Ch ) {
                                                 return PDMDBDecodeMapping{DecodingConds{{&r1Cr, &r2Cr}, &r2Ch}, p};
                                               } );
      } else {
        // needs to depend on 'something' so fake a dependency on Rich1
        Detector::Rich1::addConditionDerivation( parent );
        // return an unintialised object
        return parent->addConditionDerivation(
            {Detector::Rich1::DefaultConditionKey}, std::move( key ),
            [p = parent]( const Detector::Rich1& ) { return PDMDBDecodeMapping{p}; } );
      }
    }

  private:
    /// Define the messenger entity
    inline auto messenger() const noexcept {
      assert( m_parent );
      return m_parent;
    }

  private:
    // data

    /// R type data
    RTypeRichData m_pdmDataR;

    /// H type data
    PDMData m_pdmDataH;

    /// Flag to indicate initialisation status
    bool m_isInitialised{false};

    /// Pointer back to parent algorithm (for messaging)
    const Gaudi::Algorithm* m_parent{nullptr};
  }; // namespace Rich::Future::DAQ

} // namespace Rich::Future::DAQ
