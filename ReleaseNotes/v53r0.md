2021-07-07 LHCb v53r0
===

This version uses
Gaudi [v36r0](../../../../Gaudi/-/tags/v36r0) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to LHCb [v52r2](/../../tags/v52r2), with the following changes:


### Fixes ~"bug fix" ~workaround

- ~Core ~Conditions | Change interfaces to make geometry always explicit, !3019 (@sponce)
- ~Build | Set data packages variables only for build/test environment, !3127 (@clemenci)


### Enhancements ~enhancement

- ~Tracking | More proxies for PrVeloTracks and PrFittedForwardTracks, to make conversion easier later, !3110 (@decianm)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | Rewrite CMake configuration in "modern CMake", !2931 (@clemenci) :star:

