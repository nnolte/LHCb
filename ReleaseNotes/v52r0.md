2021-03-12 LHCb v52r0
===

This version uses
Gaudi [v35r2](../../../../Gaudi/-/tags/v35r2) and
LCG [97a](http://lcginfo.cern.ch/release/97a/) with ROOT 6.20.06.

This version is released on `master` branch.
Built relative to LHCb [v51r3](/../../tags/v51r3), with the following changes:

### New features ~"new feature"

- ~Configuration | Allow PyConf component properties to declare data dependencies, !2930 (@apearce) [Moore#51]
- ~Configuration | Move PyConf from Moore to LHCb, !2906 (@rmatev) [Moore#116,`#12345,`Moore#12345] :star:


### Fixes ~"bug fix" ~workaround

- ~Tracking | Increase type-safety in LHCbID, !2956 (@graven)
- ~Tracking ~UT | Fix stripID order in DeUTSector and DeUTSensor, !2914 (@xuyuan) [Rec#174]
- ~VP ~Conditions | Register VP daughters with updatemgrservice, !2641 (@wouter) [LHCBPS-1850]
- ~Persistency | Remove static_vector usage in LHCb::RawEvent, !2926 (@apearce) [#114]
- ~Core | Fix compile error and segmentation fault in scheduler on some ARM architectures, !2947 (@ahennequ)


### Enhancements ~enhancement

- ~Configuration | Add TCK option to ApplicationOptions, !2938 (@raaij)
- ~Configuration | Extend all_inputs method to handle list input for class Algorithm in PyConf, !2911 (@axu)
- ~Configuration ~Conditions | Update of the LHCbApp configurable for the DD4hep geometry and new conditions, !2898 (@bcouturi)
- ~Tracking ~"Event model" | Make the Velo hit container flexible, !2880 (@peilian)
- ~Tracking ~"Event model" | Move TracksTag into each explicit track containers, !2855 (@peilian)
- ~Persistency | Reimplement mapping of `BankType` -> `span<RawBank const*>` in RawEvent, !2932 (@graven)
- ~Persistency | Remove stream check from PackParticlesAndVertices, !2928 (@apearce)
- ~Persistency | Move PersistRecoConf and reading to LHCb, !2927 (@nskidmor)
- ~Conditions | Made it possible to register a Derived Condition for non copyable type, !2901 (@sponce)
- ~Utilities | Fix meta_enum for use as Gaudi::Property, !2951 (@graven)
- ~Accelerators | Update RetinaCluster algorithms, !2918 (@gbassi)
- ~Build | Disable validateWithReference for skylake_avx512, !2944 (@rmatev)
- ~Build | Add exclusion in test for DD4hep conditions messages, !2909 (@bcouturi)
- ~Build | Fix test faiures due to ordering of Handles initialization, !2888 (@sponce)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Adapt PyConf API to snake_case convention, !2948 (@erodrigu)
- ~Configuration | Fixed python code for bad usage of 'in', !2897 (@sponce)
- ~Decoding ~Muon | Modernized MuonRec algorithm, !2952 (@sponce)
- ~VP ~UT ~FT ~"Event model" ~Persistency | Removal of Velo, ST, OT, IT, PRS, SPD from master, !2804 (@pkoppenb) :star:
- ~UT | Modernize/cleanup UT code, !2892 (@graven)
- ~Functors ~Simulation | Remove mentions of obsolete LoKiGenMC, !2950 (@amathad)
- ~Functors ~Simulation | Move Phys/LoKiGen from LHCb to Gauss, !2871 (@dmuller) :star:
- ~Persistency | Tweak HltPackedDataWriter, !2904 (@graven)
- ~Core | Removed unused tbb include file, !2902 (@sponce)
- ~Conditions | Add missing const to DetectorElement::createCondition arguments, !2916 (@graven)
- ~Utilities | Small fixes to be ready for Boost 1.75, !2953 (@clemenci) [SPI-1799]
- ~Utilities | Do not use C variable-length arrays, !2936 (@rmatev) [Alignment#4]
- ~Utilities | Added simple algorithm to iterate though a Detector's volumes, !2608 (@bcouturi)
- ~Simulation | Fix python relative import, !2908 (@rmatev)
- ~Build | Fix Boost deprecation notes, !2923 (@rmatev)
- ~Build | Fixes for LCG 99, !2912 (@clemenci) [LBCORE-1935]
- Disable LoKi welcome message by default, !2962 (@apearce)
- Add `constexpr` to ChannelIDs, and make constructors `explicit`, !2959 (@graven)
- Update LHCbID test with VELO -> VP, !2929 (@erodrigu)


### Documentation ~Documentation

- PyConf documentation updates, !2943 (@erodrigu)
- PyConf documentation updates, !2941 (@erodrigu)
- Use DecisionLine in PyConf.tonic docs, !2907 (@apearce)
