###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import ApplicationMgr, LHCbApp

from Configurables import (ProduceProtoParticles, ProtoParticlePacker,
                           HltANNSvc)

from PRConfig import TestFileDB

produceProtoParticle = ProduceProtoParticles(
    Output="/Event/Fake/ProtoParticles", )

locations = {}
locations["/Event/Fake/ProtoParticles"] = 0

hltann = HltANNSvc(allowUndefined=False, PackedObjectLocations=locations)

partPacker = ProtoParticlePacker(
    InputName=produceProtoParticle.Output,
    OutputName="/Event/Fake/Packed/ProtoParticles",
    ANNSvc=hltann.getFullName(),
    EnableCheck=True,
    OutputLevel=5)

ApplicationMgr().TopAlg = [produceProtoParticle, partPacker]

app = LHCbApp()
app.EvtMax = 1

f = TestFileDB.test_file_db[
    "2017HLTCommissioning_BnoC_MC2015_Bs2PhiPhi_L0Processed"]
f.setqualifiers(configurable=app)
f.run(configurable=app, withDB=True)
