###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for decorating locations with output prefix."""
import os
import re


def prefix(loc, tes_prefix='/Event'):
    """Add a prefix to a location.

    `/Event` and `tes_prefix` are stripped from the beginning of `loc`, and
    then `tes_prefix` is prepended.
    Args:
        loc (str): Location to prefix.
        tes_prefix (str): Prefix to add to `loc`.
    """
    unprefixed = re.sub('^({0}|/Event)/'.format(tes_prefix), '', loc)
    return os.path.join(tes_prefix, unprefixed)


def packed_prefix(loc, tes_prefix='/Event'):
    """Add a prefix to a location.

    `/Event` and `tes_prefix` are stripped from the beginning of `loc`, and
    then `tes_prefix` and `p` are prepended.
    Args:
        loc (str): Location to prefix.
        tes_prefix (str): Prefix to add to `loc`.
    """
    unprefixed = re.sub('^({0}|/Event)/'.format(tes_prefix), '', loc)
    return os.path.join(tes_prefix, "p" + unprefixed)


def unpacked_prefix(loc, tes_prefix='/Event'):
    """Add a prefix to a location.

    `/Event` and `tes_prefix` are stripped from the beginning of `loc`, and
    then `tes_prefix` is prepended after removing `p`.
    Args:
        loc (str): Location to prefix.
        tes_prefix (str): Prefix to add to `loc`.
    """
    unprefixed = re.sub('^({0}/|/Event)'.format(tes_prefix), '', loc)
    unprefixed = unprefixed[1:]
    return os.path.join(tes_prefix, unprefixed)
