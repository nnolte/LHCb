###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# These numbers correspond to class ids mostly.
# They are used to decide which unpacker to use
# Should not be changed!!!
def classid_map():
    classID_map = {
        "CaloClusters": 1541,
        "CaloDigits": 1542,
        "CaloAdcs": 1543,
        "Tracks": 1550,
        "CaloHypos": 1551,
        "ProtoParticles": 1552,
        "PVs": 1553,
        "TwoProngVertices": 1554,
        "WeightsVectors": 1555,
        "P2VRelations": 1560,
        "RichPIDs": 1561,
        "PP2MCPRelations": 1562,
        "MuonPIDs": 1571,
        "Particles": 1581,
        "Vertices": 1582,
        "FlavourTags": 1583,
        "RecSummary": 1006,
        "P2InfoRelations": 1584,
        "P2IntRelations": 1591,
        #this has same class id as P2Vrelations, so add a 100 to it
        "P2MCPRelations": 1660,
    }

    return classID_map


def inv_classid_map():
    return {v: k for k, v in classid_map().items()}


# These are the known types that we can persist
def type_map():
    type_map = {
        "KeyedContainer<LHCb::Particle,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Particles",
        "KeyedContainer<LHCb::ProtoParticle,Containers::KeyedObjectManager<Containers::hashmap> >":
        "ProtoParticles",
        "KeyedContainer<LHCb::Event::v1::Track,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Tracks",
        "KeyedContainer<LHCb::RichPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "RichPIDs",
        "KeyedContainer<LHCb::MuonPID,Containers::KeyedObjectManager<Containers::hashmap> >":
        "MuonPIDs",
        "KeyedContainer<LHCb::RecVertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "PVs",
        "KeyedContainer<LHCb::Vertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        "Vertices",

        #"KeyedContainer<LHCb::TwoProngVertex,Containers::KeyedObjectManager<Containers::hashmap> >":
        #"TwoProngVertices",
        "KeyedContainer<LHCb::CaloHypo,Containers::KeyedObjectManager<Containers::hashmap> >":
        "CaloHypos",

        #"KeyedContainer<LHCb::CaloCluster,Containers::KeyedObjectManager<Containers::hashmap> >":
        #"CaloClusters",

        #"KeyedContainer<LHCb::CaloDigit,Containers::KeyedObjectManager<Containers::hashmap> >":
        #"CaloDigits",

        #"KeyedContainer<LHCb::CaloAdc,Containers::KeyedObjectManager<Containers::hashmap> >":
        #"CaloAdcs",
        "KeyedContainer<LHCb::FlavourTag,Containers::KeyedObjectManager<Containers::hashmap> >":
        "FlavourTags",
        "LHCb::RecSummary":
        "RecSummary",

        #"KeyedContainer<LHCb::WeightsVector,Containers::KeyedObjectManager<Containers::hashmap> >":
        #"WeightsVectors",
        "LHCb::Relation1D<LHCb::Particle,LHCb::VertexBase>":
        "P2VRelations",
        "LHCb::Relation1D<LHCb::Particle,LHCb::MCParticle>":
        "P2MCPRelations",
        "LHCb::Relation1D<LHCb::Particle,int>":
        "P2IntRelations",
        "LHCb::Relation1D<LHCb::Particle,LHCb::RelatedInfoMap>":
        "P2InfoRelations",
        "LHCb::RelationWeighted1D<LHCb::ProtoParticle,LHCb::MCParticle,double>":
        "PP2MCPRelations"
    }

    return type_map
