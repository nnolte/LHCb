/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <sstream>

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SmartDataPtr.h"

// DetDesc
#include "DetDesc/Condition.h"
#include "DetDesc/SolidBox.h"

// local
#include "RichDet/DeRich.h"
#include "RichDet/DeRichPMT.h"

// RichUtils
#include "RichUtils/FastMaths.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DeRichPMT
//
// 2011-10-11 : Sajan Easo
//-----------------------------------------------------------------------------
const CLID CLID_DeRichPMT = 12026; // User defined

//=============================================================================

const CLID& DeRichPMT::classID() { return CLID_DeRichPMT; }

//=============================================================================

StatusCode DeRichPMT::initialize() {
  MsgStream msg( msgSvc(), "DeRichPMT" );

  // store the name of the PMT, without the /dd/Structure part
  const auto pos = name().find( "MAPMT:" );
  setMyName( std::string::npos != pos ? name().substr( pos ) : "DeRichPMT_NO_NAME" );
  if ( msgLevel( MSG::DEBUG, msg ) ) msg << MSG::DEBUG << "Initialize " << myName() << endmsg;

  // extract PMT overall module and copy number from detector element name
  const auto pos2 = name().find( ":" );
  if ( std::string::npos == pos2 ) {
    msg << MSG::FATAL << "A PMT without a module number" << endmsg;
    return StatusCode::FAILURE;
  }
  const auto s1   = name().substr( pos2 + 1 );
  const auto pos3 = s1.find( "/MAPMT:" );
  if ( std::string::npos == pos3 ) {
    msg << MSG::FATAL << "A PMT without a copy number" << endmsg;
    return StatusCode::FAILURE;
  }

  // Common DePD init
  auto sc = DeRichPD::initialize();
  if ( !sc ) { return sc; }

  m_dePmtAnode = ( !childIDetectorElements().empty() ? childIDetectorElements().front() : nullptr );
  if ( !m_dePmtAnode ) {
    fatal() << "DePMT : Cannot find PmtAnode detector element" << endmsg;
    return StatusCode::FAILURE;
  }

  // register for updates when geometry changes
  updMgrSvc()->registerCondition( this, geometry(), &DeRichPMT::updateGeometry );
  updMgrSvc()->registerCondition( this, m_dePmtAnode->geometry(), &DeRichPMT::updateGeometry );
  // Update PMT QE values whenever DeRichSystem updates
  // Turn this off for now to avoid dependency on DeRichSystem. Anyway we do not yet have
  // a complete set of PMT Q.E. conditions, so come back to this once this is in place.
  // instead of depending on DeRichSystem we should just depend on the relevant condition.
  // For now, just run the initialisation once by hand.
  sc = DeRichPMT::initPMTQuantumEff();
  if ( !sc ) { return sc; }
  // updMgrSvc()->registerCondition( this, deRichSys(), &DeRichPMT::initPMTQuantumEff );

  // Trigger first update
  sc = updMgrSvc()->update( this );
  if ( !sc ) { fatal() << "UMS updates failed" << endmsg; }

  return sc;
}

//=============================================================================

DetectorElementPlus* DeRichPMT::getFirstRich() {
  DetectorElementPlus*              de{nullptr};
  SmartDataPtr<DetectorElementPlus> afterMag( dataSvc(), "/dd/Structure/LHCb/AfterMagnetRegion" );
  if ( !afterMag ) {
    error() << "Could not load AfterMagnetRegion det elem" << endmsg;
  } else {
    const auto firstRichLoc =
        ( afterMag->exists( "RichDetectorLocations" ) ? afterMag->paramVect<std::string>( "RichDetectorLocations" )[0]
                                                      : DeRichLocations::OldRich1 );
    SmartDataPtr<DetectorElementPlus> deRich( dataSvc(), firstRichLoc );
    if ( deRich ) { de = deRich; }
  }
  if ( !de ) { error() << "Could not load DeRich for DeRichPMT" << endmsg; }
  return de;
}

//=============================================================================
void DeRichPMT::initialise( const DetectorElementPlus* deRich, PMTData& pddata ) {

  using namespace LHCb::SIMD;

  // check if already initialised
  if ( !deRich || pddata.initialised ) return;

  pddata.initialised = true;

  const auto PmtAnodeZSize         = deRich->param<double>( "RichPmtAnodeZSize" );
  const auto PmtAnodeLocationInPmt = deRich->param<double>( "RichPmtSiliconDetectorLocalZlocation" );
  const auto PmtPixelXSize         = deRich->param<double>( "RichPmtPixelXSize" );
  const auto PmtPixelYSize         = deRich->param<double>( "RichPmtPixelYSize" );
  pddata.PmtEffectivePixelXSize    = SIMDFP( PmtPixelXSize );
  pddata.PmtEffectivePixelYSize    = SIMDFP( PmtPixelYSize );
  pddata.PmtAnodeHalfThickness     = SIMDFP( PmtAnodeZSize * 0.5f );
  pddata.PmtNumPixCol              = SIMDUINT( deRich->param<int>( "RichPmtNumPixelCol" ) );
  pddata.PmtNumPixRow              = SIMDUINT( deRich->param<int>( "RichPmtNumPixelRow" ) );
  pddata.PmtNumPixColFrac          = simd_cast<SIMDFP>( pddata.PmtNumPixCol - SIMDUINT::One() ) * SIMDFP( 0.5f );
  pddata.PmtNumPixRowFrac          = simd_cast<SIMDFP>( pddata.PmtNumPixRow - SIMDUINT::One() ) * SIMDFP( 0.5f );

  pddata.PmtQwZSize         = SIMDFP( deRich->param<double>( "RichPmtQuartzZSize" ) );
  const auto QwToAnodeZDist = deRich->param<double>( "RichPmtQWToSiMaxDist" );

  pddata.zShift = SIMDFP( QwToAnodeZDist + PmtAnodeLocationInPmt );

  pddata.RichPmtAnodeXSize = deRich->param<double>( "RichPmtAnodeXSize" );
  pddata.RichPmtAnodeYSize = deRich->param<double>( "RichPmtAnodeYSize" );

  if ( deRich->exists( "Rich2PMTArrayConfig" ) && //
       deRich->exists( "RichGrandPmtAnodeXSize" ) ) {
    const auto GrandPmtAnodeZSize      = deRich->param<double>( "RichGrandPmtAnodeZSize" );
    const auto GrandPmtPixelXSize      = deRich->param<double>( "RichGrandPmtPixelXSize" );
    const auto GrandPmtPixelYSize      = deRich->param<double>( "RichGrandPmtPixelYSize" );
    pddata.GrandPmtEdgePixelXSize      = SIMDFP( deRich->param<double>( "RichGrandPmtEdgePixelXSize" ) );
    pddata.GrandPmtEdgePixelYSize      = SIMDFP( deRich->param<double>( "RichGrandPmtEdgePixelYSize" ) );
    pddata.GrandPmtEffectivePixelXSize = SIMDFP( GrandPmtPixelXSize );
    pddata.GrandPmtEffectivePixelYSize = SIMDFP( GrandPmtPixelYSize );
    pddata.GrandPmtAnodeHalfThickness  = SIMDFP( GrandPmtAnodeZSize * 0.5f );
    pddata.RichGrandPmtAnodeXSize      = deRich->param<double>( "RichGrandPmtAnodeXSize" );
    pddata.RichGrandPmtAnodeYSize      = deRich->param<double>( "RichGrandPmtAnodeYSize" );
  }
}

//=============================================================================

StatusCode DeRichPMT::getPMTParameters() {

  const auto deRich = getFirstRich();
  if ( !deRich ) { return StatusCode::FAILURE; }
  m_deRich = deRich;

  {
    const std::string effnumPixCond = "RichPmtTotNumPixel";
    const auto        cExists       = deRich->exists( effnumPixCond );
    m_effNumActivePixs              = ( cExists ? (FP)deRich->param<int>( effnumPixCond ) : 64.0 );
    _ri_debug << "EffNumPixs = " << cExists << " | " << m_effNumActivePixs << endmsg;
  }

  // shared PD data
  initialise( deRich, m_pddata );

  // Default initialise some DePD base parameters
  setPmtIsGrandFlag( PmtIsGrand() );

  // cache to local matrix
  m_toLocM = geometryPlus()->toLocalMatrix();

  // Cache SIMD info

  // transform does not like direct assignent from scalar version :(
  {
    double xx{0}, xy{0}, xz{0}, dx{0}, yx{0}, yy{0};
    double yz{0}, dy{0}, zx{0}, zy{0}, zz{0}, dz{0};
    // and to global
    using FP = Rich::SIMD::DefaultScalarFP;
    toGlobalMatrix().GetComponents( xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz );
    m_toGlobalMatrixSIMD.SetComponents( (FP)xx, (FP)xy, (FP)xz, (FP)dx, //
                                        (FP)yx, (FP)yy, (FP)yz, (FP)dy, //
                                        (FP)zx, (FP)zy, (FP)zz, (FP)dz );
  }

  // read PMT properties from SIMCOND
  auto sc = readPMTPropertiesFromDB();
  if ( !sc ) { return sc; }

  return sc;
}

//=============================================================================

void DeRichPMT::setPmtIsGrandFlag( const bool isGrand ) {
  m_PmtIsGrand = isGrand;
  // Update cached size parameters
  const auto* deRich = getFirstRich();
  if ( deRich ) {
    if ( isGrand ) {
      const auto GrandPmtPixelXSize = deRich->param<double>( "RichGrandPmtPixelXSize" );
      const auto GrandPmtPixelYSize = deRich->param<double>( "RichGrandPmtPixelYSize" );
      m_pixelArea                   = GrandPmtPixelXSize * GrandPmtPixelYSize;
    } else {
      const auto PmtPixelXSize = deRich->param<double>( "RichPmtPixelXSize" );
      const auto PmtPixelYSize = deRich->param<double>( "RichPmtPixelYSize" );
      m_pixelArea              = PmtPixelXSize * PmtPixelYSize;
    }
    m_effPixelArea = m_pixelArea; // PMTs have no demagnification
  } else {
    error() << "Could not load First Rich" << endmsg;
  }
  if ( PmtIsGrand() && rich() == Rich::Rich1 ) { warning() << "Setting RICH1 PMT 'Grand'" << endmsg; }
}

//=============================================================================

StatusCode DeRichPMT::initPMTQuantumEff() {
  const auto* deRich = getFirstRich();
  if ( !deRich ) { return StatusCode::FAILURE; }
  const auto                      PmtQELocation = deRich->param<std::string>( "RichPmtQETableName" );
  SmartDataPtr<TabulatedProperty> pmtQuantumEffTabProp( dataSvc(), PmtQELocation );
  m_pdQuantumEffFunc = std::make_shared<Rich::TabulatedProperty1D>( pmtQuantumEffTabProp );
  return StatusCode::SUCCESS;
}

//=============================================================================

StatusCode DeRichPMT::updateGeometry() { return getPMTParameters(); }

//=============================================================================

bool DeRichPMT::detectionPoint( const LHCb::RichSmartID smartID,         //
                                Gaudi::XYZPoint&        detectPoint,     //
                                bool                    photoCathodeSide //
                                ) const {

  auto aLocalHit = getAnodeHitCoordFromMultTypePixelNum( smartID.pixelCol(), smartID.pixelRow() );

  aLocalHit.SetZ( aLocalHit.Z() + scalar( m_pddata.zShift ) );

  // for now assume negligible refraction effect at the QW.

  if ( !photoCathodeSide ) { aLocalHit.SetZ( scalar( m_pddata.PmtQwZSize ) + aLocalHit.Z() ); }

  detectPoint = ( toGlobalMatrix() * aLocalHit );

  return true;
}

//===============================================================================================

DeRichPD::SIMDFP::mask_type DeRichPMT::detectionPoint( const SmartIDs& smartID,         //
                                                       SIMDPoint&      detectPoint,     //
                                                       bool            photoCathodeSide //
                                                       ) const {

  // return status
  SIMDFP::mask_type ok( true );

  // // Just use a scalar loop here...
  // SIMDFP X(0), Y(0), Z(0);
  // for ( std::size_t i = 0; i < SIMDFP::Size; ++i )
  // {
  //   Gaudi::XYZPoint p{0,0,0};
  //   ok[i] = detectionPoint( smartID[i], p, photoCathodeSide );
  //   if ( ok[i] )
  //   {
  //     X[i] = p.X();
  //     Y[i] = p.Y();
  //     Z[i] = p.Z();
  //   }
  // }
  // detectPoint = { X, Y, Z };

  // SIMD version

  // Extract the row, col numbers
  SIMDUINT row{}, col{};
  for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
    // ID valid test not needed as invalid IDs will anyway return 0
    row[i] = smartID[i].pixelRow();
    col[i] = smartID[i].pixelCol();
  }

  // make local hit
  auto aLocalHit = getAnodeHitCoordFromMultTypePixelNum( col, row );
  aLocalHit.SetZ( m_pddata.zShift + aLocalHit.Z() );

  // for now assume negligible refraction effect at the QW.

  if ( !photoCathodeSide ) { aLocalHit.SetZ( m_pddata.PmtQwZSize + aLocalHit.Z() ); }

  detectPoint = m_toGlobalMatrixSIMD * aLocalHit;

  return ok;
}

//===============================================================================================

Gaudi::XYZPoint DeRichPMT::detPointOnAnode( const LHCb::RichSmartID& smartID ) const {
  return ( m_dePmtAnode->geometryPlus()->toGlobal(
      getAnodeHitCoordFromMultTypePixelNum( smartID.pixelCol(), smartID.pixelRow() ) ) );
}

//=============================================================================

StatusCode DeRichPMT::readPMTPropertiesFromDB() {

  const auto nrOfPixelsInPmt = m_pddata.PmtNumPixCol[0] * m_pddata.PmtNumPixRow[0];

  // check if the condition exists
  if ( !hasCondition( DeRichLocations::PMTPropertiesCondName ) ) {

    debug() << "Condition with PMT properties not found. Filling channel properties with zero values." << endmsg;

    m_PmtChannelGainMean  = PMTChannelProperty{{}};
    m_PmtChannelGainRms   = PMTChannelProperty{{}};
    m_PmtChannelThreshold = PMTChannelProperty{{}};
    m_PmtChannelSinRatio  = PMTChannelProperty{{}};
    m_PmtAverageOccupancy = 0;

    return StatusCode::SUCCESS;

  } else {

    const auto cond = condition( DeRichLocations::PMTPropertiesCondName );

    m_PmtChannelGainMean =
        toarray<float, LHCb::RichSmartID::MaPMT::TotalPixels>( cond->paramAsDoubleVect( "GainMean" ) );
    m_PmtChannelGainRms = toarray<float, LHCb::RichSmartID::MaPMT::TotalPixels>( cond->paramAsDoubleVect( "GainRms" ) );
    m_PmtChannelThreshold =
        toarray<float, LHCb::RichSmartID::MaPMT::TotalPixels>( cond->paramAsDoubleVect( "Threshold" ) );
    m_PmtAverageOccupancy = cond->paramAsDouble( "AverageOccupancy" );

    // check if the SIN info is available in the condition DB
    if ( cond->exists( DeRichLocations::PMTSinInfoParamName ) ) {
      m_PmtChannelSinRatio = toarray<float, LHCb::RichSmartID::MaPMT::TotalPixels>(
          cond->paramAsDoubleVect( DeRichLocations::PMTSinInfoParamName ) );
    } else {
      debug() << "Parameter: " << DeRichLocations::PMTSinInfoParamName
              << "\t not found. SIN info parameters values will be set to zero." << endmsg;
      m_PmtChannelSinRatio = PMTChannelProperty{{}};
    }

    // check consistency
    if ( nrOfPixelsInPmt != m_PmtChannelGainMean.size() ||  //
         nrOfPixelsInPmt != m_PmtChannelGainRms.size() ||   //
         nrOfPixelsInPmt != m_PmtChannelThreshold.size() || //
         nrOfPixelsInPmt != m_PmtChannelSinRatio.size() ) {

      error() << "Nr of channel properties from SIMCOND doesn't match the expected number of channels.\n"
              << "Condition   :\t" << cond.path() << "\n"
              << "#channels   :\t" << nrOfPixelsInPmt << "\n"
              << "#Gain mean  :\t" << m_PmtChannelGainMean.size() << "\n"
              << "#Gain rms   :\t" << m_PmtChannelGainRms.size() << "\n"
              << "#Threshold  :\t" << m_PmtChannelThreshold.size() << "\n"
              << "#SIN ratio   :\t" << m_PmtChannelSinRatio.size() << "\n"
              << endmsg;
      return StatusCode::FAILURE;
    }

    return StatusCode::SUCCESS;
  }
}

//=============================================================================
