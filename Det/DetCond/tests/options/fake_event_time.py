###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *


@appendPostConfigAction
def bindFakeEventTime():
    '''
    Ensure that the fake event time is in sync in all parites.
    '''
    from Configurables import EventClockSvc, FakeEventTime
    from Configurables import LHCb__Tests__FakeEventTimeProducer as FET
    from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as ReserveIOV
    ecs = EventClockSvc()
    ecs.addTool(FakeEventTime, "EventTimeDecoder")
    ecs.InitialTime = ecs.EventTimeDecoder.StartTime
    odin_path = '/Event/DummyODIN'
    app = ApplicationMgr()
    app.TopAlg = [
        FET('FakeEventTime',
            ODIN=odin_path,
            Start=ecs.EventTimeDecoder.StartTime / 1E9,
            Step=ecs.EventTimeDecoder.TimeStep / 1E9),
        ReserveIOV('ReserveIOV', ODIN=odin_path),
    ] + app.TopAlg
