/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Gaudi/Property.h>
#include <GaudiAlg/FixTESPath.h>
#include <GaudiAlg/FunctionalUtilities.h>
#include <GaudiKernel/DataObjectHandle.h>
#include <GaudiKernel/EventContext.h>
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/ServiceHandle.h>
#include <GaudiKernel/ThreadLocalContext.h>
#include <GaudiKernel/detected.h>
#include <Kernel/STLExtensions.h>
#include <LbDD4hep/IDD4hepSvc.h>
#include <any>
#include <stdexcept>
#include <type_traits>

namespace {
  template <typename Transform>
  struct construct_constructor;

  template <typename Ret, typename... Args>
  struct construct_constructor<Ret( Args const&... )> {
    static_assert( std::is_constructible_v<Ret, Args const&...> );
    [[nodiscard]] static constexpr auto
    construct( Args const&... args ) noexcept( std::is_nothrow_constructible_v<Ret, Args const&...> ) {
      return Ret{args...};
    }
  };

  template <typename Transform>
  constexpr auto invoke_constructor = construct_constructor<Transform>::construct;

  template <typename Algo>
  using has_FixTESPath = decltype( std::declval<Algo>().rootInTES() );
  template <typename Algo>
  inline constexpr bool has_FixTESPath_v = Gaudi::cpp17::is_detected_v<has_FixTESPath, Algo>;

} // namespace

namespace LHCb::Det::LbDD4hep {

  using ConditionContext = IDD4hepSvc::DD4HepSlicePtr;

  /// Helper to hold a pointer/reference to a DetectorElement in a
  /// technology independent way.
  template <typename T>
  class DetElementRef {
  public:
    DetElementRef()                       = default;
    DetElementRef( const DetElementRef& ) = default;
    DetElementRef( DetElementRef&& )      = default;
    DetElementRef& operator=( const DetElementRef& ) = default;
    DetElementRef& operator=( DetElementRef&& ) = default;

    DetElementRef( const T& det ) : m_det{det} {}

    const T* operator->() const { return &m_det; }
    const T& operator*() const { return m_det; }

  private:
    T m_det = nullptr;
  };

  template <typename T>
  class ConditionAccessorBase {

    template <typename... Args, std::size_t... Is>
    ConditionAccessorBase( const std::tuple<Args...>& args, std::index_sequence<Is...> )
        : ConditionAccessorBase( std::get<Is>( args )... ) {}

  public:
    template <typename Owner>
    ConditionAccessorBase( Owner* owner, const std::string& keyName, const std::string& keyDefault,
                           const std::string& keyDoc = "" )
        : m_key{owner, keyName, keyDefault, keyDoc}, currentConditionContext{[owner]() -> const ConditionContext& {
          return owner->getConditionContext( Gaudi::Hive::currentContext() );
        }} {}

    template <typename Owner>
    ConditionAccessorBase( Owner* owner, const std::string& key )
        : m_key{{}, key, {}}, currentConditionContext{[owner]() -> const ConditionContext& {
          return owner->getConditionContext( Gaudi::Hive::currentContext() );
        }} {}

    template <typename... Args>
    ConditionAccessorBase( const std::tuple<Args...>& args )
        : ConditionAccessorBase( args, std::index_sequence_for<Args...>{} ) {}

    // Condition accessors can neither be moved nor copied
    ConditionAccessorBase( const ConditionAccessorBase& ) = delete;
    ConditionAccessorBase( ConditionAccessorBase&& )      = delete;
    ConditionAccessorBase& operator=( const ConditionAccessorBase& ) = delete;
    ConditionAccessorBase& operator=( ConditionAccessorBase&& ) = delete;

    const std::string& key() const { return m_key; }

  protected:
    dd4hep::Condition::key_type getKey() const {
      const auto colonPos = m_key.value().find_first_of( ':' );
      if ( colonPos == std::string::npos ) {
        throw std::runtime_error( "Expected a ':' in condition key " + m_key.value() );
      }
      auto                           path = m_key.value().substr( 0, colonPos );
      auto                           hash = dd4hep::ConditionKey::itemCode( m_key.value().substr( colonPos + 1 ) );
      dd4hep::ConditionKey::KeyMaker m( dd4hep::detail::hash32( path ), hash );
      return m.hash;
    }

    // Configurable key which this ConditionAccessor points to.
    Gaudi::Property<std::string> m_key;
    // Helper to access the current (TLS based) condition context
    std::function<const ConditionContext&()> currentConditionContext{[]() -> const ConditionContext& {
      throw GaudiException( "no owner defined: no access to current ConditionContext", "ConditionAccessor::get()",
                            StatusCode::FAILURE );
    }};
  };

  template <typename T>
  struct ConditionAccessor : ConditionAccessorBase<T> {
    using ConditionAccessorBase<T>::ConditionAccessorBase;
    decltype( auto ) get( const ConditionContext& ctx ) const {
      try {
        const dd4hep::Condition& cond = ctx->pool->get( this->getKey() );
        if constexpr ( detail::IsHandle_v<T> ) {
          return T( cond );
        } else {
          using DT = std::decay_t<T>;
          if constexpr ( !detail::PassAsAny<DT>::value ) {
            return cond.get<DT>();
          } else {
            const DT* p = nullptr;
            // First see if type is std::any. If it is we can then apply our own type checks
            // as we cannot rely on those in dd4hep itself giving anything like good diagnostics.
            // Unfortunately due to how dd4hep handles this no other way than just trying a
            // cast to std::any and catching the bad_cast exception if that fails.
            try {
              if constexpr ( detail::IsBoxed<DT>::value ) {
                const auto* cp = std::any_cast<detail::CopyWrapper<DT>>( &cond.get<std::any>() );
                if ( cp ) { p = &static_cast<const DT&>( *cp ); }
              } else {
                p = std::any_cast<DT>( &cond.get<std::any>() );
              }
              if ( !p ) { detail::throwTypeMismatch( cond.get<std::any>().type(), typeid( DT ) ); }
            } catch ( const std::bad_cast& ) {
              // dd4hep 'cast' to std::any failed. Fallback to directly accessing type
              // i.e. we just assume that is what dd4hep has stored.
              // If not, and we get another bad_cast exception, throw as meaningful exception as we can.
              try {
                p = &cond.get<DT>();
              } catch ( const std::bad_cast& ) { detail::throwTypeMismatch( typeid( nullptr ), typeid( DT ) ); }
            }
            assert( p );
            return *p;
          }
        }
      } catch ( const std::exception& e ) {
        std::stringstream sstr;
        sstr << "Error getting condition '" << this->m_key.value() << "' (" << std::hex << std::uppercase
             << this->getKey() << "): " << e.what();
        throw std::runtime_error( sstr.str() );
      }
    }
    decltype( auto ) get() const { return get( this->currentConditionContext() ); }
  };

  template <typename Base>
  class ConditionAccessorHolder : public Base {
    using source_location = LHCb::cxx::source_location;

  public:
    /// Forward to base class constructor
    using Base::Base;

    /// Helper to expose this class to specializations without having to spell
    /// the whole name (see Gaudi::Examples::Conditions::UserAlg)
    using base_class = ConditionAccessorHolder<Base>;

    // The base class exposes to the user all the other components of the
    // chosen back-end's interface: accessors and contexts.
    template <typename T>
    using ConditionAccessor = LHCb::Det::LbDD4hep::ConditionAccessor<T>;
    using ConditionContext  = LHCb::Det::LbDD4hep::ConditionContext;

    template <typename T>
    using DetElementRef = LHCb::Det::LbDD4hep::DetElementRef<T>;

  public:
    template <typename Transform, size_t N = detail::arity_v<Transform>>
    bool addConditionDerivation( const std::array<std::string, N>& inputKeys, const std::string& outputKey,
                                 Transform       f       = invoke_constructor<Transform>,
                                 source_location src_loc = source_location::current() ) const {
      using derived_condition_t = boost::callable_traits::return_type_t<Transform>;
      static_assert(
          (std::is_move_assignable_v<derived_condition_t> || std::is_copy_assignable_v<derived_condition_t>));
      auto func =
          IDD4hepSvc::DD4HepDerivationFunc{new GenericConditionUpdateCall( std::move( f ), std::move( src_loc ) )};
      return m_dd4hepSvc->add( make_span( begin( inputKeys ), end( inputKeys ) ), std::move( outputKey ), func );
    }

    template <typename Transform, size_t N = detail::arity_v<Transform>>
    bool addSharedConditionDerivation( const std::array<std::string, N>& inputKeys, const std::string& outputKey,
                                       Transform       f       = invoke_constructor<Transform>,
                                       source_location src_loc = source_location::current() ) const {
      using derived_condition_t = boost::callable_traits::return_type_t<Transform>;
      static_assert(
          (std::is_move_assignable_v<derived_condition_t> || std::is_copy_assignable_v<derived_condition_t>));
      auto func = IDD4hepSvc::DD4HepDerivationFunc{new FPointerConditionUpdateCall( f, std::move( src_loc ) )};
      return m_dd4hepSvc->addShared( make_span( begin( inputKeys ), end( inputKeys ) ), std::move( outputKey ), func );
    }

    const ConditionContext& getConditionContext( const EventContext& /* ctx */ ) const {
      // context is taken from thread local storage for the handles
      return *m_ctxHandle.get();
    }

    IDD4hepSvc& getDataSvc() { return *m_dd4hepSvc; }

  private:
    // We must declare a dependency on the condition context
    // This context is actually the DD4Hep IOV stored in the TES
    DataObjectReadHandle<ConditionContext> m_ctxHandle{DataObjID{IDD4hepSvc::DefaultSliceLocation}, this};

    ServiceHandle<IDD4hepSvc> m_dd4hepSvc{this, "DD4hepSvc", "LHCb::Det::LbDD4hep::DD4hepSvc",
                                          "underlying DD4Hep service handling conditions"};
  };

  template <typename C, typename A>
  const C& get( const ConditionAccessor<C>& handle, const ConditionAccessorHolder<A>& algo, const EventContext& ctx ) {
    return handle.get( algo.getConditionContext( ctx ) );
  }
  template <template <typename> class HANDLE, typename T, typename A,
            typename std::enable_if_t<std::is_base_of_v<dd4hep::Handle<T>, HANDLE<T>>>* = nullptr>
  const HANDLE<T> get( const ConditionAccessor<HANDLE<T>>& handle, const ConditionAccessorHolder<A>& algo,
                       const EventContext& ctx ) {
    return handle.get( algo.getConditionContext( ctx ) );
  }

  template <typename C>
  const std::string& getKey( const ConditionAccessor<C>& handle ) {
    return handle.key();
  }

  template <typename... C>
  struct useConditionHandleFor {
    template <typename T>
    using InputHandle = std::enable_if_t<std::disjunction_v<std::is_same<std::decay_t<T>, std::decay_t<C>>...>,
                                         ConditionAccessor<std::decay_t<T>>>;
  };

  // Inherit from base Algorithm, wrapped in FixTESPath if not already there
  template <typename Algorithm = Gaudi::Algorithm>
  using AlgorithmWithCondition =
      ConditionAccessorHolder<std::conditional_t<has_FixTESPath_v<Algorithm>, Algorithm, FixTESPath<Algorithm>>>;

  template <typename... C>
  using usesConditions =
      Gaudi::Functional::Traits::use_<useConditionHandleFor<C...>,
                                      Gaudi::Functional::Traits::BaseClass_t<AlgorithmWithCondition<>>>;

  template <typename B, typename... C>
  using usesBaseAndConditions =
      Gaudi::Functional::Traits::use_<useConditionHandleFor<C...>,
                                      Gaudi::Functional::Traits::BaseClass_t<AlgorithmWithCondition<B>>>;
} // namespace LHCb::Det::LbDD4hep
