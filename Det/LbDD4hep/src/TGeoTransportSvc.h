/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "TGeoManager.h"
#include "TGeoMaterial.h"

#include "DetDesc/ITransportSvc.h"
#include "DetDesc/TransportSvcException.h"

#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IDataStoreAgent.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/StatEntity.h"
#include "GaudiKernel/StatusCode.h"

#include <iostream>
#include <map>
#include <mutex>
#include <string>

class IDataProviderSvc;
class IMessageSvc;

#include "DetDesc/IDetectorElement.h"
class ISvcLocator;
class GaudiException;

/**
 * Temporary structure for the comparison of the material seen in TGeo and in
 * LHCb
 */
struct MatIntervalCmp {
  Double_t                 start{0};
  Double_t                 end{0};
  Double_t                 radlength{0};
  std::string              materialName;
  TGeoMaterial*            material{nullptr};
  std::vector<std::string> locations;

  Double_t                 LHCbstart{0};
  Double_t                 LHCbend{0};
  Double_t                 LHCbradlength{0};
  std::string              LHCbmaterialName;
  std::vector<std::string> LHCblocations;
};

class MaterialsDSAgent : public IDataStoreAgent {

public:
  Material*   m_material{nullptr};
  std::string m_materialName;

  MaterialsDSAgent( std::string materialName ) : m_materialName( "/" + materialName ) {}
  virtual bool analyse( IRegistry* pObject, int /* level */ ) override {

    if ( pObject->name() == m_materialName ) {
      m_material = (Material*)pObject->object();
      return false;
    }
    return true;
  }
};

class TGeoTransportSvc : public extends<Service, ITransportSvc> {

private:
  struct AccelCache {};
  mutable std::any m_accelCache{AccelCache{}};

  /* Method to look up materials from the material data service */
  const Material* findMaterial( std::string shortname ) const;

  IDataProviderSvc* m_dataSvc    = nullptr;
  IDataManagerSvc*  m_dataMgrSvc = nullptr;

  ///
public:
  /// constructor
  using base_class::base_class;
  ///

  // Create an instance of the accelerator cache
  std::any createCache() const override { return AccelCache{}; }

public:
  StatusCode initialize() override;
  StatusCode finalize() override;

  double distanceInRadUnits( const Gaudi::XYZPoint& Point1, const Gaudi::XYZPoint& Point2,
                             IGeometryInfo const& geometry, double Threshold,
                             IGeometryInfo* GeometryGuess ) const override;

  /** Estimate the distance between 2 points in units
   *  of radiation length units
   *  Similar to distanceInRadUnits but with an additional accelerator
   *  cache for local client storage. This method, unlike distanceInRadUnits
   *  is re-entrant and thus thread safe.
   *  @param point1 first  point
   *  @param point2 second point
   *  @param threshold threshold value
   *  @param geometry the geometry to be used
   *  @param geometryGuess a guess for navigation
   */
  virtual double distanceInRadUnits_r( const Gaudi::XYZPoint& point1, const Gaudi::XYZPoint& point2,
                                       std::any& accelCache, IGeometryInfo const& geometry, double threshold = 0,
                                       IGeometryInfo* geometryGuess = nullptr ) const override;

  /** general method ( returns the "full history" of the volume
   *  boundary intersections
   * with different material properties between 2 points )
   *  @see ITransportSvc
   *  @see IGeometryInfo
   *  @see ILVolume
   *  @param point               initial point on the line
   *  @param vect                direction vector of the line
   *  @param tickMin             minimal value of line paramater
   *  @param tickMax             maximal value of line parameter
   *  @param intersept           (output) container of intersections
   *  @param threshold           threshold value
   *  @param geometry            the geometry to be used
   *  @param geometryGuess       a guess for navigation
   */
  unsigned long intersections( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vect, const ISolid::Tick& tickMin,
                               const ISolid::Tick& tickMax, ILVolume::Intersections& intersept,
                               IGeometryInfo const& geometry, double threshold,
                               IGeometryInfo* geometryGuess ) const override;

  /** general method ( returns the "full history" of the volume
   *  boundary intersections
   *  with different material properties between 2 points )
   *  Similar to intersections but with an additional accelerator
   *  cache for local client storage. This method, unlike the above
   *  is re-entrant and thus thread safe.
   *  @see ILVolume
   *  @see IPVolume
   *  @see ISolid
   *  @see IGeometryInfo
   *  @see Material
   *  @param point   initial point on the line
   *  @param vector  direction vector of the line
   *  @param tickMin minimal value of line paramater
   *  @param tickMax maximal value of line parameter
   *  @param intersept (output) container of intersections
   *  @param accelCache Accelerator cache
   *  @param threshold threshold value
   *  @param geometry the geometry to be used
   *  @param geometryGuess a guess for navigation
   */
  unsigned long intersections_r( const Gaudi::XYZPoint& point, const Gaudi::XYZVector& vector,
                                 const ISolid::Tick& tickMin, const ISolid::Tick& tickMax,
                                 ILVolume::Intersections& intersept, std::any& accelCache,
                                 IGeometryInfo const& geometry, double threshold = 0,
                                 IGeometryInfo* geometryGuess = nullptr ) const override;

  void getTGeoIntersections( Gaudi::XYZPoint start, Gaudi::XYZPoint end, std::vector<MatIntervalCmp>& intersepts,
                             TGeoManager* m ) const;
};
