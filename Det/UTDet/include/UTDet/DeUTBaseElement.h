/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/DetectorElement.h"
#include "DetDesc/DetectorElementException.h"
#include "Detector/UT/ChannelID.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include <algorithm>
#include <string>
#include <vector>

class DeUTBaseElement : public DetDesc::DetectorElementPlus {

public:
  /** Constructor */
  using DetectorElementPlus::DetectorElementPlus;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** centre in global frame
   * @return global centre
   */
  [[nodiscard]] Gaudi::XYZPoint globalCentre() const { return m_globalCentre; }

  /** convert x,y,z in local frame to global frame
   * @param x local x
   * @param y local y
   * @param z local z
   * @return point
   */
  [[nodiscard]] Gaudi::XYZPoint globalPoint( double x, double y, double z ) const;

  /** detector element id  - for experts only !*/
  [[nodiscard]] LHCb::Detector::UT::ChannelID elementID() const { return m_elementID; }

  /** check whether contains
   *  @param  aChannel channel
   *  @return bool
   */
  [[nodiscard]] virtual bool contains( LHCb::Detector::UT::ChannelID aChannel ) const = 0;

  /** set detector element id  - for experts only !*/
  DeUTBaseElement& setElementID( LHCb::Detector::UT::ChannelID chanID ) {
    m_elementID = chanID;
    return *this;
  }

  /** get the parent of the detector element
  @return parenttype */
  template <typename TYPE>
  [[nodiscard]] auto getParent() const {
    using ParentType = typename TYPE::parent_type;
    auto* parent     = dynamic_cast<ParentType*>( this->parentIDetectorElementPlus() );
    if ( !parent ) { throw GaudiException( "Orphaned detector element", "DeUTBaseElement", StatusCode::FAILURE ); }
    return parent;
  }

  /** retrieve the children
   * @return children
   */
  template <typename TYPE>
  [[nodiscard]] std::vector<typename TYPE::child_type const*> getChildren();

  /**
   * call back for update service
   * @param caller
   * @param object
   * @param mf
   * @param forceUpdate force update
   * @return StatusCode Success or Failure
   */
  template <typename CallerClass, typename ObjectClass>
  StatusCode registerCondition( CallerClass* caller, ObjectClass* object,
                                typename ObjectMemberFunction<CallerClass>::MemberFunctionType mf,
                                bool                                                           forceUpdate = true );

  /**
   * call back for update service
   * @param caller
   * @param conditionName
   * @param mf
   * @param forceUpdate force update
   * @return StatusCode Success or Failure
   */
  template <typename CallerClass>
  StatusCode registerCondition( CallerClass* caller, const std::string& conditionName,
                                typename ObjectMemberFunction<CallerClass>::MemberFunctionType mf,
                                bool                                                           forceUpdate = true );

private:
  StatusCode cachePoint();

  [[nodiscard]] static bool duplicate( std::string_view testString, const std::vector<std::string>& names ) {
    return std::find( names.begin(), names.end(), testString ) != names.end();
  }

  LHCb::Detector::UT::ChannelID m_elementID;
  Gaudi::XYZPoint               m_globalCentre;
};

#include "DetDesc/IGeometryInfo.h"

template <typename TYPE>
inline std::vector<typename TYPE::child_type const*> DeUTBaseElement::getChildren() {

  using cType                     = typename TYPE::child_type;
  const unsigned int        nElem = childIDetectorElements().size();
  std::vector<cType const*> childVector;
  childVector.reserve( nElem );
  std::vector<std::string> names;
  names.reserve( nElem );

  for ( auto* iChild : childIDetectorElements() ) {
    auto* aChild = dynamic_cast<cType const*>( iChild );
    if ( !aChild ) continue;
    if ( !duplicate( aChild->name(), names ) ) {
      names.push_back( aChild->name() );
      childVector.push_back( aChild );
    } else {
      MsgStream msg( msgSvc(), name() );
      msg << MSG::WARNING << "tried to make duplicate detector element !" << aChild->name() << endmsg;
    } // if
  }   // iStation

  if ( childVector.empty() ) {
    throw GaudiException( "Sterile detector element", "DeUTBaseElement", StatusCode::FAILURE );
  }

  return childVector;
}

template <typename CallerClass, typename ObjectClass>
inline StatusCode DeUTBaseElement::registerCondition( CallerClass* caller, ObjectClass* object,
                                                      typename ObjectMemberFunction<CallerClass>::MemberFunctionType mf,
                                                      bool forceUpdate ) {

  // initialize method
  MsgStream  msg( msgSvc(), name() );
  StatusCode sc = StatusCode::SUCCESS;

  try {
    //   if (forceUpdate) updMgrSvc()->invalidate(this);
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Registering conditions" << endmsg;
    updMgrSvc()->registerCondition( caller, object, mf );
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Start first update" << endmsg;
    if ( forceUpdate ) {
      sc = updMgrSvc()->update( caller );
      if ( sc.isFailure() ) { msg << MSG::WARNING << "failed to update detector element " << endmsg; }
    }
  } catch ( DetectorElementException& e ) {
    msg << MSG::ERROR << " Failed to update condition:" << caller->name() << endmsg;
    msg << MSG::ERROR << e << endmsg;
    return StatusCode::FAILURE;
  }
  return sc;
}

template <typename CallerClass>
inline StatusCode DeUTBaseElement::registerCondition( CallerClass* caller, const std::string& conditionName,
                                                      typename ObjectMemberFunction<CallerClass>::MemberFunctionType mf,
                                                      bool forceUpdate ) {

  // initialize method
  MsgStream  msg( msgSvc(), name() );
  StatusCode sc = StatusCode::SUCCESS;

  try {
    // if (forceUpdate) updMgrSvc()->invalidate(this);
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Registering " << conditionName << " condition" << endmsg;
    updMgrSvc()->registerCondition( caller, condition( conditionName ).path(), mf );
    if ( msg.level() <= MSG::DEBUG ) msg << MSG::DEBUG << "Start first update" << endmsg;
    if ( forceUpdate ) {
      sc = updMgrSvc()->update( caller );
      if ( sc.isFailure() ) {
        msg << MSG::WARNING << "failed to update detector element " << condition( conditionName ).path() << endmsg;
      }
    }
  } catch ( DetectorElementException& e ) {
    msg << MSG::ERROR << " Failed to update condition:" << conditionName << " " << caller->name() << endmsg;
    msg << MSG::ERROR << "Message is " << e << endmsg;
    return StatusCode::FAILURE;
  }
  return sc;
}
