/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Detector/UT/ChannelID.h"
#include "UTDet/DeUTBaseElement.h"
#include "UTDet/DeUTLayer.h"
#include <string>
#include <vector>

class DeUTSector;

/** @class DeUTStave DeUTStave.h UTDet/DeUTStave.h
 *
 *  Class representing a UT Stave (13 or 7 sensors)
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

static const CLID CLID_DeUTStave = 9310;

class DeUTStave : public DeUTBaseElement {

public:
  /** parent type */
  using parent_type = const DeUTLayer;

  /** child type */
  using child_type = const DeUTSector;

  /** children */
  using Children = std::vector<child_type*>;

  /** Constructor */
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTStave; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** region where Stave is located
   * @return m_region
   */
  [[nodiscard]] unsigned int detRegion() const { return m_detRegion; }

  /// Workaround to prevent hidden base class function
  [[nodiscard]] const std::type_info& type( const std::string& name ) const override {
    return ParamValidDataObject::type( name );
  }
  /** indicate the Stave type (A/B/C/D)
   * @return m_type
   */
  [[nodiscard]] const std::string& type() const { return m_type; }

  /** indicate the Stave is rotated around Z or not
   * @return m_staveRotZ
   */
  [[nodiscard]] const std::string& staveRotZ() const { return m_staveRotZ; }

  /** first readout sector on Stave
   * @return m_firstSector
   */
  [[nodiscard]] unsigned int firstSector() const { return m_firstSector; }

  /** number of readout sectors expected
   * @return m_numSectors
   */
  [[nodiscard]] unsigned int numSectorsExpected() const { return m_numSectors; }

  /** last readout sector on Stave
   * @return m_firstSector
   */
  [[nodiscard]] unsigned int lastSector() const { return firstSector() + m_sectors.size() - 1u; }

  /** test whether contains channel
   * @param  aChannel test channel
   * @return bool
   */
  [[nodiscard]] bool contains( const LHCb::Detector::UT::ChannelID aChannel ) const override;

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to stream */
  MsgStream& printOut( MsgStream& os ) const override;

  /**  locate sector based on a channel id
  @return  sector */
  [[nodiscard]] const DeUTSector* findSector( const LHCb::Detector::UT::ChannelID aChannel ) const;

  /** locate sector based on a point
  @return sector */
  [[nodiscard]] const DeUTSector* findSector( const Gaudi::XYZPoint& point ) const;

  /** children */
  [[nodiscard]] const DeUTStave::Children& sectors() const { return m_sectors; }

  /** column number */
  [[nodiscard]] unsigned int column() const { return m_column; }

  /** production id */
  [[nodiscard]] unsigned int prodID() const { return m_prodID; }

  /**
   * fraction active channels
   * @return bool fraction active
   */
  [[nodiscard]] double fractionActive() const;

  /** version */
  [[nodiscard]] const std::string& versionString() const { return m_versionString; }

  /** output operator for class DeUTStave
   *  @see DeUTStave
   *  @see MsgStream
   *  @param os      reference to STL output stream
   *  @param aStave reference to DeUTStave object
   */
  friend std::ostream& operator<<( std::ostream& os, const DeUTStave& aStave ) { return aStave.printOut( os ); }

  /** output operator for class DeUTStave
   *  @see DeUTStave
   *  @see MsgStream
   *  @param os      reference to MsgStream output stream
   *  @param aStave reference to DeUTStave object
   */
  friend MsgStream& operator<<( MsgStream& os, const DeUTStave& aStave ) { return aStave.printOut( os ); }

private:
  StatusCode updateProdIDCondition();

  unsigned int m_detRegion   = 0;
  unsigned int m_firstSector = 0;
  unsigned int m_column      = 0;
  std::string  m_type;
  std::string  m_staveRotZ;
  unsigned int m_numSectors = 0;
  parent_type* m_parent     = nullptr;
  Children     m_sectors;
  unsigned int m_prodID        = 0;
  std::string  m_versionString = "DC11";
  std::string  m_prodIDString  = "ProdID";
};

inline bool DeUTStave::contains( const LHCb::Detector::UT::ChannelID aChannel ) const {
  return ( aChannel.detRegion() == m_detRegion && aChannel.sector() >= m_firstSector &&
           aChannel.sector() < m_firstSector + m_sectors.size() ) &&
         m_parent->contains( aChannel );
}

[[deprecated( "first deref" )]] inline std::ostream& operator<<( std::ostream& os, const DeUTStave* aStave ) {
  return os << *aStave;
}
[[deprecated( "first deref" )]] inline MsgStream& operator<<( MsgStream& os, const DeUTStave* aStave ) {
  return os << *aStave;
}
