/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UTDet/DeUTStation.h"
#include "DetDesc/IGeometryInfo.h"
#include "Detector/UT/ChannelID.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTLayer.h"
#include <algorithm>
#include <numeric>

using namespace LHCb;

/** @file DeUTStation.cpp
 *
 *  Implementation of class :  DeUTStation
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

const CLID& DeUTStation::clID() const { return DeUTStation::classID(); }

StatusCode DeUTStation::initialize() {
  // initialize
  return DeUTBaseElement::initialize().andThen( [&] {
    // and the parent
    m_id = param<int>( "stationID" );

    Detector::UT::ChannelID aChan( Detector::UT::ChannelID::detType::typeUT, id(), 0, 0, 0, 0 );
    setElementID( aChan );
    m_nickname = UTNames::StationToString( aChan );

    // get the children
    m_layers = getChildren<DeUTStation>();
  } );
}

std::ostream& DeUTStation::printOut( std::ostream& os ) const {
  os << " Station: " << m_id << std::endl;
  os << " Nickname: " << m_nickname << std::endl;
  return os;
}

MsgStream& DeUTStation::printOut( MsgStream& os ) const {
  os << " Station : " << m_id << endmsg;
  os << " Nickname: " << m_nickname << endmsg;
  return os;
}

const DeUTLayer* DeUTStation::findLayer( const Detector::UT::ChannelID aChannel ) const {
  auto iter =
      std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->contains( aChannel ); } );
  return iter != m_layers.end() ? *iter : nullptr;
}

const DeUTLayer* DeUTStation::findLayer( const Gaudi::XYZPoint& point ) const {
  auto iter =
      std::find_if( m_layers.begin(), m_layers.end(), [&]( const DeUTLayer* l ) { return l->isInside( point ); } );
  return iter != m_layers.end() ? *iter : nullptr;
}

double DeUTStation::fractionActive() const {
  return std::accumulate( m_layers.begin(), m_layers.end(), 0.0,
                          [&]( double f, const DeUTLayer* l ) { return f + l->fractionActive(); } ) /
         double( m_layers.size() );
}
