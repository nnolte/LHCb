/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "DetDesc/Condition.h"
#include "MuonDet/CLID_MuonTell40PCI.h"

#include "GaudiKernel/DataObject.h"

#include <vector>

/**
 *  @author Alessia Satta
 *  @date   2004-01-05
 */
class MuonTell40PCI : public Condition {
public:
  StatusCode initialize() override;
  /// Class ID of this class
  static const CLID& classID() { return CLID_MuonTell40PCI; };
  long               Tell40Number() { return m_Tell40Number; };
  void               setTell40Number( long number ) { m_Tell40Number = number; };
  long               Tell40PCINumber() { return m_PCINumber; };

  void        setTell40PCINumber( long number ) { m_PCINumber = number; };
  void        setStation( long number ) { m_station = number; };
  int         getStation() { return m_station; };
  void        setActiveLinkNumber( long number ) { m_numberOfActiveLink = number; };
  int         activeLinkNumber() { return m_numberOfActiveLink; };
  void        setSourceID( long number ) { m_sourceID = number; };
  long        getSourceID() { return m_sourceID; };
  std::string getODEName( int i ) { return m_ODEName[i]; };
  long        getODENumber( int i ) { return m_ODENumberList[i]; };
  long        getLinkNumber( int i ) { return m_linkConnection[i]; };

  void addLayout( long region, long X, long Y );
  long getTSLayoutX( long i ) { return m_regionLayout[i][0]; };
  long getTSLayoutY( long i ) { return m_regionLayout[i][1]; };
  // long        getODEPosition( long ode, long link, bool hole = true );
  long getODEAndLinkPosition( long ode, long link, bool hole = true );
  void addLinkAndODE( long index, long ode, long link );

private:
  long        m_station;
  long        m_PCINumber;
  long        m_Tell40Number;
  long        m_sourceID;
  std::string m_ODEName[24];
  long        m_regionLayout[4][2];
  long        m_ODENumberList[24];
  long        m_linkConnection[24];
  long        m_numberOfActiveLink = 0;
};
