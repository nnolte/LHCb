/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Detector/Muon/Layout.h"
#include "MuonDet/DeMuonChamber.h"
#include "MuonDet/DeMuonDetector.h"

using MuonLayout = LHCb::Detector::Muon::Layout;

/**
 *  @author Alessio Sarti
 *  @date   2005-10-06
 */
class MuonChamberLayout : public DetDesc::DetectorElementPlus {
public:
  /// Standard constructor
  MuonChamberLayout( MuonLayout R1, MuonLayout R2, MuonLayout R3, MuonLayout R4, bool isM1defined );

  // Void constructor: calls the previous one with default grid Layout
  MuonChamberLayout();

  // Fill the vector with all the chambers
  StatusCode initialize() override;

  // Copy function
  void Copy( MuonChamberLayout& lay );

  // Fill the vector with all the chambers
  std::vector<DeMuonChamber*> fillChambersVector( DeMuonDetector const& muonDet );

  // Find the most likely chamber for a given x,y,station set
  void chamberMostLikely( float x, float y, int station, int& chmb, int& reg ) const;

  // Return the tiles of the neighbor Chambers
  std::vector<int> neighborChambers( float myX, float myY, int stat, int x_direction, int y_direction ) const;

  // Return the tiles of the neighbor Chambers
  void returnChambers( int sta, float st_x, float st_y, int x_dir, int y_dir, std::vector<int>& regs,
                       std::vector<int>& chs ) const;

  // Convert tiles in chambers
  std::vector<DeMuonChamber*> createChambersFromTile( std::vector<LHCb::Detector::Muon::TileID> mytiles );

  // Returns the chamber number for a shift in X and Y direction
  void chamberXY( int sx, int sy, int shx, int shy, int reg, std::vector<int>& chamberNumber ) const;

  // Returns the Tile for a given chamber
  LHCb::Detector::Muon::TileID tileChamber( DeMuonChamber* chmb ) const;

  // Returns the Tile for a given chamber number
  LHCb::Detector::Muon::TileID tileChamberNumber( int sta, int reg, int chmbNum ) const;

  std::optional<DeMuonDetector::TilePosition> position( const LHCb::Detector::Muon::TileID& tile );

  // Fill the system grids for a chamber in a given region
  StatusCode fillSystemGrids( DeMuonChamber* deChmb, int vIdx, int reg );

  // Returns the region for a given chamber with numbering scheme
  // defined in the MuonGeometry.h file
  int findRegion( int chamber ) const;

  // Sets the xS, yS reference values (needed for grid computation)
  void setGridStep();

  // Checks if a region lowering is needed
  bool shouldLowReg( int idX, int idY, int reg ) const;

  // Returns the layout
  MuonLayout layout( int region );

  void setXGrid( std::vector<unsigned int> xg );

  void setYGrid( std::vector<unsigned int> yg );

  // Sets the layout
  void setLayout( int region, MuonLayout lay );

  // Assigna a given encode to the chamber
  void setChamberInGrid( int enc, int num );

  // Reconstruct the encode of a given chamber
  int getEncode( int idx, int idy, int reg );

  // Converts the chamber tile into a chamber number
  int getChamberNumber( const LHCb::Detector::Muon::TileID& tile );

  // Function for chamber x,y,z retrieval from tile info
  std::optional<DeMuonDetector::TilePosition> getXYZChamberTile( const LHCb::Detector::Muon::TileID& tile,
                                                                 bool                                toGlob );

  /// get position of a "named" chamber
  /// NOTE: station and region are indexed from 0 (C style)
  /// chamberNum is the real chamber number (from 0)
  std::optional<DeMuonDetector::TilePosition> getXYZChamber( const int& station, const int& region,
                                                             const int& chamberNum, bool toGlob );

  /// get position of chamber or gas gap with caching of results and pointers
  /// NOTE: station, region and gapNum are indexed from 0 (C style)
  /// chamberNum is the real chamber number (from 0)
  std::optional<DeMuonDetector::TilePosition> getXYZ( const int& station, const int& region, const int& chamberNum,
                                                      const int& gapNum, bool toGlob );

  /// get xyz of specific pad
  std::optional<DeMuonDetector::TilePosition> getXYZPad( const LHCb::Detector::Muon::TileID& tile );

  /// get postion of logical channel (may be multiple chambers)
  std::optional<DeMuonDetector::TilePosition> getXYZLogical( const LHCb::Detector::Muon::TileID& tile );

  /// get xyz of twelfth (useful for defining regions)
  std::optional<DeMuonDetector::TilePosition> getXYZTwelfth( const LHCb::Detector::Muon::TileID& tile );

  /// returns the chamber number (same for each station) on the corner of
  /// the region
  int getTwelfthCorner( const int& region, const int& twelfth, const int& chamberNum );

  /// get the xIndex and yIndex of the corner chamber in the twelfth
  void getTwelfthCornerIndex( const int& region, const int& twelfth, const int& chamberNum, int& xPos, int& yPos );

  void localToglobal( const IGeometryInfoPlus* gInfo, const Gaudi::XYZPoint& cent, const Gaudi::XYZPoint& corn,
                      double& dx, double& dy, double& dz );

  /// get the chamber number (vector) from the MuonTile
  std::vector<unsigned int> Tile2ChamberNum( const LHCb::Detector::Muon::TileID& tile );

  /// get the chamber number (vector) from the logical channel tile
  std::vector<unsigned int> Logical2ChamberNum( const LHCb::Detector::Muon::TileID& tile );

  /// get the chamber number (vector) from the twelfth-chamber tile
  std::vector<unsigned int> Twelfth2ChamberNum( const LHCb::Detector::Muon::TileID& tile );

private:
  // Returns the grid indexes for a given X,Y couple
  void gridPosition( float x, float y, int iS, int& idx, int& idy, int& reg ) const;

private:
  // Chambers
  std::vector<int>            m_offSet;
  std::vector<unsigned int>   m_cgX;
  std::vector<unsigned int>   m_cgY;
  std::vector<DeMuonChamber*> m_ChVec;
  std::vector<unsigned int>   m_chaVect;

  // Smallest chmb dimensions
  std::vector<float> m_xS;
  std::vector<float> m_yS;

  MuonLayout m_layout[4];

  // ChamberVector as a function of the ixds in the region grid
  std::vector<int> m_chamberGrid;

  // size of logical channels
  std::vector<unsigned int> m_logVertGridX;
  std::vector<unsigned int> m_logVertGridY;
  std::vector<unsigned int> m_logHorizGridX;
  std::vector<unsigned int> m_logHorizGridY;
  // size of pads
  std::vector<unsigned int> m_padGridX;
  std::vector<unsigned int> m_padGridY;

  // definition of M1 according to DB, filled in DeMuonDetector::initialize()
  bool m_isM1defined = false;
};

// -----------------------------------------------------------------------------
//   end of class
// -----------------------------------------------------------------------------

// Returns the Layout
inline MuonLayout MuonChamberLayout::layout( int region ) {
  // Region index starts from 0 (R1 -> idx = 0)
  return m_layout[region];
}

// Sets the xGrid
inline void MuonChamberLayout::setXGrid( std::vector<unsigned int> xg ) { m_cgX = xg; }

// Set chamber in grid
inline void MuonChamberLayout::setChamberInGrid( int enc, int num ) { m_chamberGrid.at( enc ) = num; }

// Sets the Layout
inline void MuonChamberLayout::setYGrid( std::vector<unsigned int> yg ) { m_cgY = yg; }

// Sets the Layout
inline void MuonChamberLayout::setLayout( int region, MuonLayout lay ) {
  // Region index starts from 0 (R1 -> idx = 0)
  m_layout[region] = lay;
}

// Gets the encoding
inline int MuonChamberLayout::getEncode( int idx, int idy, int reg ) {
  // Returns  the encoding of a given chamber
  int enc = idx + 4 * m_cgX.at( reg ) * idy + m_offSet.at( reg );
  return enc;
}
