/***************************************************************************** \
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "MuonDet/MuonTell40PCI.h"

#include <string>

StatusCode MuonTell40PCI::initialize() {
  return Condition::initialize().andThen( [&] {
    const auto TNumb = this->param<int>( "Tell40Number" );
    setTell40Number( TNumb );
    const auto PCINumb = this->param<int>( "PCINumber" );
    setTell40PCINumber( PCINumb );
    const auto tellStation = this->param<int>( "StationNumber" );
    setStation( tellStation );
    const auto ALinkNumb = this->param<int>( "ActiveLinkNumber" );
    setActiveLinkNumber( ALinkNumb );
    const auto OdeList = this->paramVect<int>( "ODEList" );
    const auto OdeLink = this->paramVect<int>( "ODELink" );

    if ( OdeList.size() != OdeLink.size() ) { return StatusCode::FAILURE; }
    int  internal_active = 0;
    long counter         = 0;
    auto link            = OdeLink.begin();
    for ( auto name = OdeList.begin(); name < OdeList.end() && link < OdeLink.end(); name++, link++, counter++ ) {
      addLinkAndODE( counter, *name, *link );
      m_ODEName[counter] = "NODEBoard" + std::to_string( *name );
      if ( ( *name ) != 0 ) internal_active++;
    }
    if ( internal_active != ALinkNumb ) return StatusCode::FAILURE;
    const auto LayoutList = this->paramVect<int>( "TSLayout" );
    if ( LayoutList.size() != 12 ) return StatusCode::FAILURE;
    addLayout( ( LayoutList[0] ), ( LayoutList[1] ), ( LayoutList[2] ) );
    addLayout( ( LayoutList[3] ), ( LayoutList[4] ), ( LayoutList[5] ) );
    addLayout( ( LayoutList[6] ), ( LayoutList[7] ), ( LayoutList[8] ) );
    addLayout( ( LayoutList[9] ), ( LayoutList[10] ), ( LayoutList[11] ) );

    return StatusCode::SUCCESS;
  } );
}

void MuonTell40PCI::addLinkAndODE( long index, long ode, long link ) {
  m_ODENumberList[index]  = ode;
  m_linkConnection[index] = link;
}

void MuonTell40PCI::addLayout( long region, long X, long Y ) {
  m_regionLayout[region][0] = X;
  m_regionLayout[region][1] = Y;
}

long MuonTell40PCI::getODEAndLinkPosition( long ode, long link, bool hole ) {
  long no_hole = 0;
  for ( int i = 0; i < 24; i++ ) {
    if ( m_ODENumberList[i] == ode && m_linkConnection[i] == link ) { return hole ? i : no_hole; }
    if ( m_ODENumberList[i] > 0 ) no_hole++;
  }
  return -1;
}
