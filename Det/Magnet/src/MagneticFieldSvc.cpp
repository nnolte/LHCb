/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "MagnetCondLocations.h"
#include <Core/MagneticFieldGrid.h>
#include <Core/MagneticFieldGridReader.h>
#include <DetDesc/Condition.h>
#include <DetDesc/IConditionDerivationMgr.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/IUpdateManagerSvc.h>
#include <GaudiKernel/MsgStream.h>
#include <GaudiKernel/Point3DTypes.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/StdArrayAsProperty.h>
#include <GaudiKernel/SystemOfUnits.h>
#include <GaudiKernel/Vector3DTypes.h>
#include <Kernel/ILHCbMagnetSvc.h>
#include <Magnet/DeMagnet.h>
#include <cstdlib>
#include <fstream>
#include <map>
#include <sstream>
#include <string>
#include <vector>

using MagneticFieldGridReader = LHCb::Magnet::MagneticFieldGridReader;

/** @class MagneticFieldSvc MagneticFieldSvc.h
 *  A service for finding the magnetic field vector at a given
 *  point in space. Based on original implementation by I.Last and
 *  G.Gracia via Gaudi_gufld (modified from SICBMC)
 *
 *  @author Edgar De Oliveira
 *  @date   2002-05-21
 *  Updated and further developped - Adlene Hicheur
 */

/** @file MagneticFieldSvc.cpp
 *  Implementation of MagneticFieldSvc class
 *
 *  @author Edgar De Oliveira
 *  @date   2002-05-21
 *  Updated and further developed - Adlene Hicheur
 *  Updated and further developed - Marco Cattaneo
 */

class MagneticFieldSvc : public extends<Service, ILHCbMagnetSvc> {

public:
  /// Standard Constructor.
  /// @param  name   String with service name
  /// @param  svc    Pointer to service locator interface
  MagneticFieldSvc( const std::string& name, ISvcLocator* svc );

  /// Initialise the service (Inherited Service overrides)
  StatusCode initialize() override;

  /// Finalise the service (Inherited Service overrides)
  StatusCode finalize() override;

  using ILHCbMagnetSvc::fieldVector;
  /** Implementation of IMagneticFieldSvc interface.
   * @param[in]  xyz Point at which magnetic field vector will be given
   * @return Magnetic field vector.
   */
  Gaudi::XYZVector fieldVector( const Gaudi::XYZPoint& xyz ) const override;

  /// Returns the field grid
  const LHCb::Magnet::MagneticFieldGrid* fieldGrid() const override { return &m_magFieldGrid; }

  bool useRealMap() const override; ///< True is using real map

  double signedRelativeCurrent() const override;

  // True if the down polarity map is loaded
  bool isDown() const override;

private:
  StatusCode initializeWithCondDB();    ///< default get magnet data from CondDB
  StatusCode initializeWithoutCondDB(); ///< alternative get magnet data from job options
  StatusCode i_updateConditions();      ///< Reads from conditions

private:
  // Properties to configure the service
  Gaudi::Property<bool>   m_UseConditions{this, "UseConditions",
                                        true}; ///< Get data from CondDB or options. Default CondDB
  Gaudi::Property<bool>   m_UseSetCurrent{this, "UseSetCurrent", false}; ///< Use Set or Measured current. Default false
  Gaudi::Property<double> m_nominalCurrent{this, "NominalCurrent", 5850,
                                           "Nominal magnet current in Amps"}; ///< Nominal magnet current to normalise
                                                                              ///< rescaling
  Gaudi::Property<std::string> m_mapFilePath{
      this, "FieldMapPath", {}, "Directory where field map files are located, including trailing separator"};

  // Special properties to use constant field (and no condDB!)
  Gaudi::Property<bool> m_useConstField{this, "UseConstantField", false}; ///< Job option to use constant field
  Gaudi::Property<std::array<double, 3>> m_constFieldVector = {
      this, "ConstantFieldVector", {0., 0., 0.}}; ///< Option for constant field value

  // Properties to over-ride values in CondDB
  Gaudi::Property<std::vector<std::string>> m_mapFileNames{
      this, "FieldMapFiles", {}, "Vector of file names for the field map. If set, over-rides CondDB value"};
  Gaudi::Property<bool>   m_forcedToUseDownMap{this, "ForceToUseDownMap", false,
                                             "Force to use down map. If set, over-rides CondDB value"};
  Gaudi::Property<bool>   m_forcedToUseUpMap{this, "ForceToUseUpMap", false,
                                           "Force to use up map. If set, over-rides CondDB value"};
  Gaudi::Property<double> m_forcedScaleFactor{
      this, "ForcedSignedCurrentScaling", 9999,
      "Factor by which to rescale the field map. If set, over-rides CondDB value"};
  bool m_mapFromOptions = false;

  // Private data

  Condition* m_mapFilesUpPtr   = nullptr; ///< Pointer to FieldMapFilesUp condition
  Condition* m_mapFilesDownPtr = nullptr; ///< Pointer to FieldMapFilesDown condition
  Condition* m_scaleUpPtr      = nullptr; ///< Pointer to ScaleUp condition
  Condition* m_scaleDownPtr    = nullptr; ///< Pointer to ScaleDown condition
  Condition* m_currentPtr      = nullptr; ///< Pointer to Measured or Set condition

  SmartIF<IUpdateManagerSvc> m_updMgrSvc; ///< Pointer to UpdateManagerSvc

  LHCb::Magnet::MagneticFieldGrid m_magFieldGrid;

  bool m_isDown = false; ///< Cache the field polarity

  double m_signedCurrent{0}; ///< Cache the field current

private:
  // update the cached field polarity and current
  void cacheFieldConstants() {
    // Polarity
    m_isDown = m_magFieldGrid.fieldVectorClosestPoint( {0, 0, 5200} ).y() < 0;
    // current
    const int sign  = ( m_isDown ? -1 : +1 );
    m_signedCurrent = std::abs( m_magFieldGrid.scaleFactor() ) * sign;
  }
};

DECLARE_COMPONENT( MagneticFieldSvc )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MagneticFieldSvc::MagneticFieldSvc( const std::string& name, ISvcLocator* svc ) : extends( name, svc ) {
  const char* fmroot = std::getenv( "FIELDMAPROOT" );
  if ( fmroot ) {
    m_mapFilePath = fmroot + std::string{"/cdf/"};
  } else {
    m_mapFilePath = "";
  }
}

//=============================================================================
// Initialize
//=============================================================================
StatusCode MagneticFieldSvc::initialize() {
  StatusCode status = extends::initialize();
  if ( status.isFailure() ) return status;

#ifdef USE_DD4HEP
  error() << "MagneticFieldSvc cannot be used in DD4hep builds" << endmsg;
  return StatusCode::SUCCESS;
#else
  m_updMgrSvc = service( "UpdateManagerSvc" );
  if ( !m_updMgrSvc ) {
    error() << "Cannot find the UpdateManagerSvc" << endmsg;
    return StatusCode::FAILURE;
  }

  if ( m_useConstField.value() ) {
    // Constant field requested, do not use any field map
    warning() << "using constant magnetic field with field vector " << m_constFieldVector << " (Tesla)" << endmsg;

    MagneticFieldGridReader reader( m_mapFilePath );
    reader.fillConstantField( {m_constFieldVector[0] * Gaudi::Units::tesla, m_constFieldVector[1] * Gaudi::Units::tesla,
                               m_constFieldVector[2] * Gaudi::Units::tesla},
                              m_magFieldGrid );

    // register anyway with UpdateManagerSvc, so clients can register callbacks
    // transparently, even if they are never going to be called
    m_updMgrSvc->registerCondition( this );

  } else {

    if ( m_UseConditions.value() ) {
      // Normal case, use conditions database
      status = initializeWithCondDB();
    } else {
      status = initializeWithoutCondDB();
      // register anyway with UpdateManagerSvc, so clients can register callbacks
      // transparently, even if they are never going to be called
      m_updMgrSvc->registerCondition( this );
    }
  }

  // Now register the derived condition DeMagnet
  // DetDesc only
  // First get the condition derivation manager
  auto cdm = m_updMgrSvc.as<LHCb::DetDesc::IConditionDerivationMgr>();
  if ( !cdm ) {
    error() << "cannot access IConditionDerivationMgr" << endmsg;
    return StatusCode::FAILURE;
  }
  LHCb::DetDesc::addConditionDerivation( *cdm, {MagnetCondLocations::Measured}, LHCb::Det::Magnet::det_path,
                                         [this]( const ParamValidDataObject& ) { return DeMagnet( this ); } );
  // update the cached field constants
  cacheFieldConstants();

  return status;
#endif
}

//=============================================================================
// Finalize
//=============================================================================
StatusCode MagneticFieldSvc::finalize() {
  m_updMgrSvc.reset();
  return extends::finalize();
}

//=============================================================================
bool MagneticFieldSvc::useRealMap() const
//=============================================================================
{
  return ( m_mapFileNames.size() == 4 );
}

//=============================================================================
StatusCode MagneticFieldSvc::initializeWithCondDB()
//=============================================================================
{

  // Polarity and current
  m_updMgrSvc->registerCondition( this,
                                  m_UseSetCurrent.value() ? MagnetCondLocations::Set : MagnetCondLocations::Measured,
                                  &MagneticFieldSvc::i_updateConditions, m_currentPtr );

  // FieldMap file name(s). If not over-ridden by options, get from CondDB
  if ( !m_mapFileNames.empty() ) {
    warning() << "Requested condDB but using manually set field map file name(s) = " << m_mapFileNames.value()
              << endmsg;

    m_mapFromOptions = true;
    MagneticFieldGridReader reader( m_mapFilePath );
    if ( !( m_mapFileNames.size() == 1 ? reader.readDC06File( m_mapFileNames.value().front(), m_magFieldGrid )
                                       : reader.readFiles( m_mapFileNames.value(), m_magFieldGrid ) ) ) {
      return StatusCode::FAILURE;
    }
  } else {
    m_updMgrSvc->registerCondition( this, MagnetCondLocations::FieldMapFilesUp, &MagneticFieldSvc::i_updateConditions,
                                    m_mapFilesUpPtr );
    m_updMgrSvc->registerCondition( this, MagnetCondLocations::FieldMapFilesDown, &MagneticFieldSvc::i_updateConditions,
                                    m_mapFilesDownPtr );
  }

  // Scaling factor. If not over-ridden by options, get it from Options
  if ( m_forcedScaleFactor < 9998. ) {
    warning() << "Requested condDB but using manually set signed scale factor = " << m_forcedScaleFactor.value()
              << endmsg;
    m_magFieldGrid.setScaleFactor( m_forcedScaleFactor );
  } else {
    m_updMgrSvc->registerCondition( this, MagnetCondLocations::ScaleUp, &MagneticFieldSvc::i_updateConditions,
                                    m_scaleUpPtr );
    m_updMgrSvc->registerCondition( this, MagnetCondLocations::ScaleDown, &MagneticFieldSvc::i_updateConditions,
                                    m_scaleDownPtr );
  }

  // Initialize the service using the current conditions values
  return m_updMgrSvc->update( this );
}

//=============================================================================
StatusCode MagneticFieldSvc::initializeWithoutCondDB()
//=============================================================================
{
  warning() << "Not using CondDB, entirely steered by options" << endmsg;

  if ( m_mapFileNames.empty() ) {
    error() << "Field Map filename(s) not set" << endmsg;
    return StatusCode::FAILURE;
  }

  m_mapFromOptions = true;

  double scaleFactor = m_forcedScaleFactor;

  if ( m_forcedScaleFactor > 9998. ) {
    scaleFactor = 1.;
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Scale factor set to default = " << scaleFactor << endmsg;
  }

  m_magFieldGrid.setScaleFactor( scaleFactor );

  // update the field
  MagneticFieldGridReader reader( m_mapFilePath );
  return ( m_mapFileNames.size() == 1 ? reader.readDC06File( m_mapFileNames.value().front(), m_magFieldGrid )
                                      : reader.readFiles( m_mapFileNames, m_magFieldGrid ) )
             ? StatusCode::SUCCESS
             : StatusCode::FAILURE;
}

//=============================================================================
StatusCode MagneticFieldSvc::i_updateConditions()
//=============================================================================
{
  if ( msgLevel( MSG::DEBUG ) ) debug() << "updateConditions called" << endmsg;

  if ( m_forcedToUseDownMap.value() && m_forcedToUseUpMap.value() )
    warning() << "inconsistent settings, forced to use Down AND Uo map = " << endmsg;

  double polarity = 0;
  if ( m_forcedToUseDownMap.value() ) { polarity = -1.0; }
  if ( m_forcedToUseUpMap.value() ) { polarity = +1.0; }
  if ( !m_forcedToUseDownMap.value() && !m_forcedToUseUpMap.value() ) {
    polarity = m_currentPtr->param<int>( "Polarity" );
  }

  // Update the scale factor
  if ( m_forcedScaleFactor > 9998. ) {
    const double current = m_currentPtr->param<double>( "Current" );

    const auto& coeffs = ( polarity > 0 ? m_scaleUpPtr : m_scaleDownPtr )->param<std::vector<double>>( "Coeffs" );

    m_magFieldGrid.setScaleFactor( coeffs[0] + ( coeffs[1] * ( current / m_nominalCurrent ) ) );
  }

  // Update the field map file
  StatusCode sc = StatusCode::SUCCESS;
  if ( !m_mapFromOptions ) {
    // Convention used: positive polarity is "Up" (+y), negative is "Down" (-y)
    auto files = ( polarity > 0 ? m_mapFilesUpPtr : m_mapFilesDownPtr )->param<std::vector<std::string>>( "Files" );

    // test the cache
    if ( m_mapFileNames != files ) {
      // update the cache
      m_mapFileNames = files;
      // update the field
      MagneticFieldGridReader reader( m_mapFilePath );
      sc = ( m_mapFileNames.size() == 1 ? reader.readDC06File( m_mapFileNames.value().front(), m_magFieldGrid )
                                        : reader.readFiles( m_mapFileNames.value(), m_magFieldGrid ) )
               ? StatusCode::SUCCESS
               : StatusCode::FAILURE;
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Field map files updated: " << m_mapFileNames << endmsg;
    }
  }

  // update the cached field constants
  cacheFieldConstants();

  // Print a message
  static std::map<std::string, unsigned long long> nUpdates;
  std::ostringstream                               mess;
  mess << "Map scaled by factor " << m_magFieldGrid.scaleFactor() << " with polarity internally used: " << polarity
       << " signed relative current: " << signedRelativeCurrent();
  if ( nUpdates[mess.str()]++ < 2 ) {
    info() << mess.str() << endmsg;
  } else if ( nUpdates[mess.str()] == 2 ) {
    info() << "Message '" << mess.str() << "' is SUPPRESSED" << endmsg;
  }

  return sc;
}

//=============================================================================

Gaudi::XYZVector MagneticFieldSvc::fieldVector( const Gaudi::XYZPoint& xyz ) const {
  return m_magFieldGrid.fieldVectorLinearInterpolation( xyz );
}

//=============================================================================

bool MagneticFieldSvc::isDown() const { return m_isDown; }

//=============================================================================

double MagneticFieldSvc::signedRelativeCurrent() const { return m_signedCurrent; }

//=============================================================================
