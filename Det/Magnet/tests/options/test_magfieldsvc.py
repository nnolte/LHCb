#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
from Gaudi.Configuration import *
from Configurables import MagFielSvcTester, LHCbApp, CondDB
from PRConfig import TestFileDB

# - Algorithms
topalgs = [
    MagFielSvcTester("MagFielSvcTester"),
]

# Application manager
app = LHCbApp(EvtMax=1)
CondDB().Upgrade = True

ApplicationMgr(TopAlg=topalgs)
TestFileDB.test_file_db['MiniBrunel_2018_MinBias_FTv4_DIGI'].run(
    configurable=app)
