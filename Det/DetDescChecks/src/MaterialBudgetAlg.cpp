/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/ITransportSvc.h"
#include "LHCbAlgs/Consumer.h"

#include <DetDesc/DetectorElement.h>
#include <DetDesc/GenericConditionAccessorHolder.h>

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/VectorsAsProperty.h"
#include "LHCbAlgs/Consumer.h"
#include "boost/timer/progress_display.hpp"
#include <algorithm>
#include <functional>
#include <string>

namespace DetDesc {
  /** @class MaterialBudget MaterialBudgetAlg.h
   *
   *  Simple algorithm, which perform the evaluation of the
   *  material budget using Transport Service ("LHCb's GEANE")
   *
   *  The algorithm produces 2 2-dimentional plots with the
   *  evaluationfo the material budget ( in units of radiation length)
   *  between origin vertex and x-y point at the reference plane.
   *  The plot need to be normalized properly. Normalization is given
   *  by histogram number 2. The proper normalization is achieved e.g in PAW
   *
   *  @code
   *
   *  PAW> hi/oper/div 1 2 10
   *  PAW> hi/plot     10     lego1
   *
   *  @endcode
   *
   *  The full list of algorithm properties and their default values:
   *  <ul>
   *  <li> @p ShootingPoint  Position of the "origin vertex"
   *                             (default value <tt>{ 0. , 0. , 0. }</tt>)
   *  <li> @ Shots           Number of random shots per event
   *                                           (default value is @p  1000 )
   *  <li> @ zPlane          @p Z -position of the reference plane
   *                                           (default value is @p 12000 )
   *  <li> @ xMax            Maximal value of @p X at reference plane
   *                                           (default value is @p  4000 )
   *  <li> @ yMax            Maximal value of @p Y at reference plane
   *                                           (default value is @p  3000 )
   *  <li> @ xMin            Minimal value of @p X at reference plane
   *                                           (default value is @p     0 )
   *  <li> @ yMin            Minimal value of @p Y at reference plane
   *                                           (default value is @p     0 )
   *  <li> @ nBx             Number of bins in @p X -direction
   *                                           (default value is @p    50 )
   *  <li> @ nBy             Number of bins in @p Y -direction
   *                                           (default value is @p    50 )
   *  <li> @ TransportSvc    The name of Gaudi Transport Service
   *                                  (default value is @p "TransportSvc" )
   *  <li> @ RndmService     The name of Gaudi Random Numbers Service
   *                                    (default value is @p "RndmGenSvc" )
   *  </ul>
   *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
   *  @date   23/04/2002
   *
   *  added by W. Pokorski:
   *  - Grid      flag to switch between random shooting (0) and grid (1)
   *  - xbinref   x-size of the reference bin (to be scaled as m_z/m_zref)
   *  - ybinref   y-size of the reference bin (to be scaled as m_z/m_zref)
   *  - zref      reference z position (at which xbinref and ybinref
   *                         are given)
   */
  class MaterialBudget
      : public LHCb::Algorithm::Consumer<void( const DetectorElement& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DetectorElement>> {
  public:
    MaterialBudget( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {KeyValue{"DELHCbPath", LHCb::standard_geometry_top}} ) {}
    StatusCode initialize() override;
    void       operator()( const DetectorElement& lhcb ) const override;

  private:
    void makeRandomShots( IGeometryInfo const& geometry ) const;
    void makeGridShots( IGeometryInfo const& geometry ) const;
    void makePsrapShots( IGeometryInfo const& geometry ) const;

    ServiceHandle<ITransportSvc> m_trSvc{this, "TransportService", "TransportSvc", "transport service"};

    Gaudi::Property<Gaudi::XYZPoint> m_vertex{this, "ShootingPoint", {}, "Position of shooting vertex"};
    Gaudi::Property<int>             m_shots{this, "Shots", 1000, "Number of random short per event"};
    Gaudi::Property<double>          m_z{this, "zPlane", 12 * Gaudi::Units::meter, "Z-position of reference plane"};
    Gaudi::Property<double>          m_xMax{this, "xMax", 4 * Gaudi::Units::meter, "Maximal X at reference plane"};
    Gaudi::Property<double>          m_yMax{this, "yMax", 3 * Gaudi::Units::meter, "Maximal Y at reference plane"};
    Gaudi::Property<double>          m_xMin{this, "xMin", 0 * Gaudi::Units::meter, "Minimal X at reference plane"};
    Gaudi::Property<double>          m_yMin{this, "yMin", 0 * Gaudi::Units::meter, "Minimal Y at reference plane"};
    Gaudi::Property<double>          m_etaMax{this, "etaMax", 5.5};
    Gaudi::Property<double>          m_phiMax{this, "phiMax", 270.};
    Gaudi::Property<double>          m_etaMin{this, "etaMin", 2.0};
    Gaudi::Property<double>          m_phiMin{this, "phiMin", 90.};
    Gaudi::Property<int>             m_nbx{this, "nBx", 360};
    Gaudi::Property<int>             m_nby{this, "nBy", 300};
    Gaudi::Property<bool> m_grid{this, "Grid", true, "If set, enables  \"grid shooting\" (added by W. Pokorski)"};
    Gaudi::Property<bool> m_psrap{
        this, "Rapidity", false,
        "parameter to switch between cartesian (false, default) and pseudorapidity (true) flat scans"};
    Gaudi::Property<double> m_xbinref{
        this, "xbinref", 20.0,
        "Reference grid in m_grid is true. The actual grid will be scaled according to m_z/m_zref"};
    Gaudi::Property<double> m_ybinref{
        this, "ybinref", 20.0,
        "Reference grid in m_grid is true. The actual grid will be scaled according to m_z/m_zref"};
    Gaudi::Property<double> m_zref{
        this, "zref", 10000.0,
        "Reference grid in m_grid is true. The actual grid will be scaled according to m_z/m_zref"};
  };

} // namespace DetDesc

StatusCode DetDesc::MaterialBudget::initialize() {
  return GaudiHistoAlg::initialize().andThen( [&]() -> StatusCode {
    Assert( 0 != randSvc(), "IRndmGenSvc* points to NULL!" );
    // transform parameters
    if ( m_xMin > m_xMax ) { std::swap( m_xMin, m_xMax ); }
    if ( m_yMin > m_yMax ) { std::swap( m_yMin, m_yMax ); }
    // adjust number of bins
    if ( 0 >= m_nbx ) { m_nbx = 50; }
    if ( 0 >= m_nby ) { m_nby = 50; }
    if ( m_grid && m_psrap ) { return Error( " Asked for X-Y and Eta-Phi scans " ); }
    // for grid we calculate x/yMin/Max from the size and number of bins
    if ( m_grid ) {
      m_xMin = -( m_xbinref * m_nbx * m_z ) / ( 2.0 * m_zref );
      m_yMin = -( m_ybinref * m_nby * m_z ) / ( 2.0 * m_zref );
      m_xMax = -m_xMin;
      m_yMax = -m_yMin;
    }
    if ( m_psrap ) {
      m_xMin = m_etaMin;
      m_yMin = m_phiMin;
      m_xMax = m_etaMax;
      m_yMax = m_phiMax;
    }
    return StatusCode::SUCCESS;
  } );
}

void DetDesc::MaterialBudget::operator()( const DetectorElement& lhcb ) const {
  if ( m_grid ) {
    makeGridShots( *lhcb.geometry() );
  } else if ( m_psrap ) {
    makePsrapShots( *lhcb.geometry() );
  }
  makeRandomShots( *lhcb.geometry() );
}

void DetDesc::MaterialBudget::makeRandomShots( IGeometryInfo const& geometry ) const {
  // get random number generator
  Rndm::Numbers x( randSvc(), Rndm::Flat( m_xMin, m_xMax ) );
  Rndm::Numbers y( randSvc(), Rndm::Flat( m_yMin, m_yMax ) );
  // Accelerator cache for transport service
  auto accelCache = m_trSvc->createCache();
  // make 'shots'
  boost::timer::progress_display progress( m_shots );
  for ( int shot = 0; shot < m_shots; ++shot, ++progress ) {
    // point at reference plane
    const Gaudi::XYZPoint point( x(), y(), m_z );
    // evaluate the distance
    const double dist = m_trSvc->distanceInRadUnits_r( m_vertex, point, accelCache, geometry );

    // fill material budget histogram
    plot2D( point.x(), point.y(), 1, "Material Budget", m_xMin, m_xMax, m_yMin, m_yMax, m_nbx, m_nby,
            dist ); // weight
    // fill the normalization histogram
    plot2D( point.x(), point.y(), 2, "Budget Normalization", m_xMin, m_xMax, m_yMin, m_yMax, m_nbx, m_nby );
  }
}

void DetDesc::MaterialBudget::makeGridShots( IGeometryInfo const& geometry ) const {
  // put in a transformation to go from XY to Eta-Phi.
  const double dxgrid = m_xbinref * m_z / m_zref;
  const double dygrid = m_ybinref * m_z / m_zref;
  // xx and yy refer to the two non-Z dimensions, be them cartesian or
  // whatever. x and y are cartesian.
  // Accelerator cache for transport service
  auto accelCache = m_trSvc->createCache();
  /// make a progress bar
  boost::timer::progress_display progress( m_nbx * m_nby );
  for ( double y = m_yMin + dygrid / 2; y <= m_yMax; y += dygrid ) {
    for ( double x = m_yMin + dxgrid / 2; x <= m_xMax; x += dxgrid ) {
      // "shooting" point at the reference plane
      const Gaudi::XYZPoint point( x, y, m_z );
      // evaluate the distance
      const double dist = m_trSvc->distanceInRadUnits_r( m_vertex, point, accelCache, geometry );
      // fill material budget histogram
      plot2D( point.x(), point.y(), 1, "Material Budget", m_xMin, m_xMax, m_yMin, m_yMax, m_nbx, m_nby,
              dist ); // weight
      // fill the "normalization" histogram  (must be flat)
      plot2D( point.x(), point.y(), 2, "Budget Normalization", m_xMin, m_xMax, m_yMin, m_yMax, m_nbx, m_nby );
      // show the progress
      ++progress;
    }
  }
}

void DetDesc::MaterialBudget::makePsrapShots( IGeometryInfo const& geometry ) const {
  // put in a transformation to go from XY to Eta-Phi.
  const double dxgrid = ( m_xMax - m_xMin ) / m_nbx;
  const double dygrid = ( m_yMax - m_yMin ) / m_nby;
  // Accelerator cache for transport service
  auto accelCache = m_trSvc->createCache();
  /// make a pregress bar
  boost::timer::progress_display progress( m_nbx * m_nby );
  for ( double yy = m_yMin + dygrid / 2; yy <= m_yMax; yy += dygrid ) {
    for ( double xx = m_yMin + dxgrid / 2; xx <= m_xMax; xx += dxgrid ) {
      const double theta = 2.0 * atan( exp( -1.0 * xx ) );
      const double phi   = yy * Gaudi::Units::degree;
      // make sure theta in not 90 or 270!!!!
      const double x = sin( theta ) * cos( phi ) * m_z / cos( theta );
      const double y = sin( theta ) * sin( phi ) * m_z / cos( theta );
      // "shooting" point at the reference plane
      const Gaudi::XYZPoint point( x, y, m_z );
      // evaluate the distance
      const double dist = m_trSvc->distanceInRadUnits_r( m_vertex, point, accelCache, geometry );
      // fill material budget histogram
      plot2D( xx, yy, 1, "Material Budget", m_xMin, m_xMax, m_yMin, m_yMax, m_nbx, m_nby,
              dist ); // weight
      // fill the "helper" histogram
      plot2D( x, y, 2, "Material Budget Check", -600, 600, -600, 600, m_nbx, m_nby );
      // show the progress
      ++progress;
    }
  }
}

DECLARE_COMPONENT( DetDesc::MaterialBudget )
