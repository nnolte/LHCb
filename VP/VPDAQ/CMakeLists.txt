###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
VP/VPDAQ
--------
#]=======================================================================]

gaudi_add_module(VPDAQ
    SOURCES
        src/VPRetinaClusterCreator.cpp
        src/VPRetinaClusterDecoder.cpp
        src/VPRetinaFullClustering.cpp
        src/VPRetinaMatrix.cpp
        src/VPRetinaSPmixer.cpp
        src/VPSuperPixelBankEncoder.cpp
        src/VPRetinaTopologyID.cpp
        src/VPRetinaFullClusterDecoder.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::LHCbKernel
        LHCb::LHCbAlgsLib
        LHCb::TrackEvent
        LHCb::VPDetLib
        LHCb::VPKernel
)

gaudi_add_tests(QMTest)
