/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/VP/VPChannelID.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPFullCluster.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include "VPRetinaClusterConstants.h"
#include "VPRetinaFullCluster.h"
#include "VPRetinaMatrix.h"
#include "VPRetinaTopologyID.h"
#include <array>
#include <iomanip>
#include <iterator>
#include <tuple>
#include <vector>

/** @class VPRetinaFullClustering VPRetinaFullClustering.h
 * @author Federico Lazzari
 * @date   2018-06-20
 */
namespace {
  constexpr auto to_chip( uint32_t id_X ) { return LHCb::Detector::VPChannelID::ChipID{id_X / Pixel::CHIP_COLUMNS}; }
  constexpr auto to_column( uint32_t id_X ) {
    return LHCb::Detector::VPChannelID::ColumnID{id_X % Pixel::CHIP_COLUMNS};
  }
  constexpr auto to_row( uint32_t sp_row, uint32_t shift_r ) {
    return LHCb::Detector::VPChannelID::RowID{sp_row * 4 + shift_r};
  }
} // namespace

// Namespace for locations in TDS
namespace LHCb {
  namespace VPFullClusterLocation {
    inline const std::string Offsets = "Raw/VP/FullClustersOffsets";
  }
} // namespace LHCb

class VPRetinaFullClustering
    : public LHCb::Algorithm::MultiTransformer<
          std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
              const LHCb::RawEvent&, const DeVP& ),
          LHCb::Algorithm::Traits::usesConditions<DeVP>> {

public:
  /// Standard constructor
  VPRetinaFullClustering( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
  operator()( const LHCb::RawEvent&, const DeVP& ) const override;

private:
  /// bank version. (change this every time semantics change!)
  static constexpr unsigned int m_bankVersion = 1;

  /// make RetinaClusters from bank
  std::vector<LHCb::VPRetinaFullCluster> makeRetinaFullClusters( LHCb::span<const uint32_t>            bank,
                                                                 LHCb::Detector::VPChannelID::SensorID sensor ) const;

  Gaudi::Property<unsigned int> m_chain_length{
      this, "chain_length", 20,
      "Number of RetinaMatrix in a chain, a lower number increase clone, a bigger number require more space in FPGA"};
};

using namespace LHCb;
using namespace VPRetinaCluster;

DECLARE_COMPONENT( VPRetinaFullClustering )

namespace {
  struct SPCache {
    std::array<float, 4> fxy;
    unsigned char        pattern;
    unsigned char        nx1;
    unsigned char        nx2;
    unsigned char        ny1;
    unsigned char        ny2;
  };
  //=========================================================================
  // Cache Super Pixel cluster patterns.
  //=========================================================================
  auto create_SPPatterns() {
    std::array<SPCache, 256> SPCaches;
    // create a cache for all super pixel cluster patterns.
    // this is an unoptimized 8-way flood fill on the 8 pixels
    // in the super pixel.
    // no point in optimizing as this is called once in
    // initialize() and only takes about 20 us.

    // define deltas to 8-connectivity neighbours
    const int dx[] = {-1, 0, 1, -1, 0, 1, -1, 1};
    const int dy[] = {-1, -1, -1, 1, 1, 1, 0, 0};

    // clustering buffer for isolated superpixels.
    unsigned char sp_buffer[8];

    // SP index buffer and its size for single SP clustering
    unsigned char sp_idx[8];
    unsigned char sp_idx_size = 0;

    // stack and stack pointer for single SP clustering
    unsigned char sp_stack[8];
    unsigned char sp_stack_ptr = 0;

    // loop over all possible SP patterns
    for ( unsigned int sp = 0; sp < 256; ++sp ) {
      sp_idx_size = 0;
      for ( unsigned int shift = 0; shift < 8; ++shift ) {
        const unsigned char p = sp & ( 1 << shift );
        sp_buffer[shift]      = p;
        if ( p ) { sp_idx[sp_idx_size++] = shift; }
      }

      // loop over pixels in this SP and use them as
      // cluster seeds.
      // note that there are at most two clusters
      // in a single super pixel!
      unsigned char clu_idx = 0;
      for ( unsigned int ip = 0; ip < sp_idx_size; ++ip ) {
        unsigned char idx = sp_idx[ip];

        if ( 0 == sp_buffer[idx] ) { continue; } // pixel is used

        sp_stack_ptr             = 0;
        sp_stack[sp_stack_ptr++] = idx;
        sp_buffer[idx]           = 0;
        unsigned char x          = 0;
        unsigned char y          = 0;
        unsigned char n          = 0;

        while ( sp_stack_ptr ) {
          idx                     = sp_stack[--sp_stack_ptr];
          const unsigned char row = idx % 4;
          const unsigned char col = idx / 4;
          x += col;
          y += row;
          ++n;

          for ( unsigned int ni = 0; ni < 8; ++ni ) {
            const char ncol = col + dx[ni];
            if ( ncol < 0 || ncol > 1 ) continue;
            const char nrow = row + dy[ni];
            if ( nrow < 0 || nrow > 3 ) continue;
            const unsigned char nidx = ( ncol << 2 ) | nrow;
            if ( 0 == sp_buffer[nidx] ) continue;
            sp_stack[sp_stack_ptr++] = nidx;
            sp_buffer[nidx]          = 0;
          }
        }

        const uint32_t cx = x / n;
        const uint32_t cy = y / n;
        const float    fx = x / static_cast<float>( n ) - cx;
        const float    fy = y / static_cast<float>( n ) - cy;

        // store the centroid pixel
        SPCaches[sp].pattern |= ( ( cx << 2 ) | cy ) << 4 * clu_idx;

        // set the two cluster flag if this is the second cluster
        SPCaches[sp].pattern |= clu_idx << 3;

        // set the pixel fractions
        SPCaches[sp].fxy[2 * clu_idx]     = fx;
        SPCaches[sp].fxy[2 * clu_idx + 1] = fy;

        // increment cluster count. note that this can only become 0 or 1!
        ++clu_idx;
      }
    }
    return SPCaches;
  }
  // SP pattern buffers for clustering, cached once.
  // There are 256 patterns and there can be at most two
  // distinct clusters in an SP.
  static const std::array<SPCache, 256> s_SPCaches = create_SPPatterns();
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaFullClustering::VPRetinaFullClustering( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer(
          name, pSvcLocator,
          {KeyValue{"RawEventLocation", LHCb::RawEventLocation::Default}, KeyValue{"DEVP", LHCb::Det::VP::det_path}},
          {KeyValue{"ClusterLocation", LHCb::VPFullClusterLocation::Default},
           KeyValue{"ClusterOffsets", LHCb::VPFullClusterLocation::Offsets}} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>> VPRetinaFullClustering::
                                                                                                operator()( const LHCb::RawEvent& rawEvent, const DeVP& devp ) const {
  auto result = std::tuple<std::vector<LHCb::VPFullCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};

  const auto& tBanks = rawEvent.banks( LHCb::RawBank::VP );
  if ( tBanks.empty() ) return result;

  const unsigned int version = tBanks[0]->version();
  if ( version != 2 ) {
    warning() << "Unsupported raw bank version (" << version << ")" << endmsg;
    return result;
  }

  // Since the pool is local, to first preallocate the pool, then count hits per module,
  // and then preallocate per module and move hits might not be faster than adding
  // directly to the PixelModuleHits (which would require more allocations, but
  // not many if we start with a sensible default)
  auto& [pool, offsets] = result;
  const unsigned int startSize = 10000U;
  pool.reserve( startSize );

  // 1 module = N Cluster -> N x M channelIDs
  std::vector<std::vector<LHCb::Detector::VPChannelID>> channelIDs;
  channelIDs.reserve( 400 );

  uint32_t max_sp = 0;

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Read " << tBanks.size() << " raw banks from TES" << endmsg; }

  // Loop over VP RawBanks
  for ( auto iterBank : tBanks ) {

    const auto         sensor = LHCb::Detector::VPChannelID::SensorID( iterBank->sourceID() );
    const unsigned int module = 1 + ( to_unsigned( sensor ) / VP::NSensorsPerModule );
    int                n_hits = 0;

    const auto& ltg = devp.ltg( sensor );

    const uint32_t nsp = iterBank->range<uint32_t>()[0];

    if ( nsp > max_sp ) { max_sp = nsp; }

    auto retinaClusters = makeRetinaFullClusters( iterBank->range<uint32_t>(), sensor );

    for ( auto iterCluster : retinaClusters ) {
      const uint32_t cx = ( iterCluster.word() >> ( Row_nbit + fracCol_nbit ) ) & intCol_mask;
      const float    fx = ( ( iterCluster.word() >> Row_nbit ) & fracCol_mask ) / 4.;
      const auto     cy = LHCb::Detector::VPChannelID::RowID{( iterCluster.word() >> fracRow_nbit ) & intRow_mask};
      const float    fy = ( iterCluster.word() & fracRow_mask ) / 4.;

      const uint32_t cx_frac_half = ( iterCluster.word() >> 11 ) & 0x1;
      const uint32_t cx_frac_quarter = ( iterCluster.word() >> 10 ) & 0x1;
      const auto     or_fx = LHCb::Detector::VPChannelID::OrfxID{( cx_frac_half | cx_frac_quarter )};

      const uint32_t cy_frac_half = ( iterCluster.word() >> 1 ) & 0x1;
      const uint32_t cy_frac_quarter = ( iterCluster.word() ) & 0x1;
      const auto     or_fy = LHCb::Detector::VPChannelID::OrfyID{( cy_frac_half | cy_frac_quarter )};

      LHCb::Detector::VPChannelID cid( sensor, to_chip( cx ), to_column( cx ), cy, or_fx, or_fy );
      channelIDs.push_back( iterCluster.channelIDs() );

      const float local_x = devp.local_x( cx ) + fx * devp.x_pitch( cx );
      const float local_y = ( to_unsigned( cy ) + 0.5 + fy ) * devp.pixel_size();

      const float gx = ltg[0] * local_x + ltg[1] * local_y + ltg[9];
      const float gy = ltg[3] * local_x + ltg[4] * local_y + ltg[10];
      const float gz = ltg[6] * local_x + ltg[7] * local_y + ltg[11];

      pool.emplace_back( fx, fy, gx, gy, gz, cid, std::move( channelIDs.back() ) );
      n_hits++;
      ++offsets[module];
    }

  } // loop over all banks

  std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );
  // Do we actually need to sort the hits ? [ depends, if the offline clustering will be re-run and tracking will use
  // those clusters , yes, otherwise no ]
  for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
    // In even modules you fall in the branching at -180, 180 degrees, you want to do that continuos
    std::stable_sort(
        pool.begin() + offsets[moduleID], pool.begin() + offsets[moduleID + 1],
        []( const LHCb::VPFullCluster& a, const LHCb::VPFullCluster& b ) { return a.channelID() < b.channelID(); } );
  }
  if ( msgLevel( MSG::DEBUG ) ) {
    for ( auto& cl : pool ) {
      info() << "----" << endmsg;
      info() << cl << endmsg;
      info() << " [fx,fy] " << cl.xfraction() << "," << cl.yfraction() << endmsg;
      info() << " [x,y,z] " << cl.x() << "," << cl.y() << "," << cl.z() << endmsg;
      info() << "pixels" << endmsg;
      for ( auto& pixel : cl.pixels() ) { info() << "\t" << pixel << endmsg; }
    }
  }
  if ( msgLevel( MSG::DEBUG ) ) { info() << "N VPFullCluster produced :  " << pool.size() << endmsg; }

  return result;
}

//=============================================================================
// make RetinaClusters from bank
//=============================================================================
std::vector<LHCb::VPRetinaFullCluster>
VPRetinaFullClustering::makeRetinaFullClusters( LHCb::span<const uint32_t>            bank,
                                                LHCb::Detector::VPChannelID::SensorID sensor ) const {
  assert( bank.size() == 1 + bank[0] );

  std::vector<VPRetinaMatrix> RetinaMatrixVector;
  RetinaMatrixVector.reserve( 20 );
  std::vector<LHCb::VPRetinaFullCluster> RetinaCluster;
  VPRetinaTopologyID                     topo_obj;
  std::vector<uint32_t>                  vec_topoID2x4 = topo_obj.topo2x4_to_topoIDfrac();

  // Read super pixel
  for ( const uint32_t sp_word : bank.subspan( 1 ) ) {

    uint8_t sp = sp_word & 0xFFU;

    if ( 0 == sp ) continue; // protect against zero super pixels.

    const uint32_t sp_addr          = ( sp_word & 0x007FFF00U ) >> 8;
    const uint32_t sp_row           = sp_addr & 0x3FU;
    const uint32_t sp_col           = ( sp_addr >> 6 );
    const uint32_t no_sp_neighbours = sp_word & 0x80000000U;
    const uint32_t no_sp_neigh_flag = no_sp_neighbours >> 31;

    // if a super pixel is not isolated
    // we use Retina Clustering Algorithm
    if ( !no_sp_neighbours ) {
      // we look for already created Retina
      auto iterRetina = std::find_if( RetinaMatrixVector.begin(), RetinaMatrixVector.end(),
                                      [&]( const VPRetinaMatrix& m ) { return m.IsInRetina( sp_row, sp_col ); } );
      if ( iterRetina != RetinaMatrixVector.end() ) {
        iterRetina->AddSP( sp_row, sp_col, sp );
        continue;
      } else {
        // if we have not reached maximum chain length
        // we create a new retina
        if ( RetinaMatrixVector.size() < m_chain_length ) {
          RetinaMatrixVector.emplace_back( sp_row, sp_col, sp, sensor );
          continue;
        } else {
          // continue;
        }
      }
    }

    // if a super pixel is isolated or the RetinaMatrix chain is full
    // the clustering boils down to a simple pattern look up.

    // there is always at least one cluster in the super
    // pixel. look up the pattern and add it.

    // there is always at least one cluster in the super
    // pixel. look up the pattern and add it.

    // remove after caches rewrite
    const auto&    spcache = s_SPCaches[sp];
    const uint32_t idx     = spcache.pattern;

    const uint32_t row = idx & 0x03U;
    const uint32_t col = ( idx >> 2 ) & 1;

    uint64_t cX;
    uint64_t cY;
    // if the two clusters in the SP are
    // top-left for the first cluster
    // and bottom-right for the second cluster
    // aka if two clusters are present in the isolated SP
    // and the column of the two clusters is different
    // and the row of the first is bigger that the row of the second
    // then the coordinates of the second cluster are computed
    if ( ( idx & 8 ) && ( ( ( idx >> 2 ) & 1 ) != ( ( idx >> 6 ) & 1 ) ) &&
         ( ( idx & 0x03U ) > ( ( idx >> 4 ) & 3 ) ) ) {
      cX = ( ( sp_col * 2 + ( ( idx >> 6 ) & 3 ) ) << 2 ) + ( uint64_t )( spcache.fxy[2] * 4 + 0.5 );
      cY = ( ( sp_row * 4 + ( ( idx >> 4 ) & 1 ) ) << 2 ) + ( uint64_t )( spcache.fxy[3] * 4 + 0.5 );
      // if a single cluster is present in the isolated SP or
      // they are not top-left and bottom-right
      // compute the coordinates of the first cluster
    } else {
      cX = ( ( sp_col * 2 + col ) << fracCol_nbit ) + ( uint64_t )( spcache.fxy[0] * 4 + 0.5 );
      cY = ( ( sp_row * 4 + row ) << fracRow_nbit ) + ( uint64_t )( spcache.fxy[1] * 4 + 0.5 );
    }

    std::vector<LHCb::Detector::VPChannelID> channelIDs;
    uint32_t                                 shift_r = 0;
    while ( ( ( ( sp >> shift_r ) & 0x1 ) == 0 ) && ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 0 ) ) ++shift_r;
    while ( ( ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) || ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) ) && shift_r < 4 ) {
      if ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) {
        const uint32_t id_X = sp_col * 2;
        channelIDs.emplace_back( sensor, to_chip( id_X ), to_column( id_X ), to_row( sp_row, shift_r ) );
      }
      if ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) {
        const uint32_t id_X = sp_col * 2 + 1;
        channelIDs.emplace_back( sensor, to_chip( id_X ), to_column( id_X ), to_row( sp_row, shift_r ) );
      }
      ++shift_r;
    }

    uint32_t topoID2x4 = ( vec_topoID2x4[sp] >> ( fracRow_nbit + fracCol_nbit ) );

    RetinaCluster.emplace_back( 1 << isoBit_shift | no_sp_neigh_flag << isoFlag_shift | topoID2x4 << topo2x4_shift |
                                    cX << Row_nbit | cY,
                                channelIDs );

    // if two clusters are present in the isolated SP
    if ( idx & 8 ) {
      const uint32_t row = ( idx >> 4 ) & 3;
      const uint32_t col = ( idx >> 6 ) & 1;
      uint64_t       cX;
      uint64_t       cY;
      // if the two clusters in the SP are
      // top-left for the first cluster
      // and bottom-right for the second cluster
      // aka if two clusters are present in the isolated SP
      // and the column of the two clusters is different
      // and the row of the first is bigger that the row of the second
      // then the coordinates of the second cluster are computed
      if ( ( idx & 8 ) && ( ( ( idx >> 2 ) & 1 ) != ( ( idx >> 6 ) & 1 ) ) &&
           ( ( idx & 0x03U ) > ( ( idx >> 4 ) & 3 ) ) ) {
        cX = ( ( sp_col * 2 + ( ( idx >> 2 ) & 1 ) ) << 2 ) + ( uint64_t )( spcache.fxy[0] * 4 + 0.5 );
        cY = ( ( sp_row * 4 + ( idx & 0x03U ) ) << 2 ) + ( uint64_t )( spcache.fxy[1] * 4 + 0.5 );
        // otherwise compute the coordinates of the second cluster
      } else {
        cX = ( ( sp_col * 2 + col ) << fracCol_nbit ) + ( uint64_t )( spcache.fxy[2] * 4 + 0.5 );
        cY = ( ( sp_row * 4 + row ) << fracRow_nbit ) + ( uint64_t )( spcache.fxy[3] * 4 + 0.5 );
      }

      std::vector<LHCb::Detector::VPChannelID> channelIDs;
      while ( ( ( ( sp >> shift_r ) & 0x1 ) == 0 ) && ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 0 ) ) ++shift_r;
      while ( ( ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) || ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) ) &&
              shift_r < 4 ) {
        if ( ( ( sp >> shift_r ) & 0x1 ) == 1 ) {
          const uint32_t id_X = sp_col * 2;
          channelIDs.emplace_back( sensor, to_chip( id_X ), to_column( id_X ), to_row( sp_row, shift_r ) );
        }
        if ( ( ( sp >> ( shift_r + 4 ) ) & 0x1 ) == 1 ) {
          const uint32_t id_X = sp_col * 2 + 1;
          channelIDs.emplace_back( sensor, to_chip( id_X ), to_column( id_X ), to_row( sp_row, shift_r ) );
        }
        ++shift_r;
      }

      uint32_t topoID2x4 = ( vec_topoID2x4[sp] >> ( fracRow_nbit + fracCol_nbit ) );

      RetinaCluster.emplace_back( 1 << isoBit_shift | no_sp_neigh_flag << isoFlag_shift | topoID2x4 << topo2x4_shift |
                                      cX << Row_nbit | cY,
                                  channelIDs );
    }
  }

  // searchRetinaCluster

  for ( auto& m : RetinaMatrixVector ) {
    const auto& clusters = m.SearchFullCluster();
    RetinaCluster.insert( end( RetinaCluster ), begin( clusters ), end( clusters ) );
  };
  return RetinaCluster;
}
