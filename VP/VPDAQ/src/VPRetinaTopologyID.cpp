/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "VPRetinaTopologyID.h"
#include <cstdint>
#include <iostream>
#include <tuple>
#include <vector>

// constructor
VPRetinaTopologyID::VPRetinaTopologyID() {}

// function that returns true if 3x3 topology corresponds to an actual cluster
bool VPRetinaTopologyID::is3x3cluster( unsigned int topo3x3 ) {
  if ( ( ( topo3x3 & 1 ) == 1 ) || ( ( ( ( topo3x3 >> 1 ) & 1 ) == 1 ) && ( ( ( topo3x3 >> 3 ) & 1 ) == 1 ) ) ) {
    return true;
  } else {
    return false;
  }
}

// function that returns true if 2x4 topology corresponds to an actual cluster
bool VPRetinaTopologyID::is2x4cluster( unsigned int topo2x4 ) {
  if ( topo2x4 != 0 ) {
    return true;
  } else {
    return false;
  }
}

// compute the fractional coordinates from a 3x3 cluster topology
std::tuple<int, int> VPRetinaTopologyID::frac3x3cluster( unsigned int topo3x3 ) {
  uint32_t shift_col = 0;
  uint32_t shift_row = 0;
  uint32_t n         = 0;

  for ( unsigned int iiX = 0; iiX < 3; ++iiX ) {
    for ( unsigned int iiY = 0; iiY < 3; ++iiY ) {
      if ( topo3x3 & ( 1 << ( iiX * 3 + iiY ) ) ) {
        shift_col += iiX;
        shift_row += iiY;
        n++;
      }
    }
  }
  return std::make_tuple( ( uint64_t )( ( ( shift_row << 2 ) + n / 2 ) / n ),
                          ( uint64_t )( ( ( shift_col << 2 ) + n / 2 ) / n ) );
}

// compute the fractional coordinates from a 2x4 cluster topology
std::tuple<int, int> VPRetinaTopologyID::frac2x4cluster( unsigned int topo2x4 ) {
  uint32_t shift_col = 0;
  uint32_t shift_row = 0;
  uint32_t n         = 0;

  for ( unsigned int iiX = 0; iiX < 2; ++iiX ) {
    for ( unsigned int iiY = 0; iiY < 4; ++iiY ) {
      if ( topo2x4 & ( 1 << ( iiX * 4 + iiY ) ) ) {
        shift_col += iiX;
        shift_row += iiY;
        n++;
      }
    }
  }
  return std::make_tuple( ( uint64_t )( ( ( shift_row << 2 ) + n / 2 ) / n ),
                          ( uint64_t )( ( ( shift_col << 2 ) + n / 2 ) / n ) );
}

// compute the equivalent 3x3 cluster topology
uint32_t VPRetinaTopologyID::equivalent_topo3x3( unsigned int topo3x3 ) {
  if ( ( ( topo3x3 & 2 ) == 0 ) && ( ( topo3x3 & 8 ) == 0 ) && ( ( topo3x3 & 16 ) == 0 ) ) { // bottom-left corner
                                                                                             // pixel isolated
    return topo3x3 & 1;
  } else if ( ( ( topo3x3 & 8 ) == 0 ) && ( ( topo3x3 & 16 ) == 0 ) && ( ( topo3x3 & 32 ) == 0 ) ) { // central column
                                                                                                     // all zero
    return topo3x3 & ( 1 + 2 + 4 );
  } else if ( ( ( topo3x3 & 2 ) == 0 ) && ( ( topo3x3 & 16 ) == 0 ) && ( ( topo3x3 & 128 ) == 0 ) ) { // central row
                                                                                                      // all zero
    return topo3x3 & ( 1 + 8 + 64 );
  } else if ( ( ( topo3x3 & 16 ) == 0 ) && ( ( topo3x3 & 32 ) == 0 ) && ( ( topo3x3 & 128 ) == 0 ) ) { // top-right
                                                                                                       // corner pixel
                                                                                                       // isolated

    return topo3x3 & ( 1 + 2 + 4 + 8 + 64 );
  } else if ( ( ( topo3x3 & 2 ) == 0 ) && ( ( topo3x3 & 16 ) == 0 ) && ( ( topo3x3 & 32 ) == 0 ) ) { // top-left corner
                                                                                                     // pixel isolated
    return topo3x3 & ( 1 + 8 + 64 + 128 + 256 );
  } else if ( ( ( topo3x3 & 8 ) == 0 ) && ( ( topo3x3 & 16 ) == 0 ) && ( ( topo3x3 & 128 ) == 0 ) ) { // bottom-right
                                                                                                      // corner pixel
                                                                                                      // isolated
    return topo3x3 & ( 1 + 2 + 4 + 32 + 256 );
  } else {
    return topo3x3;
  }
}

// break down an isolated SP into its two components: if the SP contains a single cluster, the SP itself and a null SP
// are returned. If the SP contains two clusters, two SPs are returned containg one cluster each.
std::tuple<uint32_t, uint32_t> VPRetinaTopologyID::breakdown_topo2x4( unsigned int topo2x4 ) {
  uint32_t SP0;
  uint32_t SP1;
  if ( ( ( topo2x4 & 2 ) == 0 ) && ( ( topo2x4 & 32 ) == 0 ) ) {
    SP0 = topo2x4 & ( 1 + 16 );
    SP1 = topo2x4 & ( 4 + 8 + 64 + 128 );
  } else if ( ( ( topo2x4 & 4 ) == 0 ) && ( ( topo2x4 & 64 ) == 0 ) ) {
    SP0 = topo2x4 & ( 1 + 2 + 16 + 32 );
    SP1 = topo2x4 & ( 8 + 128 );
  } else {
    SP0 = topo2x4;
    SP1 = 0;
  }

  if ( SP0 != 0 ) {
    return std::make_tuple( SP0, SP1 );
  } else {
    return std::make_tuple( SP1, 0 );
  }
}

//===================================================================================================================
// Function to compute the cluster topology ID and the coordinate fractional parts from the full 3x3 cluster topology
//===================================================================================================================

std::vector<uint32_t> VPRetinaTopologyID::topo3x3_to_topoIDfrac() {
  uint32_t num_3x3clu = 0;

  std::vector<uint32_t> vec_topo3x3( 512, 0 );
  std::vector<uint32_t> vec_topo3x3eqv( 512, 0 );
  std::vector<uint32_t> fracrow_clu3x3( 512, 0 );
  std::vector<uint32_t> fraccol_clu3x3( 512, 0 );
  std::vector<uint32_t> vec_topoID3x3( 512, 0 );
  std::vector<uint32_t> vec_topoIDfrac3x3( 512, 0 );
  std::vector<uint32_t> cnt3x3( 16, 0 );

  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    vec_topo3x3[topo3x3] = topo3x3;
    if ( is3x3cluster( topo3x3 ) ) {
      num_3x3clu += 1;
      vec_topo3x3eqv[topo3x3] = equivalent_topo3x3( topo3x3 );
      fracrow_clu3x3[topo3x3] = ( std::get<0>( frac3x3cluster( equivalent_topo3x3( topo3x3 ) ) ) ) % 4;
      fraccol_clu3x3[topo3x3] = ( std::get<1>( frac3x3cluster( equivalent_topo3x3( topo3x3 ) ) ) ) % 4;
      if ( topo3x3 == vec_topo3x3eqv[topo3x3] ) {
        vec_topoID3x3[topo3x3] = cnt3x3[( fracrow_clu3x3[topo3x3] % 4 ) * 4 + ( fraccol_clu3x3[topo3x3] % 4 )];
        cnt3x3[( fracrow_clu3x3[topo3x3] % 4 ) * 4 + ( fraccol_clu3x3[topo3x3] % 4 )] += 1;
      } else {
        vec_topoID3x3[topo3x3] = vec_topoID3x3[vec_topo3x3eqv[topo3x3]];
        //         for ( unsigned int topo3x3eqv = 0; topo3x3eqv < 512; ++topo3x3eqv ) {
        //           if ( vec_topo3x3eqv[topo3x3] == topo3x3eqv ) { vec_topoID3x3[topo3x3] = vec_topoID3x3[topo3x3eqv];
        //           }
        //         }
      }
    } else {
      vec_topo3x3eqv[topo3x3] = 0;
      fracrow_clu3x3[topo3x3] = 0;
      fraccol_clu3x3[topo3x3] = 0;
      vec_topoID3x3[topo3x3]  = 31;
    }
  }

  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    vec_topoIDfrac3x3[topo3x3] =
        ( vec_topoID3x3[topo3x3] << 4 ) | ( fraccol_clu3x3[topo3x3] << 2 ) | ( fracrow_clu3x3[topo3x3] );
  }

  return vec_topoIDfrac3x3;
}

//====================================================================================================================
// Function to compute the cluster topology ID and the coordinate fractional parts  from the full 2x4 cluster topology
//====================================================================================================================

std::vector<uint32_t> VPRetinaTopologyID::topo2x4_to_topoIDfrac() {
  uint32_t num_2x4clu = 0;

  std::vector<uint32_t> vec_topo2x4( 256, 0 );
  std::vector<uint32_t> vec_topo2x4_bd0( 256, 0 );
  std::vector<uint32_t> vec_topo2x4_bd1( 256, 0 );
  std::vector<uint32_t> fracrow_clu2x4_0( 256, 0 );
  std::vector<uint32_t> fracrow_clu2x4_1( 256, 0 );
  std::vector<uint32_t> fraccol_clu2x4_0( 256, 0 );
  std::vector<uint32_t> fraccol_clu2x4_1( 256, 0 );
  std::vector<uint32_t> vec_topoID2x4( 256, 0 );
  std::vector<uint32_t> vec_topoIDfrac2x4( 256, 0 );
  std::vector<uint32_t> cnt2x4( 16, 0 );

  for ( unsigned int topo2x4 = 0; topo2x4 < 256; ++topo2x4 ) {
    vec_topo2x4[topo2x4] = topo2x4;
    if ( is2x4cluster( topo2x4 ) ) {
      num_2x4clu += 1;
      uint32_t SP0             = std::get<0>( breakdown_topo2x4( topo2x4 ) );
      vec_topo2x4_bd0[topo2x4] = SP0;
      uint32_t ID0;
      uint32_t SP1             = std::get<1>( breakdown_topo2x4( topo2x4 ) );
      vec_topo2x4_bd1[topo2x4] = SP1;
      uint32_t ID1;
      if ( SP0 == 0 ) {
        ID0 = 63;
      } else {
        fracrow_clu2x4_0[topo2x4] = ( std::get<0>( frac2x4cluster( SP0 ) ) ) % 4;
        fraccol_clu2x4_0[topo2x4] = ( std::get<1>( frac2x4cluster( SP0 ) ) ) % 4;
        if ( SP1 == 0 ) {
          ID0 = cnt2x4[( fracrow_clu2x4_0[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_0[topo2x4] % 4 )];
          cnt2x4[( fracrow_clu2x4_0[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_0[topo2x4] % 4 )] += 1;
        } else {
          ID0 = vec_topoID2x4[SP0] & 63;
        }
      }
      if ( SP1 == 0 ) {
        ID1 = 63;
      } else {
        fracrow_clu2x4_1[topo2x4] = ( std::get<0>( frac2x4cluster( SP1 ) ) ) % 4;
        fraccol_clu2x4_1[topo2x4] = ( std::get<1>( frac2x4cluster( SP1 ) ) ) % 4;
        if ( SP0 == 0 ) {
          ID1 = cnt2x4[( fracrow_clu2x4_1[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_1[topo2x4] % 4 )];
          cnt2x4[( fracrow_clu2x4_1[topo2x4] % 4 ) * 4 + ( fraccol_clu2x4_1[topo2x4] % 4 )] += 1;
        } else {
          ID1 = vec_topoID2x4[SP1] & 63;
        }
      }

      vec_topoID2x4[topo2x4] = ( ID1 << 6 ) + ( ID0 );

    } else {

      fracrow_clu2x4_0[topo2x4] = 0;
      fraccol_clu2x4_0[topo2x4] = 0;
      fracrow_clu2x4_1[topo2x4] = 0;
      fraccol_clu2x4_1[topo2x4] = 0;
      vec_topoID2x4[topo2x4]    = ( 63 << 6 ) + 63;
    }
  }

  for ( unsigned int topo2x4 = 0; topo2x4 < 256; ++topo2x4 ) {
    vec_topoIDfrac2x4[topo2x4] = ( ( ( vec_topoID2x4[topo2x4] >> 6 ) & 63 ) << 14 ) |
                                 ( fraccol_clu2x4_1[topo2x4] << 12 ) | ( fracrow_clu2x4_1[topo2x4] << 10 ) |
                                 ( ( vec_topoID2x4[topo2x4] & 63 ) << 4 ) | ( fraccol_clu2x4_0[topo2x4] << 2 ) |
                                 ( fracrow_clu2x4_0[topo2x4] );
  }

  return vec_topoIDfrac2x4;
}

//============================================================================================================
// Function to retrieve the full 3x3 cluster topology from the topology ID and the coordinate fractional parts
//============================================================================================================

uint32_t VPRetinaTopologyID::topoIDfrac_to_topo3x3( uint32_t sensor, uint32_t topoID, uint32_t fx, uint32_t fy ) {
  std::vector<uint32_t> vec_topoIDfrac3x3 = topo3x3_to_topoIDfrac();
  for ( unsigned int topo3x3 = 0; topo3x3 < 512; ++topo3x3 ) {
    if ( ( sensor % 4 == 0 ) || ( sensor % 4 == 3 ) ) {
      if ( vec_topoIDfrac3x3[topo3x3] == ( ( topoID << 4 ) | ( fx << 2 ) | fy ) ) { return topo3x3; }
    } else {
      // in case the reversed search pattern is used (sensor 1 or 2), we need to flip the 3x3 topology along the central
      // column, compute the fractional column of the flipped topology and build the symm_topoIDfrac3x3 key with the
      // flipped quantities
      unsigned int equiv_topo3x3 = equivalent_topo3x3( topo3x3 );
      unsigned int symm_topo3x3 =
          ( ( equiv_topo3x3 << 6 ) & 0x1c0 ) | ( equiv_topo3x3 & 0x38 ) | ( ( equiv_topo3x3 >> 6 ) & 0x7 );
      unsigned int symm_frac_col      = ( 4 - ( ( vec_topoIDfrac3x3[equiv_topo3x3] >> 2 ) & 0x3 ) ) % 4;
      unsigned int symm_topoIDfrac3x3 = ( vec_topoIDfrac3x3[equiv_topo3x3] & 0x1F0 ) | ( symm_frac_col << 2 ) |
                                        ( vec_topoIDfrac3x3[equiv_topo3x3] & 0x3 );
      if ( symm_topoIDfrac3x3 == ( ( topoID << 4 ) | ( fx << 2 ) | fy ) ) { return symm_topo3x3; }
    }
  }
  return vec_topoIDfrac3x3.size();
}

//============================================================================================================
// Function to retrieve the full 2x4 cluster topology from the topology ID and the coordinate fractional parts
//============================================================================================================

uint32_t VPRetinaTopologyID::topoIDfrac_to_topo2x4( uint32_t topoID, uint32_t fx, uint32_t fy ) {
  std::vector<uint32_t> vec_topoIDfrac2x4 = topo2x4_to_topoIDfrac();

  for ( uint32_t i = 0; i < vec_topoIDfrac2x4.size(); i++ ) {
    if ( ( vec_topoIDfrac2x4[i] & 1023 ) == ( ( topoID << 4 ) | ( fx << 2 ) | fy ) ) { return i; }
  }
  return vec_topoIDfrac2x4.size();
}
