/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

namespace VPRetinaCluster {

  inline constexpr unsigned int sensorID_shift     = 22;
  inline constexpr unsigned int fracRow_nbit       = 2;
  inline constexpr unsigned int fracRow_mask       = 0x3;
  inline constexpr unsigned int fracCol_nbit       = 2;
  inline constexpr unsigned int fracCol_mask       = 0x3;
  inline constexpr unsigned int intRow_nbit        = 8;
  inline constexpr unsigned int intRow_mask        = 0xff;
  inline constexpr unsigned int intCol_nbit        = 10;
  inline constexpr unsigned int intCol_mask        = 0x3ff;
  inline constexpr unsigned int Row_nbit           = 10;
  inline constexpr unsigned int Col_nbit           = 12;
  inline constexpr unsigned int isoBit_shift       = 30;
  inline constexpr unsigned int isoFlag_shift      = 29;
  inline constexpr unsigned int selfContFlag_shift = 29;
  inline constexpr unsigned int edgeFlag_shift     = 28;
  inline constexpr unsigned int topo2x4_shift      = 23;
  inline constexpr unsigned int topo2x4_nbit       = 6;
  inline constexpr unsigned int topo2x4_mask       = 0x3f;
  inline constexpr unsigned int topo3x3_shift      = 23;
  inline constexpr unsigned int topo3x3_nbit       = 5;
  inline constexpr unsigned int topo3x3_mask       = 0x1f;

} // namespace VPRetinaCluster
