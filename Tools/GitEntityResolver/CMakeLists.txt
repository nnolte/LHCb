###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Tools/GitEntityResolver
-----------------------
#]=======================================================================]

gaudi_add_header_only_library(GitEntityResolverLib
    LINK
        Boost::filesystem
        LHCb::LHCbKernel
        PkgConfig::git2
)

gaudi_add_module(GitEntityResolver
    SOURCES
        src/GitEntityResolver.cpp
    LINK
        Boost::filesystem
        Boost::headers
        Gaudi::GaudiKernel
        LHCb::GitEntityResolverLib
        LHCb::LHCbKernel
        LHCb::XmlToolsLib
        XercesC::XercesC
)

gaudi_install(PYTHON)

gaudi_install(SCRIPTS)

gaudi_add_tests(QMTest)
set_property(
    TEST
        GitEntityResolver.basic_access.empty_tag_bare
        GitEntityResolver.basic_access.files
        GitEntityResolver.basic_access.head
        GitEntityResolver.basic_access.overlay
        GitEntityResolver.basic_access.v0
        GitEntityResolver.basic_access.v1
        GitEntityResolver.basic_access.v1_bare
        GitEntityResolver.direct_mapping.files
        GitEntityResolver.direct_mapping.head
        GitEntityResolver.iov_access.partitioning
        GitEntityResolver.iov_access.upper_limit
        GitEntityResolver.iov_access.upper_limit_error
        GitEntityResolver.iov_access.v0
        GitEntityResolver.prepare
        GitEntityResolver.run_stamp
    APPEND PROPERTY 
        ENVIRONMENT
            "GIT_TEST_REPOSITORY=${CMAKE_CURRENT_BINARY_DIR}/test_repo/DDDB"
)

gaudi_add_tests(nosetests ${CMAKE_CURRENT_SOURCE_DIR}/python/GitCondDB)
gaudi_add_tests(nosetests ${CMAKE_CURRENT_SOURCE_DIR}/scripts)
gaudi_add_tests(nosetests ${CMAKE_CURRENT_SOURCE_DIR}/tests/test_scripts)
