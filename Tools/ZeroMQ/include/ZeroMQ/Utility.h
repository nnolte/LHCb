/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef SERIALIZE_UTILITY_H
#define SERIALIZE_UTILITY_H 1

namespace Detail {

  template <typename... Ts>
  struct is_container_helper {};

  // is trivial
  template <class T>
  struct is_trivial : std::conditional<std::is_trivially_copyable<typename std::decay<T>::type>::value, std::true_type,
                                       std::false_type>::type {};

} // namespace Detail

// is_pair
template <class>
struct is_pair : std::false_type {};

template <class F, class S>
struct is_pair<std::pair<F, S>> : public std::true_type {};

// is_container
template <typename T, typename _ = void>
struct is_container : std::false_type {};

template <typename T>
struct is_container<T, typename std::conditional<
                           false,
                           Detail::is_container_helper<
                               typename T::value_type, typename T::size_type, typename T::allocator_type,
                               typename T::iterator, typename T::const_iterator, decltype( std::declval<T>().size() ),
                               decltype( std::declval<T>().begin() ), decltype( std::declval<T>().end() ),
                               decltype( std::declval<T>().cbegin() ), decltype( std::declval<T>().cend() )>,
                           void>::type> : public std::true_type {};

#endif // SERIALIZE_UTILITY_H
