/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "Linker/LinkerWithKey.h"

#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Kernel/MCAssociation.h"
#include "Kernel/Particle2MCMethod.h"

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IAlgManager.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/compose.h"

#include <variant>

/**
 *  Class providing association functionality to MCParticles
 *
 *  template parameter PARENT will typically be a tool or an Algorithm
 *
 *  @author Philippe Charpentier
 *  @date   2004-04-29
 */
template <typename SOURCE = LHCb::Particle, typename PARENT = GaudiAlgorithm>
class Object2MCLinker {
public:
  // Typedef for source type
  typedef SOURCE Source;
  // Typedef for RangeFrom return type
  typedef std::vector<MCAssociation> ToRange;
  typedef ToRange::iterator          ToIterator;
  typedef ToRange::const_iterator    ToConstIterator;

  // Constructors from Algorithm
  Object2MCLinker( const PARENT* myMother, const std::string& algType, const std::string& extension,
                   std::vector<std::string> containerList )
      : m_parent( myMother )
      , m_extension( extension )
      , m_linkerAlgType( algType )
      , m_containerList( std::move( containerList ) )
      , m_linkTo( myMother->evtSvc(), nullptr, "" )
      , m_linkerTable( myMother->evtSvc(), nullptr, "" ) {}

  Object2MCLinker( const PARENT* myMother )
      : m_parent( myMother )
      , m_linkTo( myMother->evtSvc(), nullptr, "" )
      , m_linkerTable( myMother->evtSvc(), nullptr, "" ) {}

  Object2MCLinker( const PARENT* myMother, const int method, std::vector<std::string> containerList )
      : Object2MCLinker( myMother, Particle2MCMethod::algType[method], Particle2MCMethod::extension[method],
                         std::move( containerList ) ) {}

  Object2MCLinker( const PARENT* myMother, const int method, const std::string& container )
      : Object2MCLinker( myMother, method, std::vector<std::string>( 1, container ) ) {}

  Object2MCLinker( const PARENT* myMother, const std::string& algType, const std::string& extension,
                   const std::string& container )
      : Object2MCLinker( myMother, algType, extension, std::vector<std::string>( 1, container ) ) {}

  StatusCode setAlgorithm( const int method, std::vector<std::string> containerList );

  StatusCode setAlgorithm( const int method, const std::string& container ) {
    return setAlgorithm( method, std::vector<std::string>( 1, container ) );
  }

  StatusCode setAlgorithm( const std::string& algType, const std::string& extension,
                           std::vector<std::string> containerList );

  StatusCode setAlgorithm( const std::string& algType, const std::string& extension, const std::string& container ) {
    return setAlgorithm( algType, extension, std::vector<std::string>( 1, container ) );
  }

  typedef LinkedTo<LHCb::MCParticle> To;

  typedef LinkerWithKey<LHCb::MCParticle, SOURCE> Linker;

  Linker* linkerTable( const std::string& name );

  Linker* linkerTable( const std::string& name, To& test );

  ToRange rangeFrom( const SOURCE* part );

  const LHCb::MCParticle* firstMCP( const SOURCE* obj );

  const LHCb::MCParticle* firstMCP( const SOURCE* obj, double& weight );

  const LHCb::MCParticle* nextMCP() { return m_linkTo.next(); }

  const LHCb::MCParticle* nextMCP( double& weight ) {
    const LHCb::MCParticle* mcPart = m_linkTo.next();
    weight                         = ( mcPart ? m_linkTo.weight() : 0. );
    return mcPart;
  }

  double weightMCP() { return m_linkTo.weight(); }

  int associatedMCP( const SOURCE* obj );

  const LHCb::MCParticle* first( const SOURCE* obj ) { return firstMCP( obj ); }

  const LHCb::MCParticle* first( const SOURCE* obj, double& weight ) { return firstMCP( obj, weight ); }

  const LHCb::MCParticle* next() { return m_linkTo.next(); }

  const LHCb::MCParticle* next( double& weight ) { return nextMCP( weight ); }

  double weight() { return m_linkTo.weight(); }
  bool   notFound();
  bool   notFound( const std::string& contname );
  bool   checkAssociation( const SOURCE* obj, const LHCb::MCParticle* mcPart );

protected:
  const std::string& name() const { return m_parent->name(); }
  const std::string& context() const { return m_parent->context(); }
  MsgStream&         debug() { return m_parent->debug(); }
  MsgStream&         err() { return m_parent->err(); }
  MsgStream&         error() { return m_parent->error(); }
  MsgStream&         warning() { return m_parent->warning(); }
  bool               hasValidParent() const { return m_parent != nullptr; }

  IDataProviderSvc* evtSvc() const { return m_parent->evtSvc(); }
  IMessageSvc*      msgSvc() const { return m_parent->msgSvc(); }
  ISvcLocator*      svcLocator() const {
    if constexpr ( std::is_base_of_v<AlgTool, PARENT> ) {
      return m_parent->svcLoc();
    } else { // Algorithm
      return m_parent->svcLoc().get();
    }
  }

  const PARENT*            m_parent;
  std::string              m_extension;
  std::string              m_linkerAlgType;
  IAlgorithm*              m_linkerAlg = nullptr;
  std::vector<std::string> m_containerList{};

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noAlgTypeErr{m_parent, "No alg type given", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_noIAlgMgrErr{m_parent, "Could not locate IAlgManager", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_algoCreationErr{m_parent, "Could not create algorithm", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setContextErr{m_parent, "Unable to set Property Context", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_algInitErr{m_parent, "Error in algorithm initialization!", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setOutTableErr{m_parent, "Unable to set Property OutputTable",
                                                                       10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setInputErr{m_parent, "Unable to set Property InputData", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_setRITErr{m_parent, "Unable to set Property RootInTES", 10};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_ipropWarn{m_parent, "Unable to get IProperty pointer", 10};

  To     m_linkTo;
  Linker m_linkerTable;

  // Private methods
  void createLinks( const std::string& contName = "" );

  To* getLink( const std::string& contName );

  StatusCode  locateAlgorithm( const std::string& algType, const std::string& algName, IAlgorithm*& alg,
                               const std::vector<std::string>& inputData );
  std::string getGaudiRootInTES();
  StatusCode  setAlgInputData( IAlgorithm*& alg, const std::vector<std::string>& inputData );

  inline std::string containerName( const ContainedObject* obj ) const {
    return ( obj->parent() && obj->parent()->registry() ) ? obj->parent()->registry()->identifier() : std::string{};
  }
};

template <typename OBJ2MCP = LHCb::Particle, typename PARENT = GaudiAlgorithm>
class Object2FromMC : public Object2MCLinker<OBJ2MCP, PARENT> {

public:
  /// Standard constructors
  Object2FromMC( const PARENT* myMother )
      : Object2MCLinker<OBJ2MCP, PARENT>( myMother ), m_linkFrom( m_linkFromList.end() ) {}

  Object2FromMC( const PARENT* myMother, const int method, std::vector<std::string> containerList )
      : Object2MCLinker<OBJ2MCP, PARENT>( myMother, method, std::move( containerList ) )
      , m_linkFrom( m_linkFromList.end() ) {}

  Object2FromMC( const PARENT* myMother, const int method, const std::string& container )
      : Object2MCLinker<OBJ2MCP, PARENT>( myMother, method, container ), m_linkFrom( m_linkFromList.end() ) {}

  Object2FromMC( const PARENT* myMother, const std::string& algType, const std::string& extension,
                 std::vector<std::string> containerList )
      : Object2MCLinker<OBJ2MCP, PARENT>( myMother, algType, extension, std::move( containerList ) )
      , m_linkFrom( m_linkFromList.end() ) {}

  Object2FromMC( const PARENT* myMother, const std::string& algType, const std::string& extension,
                 const std::string& container )
      : Object2MCLinker<OBJ2MCP, PARENT>( myMother, algType, extension, container )
      , m_linkFrom( m_linkFromList.end() ) {}

  typedef LinkedFrom<OBJ2MCP> From;

  bool isAssociated( const KeyedObject<int>* obj ) {
    const OBJ2MCP* part = dynamic_cast<const OBJ2MCP*>( obj );
    if ( part ) return nullptr != firstMCP( part );
    const LHCb::MCParticle* mcPart = dynamic_cast<const LHCb::MCParticle*>( obj );
    if ( mcPart ) return nullptr != firstP( mcPart );
    return false;
  }

  OBJ2MCP* firstP( const LHCb::MCParticle* mcPart, double& weight ) {
    OBJ2MCP* part = firstP( mcPart );
    weight        = weightP();
    return part;
  }

  OBJ2MCP* firstP( const LHCb::MCParticle* mcPart ) {
    if ( !this->hasValidParent() || this->m_linkerAlgType.empty() || this->m_containerList.empty() ) return nullptr;

    createFromLinks();
    for ( auto fr = m_linkFromList.begin(); m_linkFromList.end() != fr; fr++ ) {
      OBJ2MCP* part = fr->first( mcPart );
      if ( part ) {
        // Remember which table and which MCParticle we are dealing with...
        m_linkFrom    = fr;
        m_linkFromMCP = mcPart;
        return part;
      }
    }
    m_linkFrom    = m_linkFromList.end();
    m_linkFromMCP = nullptr;
    return nullptr;
  }

  OBJ2MCP* nextP( double& weight ) {
    OBJ2MCP* part = nextP();
    weight        = ( part ? weightP() : 0. );
    return part;
  }

  OBJ2MCP* nextP() {
    // If there was not a first() called before, stop there
    if ( !m_linkFromMCP ) return nullptr;
    auto fr = m_linkFrom;
    if ( m_linkFromList.end() == fr ) return nullptr;
    OBJ2MCP* part = fr->next();
    if ( part ) return part;

    while ( m_linkFromList.end() != ++fr ) {
      // Get the first Particle associated to the MCP we were dealing with
      OBJ2MCP* part = fr->first( m_linkFromMCP );
      if ( part ) {
        m_linkFrom = fr;
        return part;
      }
    }
    m_linkFrom    = m_linkFromList.end();
    m_linkFromMCP = nullptr;
    return nullptr;
  }

  double weightP() { return m_linkFromList.end() != m_linkFrom ? m_linkFrom->weight() : 0.; }

  int associatedP( const KeyedObject<int>* obj ) {
    int n = 0;
    for ( auto* part = firstP( obj ); part; part = nextP() ) { ++n; }
    return n;
  }

private:
  std::vector<From>                    m_linkFromList;
  typename std::vector<From>::iterator m_linkFrom;
  const LHCb::MCParticle*              m_linkFromMCP = nullptr;

  void createFromLinks() {
    m_linkFromList.clear();
    for ( const auto& cont : this->m_containerList ) {
      const std::string name = cont + this->m_extension;
      From              test( this->evtSvc(), nullptr, name );
      if ( test.notFound() ) {
        this->createLinks( cont );
        test = From( this->evtSvc(), nullptr, name );
      }
      m_linkFromList.emplace_back( std::move( test ) );
    }
    m_linkFrom = m_linkFromList.end();
  }
};

#include "Particle2MCLinker.icpp"

/** Linker type for associations between ProtoParticles and MCParticles
 *
 *  @author Philippe Charpentier
 *  @date   2004-04-29
 */
using ProtoParticle2MCLinker = Object2FromMC<LHCb::ProtoParticle>;

/** Linker type for associations between Particles and MCParticles
 *
 *  @author Philippe Charpentier
 *  @date   2004-04-29
 */
using Particle2MCLinker = Object2FromMC<LHCb::Particle>;
