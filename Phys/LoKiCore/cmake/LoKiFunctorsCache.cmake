###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# LoKiFunctorsCache
# -----------------
#
# This module provides a function to generate and build cache libraries for
# LoKi Functors.
# @see https://its.cern.ch/jira/browse/LHCBPS-1357
#
# Usage: loki_functors_cache(<cache library name> optionfile1 [optionfile2...]
#                            [LINK_LIBRARIES library1 library2...]
#                            [INCLUDE_DIRS dir1 dir2...]
#                            [DEPENDS target1 target2...]
#                            [SPLIT <N>]
#                            [FACTORIES factory1 factory2...])
#
# where:
# - <cache library name> defines the name of the generated library
# - optionfile1 [optionfile2...] are the options defining the functors
#                                (usually the options used for the application
#                                we need to generate the cache for)
# - LINK_LIBRARIES, INCLUDE_DIRS have the same meaning as in gaudi_add_module()
# - DEPENDS target1 target2... declare tergets to be build before we can
#                              generate the cache code (e.g. "genconf" steps)
# - SPLIT <N> stated the number of files to generate per factory (default: 1)
# - FACTORIES factory1 factory2... name of the hybrids factories that will be
#                                  invoked (default: CoreFactory, HltFactory,
#                                  HybridFactory)
#
# Note: the CMake option LOKI_BUILD_FUNCTOR_CACHE can be set to False to disable
#       generation/build of declared functor caches.
#
# @author Marco Clemencic <marco.clemencic@cern.ch>
#

set(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS
    ${CMAKE_CURRENT_LIST_DIR}/LoKiFunctorsCachePostActionOpts.py
    CACHE FILEPATH "Special options file for LoKi Functors cache generation.")
mark_as_advanced(LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS)

# Workaround for gaudi/Gaudi#117
set(DISABLE_EXPANDVARS_OPTS ${CMAKE_CURRENT_LIST_DIR}/disable_expandvars.py)

set(CHECK_FUNCTOR_SOURCES ${CMAKE_CURRENT_LIST_DIR}/check_functor_sources.sh)

option(LOKI_BUILD_FUNCTOR_CACHE "Enable building of LoKi Functors Caches."
       TRUE)

# Usage: loki_functors_cache(cache_name option_file_1 [option_file_2 ...])
function(loki_functors_cache name)
  # ignore cache declaration if requested
  if(NOT LOKI_BUILD_FUNCTOR_CACHE)
    return()
  endif()

  CMAKE_PARSE_ARGUMENTS(ARG "" "SPLIT"
                            "LINK_LIBRARIES;INCLUDE_DIRS;DEPENDS;FACTORIES" ${ARGN})

  if(NOT ARG_FACTORIES)
    # default values
    set(ARG_FACTORIES CoreFactory HltFactory Hlt1HltFactory Hlt2HltFactory HybridFactory)
  endif()

  # where all the files are generated
  file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${name}_srcs)

  # Output filename(s)
  foreach(factory_type ${ARG_FACTORIES})
    if(ARG_SPLIT)
      set(idx ${ARG_SPLIT})
      while(${idx} GREATER 0)
        string(LENGTH "${idx}" idx_length)
        if(idx_length EQUAL 1)
          set(idx_string "000${idx}")
        elseif(idx_length EQUAL 2)
          set(idx_string "00${idx}")
        elseif(idx_length EQUAL 3)
          set(idx_string "0${idx}")
        else()
          set(idx_string "${idx}")
        endif()
        set(output_names FUNCTORS_${factory_type}_${idx_string}.cpp ${output_names})
        set(outputs ${name}_srcs/FUNCTORS_${factory_type}_${idx_string}.cpp ${outputs})
        math(EXPR idx "${idx} - 1")
      endwhile()
    else()
      set(ARG_SPLIT 0)
      set(output_names FUNCTORS_${factory_type}_0001.cpp)
      set(outputs ${name}_srcs/FUNCTORS_${factory_type}_0001.cpp)
    endif()
  endforeach()

  set(inputs ${LOKI_FUNCTORS_CACHE_POST_ACTION_OPTS})
  foreach(input ${ARG_UNPARSED_ARGUMENTS})
    if((NOT IS_ABSOLUTE input) AND (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${input}))
      set(input ${CMAKE_CURRENT_SOURCE_DIR}/${input})
    endif()
    set(inputs ${inputs} ${input})
  endforeach()

  set(deps ${inputs} ${ARG_DEPENDS})

  # compilation flags

  # We force no debug symbols (-g0) and we force an optimized build (-O3).
  # NDEBUG is not modified, meaning that -dbg builds will have asserts enabled.
  # This is necessary to reduce memory usage when compiling the functor cache
  # (and debugging the functor cache might be hard even with symbols).
  # If you need to have symbols to test something, either comment this line or
  # use something like target_compile_options(Moore_FunctorCache_Hlt2_... PRIVATE -g -Og)
  # to affect only one functor cache library.
  add_compile_options(-g0 -O3)

  set(tmp_ext pkl)
  if(COMMAND gaudi_common_add_build)
    add_custom_command(OUTPUT ${name}_srcs/${name}.${tmp_ext}
                      COMMAND ${env_cmd} --xml ${env_xml}
                        gaudirun.py -n -o ${name}_srcs/${name}.${tmp_ext} ${inputs}
                      DEPENDS ${deps}
                      COMMENT "Preprocess options for ${name}")

    add_custom_command(OUTPUT ${outputs}
                      COMMAND rm -f *.cpp
                      COMMAND ${env_cmd} --xml ${env_xml}
                          LOKI_GENERATE_CPPCODE=${ARG_SPLIT}
                          LOKI_DISABLE_CACHE=1
                        gaudirun.py ${DISABLE_EXPANDVARS_OPTS} ${name}.${tmp_ext}
                      COMMAND touch ${output_names}
                      COMMAND bash ${CHECK_FUNCTOR_SOURCES} ${output_names}
                      WORKING_DIRECTORY ${name}_srcs
                      DEPENDS ${name}_srcs/${name}.${tmp_ext}
                      COMMENT "Generating sources for ${name}")

    # Old style CMake configuration (gaudi_project)
    gaudi_common_add_build(${outputs}
                          LINK_LIBRARIES ${ARG_LINK_LIBRARIES}
                          INCLUDE_DIRS ${ARG_INCLUDE_DIRS})
    add_library(${name} MODULE ${outputs})
    target_link_libraries(${name} ${ARG_LINK_LIBRARIES})
    _gaudi_detach_debinfo(${name})

    gaudi_generate_componentslist(${name})

    #set_property(GLOBAL APPEND PROPERTY LOKI_FUNCTORS_CACHE_LIBRARIES ${name})

    gaudi_add_genheader_dependencies(${name})

    #----Installation details-------------------------------------------------------
    install(TARGETS ${name} LIBRARY DESTINATION lib OPTIONAL)
    #gaudi_export(MODULE ${name})
    # For debugging install generated sources
    # Would be nice if these could perhaps be zipped into a compressed tarball...
    #install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${name}_srcs DESTINATION ${CMAKE_INSTALL_PREFIX}/src )
  else()
      add_custom_command(OUTPUT ${name}_srcs/${name}.${tmp_ext}
          COMMAND ${CMAKE_BINARY_DIR}/run
              gaudirun.py -n -o ${name}_srcs/${name}.${tmp_ext} ${inputs}
          DEPENDS ${deps}
          COMMENT "Preprocess options for ${name}"
      )

      add_custom_command(OUTPUT ${outputs}
          COMMAND rm -f *.cpp
          COMMAND ${CMAKE_BINARY_DIR}/run env
              LOKI_GENERATE_CPPCODE=${ARG_SPLIT}
              LOKI_DISABLE_CACHE=1
              gaudirun.py ${DISABLE_EXPANDVARS_OPTS} ${name}.${tmp_ext}
          COMMAND touch ${output_names}
          COMMAND bash ${CHECK_FUNCTOR_SOURCES} ${output_names}
          WORKING_DIRECTORY ${name}_srcs
          DEPENDS ${name}_srcs/${name}.${tmp_ext}
          COMMENT "Generating sources for ${name}"
      )

    # Note: this is a stripped down version of "gaudi_add_module"
    add_library(${name} MODULE "${outputs}")
    if(ARG_LINK_LIBRARIES)
        target_link_libraries(${name} PRIVATE ${ARG_LINK_LIBRARIES})
    endif()
    install(
        TARGETS ${name}
        LIBRARY DESTINATION "${GAUDI_INSTALL_PLUGINDIR}"
        ${_gaudi_install_optional})
    # Create a symlink to the module in a directory that is added to the rpath of every target of the build
    add_custom_command(TARGET ${name} POST_BUILD
        BYPRODUCTS ${CMAKE_BINARY_DIR}/.plugins/${CMAKE_SHARED_MODULE_PREFIX}${name}${CMAKE_SHARED_MODULE_SUFFIX}
        COMMAND ${CMAKE_COMMAND} -E create_symlink $<TARGET_FILE:${name}> ${CMAKE_BINARY_DIR}/.plugins/$<TARGET_FILE_NAME:${name}>)
    # generate the list of components for all the plugins of the project
    add_custom_command(TARGET ${name} POST_BUILD
        BYPRODUCTS ${name}.components
        COMMAND run $<TARGET_FILE:Gaudi::listcomponents> --output ${name}.components $<TARGET_FILE_NAME:${name}>)
    set(merge_target ${PROJECT_NAME}_MergeComponents)
    # compatibility with Gaudi without gaudi/Gaudi!1308
    if(TARGET MergeRootmaps)
      set(merge_target MergeRootmaps)
    endif()
    _merge_files(${merge_target} "${CMAKE_BINARY_DIR}/${PROJECT_NAME}.components" # see private functions at the end
        ${name} "${CMAKE_CURRENT_BINARY_DIR}/${name}.components"
        "Merging .components files")
    if(TARGET listcomponents) # if we are building Gaudi (this function is not called from a downstream project)
        add_dependencies(${name} listcomponents)
    endif()
    # To append the path to the generated library to LD_LIBRARY_PATH with run
    set_property(TARGET target_runtime_paths APPEND
        PROPERTY runtime_ld_library_path $<SHELL_PATH:$<TARGET_FILE_DIR:${name}>>)
  endif()

endfunction()
