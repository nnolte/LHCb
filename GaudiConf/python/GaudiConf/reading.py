##############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring an application to read Moore HLT2 output."""
from __future__ import absolute_import
import os, json, XRootD.client

from .PersistRecoConf import PersistRecoPacking

from PyConf.location_prefix import unpacked_prefix
from PyConf.object_types import inv_classid_map
from PyConf.application import (
    default_raw_event, make_data_with_FetchDataFromFile, ComponentConfig)

from PyConf.components import force_location, setup_component

from GaudiConf import IOExtension
from Gaudi.Configuration import WARNING as OUTPUTLEVEL

__all__ = [
    "mc_unpackers", "unpackers", "decoder", "hlt_decisions", "unpack_rawevent"
]

RawBanks = {
    "HltRoutingBits": "/Event/DAQ/RawBanks/HltRoutingBits",
    "HltLumiSummary": "/Event/DAQ/RawBanks/HltLumiSummary",
    "ODIN": "/Event/DAQ/RawBanks/ODIN",
    "DstData": "/Event/DAQ/RawBanks/DstData",
    "HltSelReports": "/Event/DAQ/RawBanks/HltSelReports",
    "HltDecReports": "/Event/DAQ/RawBanks/HltDecReports"
}


def mc_unpackers(process='Hlt2',
                 filtered_mc=True,
                 configurables=True,
                 output_level=OUTPUTLEVEL):
    """Return a list of unpackers for reading Monte Carlo truth objects

    This must run BEFORE unpackers!!!.

    Args:
        process (str): 'Turbo', 'Spruce' or 'Hlt2'.
        filtered_mc (bool): If True, assume Moore saved only a filtered
                            subset of the input MC objects.
        configurables (bool): set to False to use PyConf Algorithm.

    """
    assert process == "Hlt2" or process == "Turbo" or process == "Spruce", 'MC unpacker helper only accepts Turbo, Spruce or Hlt2 process'

    mc_prefix = '/Event/HLT2' if filtered_mc else "/Event"
    if process == "Spruce":
        mc_prefix = '/Event/Spruce/HLT2'

    if configurables:
        from Configurables import UnpackMCParticle, UnpackMCVertex
        unpack_mcp = UnpackMCParticle(
            InputName=os.path.join(mc_prefix, "pSim/MCParticles"),
            OutputName=os.path.join(mc_prefix, "MC/Particles"),
            OutputLevel=output_level,
        )
        unpack_mcv = UnpackMCVertex(
            InputName=os.path.join(mc_prefix, "pSim/MCVertices"),
            OutputName=os.path.join(mc_prefix, "MC/Vertices"),
            OutputLevel=output_level,
        )
    else:
        from PyConf.application import make_data_with_FetchDataFromFile
        from PyConf.Algorithms import UnpackMCParticle, UnpackMCVertex
        unpack_mcp = UnpackMCParticle(
            InputName=make_data_with_FetchDataFromFile(
                os.path.join(mc_prefix, "pSim/MCParticles")),
            outputs={
                "OutputName":
                force_location(os.path.join(mc_prefix, "MC/Particles"))
            },
            OutputLevel=output_level)

        unpack_mcv = UnpackMCVertex(
            InputName=make_data_with_FetchDataFromFile(
                os.path.join(mc_prefix, "pSim/MCVertices")),
            outputs={
                "OutputName":
                force_location(os.path.join(mc_prefix, "MC/Vertices"))
            },
            OutputLevel=output_level,
        )

    return [unpack_mcp, unpack_mcv]


def unpack_rawevent(bank_types=["DstData", "HltDecReports"],
                    process='Hlt2',
                    stream="default",
                    raw_event_format=0.3,
                    configurables=True,
                    output_level=OUTPUTLEVEL):
    """Return RawBank:Views of banks in RawEvent.

    Args:
        bank_types (list of str): RawBanks to create RawBank:Views of.
        process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'
    if process == "Spruce" or process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
    else:
        bank_location = default_raw_event(["DstData"],
                                          raw_event_format=raw_event_format)

    for rb in bank_types:
        if rb not in RawBanks.keys():
            RawBanks[rb] = "/Event/DAQ/RawBanks/" + rb

    if configurables:
        from Configurables import LHCb__UnpackRawEvent
        bank_location = bank_location.location
        unpackrawevent = LHCb__UnpackRawEvent(
            BankTypes=bank_types,
            RawBankLocations=[RawBanks[rb] for rb in bank_types],
            RawEventLocation=bank_location,
            OutputLevel=output_level)
    else:
        from PyConf.Algorithms import LHCb__UnpackRawEvent
        unpackrawevent = LHCb__UnpackRawEvent(
            BankTypes=bank_types,
            RawBankLocations=[
                force_location(RawBanks[rb]) for rb in bank_types
            ],
            RawEventLocation=bank_location,
            OutputLevel=output_level)

    return unpackrawevent


def unpackers(locations,
              ann,
              process='Hlt2',
              data_type='Upgrade',
              configurables=True,
              mc={},
              output_level=OUTPUTLEVEL):
    """Return a list of unpackers for reading reconstructed objects.

    This must run AFTER mc_unpackers if MC data!!!.

    Args:
        process (str): 'Turbo' or 'Spruce' or 'Hlt2'.
        data_type (str): The data type to configure PersistRecoPacking (only option is Upgrade atm)
        mc: MCParticle and MCVertices locations
        ann: ANNSvc component, needed for finding linker locations
        locations: list of packed object locations to unpack
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    RECO = ''
    if process == "Spruce":
        TES_ROOT = '/Event/Spruce'
        RECO = 'HLT2'
    else:
        TES_ROOT = '/Event/HLT2'

    inputs = make_dict(locations, TES_ROOT, ann)
    outputs = {}
    for k, v in inputs.items():
        outputs[k] = [unpacked_prefix(l, TES_ROOT) for l in v]

    mc_loc = {}
    if len(mc) > 1:
        if configurables:
            mc_loc = {
                'Particles': mc[0].OutputName,
                'Vertices': mc[1].OutputName
            }
        else:
            mc_loc = {
                'Particles': mc[0].OutputName.location,
                'Vertices': mc[1].OutputName.location
            }

    prpacking = PersistRecoPacking(
        annsvc=ann.getFullName(),
        stream=TES_ROOT,
        reco_stream=RECO,
        data_type=data_type,
        packed=inputs,
        unpacked=outputs,
        mc=mc_loc)

    unpack_persistreco = prpacking.unpackers(
        configurables=configurables, output_level=output_level)

    return unpack_persistreco


def decoder(locations,
            ann,
            process='Hlt2',
            stream='default',
            raw_event_format=0.3,
            data_type='Upgrade',
            configurables=True,
            output_level=OUTPUTLEVEL):
    """Return a DstData raw bank decoder.

    Args:
        process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        data_type (str): The data type to configure PersistRecoPacking.
        ann: ANNSvc component, needed for finding linker locations
        locations: list of packed object locations to unpack
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    TES_ROOT = '/Event/HLT2'
    if process == "Spruce":
        bank_location = make_data_with_FetchDataFromFile(stream)
        TES_ROOT = '/Event/Spruce'
    elif process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
    else:
        bank_location = default_raw_event(["DstData"],
                                          raw_event_format=raw_event_format)

    if configurables:
        from Configurables import HltPackedBufferDecoder
        bank_location = bank_location.location
        decoder = HltPackedBufferDecoder(
            SourceID='Hlt2',
            ANNSvc=ann.getFullName(),
            RawEventLocations=bank_location,
            RawBanks=RawBanks["DstData"],
            DecReports=RawBanks["HltDecReports"],
            OutputBuffers=locations,
            OutputLevel=output_level)
    else:
        from PyConf.Algorithms import HltPackedBufferDecoder

        # Decoder is a splitting transformer
        # PyConf Algorithms expect a single output location
        # so following workaorund is needed to have a list as output
        import collections
        location_map = collections.OrderedDict()

        for l in range(len(locations)):
            location_map["Buffer" + str(l)] = locations[l]

        def decoder_transform(**outputs):
            dict = {
                location_map[k]: tes_location
                for k, tes_location in outputs.items()
            }

            return {"OutputBuffers": locations}

        decoder = HltPackedBufferDecoder(
            SourceID='Hlt2',
            ANNSvc=ann.getFullName(),
            RawEventLocations=bank_location,
            RawBanks=force_location(RawBanks["DstData"]),
            DecReports=force_location(RawBanks["HltDecReports"]),
            outputs={prop: None
                     for prop in location_map},
            output_transform=decoder_transform,
            OutputLevel=output_level)

    return decoder


def make_dict(locations, stream, ann):
    """Return a dictionary of object type: location list
       This is needed to decide which unpacker to use for a given object
    Args:
        stream (str): TES location prefix
        ann: ANNSvc component, needed for finding object types from json file
        locations: list of packed object locations to unpack
    """
    dict = PersistRecoPacking(stream=stream).dictionary()
    ptype = ann.PackedObjectTypes
    if ptype:
        classids = inv_classid_map()
        for loc in locations:
            if loc in ptype.keys():
                # ANN service doesn't allow duplicate keys/values
                # When an object type is registered for a location, type number is increased by i x 10^4
                # Here we undo that operation
                number = ptype[loc] % 10000
                type = classids[number]
                dict[type] += [loc]

    return dict


def make_locations(locations, stream):
    """Return a location list to unpack and decode
    Args:
        stream (str): TES location prefix
        locations: PackedObjectLocations dict from json file

    """
    locations_to_decode = []
    for key in locations.keys():
        if key.startswith(stream + "/p") or key.startswith(stream + "p"):
            locations_to_decode.append(force_location(key))

    return locations_to_decode


def hlt_decisions(process='Hlt2',
                  stream="default",
                  output_loc="/Event/Hlt/DecReports",
                  raw_event_format=0.3,
                  configurables=True,
                  output_level=OUTPUTLEVEL,
                  source='Hlt2'):
    """Return a HltDecReportsDecoder instance for HLT1, HLT2, Spruce or Turbo decisions.

    Args:
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        output_loc (str): TES location to put decoded DecReports.
        raw_event_format (float):
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
        source (str): 'Hlt1' or 'Hlt2' or 'Spruce'- indicates whether selection stage is HLT1, HLT2 or Spruce
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    if process == "Spruce" or process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
    else:
        bank_location = default_raw_event(["HltDecReports"],
                                          raw_event_format=raw_event_format)
    name = SourceID = source

    if configurables:
        from Configurables import HltDecReportsDecoder

        decode_hlt = HltDecReportsDecoder(
            name=name,
            SourceID=SourceID,
            RawEventLocations=bank_location.location,
            OutputHltDecReportsLocation=output_loc,
            OutputLevel=output_level)
    else:
        from PyConf.Algorithms import HltDecReportsDecoder

        decode_hlt = HltDecReportsDecoder(
            name=name,
            SourceID=SourceID,
            RawEventLocations=bank_location,
            outputs={
                'OutputHltDecReportsLocation': force_location(output_loc)
            },
            OutputLevel=output_level)

    return decode_hlt


def do_unpacking(annsvc,
                 stream="default",
                 locations=[],
                 process='Hlt2',
                 simulation=True,
                 configurables=True,
                 raw_event_format=0.3,
                 output_level=4):
    """Return a list of algorithm that will do a full unpacking of data

    Args:
        process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        annsvc: ANNSvc component, needed for finding linker locations and packed locations
        locations: list of packed object locations to unpack.
                   if none is given, all locations in PackedObjectLocation are taken
    """
    TES_ROOT = '/Event/HLT2'
    if process == "Spruce":
        TES_ROOT = '/Event/Spruce'

    if len(locations) == 0:
        # no locations are specified, so try to unpack everything in PackedObjectLocations
        locations = make_locations(annsvc.PackedObjectLocations, TES_ROOT)

    assert len(
        locations
    ) > 0, 'Nothing to be decoded, check the locations or ANNSvc file'

    # First we unpack rawbanks
    algs = [
        unpack_rawevent(
            raw_event_format=raw_event_format,
            stream=stream,
            process=process,
            output_level=output_level)
    ]

    # Next get the hlt2 decisions
    algs += [
        hlt_decisions(
            process=process,
            stream=stream,
            configurables=configurables,
            output_level=output_level,
            raw_event_format=raw_event_format,
            source="Hlt2",
            output_loc="/Event/Hlt2/DecReports")
    ]

    # if sprucing, get sprucing decisions
    if process == "Spruce" or process == "Turbo":
        algs += [
            hlt_decisions(
                process=process,
                stream=stream,
                output_level=output_level,
                configurables=configurables,
                source="Spruce",
                output_loc="/Event/Spruce/DecReports")
        ]

    # decode raw banks to packed data buffers
    packed_decoder = decoder(
        locations=locations,
        stream=stream,
        ann=annsvc,
        raw_event_format=raw_event_format,
        process=process,
        output_level=output_level,
        configurables=configurables)
    algs += [packed_decoder]

    # get mc unpackers (shouldn't this be only for simulation?)
    mc_algs = []
    if simulation:
        mc_algs = mc_unpackers(
            process=process,
            configurables=configurables,
            output_level=output_level)
        algs += mc_algs

    # Finally unpack packed data buffers to objects
    algs += unpackers(
        locations,
        annsvc,
        process=process,
        output_level=output_level,
        configurables=configurables,
        mc=mc_algs)
    return algs


def get_hltAnn_dict(annsvc_json_config):
    """
    Extract the HLT ANN dictionary of HLT2/Spruce locations from a .json file.

    Args:
        annsvc_json_config (str): path to the .json file containing the HltAnnSvc configuration.
        Examples:
        1. local path:   path/to/json_file
        2. eos path:     root://eoslhcb.cern.ch//path/to/json_file

    Returns:
        Dict with all the HLT2/Spruce locations.
    """
    tck = {}
    if "root://eoslhcb.cern.ch//" in str(annsvc_json_config):
        with XRootD.client.File() as f:
            status, _ = f.open(str(annsvc_json_config))
            if not status.ok:
                raise RuntimeError(
                    f"could not open {annsvc_json_config}: {status.message}")
            status, data = f.read()
            if not status.ok:
                raise RuntimeError(
                    f"could not read {annsvc_json_config}: {status.message}")
        tck = json.loads(data.decode('utf-8'))
    elif annsvc_json_config:
        with open(os.path.expandvars(annsvc_json_config)) as f:
            tck = json.load(f)

    return tck


def set_hltAnn_svc(annsvc_json_config):
    """
    Configures the Hlt ANN service to read correctly the spruced locations using the HltAnnSvc
    configuration of the HLT2 application.

    Args:
       annsvc_json_config (str): path to the .json file containing the HltAnnSvc configuration.
       Examples:
        1. local path:   path/to/json_file
        2. eos path:     root://eoslhcb.cern.ch//path/to/json_file
    """
    config = ComponentConfig()
    tck = get_hltAnn_dict(annsvc_json_config)
    if tck:
        ann_config = tck["HltANNSvc/HltANNSvc"]

        hlt1_sel_ids = {
            str(k): v
            for k, v in ann_config["Hlt1SelectionID"].items()
        }
        hlt2_sel_ids = {
            str(k): v
            for k, v in ann_config["Hlt2SelectionID"].items()
        }
        spruce_sel_ids = {
            str(k): v
            for k, v in ann_config["SpruceSelectionID"].items()
        }
        packed_object_locs = {
            str(k): v
            for k, v in ann_config["PackedObjectLocations"].items()
        }

        packed_object_types = {}
        if "PackedObjectTypes" in ann_config.keys():
            packed_object_types = {
                str(k): v
                for k, v in ann_config["PackedObjectTypes"].items()
            }

        config.add(
            setup_component(
                "HltANNSvc",
                Hlt1SelectionID=hlt1_sel_ids,
                Hlt2SelectionID=hlt2_sel_ids,
                SpruceSelectionID=spruce_sel_ids,
                PackedObjectLocations=packed_object_locs,
                PackedObjectTypes=packed_object_types))
    else:
        config.add(setup_component("HltANNSvc"))
    return config
