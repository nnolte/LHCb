###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Packing configuration for HLT persisted reconstruction."""

import os

from PyConf.Algorithms import RecVertexPacker, RecVertexUnpacker
from PyConf.Algorithms import VertexPacker, VertexUnpacker
from PyConf.Algorithms import RichPIDPacker, RichPIDUnpacker
from PyConf.Algorithms import MuonPIDPacker, MuonPIDUnpacker
from PyConf.Algorithms import ProtoParticlePacker, ProtoParticleUnpacker
from PyConf.Algorithms import ParticlePacker, ParticleUnpacker
from PyConf.Algorithms import TrackPacker, TrackUnpacker
from PyConf.Algorithms import FlavourTagPacker, FlavourTagUnpacker
from PyConf.Algorithms import CaloHypoPacker, CaloHypoUnpacker
from PyConf.Algorithms import CaloClusterPacker, CaloClusterUnpacker
from PyConf.Algorithms import CaloDigitPacker, CaloDigitUnpacker
from PyConf.Algorithms import CaloAdcPacker, CaloAdcUnpacker
from PyConf.Algorithms import RecSummaryPacker, RecSummaryUnpacker

from PyConf.Algorithms import P2VRelationPacker, P2VRelationUnpacker
from PyConf.Algorithms import P2MCPRelationPacker, P2MCPRelationUnpacker
from PyConf.Algorithms import P2IntRelationPacker, P2IntRelationUnpacker
from PyConf.Algorithms import P2InfoRelationPacker, P2InfoRelationUnpacker
from PyConf.Algorithms import PP2MCPRelationPacker, PP2MCPRelationUnpacker

from PyConf.application import make_data_with_FetchDataFromFile
from PyConf.components import get_output, force_location
from PyConf.dataflow import dataflow_config
from PyConf.location_prefix import prefix, packed_prefix, unpacked_prefix
import logging

#from Gaudi.Configuration import DEBUG as WARNING
from Gaudi.Configuration import WARNING


class PackingDescriptor(object):
    """Holds information of how and where to (un)pack objects.

    Attributes:
        name: Descriptive name of the object
        extra_inputs: Extra input locations needed by the packer algorithm to create links
        packer: Algorithm used for packing
        unpacker: Algorithm used for unpacking
    """

    def __init__(self, name, extra_inputs, packer, unpacker):
        self.name = name
        self.extra_inputs = extra_inputs
        self.packer = packer
        self.unpacker = unpacker

    def copy(self, **kwargs):
        new = PackingDescriptor(self.name, self.extra_inputs, self.packer,
                                self.unpacker)
        for k, v in kwargs.items():
            setattr(new, k, v)
        return new


standardDescriptors = {}

_standardDescriptors = [
    # PVs
    PackingDescriptor(
        name="PVs",
        extra_inputs=[],
        packer=RecVertexPacker,
        unpacker=RecVertexUnpacker),

    # Vertices
    PackingDescriptor(
        name="Vertices",
        extra_inputs=[],
        packer=VertexPacker,
        unpacker=VertexUnpacker),

    # Tracks
    PackingDescriptor(
        name="Tracks",
        extra_inputs=[],
        packer=TrackPacker,
        unpacker=TrackUnpacker),

    #  RichPIDs
    PackingDescriptor(
        name="RichPIDs",
        extra_inputs=[],
        packer=RichPIDPacker,
        unpacker=RichPIDUnpacker),

    #  MuonPIDs
    PackingDescriptor(
        name="MuonPIDs",
        extra_inputs=[],
        packer=MuonPIDPacker,
        unpacker=MuonPIDUnpacker),

    # CaloHypos
    PackingDescriptor(
        name="CaloHypos",
        extra_inputs=[],
        packer=CaloHypoPacker,
        unpacker=CaloHypoUnpacker),

    #  CaloCluster
    PackingDescriptor(
        name="CaloClusters",
        extra_inputs=[],
        packer=CaloClusterPacker,
        unpacker=CaloClusterUnpacker),

    #  CaloDigits
    PackingDescriptor(
        name="CaloDigits",
        extra_inputs=[],
        packer=CaloDigitPacker,
        unpacker=CaloDigitUnpacker),

    #  CaloAdcs
    PackingDescriptor(
        name="CaloAdcs",
        extra_inputs=[],
        packer=CaloAdcPacker,
        unpacker=CaloAdcUnpacker),

    #  Protos
    PackingDescriptor(
        name="ProtoParticles",
        extra_inputs=[],
        packer=ProtoParticlePacker,
        unpacker=ProtoParticleUnpacker),

    #  Particles
    PackingDescriptor(
        name="Particles",
        extra_inputs=[],
        packer=ParticlePacker,
        unpacker=ParticleUnpacker),

    # FlavourTags
    PackingDescriptor(
        name="FlavourTags",
        extra_inputs=[],
        packer=FlavourTagPacker,
        unpacker=FlavourTagUnpacker),

    # RecSummary
    PackingDescriptor(
        name="RecSummary",
        extra_inputs=[],
        packer=RecSummaryPacker,
        unpacker=RecSummaryUnpacker),

    #  Relations
    PackingDescriptor(
        name="P2VRelations",
        extra_inputs=[],
        packer=P2VRelationPacker,
        unpacker=P2VRelationUnpacker),
    PackingDescriptor(
        name="P2MCPRelations",
        extra_inputs=[],
        packer=P2MCPRelationPacker,
        unpacker=P2MCPRelationUnpacker),
    PackingDescriptor(
        name="P2IntRelations",
        extra_inputs=[],
        packer=P2IntRelationPacker,
        unpacker=P2IntRelationUnpacker),
    PackingDescriptor(
        name="P2InfoRelations",
        extra_inputs=[],
        packer=P2InfoRelationPacker,
        unpacker=P2InfoRelationUnpacker),
    PackingDescriptor(
        name="PP2MCPRelations",
        extra_inputs=[],
        packer=PP2MCPRelationPacker,
        unpacker=PP2MCPRelationUnpacker),
]

standardDescriptors['Upgrade'] = {
    i.name: i.copy(name=None)
    for i in _standardDescriptors
}

# These are common reco locations to be used if no location is specified as input.
standardUnpacked = {
    "ProtoParticles":
    ["/Event/Rec/ProtoP/Charged", "/Event/Rec/ProtoP/Neutrals"],
    "Tracks": ["/Event/Rec/Track/Best"],
    "PVs": ["/Event/Rec/Vertex/Primary"],
    "CaloHypos": [
        "/Event/Rec/Calo/Electrons", "/Event/Rec/Calo/Photons",
        "/Event/Rec/Calo/MergedPi0s", "/Event/Rec/Calo/SplitPhotons"
    ],
    "MuonPIDs": ["/Event/Rec/Muon/MuonPID"],
    "RichPIDs": ["/Event/Rec/Rich/PIDs"],
    "RecSummary": ["/Event/Rec/Summary"],
}

standardPacked = {}


#for unpackers: input with p (ie, pRec), output without(Rec). For packers, it's other way around.
class PersistRecoPacking(object):
    """Collection of packed object descriptors.

    Provides useful methods for dealing with the PersistReco packed objects.
    """

    def __init__(self,
                 annsvc="HltANNSvc",
                 stream="/Event/HLT2",
                 reco_stream="",
                 data_type="Upgrade",
                 mc={},
                 descriptors=standardDescriptors,
                 packed=standardPacked,
                 unpacked=standardUnpacked):
        """Collection of packed object descriptors.

        Args:
            descriptors: Dict of the form
                {data_type: <OrderedDict of PackingDescriptor objects>}.
            packed: Dict of the form {descriptor_name: packed_location list}.
            unpacked: Dict of the form {descriptor_name: unpacked_location list}.
            prefix: TES location stream in the form /Event/Spruce
            recoprefix: TES location stream in the form /Event/HLT2 for reconstruction objects
            annsvc: Name of the ANNService
            mc: in case of simulation, used in unpacking relations to MC particles
        """
        self._descriptors = descriptors[data_type]

        # if we are only unpacking/packing standard reco locations
        # attach the stream to the locations
        if packed == standardPacked:
            for k, vals in unpacked.items():
                if reco_stream not in stream:
                    vals = [prefix(l, reco_stream) for l in vals]
                packed[k] = [packed_prefix(l, stream) for l in vals]
        if unpacked == standardUnpacked:
            for k, vals in unpacked.items():
                if reco_stream not in stream:
                    vals = [prefix(l, reco_stream) for l in vals]
                unpacked[k] = [prefix(l, stream) for l in vals]

        # This is to avoid complaints about missing keys
        for name, d in self._descriptors.items():
            if not name in packed.keys():
                packed[name] = []
            if not name in unpacked.keys():
                unpacked[name] = []

        self.packed = packed
        self.unpacked = unpacked
        self.prefix = stream
        self.recoprefix = reco_stream
        self.annsvc = annsvc
        self.mc = mc

    def printPacking(self, log):
        """Simple printing to check persist reco configurtion."""
        log.debug("Packed locations == ", self.packed)
        log.debug("Unpacked locations == ", self.unpacked)
        log.debug("Stream == ", self.prefix)
        log.debug("Reco stream == ", self.recoprefix)
        log.debug("ANN service name ==", self.annsvc)
        log.debug("Is simulation ==", self.mc)

    def packedLocations(self):
        """Return a list with the packed object locations."""
        packed_locs = []
        for vals in self.packed.values():
            packed_locs += vals
        return packed_locs

    def unpackedLocations(self):
        """Return a list with the unpacked object locations."""
        unpacked_locs = []
        for vals in self.unpacked.values():
            unpacked_locs += vals
        return unpacked_locs

    def packedDict(self):
        """Return the dict with object type => packed object locations."""
        return self.packed

    def unpackedDict(self):
        """Return the dict with object type => unpacked object locations."""
        return self.unpacked

    def unpackers_by_key(self, configurables=False, output_level=WARNING):
        """Return the dictionary of unpacking algorithms."""
        algs = {}

        # make sure all types are in the dictionary
        for name, d in self._descriptors.items():
            algs[name] = []

        for name, d in self._descriptors.items():
            zipped = zip(self.unpacked[name], self.packed[name])

            for loc, ploc in zipped:
                #unpack only locations in the requested stream
                if (ploc.startswith(self.prefix)):

                    # craete a list of extra inputs for each type
                    extra_inputs = []
                    for dl in d.extra_inputs:
                        extra_inputs += self.unpacked[dl]

                    # Relations are special, deal with others first
                    #if not name.endswith("Relations"):
                    alg = d.unpacker(
                        InputName=force_location(ploc),
                        OutputLevel=output_level,
                        ANNSvc=self.annsvc,
                        outputs={'OutputName': force_location(loc)},
                        ExtraOutputs=[force_location(loc)],
                        ExtraInputs=[force_location(l) for l in extra_inputs])
                    algs[name] += [alg]

        if configurables:
            algs = self.as_configurables(algs)
        return algs

    def unpackers(self, configurables=False, output_level=WARNING):
        """Return the list of unpacking algorithms."""
        algs = []
        for unp in self.unpackers_by_key(output_level=output_level).values():
            algs += unp
        if configurables:
            algs = self.as_configurables(algs)
        return algs

    def packers_by_key(self,
                       configurables=False,
                       output_level=WARNING,
                       enable_check=False):
        """Return the dict of packing algorithms for the inputs."""
        algs = {}
        map = {}
        for name in self._descriptors.keys():
            algs[name] = []
        for name in self._descriptors.keys():
            zipped = zip(self.unpacked[name], self.packed[name])
            for loc, ploc in zipped:
                if self.recoprefix in loc:
                    if loc != unpacked_prefix(ploc, self.prefix):
                        map[loc] = unpacked_prefix(ploc, self.prefix)

        for name, d in self._descriptors.items():
            zipped = zip(self.unpacked[name], self.packed[name])

            for loc, ploc in zipped:
                if (loc.startswith(
                        self.prefix)):  #only location in the requested stream

                    alg = d.packer(
                        InputName=force_location(loc),
                        OutputLevel=output_level,
                        EnableCheck=enable_check,
                        outputs={'OutputName': force_location(ploc)},
                        ExtraOutputs=[force_location(ploc)],
                        ANNSvc=self.annsvc,
                        ContainerMap=map)
                    algs[name] += [alg]
        if configurables:
            algs = self.as_configurables(algs)
        return algs

    def packers(self,
                configurables=False,
                output_level=WARNING,
                enable_check=False):
        """Return the list of packing algorithms."""
        algs = []
        for p in self.packers_by_key(
                output_level=output_level, enable_check=enable_check).values():
            algs += p
        if configurables:
            algs = self.as_configurables(algs)
        return algs

    def dictionary(self):
        """Return the emty dict {name: []}."""
        m = {}
        for name, d in self._descriptors.items():
            m[name] = []
        return m

    def unpackedToPackedLocationMap(self):
        """Return the dict {unpacked_location: packed_location}.
        Packed Location is the location of the packed data objects to test the unpacking
        """
        m = {}
        for name, d in self._descriptors.items():
            for loc in self.unpacked[name]:
                m[loc] = packed_prefix(loc, self.prefix)
        return m

    @staticmethod
    def as_configurables(algs):
        """Convert a list of PyConf Algorithm objects to Configurables.

        The returned list is not guaranteed to be in an order which satisfies
        the data dependencies of the algorithms when run sequentially. Any
        tools the Algorithm objects reference will be ignored.

        This is required for the GaudiPython tests (PyConf uses HLT scheduler which has no support for GP),
    ie. we need Configurables not PyConf Algorithm objects
        """
        configuration = dataflow_config()
        for alg in algs:
            configuration.update(alg.configuration())
        configurables, _ = configuration.apply()
        return configurables
