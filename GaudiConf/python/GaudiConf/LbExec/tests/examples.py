###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def do_nothing(options):
    from PyConf.application import configure_input

    return configure_input(options)


def bad_function(options):
    raise TypeError("Something is wrong")


def execption_with_chain(options):
    try:
        try:
            raise Exception("Exception 1")
        except Exception:
            raise Exception("Exception 2")
    except Exception:
        raise Exception("Exception 3")


def return_none(options):
    return None


def do_something_2022(options):
    return do_nothing(None)


def do_something_2023(options, *args):
    return do_nothing(None)


def do_something_2024(arg1, arg2):
    return do_nothing(None)


def wrong_args():
    return do_nothing(None)
