###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Utilities for parsing the positional arguments to lbexec

This module provides two callable objects which can be used as types with
argparse. The majority of the code is for providing hints to the user about
what might be wrong in the case of errors.

``FunctionLoader``
------------------

Wrapper class which takes a function spec of the form ``module.name:callable``.
In the event of errors a best effort is made to advise the user of how to
correct the error. In the event the module to import or function raises an
exception tracebacks are rewritten to hide the implementation details of
lbexec.

``OptionsLoader``
------------------

Converts a '+' separated list of YAML file paths into an ``Application.Options``
object. The current application is discovered using the ``GAUDIAPPNAME``
environment variable. If required ``OVERRIDE_LBEXEC_APP`` can be passed to
override which application is loaded. This is used by projects created by
lb-dev where the value of ``GAUDIAPPNAME`` is ``${PROJECT_NAME}Dev``.
"""
import difflib
import ast
import os
import re
import shlex
import sys
import traceback
from importlib import import_module
from pathlib import Path
from types import TracebackType
from typing import Optional

import click
import pydantic
import yaml

from PyConf.application import ComponentConfig

from .options import Options as OptionsBase


class FunctionLoader:
    """Class for parsing the function_spec argument to lbexec"""

    def __init__(self, spec: str):
        self.spec = spec
        try:
            self.module_name, self.func_name = spec.split(":", 1)
        except ValueError:
            _suggest_spec_fix(spec, spec)
            sys.exit(1)

        # Import the module
        sys.path.insert(0, os.getcwd())
        try:
            module = import_module(self.module_name)
        except Exception as e:
            if isinstance(e, ModuleNotFoundError) and Path(
                    self.module_name).is_file():
                _suggest_spec_fix(spec, self.module_name, self.func_name)
                sys.exit(1)
            action_msg = f"import {self.module_name!r}"
            _raise_user_exception(e, action_msg, self)
        finally:
            sys.path.pop(0)

        # Get the function
        try:
            self._function = getattr(module, self.func_name)
        except AttributeError:
            function_names = _guess_function_names(self.module_name,
                                                   self.func_name)
            _suggest_module_fix(self.spec, self.module_name, function_names)
            sys.exit(1)

    def __call__(self, options: OptionsBase,
                 *extra_args: list[str]) -> ComponentConfig:
        """Run the user provided function and validate the result"""
        try:
            config = self._function(options, *extra_args)
        except Exception as e:
            args = ", ".join(["options"] + [repr(x) for x in extra_args])
            action_msg = "call " + click.style(
                f"{self.spec}({args})", fg="green")
            _raise_user_exception(e, action_msg, self)

        if not isinstance(config, ComponentConfig):
            log_error(f"{self._function!r} returned {type(config)}, "
                      f"expected {ComponentConfig}")
            sys.exit(1)

        return config


def OptionsLoader(options_spec: str) -> OptionsBase:
    """Convert a '+' separated list of paths to an Application.Options object."""
    # Import the Options object from the current application
    app_name = os.environ.get("OVERRIDE_LBEXEC_APP",
                              os.environ.get("GAUDIAPPNAME"))
    if not app_name:
        log_error("GAUDIAPPNAME is not set in environment!")
        sys.exit(1)
    # There is no LHCb import but we want it to be usable lbexec for merging
    if app_name == "LHCb":
        app_name = "GaudiConf.LbExec"
    app = import_module(app_name)
    Options = getattr(app, "Options", None)
    if not Options:
        log_error(f"{app_name} doesn't support lbexec")
        sys.exit(1)

    # Load and merge the various input YAML files
    options = {}
    for options_yaml in map(Path, re.split(r'(?<!\\)\+', options_spec)):
        if not options_yaml.is_file():
            log_error(f"{options_yaml} does not exist")
            sys.exit(1)
        options_data = yaml.safe_load(options_yaml.read_text())
        if not isinstance(options_data, dict):
            log_error(
                f"{options_yaml} should contain a mapping but got {options_data!r}"
            )
            sys.exit(1)
        options.update(options_data)

    # Parse the merged YAML
    try:
        return Options.parse_obj(options)
    except pydantic.ValidationError as e:
        errors = e.errors()
        log_error(f"Failed to validate options! Found {len(errors)} errors:")
        for error in errors:
            extra_msg = ""
            if error["type"] == "value_error.extra" and len(error["loc"]) == 1:
                suggestions = difflib.get_close_matches(
                    error["loc"][0], Options.schema()["properties"], n=1)
                if suggestions:
                    extra_msg = click.style(
                        f"Did you mean {suggestions[0]!r}?", fg="green")
            locs = ', '.join(map(repr, error['loc']))
            if locs == "'__root__'":
                click.echo(f" * {error['msg']}", err=True)
            else:
                click.echo(
                    f" * {click.style(error['msg'], fg='red')}: {locs} {extra_msg}",
                    err=True)
        sys.exit(1)


def log_info(message):
    click.echo(click.style("INFO: ", fg="green") + message, err=True)


def log_error(message):
    click.echo(click.style("ERROR: ", fg="red") + message, err=True)


def _suggest_spec_fix(spec, module_name: str, func_name: Optional[str] = None):
    if os.path.isfile(module_name):
        filename = Path(module_name).absolute().relative_to(Path.cwd())
        module_name = str(filename.with_suffix("")).replace(os.sep, ".")
        if module_name.endswith(".__init__"):
            module_name = module_name[:-len(".__init__")]
        # If given, assume the user's function name is correct
        func_names = [func_name
                      ] if func_name else _guess_function_names(module_name)
    else:
        func_names = _guess_function_names(module_name)
    _suggest_module_fix(spec, module_name, func_names)


def _guess_function_names(module_name: str,
                          function_name: Optional[str] = None) -> list[str]:
    module_path = _guess_module_path(module_name)
    if not module_path:
        return []
    module_ast = ast.parse(module_path.read_text())
    functions = [
        node for node in ast.iter_child_nodes(module_ast)
        if isinstance(node, ast.FunctionDef)
    ]
    function_names = [
        function.name for function in functions
        if function.args.args and function.args.args[0].arg == "options"
    ]
    if function_name:
        function_names = difflib.get_close_matches(
            function_name, function_names, cutoff=0)
    return function_names


def _suggest_module_fix(spec: str, module_name: str,
                        function_names: list[str]):
    log_error(f"There seems to be an issue with your funcion specification.")
    if function_names:
        click.echo("Did you mean one of these:\n", err=True)
        for maybe_function in function_names:
            argv = [Path(sys.argv[0]).name] + sys.argv[1:]
            original = shlex.join(argv)
            argv[sys.argv.index(spec)] = f"{module_name}:{maybe_function}"
            corrected = shlex.join(argv)
            _print_diff(original, corrected)
            click.echo(err=True)
    else:
        log_error("Failed to find a suggested fix")


def _print_diff(original: str, corrected: str):
    s = difflib.SequenceMatcher(None, original, corrected)
    s1 = s2 = ""
    for tag, i1, i2, j1, j2 in s.get_opcodes():
        fg1 = fg2 = None
        if tag in ["replace", "delete"]:
            fg1 = "red"
        if tag in ["replace", "insert"]:
            fg2 = "green"
        s1 += click.style(s.a[i1:i2], fg=fg1)
        s2 += click.style(s.b[j1:j2], fg=fg2)
    click.echo(click.style(" Original: ", fg="red") + s1, err=True)
    click.echo(click.style("Corrected: ", fg="green") + s2, err=True)


def _raise_user_exception(e, action_msg, spec):
    module_path = str(_guess_module_path(spec.module_name))

    # Make a new trace back with everything above module_path removed
    total_frames = 0
    to_ignore = None
    for frame, _ in traceback.walk_tb(sys.exc_info()[2]):
        if to_ignore is None and frame.f_code.co_filename == module_path:
            to_ignore = total_frames
        total_frames += 1
    limit = to_ignore - total_frames if to_ignore else None
    traceback.print_exception(*sys.exc_info(), limit=limit)

    log_error(f"Failed to {action_msg}, see above for more information.")
    sys.exit(1)


def _guess_module_path(name: str) -> Optional[Path]:
    """Static implementation of ``importlib.util.find_spec``

    When calling ``find_spec("foo.bar")`` Python will execute ``foo/__init__.py``.
    This is required for correctness however when showing help messages we should
    avoid running any extra code so this function attempts to guess the path to
    the module's source.
    """
    from importlib.machinery import SourceFileLoader
    from importlib.util import find_spec

    top_module, *child_modules = name.split(".")

    sys.path.insert(0, os.getcwd())
    try:
        module_spec = find_spec(top_module)
    finally:
        sys.path.pop(0)

    if not (module_spec and child_modules):
        if isinstance(getattr(module_spec, "loader", None), SourceFileLoader):
            return Path(module_spec.loader.get_filename())
        return None

    if not module_spec.submodule_search_locations:
        return None
    module_path = Path(module_spec.submodule_search_locations[0])

    for name in child_modules:
        module_path = module_path / name
    if module_path.is_dir():
        module_path = module_path / "__init__.py"
    else:
        module_path = module_path.parent / f"{module_path.name}.py"

    return module_path if module_path.is_file() else None
