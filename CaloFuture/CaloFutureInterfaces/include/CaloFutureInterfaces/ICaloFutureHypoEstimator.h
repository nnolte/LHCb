/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureInterfaces/Enums.h"
#include "CaloFutureInterfaces/ICaloFutureHypo2CaloFuture.h"
#include "DetDesc/IGeometryInfo.h"
#include "Event/CaloHypo.h"
#include "Event/Track.h"

#include "GaudiKernel/IAlgTool.h"

#include <string>

namespace LHCb::Calo::Interfaces {

  /**
   *  @author Olivier Deschamps
   *  @date   2010-08-18
   */
  struct IHypoEstimator : extend_interfaces<IAlgTool> {

    // Return the interface ID
    DeclareInterfaceID( IHypoEstimator, 3, 0 );

    virtual std::optional<double>            data( const DeCalorimeter& fromCalo, const DeCalorimeter& toCalo,
                                                   const CaloHypo& hypo, Enum::DataType type ) const = 0;
    virtual std::map<Enum::DataType, double> get_data( const DeCalorimeter& fromCalo, const DeCalorimeter& toCalo,
                                                       const CaloHypo& hypo ) const                  = 0;
    virtual StatusCode                       _setProperty( const std::string&, const std::string& )  = 0;
    virtual IHypo2Calo*                      hypo2Calo()                                             = 0;
  };
} // namespace LHCb::Calo::Interfaces
