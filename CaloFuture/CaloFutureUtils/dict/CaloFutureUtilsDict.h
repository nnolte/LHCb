/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloFutureUtils/CaloFutureParticle.h"
#include "CaloFutureUtils/CaloFutureUtils.h"
#include "CaloFutureUtils/Kinematics.h"
#include "CaloKernel/CaloVector.h"

#include "Event/CaloAdc.h"
#include "Event/CaloDigit.h"

namespace {

  struct CaloFutureUtils_Instantiations {
    std::vector<LHCb::Detector::Calo::CellID> m_1;
    std::set<LHCb::Detector::Calo::CellID>    m_2;
    LHCb::CaloDigit::Vector                   m_3;
    LHCb::CaloDigit::Set                      m_4;

    CaloVector<LHCb::CaloDigit>  m_5;
    CaloVector<LHCb::CaloDigit*> m_6;
    CaloVector<LHCb::CaloAdc>    m_7;
  };

} // namespace
