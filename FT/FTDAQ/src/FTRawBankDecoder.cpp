/***************************************************************************** \
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <range/v3/view/subrange.hpp>
#include <range/v3/view/transform.hpp>

#include <Gaudi/Property.h>
#include <GaudiAlg/GaudiAlgorithm.h>

#include <DetDesc/Condition.h>
#include <Detector/FT/FTSourceID.h>
#include <Event/FTLiteCluster.h>
#include <Event/RawBank.h>
#include <LHCbAlgs/Transformer.h>

#include <FTDAQ/FTDAQHelper.h>
#include <FTDAQ/FTRawBankParams.h>
#include <FTDAQ/FTReadoutMap.h>

#include <algorithm>
#include <netinet/in.h>

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

/** @class FTRawBankDecoder FTRawBankDecoder.h
 *  Decode the FT raw bank into FTLiteClusters
 *
 *  @author Olivier Callot
 *  @date   2012-05-11
 */
class FTRawBankDecoder
    : public LHCb::Algorithm::Transformer<
          FTLiteClusters( const EventContext&, const LHCb::RawBank::View&, const FTReadoutMap<FTRawBankDecoder>& ),
          LHCb::DetDesc::usesBaseAndConditions<GaudiAlgorithm, FTReadoutMap<FTRawBankDecoder>>> {
public:
  /// Standard constructor
  FTRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  FTLiteClusters operator()( const EventContext& evtCtx, const LHCb::RawBank::View& banks,
                             const FTReadoutMap<FTRawBankDecoder>& readoutMap ) const override;

private:
  // for MC, following property has to be same as cluster creator,
  // not sure how to ensure this
  Gaudi::Property<unsigned int> m_clusterMaxWidth{this, "ClusterMaxWidth", 4u, "Maximal cluster width"};
  // Force decoding of v5 as v4 for testing of v5 encoding; expert use only
  Gaudi::Property<bool> m_decodeV5AsV4{this, "DecodeV5AsV4", false, "Decode v5 as v4"};

  template <unsigned int version>
  FTLiteClusters decode( const EventContext&                   evtCtx, LHCb::RawBank::View,
                         const FTReadoutMap<FTRawBankDecoder>& readoutMap, unsigned int nClusters ) const;

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_corrupt{this, "Possibly corrupt data. Ignoring the cluster."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_wrongLink{
      this, "Received data from a link that should be disabled. Fix the DB."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_nonExistingModule{
      this, "Skipping cluster(s) for non-existing module."};
};

//-----------------------------------------------------------------------------
// Implementation file for class : FTRawBankDecoder
//
// 2012-05-11 : Olivier Callot
//-----------------------------------------------------------------------------

namespace {
  unsigned quarterFromChannel( LHCb::Detector::FTChannelID id ) {
    return id.uniqueQuarter() - 16u;
  } // FIXME: hardcoded.

  constexpr unsigned channelInBank( short int c ) { return ( c >> FTRawBank::cellShift ); }

  constexpr unsigned getLinkInBank( short int c ) { return ( ( c >> FTRawBank::linkShift ) ); }

  constexpr int cell( short int c ) { return ( c >> FTRawBank::cellShift ) & FTRawBank::cellMaximum; }

  constexpr int fraction( short int c ) { return ( c >> FTRawBank::fractionShift ) & FTRawBank::fractionMaximum; }

  constexpr bool cSize( short int c ) { return ( c >> FTRawBank::sizeShift ) & FTRawBank::sizeMaximum; }

  constexpr auto is_in_module = []( LHCb::Detector::FTChannelID::ModuleID mod ) {
    return [mod]( const LHCb::FTLiteCluster cluster ) { return cluster.channelID().module() == mod; };
  };

  template <typename Iter>
  void reverse_each_module( Iter first, Iter last ) {
    for ( unsigned int iMod = 0; iMod < 5; ++iMod ) {
      auto finish = std::partition_point( first, last, is_in_module( LHCb::Detector::FTChannelID::ModuleID{iMod} ) );
      std::reverse( first, finish ); // swap clusters in module
      first = finish;
    }
    assert( std::all_of( first, last, is_in_module( LHCb::Detector::FTChannelID::ModuleID{5u} ) ) &&
            "Remaining partition should all be in module 5..." );
    std::reverse( first, last );
  }

  template <typename Container, typename Fun>
  void for_each_quadrant( Container& c, Fun&& f ) {
    for ( uint16_t iQuarter = 0; iQuarter < c.nSubDetectors(); ++iQuarter ) {
      auto range = c.range_( iQuarter );
      f( range.first, range.second, iQuarter );
    }
  }
} // namespace

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTRawBankDecoder )

FTRawBankDecoder::FTRawBankDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"RawBanks", "DAQ/RawBanks/FTCluster"},
#ifdef USE_DD4HEP
                    KeyValue{"ReadoutMapStorage", "/world:AlgorithmSpecific-" + name + "-ReadoutMap"}},
#else
                    KeyValue{"ReadoutMapStorage", "AlgorithmSpecific-" + name + "-ReadoutMap"}},
#endif
                   KeyValue{"OutputLocation", LHCb::FTLiteClusterLocation::Default} ) {
}

StatusCode FTRawBankDecoder::initialize() {
  return Transformer::initialize().andThen( [&] {
    FTReadoutMap<FTRawBankDecoder>::addConditionDerivation( this, inputLocation<FTReadoutMap<FTRawBankDecoder>>() );
  } );
}

template <>
FTLiteClusters FTRawBankDecoder::decode<0>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap<FTRawBankDecoder>& readoutMap,
                                            unsigned int                          nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks

    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = quarterFromChannel( offset );

    auto make_cluster = [&clus, &quarter]( LHCb::Detector::FTChannelID chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( chan, fraction, size ), quarter );
    };

    // Loop over clusters
    auto range = bank->range<uint16_t>();
    range      = range.subspan( 4 ); // skip first 64b of header;
    if ( !range.empty() && range[range.size() - 1] == 0 )
      range = range.first( range.size() - 1 ); // remove padding at end
    for ( unsigned short int c : range ) {
      c = htons( c );

      auto channel = LHCb::Detector::FTChannelID{offset + channelInBank( c )};
      make_cluster( channel, fraction( c ), cSize( c ) );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "quarter " << quarter << " channel  " << channel << " size " << cSize( c ) << " fraction "
                << fraction( c ) << endmsg;
    }
  }
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<7>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap<FTRawBankDecoder>& readoutMap,
                                            unsigned int                          nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  // Sort the banks now by x
  // sort the banks by sourceID
  auto bankOrder = std::vector<unsigned int>( banks.size() );
  std::iota( bankOrder.begin(), bankOrder.end(), 0u );
  auto srcID = [&banks]( size_t i ) { return banks[i]->sourceID() & 0x7FF; }; // FIXME
  std::sort( bankOrder.begin(), bankOrder.end(),
             [&srcID]( size_t i1, size_t i2 ) { return srcID( i1 ) < srcID( i2 ); } );
  for ( unsigned int bankIndex : bankOrder ) { // Iterates over the banks
    const LHCb::RawBank* bank     = banks[bankIndex];
    auto                 sourceID = bank->sourceID();
    auto                 iRow     = readoutMap.iSource( sourceID );
    if ( iRow == readoutMap.nBanks() ) {
      error() << "Could not find the sourceID " << sourceID << " in the readout map" << endmsg;
      continue;
    }
    // Vector to temporarily store clusters
    std::array<boost::container::small_vector<LHCb::FTLiteCluster, FTRawBank::nbClusMaximum>,
               FTRawBank::BankProperties::NbLinksPerBank>
                                clustersInBankPerLinkID;
    unsigned int                localLinkIndex = 0;
    LHCb::Detector::FTChannelID globalSiPMID;

    // Define Lambda functions to be used in loop
    auto make_cluster = [&globalSiPMID, &clustersInBankPerLinkID, &localLinkIndex]( unsigned chan, int fraction,
                                                                                    int size ) {
      clustersInBankPerLinkID[localLinkIndex].emplace_back( LHCb::Detector::FTChannelID( globalSiPMID + chan ),
                                                            fraction, size );
    };

    // Make clusters between two channels
    auto make_clusters = [&]( unsigned firstChannel, short int c, short int c2 ) {
      unsigned int widthClus = ( cell( c2 ) - cell( c ) + 2 ); // lastCh-firstCh+4/2

      // fragmented clusters, size > 2*max size
      // only edges were saved, add middles now
      if ( widthClus > 8 ) {
        // add the first edge cluster, and then the middle clusters
        unsigned int i = 0;
        for ( ; i < widthClus - 4; i += 4 ) {
          // all middle clusters will have same size as the first cluster,
          // for max size 4, fractions is always 1
          make_cluster( firstChannel + i, 1, 0 );
        }

        // add the last edge
        unsigned int lastChannel = firstChannel + i + std::floor( ( widthClus - i - 1 ) / 2 ) - 1;
        make_cluster( lastChannel, ( widthClus - 1 ) % 2, 0 );
      } else { // big cluster size upto size 8

        make_cluster( firstChannel + ( widthClus - 1 ) / 2 - 1, ( widthClus - 1 ) % 2, widthClus );

      } // end if adjacent clusters
    };  // End lambda make_clusters

    auto clusters = bank->range<short int>();
    clusters      = clusters.subspan( 2 ); // skip first 32b of header
    if ( clusters.size() != 0 && clusters.back() == 0 )
      clusters = clusters.first( clusters.size() - 1 ); // Remove padding at the end
    while ( !clusters.empty() ) {                       // loop over the clusters
      unsigned channel = channelInBank( clusters[0] );
      localLinkIndex   = getLinkInBank( clusters[0] );
      globalSiPMID     = readoutMap.getGlobalSiPMIDFromIndex( iRow, localLinkIndex );
      if ( globalSiPMID == LHCb::Detector::FTChannelID::kInvalidChannel() ) {
        ++m_wrongLink;
        clusters = clusters.subspan( 1 );
        continue;
      }
      unsigned channelInSiPM = LHCb::Detector::FTChannelID( channel ).channel();
      if ( !cSize( clusters[0] ) ) { // Not flagged as large
        make_cluster( channelInSiPM, fraction( clusters[0] ), 4 );
      } else { // Large cluster
        // last cluster in bank or in sipm
        if ( clusters.size() == 1 || getLinkInBank( clusters[0] ) != getLinkInBank( clusters[1] ) ) {
          make_cluster( channelInSiPM, fraction( clusters[0] ), 0 );
        }
        // flagged as first cluster of a large one
        else if ( fraction( clusters[0] ) ) {
          if ( cSize( clusters[1] ) && !fraction( clusters[1] ) ) { // this should always be true
            make_clusters( channelInSiPM, clusters[0], clusters[1] );
            clusters = clusters.subspan( 1 );
          } else { // this should never happen: cannot find the end of the large cluster, flagged with Size 1 and
                   // Fraction 0
            ++m_corrupt;
          }
        } else { // this should never happen: this would be an isolated Size 1 Fraction 0 cluster (end of a large
                 // cluster)
          ++m_corrupt;
        }
      }
      clusters = clusters.subspan( 1 );
    }
    // Sort clusters within bank using the SiPMID->linkID index array from FTReadoutMap
    auto SiPMindices = readoutMap.getSiPMRemappingFromIndex( iRow );
    for ( auto index : SiPMindices ) {
      if ( index == -1 ) break; // stop when padding is reached
      // Now loop over clusters within link (ordered by definition) and add to object
      auto clusterVector = clustersInBankPerLinkID[index];
      for ( auto clusterData : clusterVector ) {
        if ( msgLevel( MSG::DEBUG ) ) debug() << clusterData.channelID() << endmsg;
        clus.addHit(
            std::forward_as_tuple( clusterData.channelID(), clusterData.fractionBit(), clusterData.pseudoSize() ),
            quarterFromChannel( clusterData.channelID() ) );
      }
    }
  } // end loop over rawbanks
  clus.setOffsets();
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<6>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap<FTRawBankDecoder>& readoutMap,
                                            unsigned int                          nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = quarterFromChannel( offset );

    // Define Lambda functions to be used in loop
    auto make_cluster = [&clus, &quarter]( unsigned int chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{chan}, fraction, size ), quarter );
    };

    // Make clusters between two channels
    auto make_clusters = [&]( unsigned firstChannel, short int c, short int c2 ) {
      unsigned int widthClus = ( cell( c2 ) - cell( c ) + 2 ); // lastCh-firstCh+4/2

      // fragmented clusters, size > 2*max size
      // only edges were saved, add middles now
      if ( widthClus > 8 ) {
        // add the first edge cluster, and then the middle clusters
        unsigned int i = 0;
        for ( ; i < widthClus - 4; i += 4 ) {
          // all middle clusters will have same size as the first cluster,
          // for max size 4, fractions is always 1
          make_cluster( firstChannel + i, 1, 0 );
        }

        // add the last edge
        unsigned int lastChannel = firstChannel + i + std::floor( ( widthClus - i - 1 ) / 2 ) - 1;
        make_cluster( lastChannel, ( widthClus - 1 ) % 2, 0 );
      } else { // big cluster size upto size 8
        make_cluster( firstChannel + ( widthClus - 1 ) / 2 - 1, ( widthClus - 1 ) % 2, widthClus );

      } // end if adjacent clusters
    };  // End lambda make_clusters

    // loop over clusters
    auto clusters = bank->range<short int>();
    clusters      = clusters.subspan( 2 ); // skip first 32b of header
    if ( !clusters.empty() && clusters.back() == 0 )
      clusters = clusters.first( clusters.size() - 1 ); // Remove padding at the end
    while ( !clusters.empty() ) {                       // loop over the clusters
      auto channel = LHCb::Detector::FTChannelID{offset + channelInBank( clusters[0] )};
      if ( !cSize( clusters[0] ) ) // Not flagged as large
        make_cluster( channel, fraction( clusters[0] ), 4 );

      else {
        // last cluster in bank or in sipm
        if ( clusters.size() == 1 || getLinkInBank( clusters[0] ) != getLinkInBank( clusters[1] ) ) {
          make_cluster( channel, fraction( clusters[0] ), 0 );
        } else if ( fraction( clusters[0] ) ) {
          if ( cSize( clusters[1] ) && !fraction( clusters[1] ) ) { // this should always be true
            make_clusters( channel, clusters[0], clusters[1] );
            clusters = clusters.subspan( 1 );
          } else { // this should never happen,
            ++m_corrupt;
          }
        } else { // this should never happen,
          ++m_corrupt;
        }
      }
      clusters = clusters.subspan( 1 );
    }
  } // end loop over rawbanks
  clus.setOffsets();
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<4>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap<FTRawBankDecoder>& readoutMap,
                                            unsigned int                          nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset = readoutMap.channelIDShift( bank->sourceID() );
    auto                        first  = bank->begin<short int>() + 2; // skip first 32b of the header
    auto                        last   = bank->end<short int>();

    if ( *( last - 1 ) == 0 && first < last ) --last; // Remove padding at the end

    auto r = ranges::make_subrange( first, last ) |
             ranges::views::transform( [&offset]( unsigned short int c ) -> LHCb::FTLiteCluster {
               return {LHCb::Detector::FTChannelID{offset + channelInBank( c )}, fraction( c ), ( cSize( c ) ? 0 : 4 )};
             } );
    clus.insert( r.begin(), r.end(), quarterFromChannel( offset ) );
  }
  return clus;
}

template <>
FTLiteClusters FTRawBankDecoder::decode<5>( const EventContext& evtCtx, LHCb::RawBank::View banks,
                                            const FTReadoutMap<FTRawBankDecoder>& readoutMap,
                                            unsigned int                          nClusters ) const {
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) { // Iterates over the banks
    LHCb::Detector::FTChannelID offset  = readoutMap.channelIDShift( bank->sourceID() );
    auto                        quarter = quarterFromChannel( offset );

    // Define Lambda functions to be used in loop
    auto make_cluster = [&clus, &quarter]( unsigned chan, int fraction, int size ) {
      clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{chan}, fraction, size ), quarter );
    };

    // Make clusters between two channels
    auto make_clusters = [&]( unsigned firstChannel, short int c, short int c2 ) {
      unsigned int delta = ( cell( c2 ) - cell( c ) );

      // fragmented clusters, size > 2*max size
      // only edges were saved, add middles now
      if ( delta > m_clusterMaxWidth ) {
        // add the first edge cluster, and then the middle clusters
        for ( unsigned int i = m_clusterMaxWidth; i < delta; i += m_clusterMaxWidth ) {
          // all middle clusters will have same size as the first cluster,
          // so re-use the fraction
          make_cluster( firstChannel + i, fraction( c ), 0 );
        }
        // add the last edge
        make_cluster( firstChannel + delta, fraction( c2 ), 0 );
      } else { // big cluster size upto size 8
        unsigned int widthClus = 2 * delta - 1 + fraction( c2 );
        make_cluster( firstChannel + ( widthClus - 1 ) / 2 - int( ( m_clusterMaxWidth - 1 ) / 2 ),
                      ( widthClus - 1 ) % 2, widthClus );
      } // end if adjacent clusters
    };  // End lambda make_clusters

    // loop over clusters
    auto it   = bank->begin<short int>() + 2; // skip first 32b of header
    auto last = bank->end<short int>();
    if ( *( last - 1 ) == 0 ) --last; // Remove padding at the end
    for ( ; it < last; ++it ) {       // loop over the clusters
      unsigned short int c       = *it;
      auto               channel = LHCb::Detector::FTChannelID{offset + channelInBank( c )};

      if ( !cSize( c ) || it + 1 == last ) // No size flag or last cluster
        make_cluster( channel, fraction( c ), 4 );
      else { // Flagged or not the last one.
        unsigned c2 = *( it + 1 );
        if ( cSize( c2 ) && getLinkInBank( c ) == getLinkInBank( c2 ) ) {
          make_clusters( channel, c, c2 );
          ++it;
        } else {
          make_cluster( channel, fraction( c ), 4 );
        }
      }
    }
  } // end loop over rawbanks
  clus.setOffsets();

  return clus;
}

template <unsigned int vrsn>
FTLiteClusters FTRawBankDecoder::decode( const EventContext& evtCtx, LHCb::RawBank::View     banks,
                                         const FTReadoutMap<FTRawBankDecoder>&, unsigned int nClusters ) const {
  static_assert( vrsn == 2 || vrsn == 3 );
  FTLiteClusters clus{nClusters, LHCb::getMemResource( evtCtx )};
  for ( const LHCb::RawBank* bank : banks ) {
    auto source  = static_cast<unsigned>( bank->sourceID() );
    auto station = LHCb::Detector::FTChannelID::StationID{source / 16 + 1u};
    auto layer   = LHCb::Detector::FTChannelID::LayerID{( source & 12 ) / 4};
    auto quarter = LHCb::Detector::FTChannelID::QuarterID{source & 3};

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "source " << source << " station " << to_unsigned( station ) << " layer " << to_unsigned( layer )
                << " quarter " << to_unsigned( quarter ) << " size " << bank->size() << endmsg;

    auto first = bank->begin<short int>();
    auto last  = bank->end<short int>();

    while ( first != last ) {
      int sipmHeader = *first++;
      if ( first == last && sipmHeader == 0 ) continue; // padding at the end...
      unsigned modulesipm = sipmHeader >> 4;
      auto     module     = LHCb::Detector::FTChannelID::ModuleID{modulesipm >> 4};
      auto     mat =
          LHCb::Detector::FTChannelID::MatID{( modulesipm & 15 ) >> 2}; // hardcoded: this should be replaced by mapping
      unsigned sipm  = modulesipm & 3;                                  // hardcoded: this should be replaced by mapping
      int      nClus = sipmHeader & 15;

      if ( msgLevel( MSG::VERBOSE ) && nClus > 0 )
        verbose() << " Module " << to_unsigned( module ) << " mat " << to_unsigned( mat ) << " SiPM " << sipm
                  << " nClusters " << nClus << endmsg;

      if ( nClus > std::distance( first, last ) ) {
        warning() << "Inconsistent size of rawbank. #clusters in header=" << nClus
                  << ", #clusters in bank=" << std::distance( first, last ) << endmsg;

        throw GaudiException( "Inconsistent size of rawbank", "FTRawBankDecoder", StatusCode::FAILURE );
      }

      if ( to_unsigned( module ) > 5 ) {
        ++m_nonExistingModule;
        first += nClus;
        continue;
      }

      if constexpr ( vrsn == 3u ) {

        for ( auto it = first; it < first + nClus; ++it ) {
          short int c        = *it;
          unsigned  channel  = c & 127;
          int       fraction = ( c >> 7 ) & 1;
          bool      cSize    = ( c >> 8 ) & 1;

          // not the last cluster
          if ( !cSize && it < first + nClus - 1 ) {
            short int c2     = *( it + 1 );
            bool      cSize2 = ( c2 >> 8 ) & 1;

            if ( !cSize2 ) { // next cluster is not last fragment
              clus.addHit(
                  std::forward_as_tuple(
                      LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel}, fraction, 4 ),
                  bank->sourceID() );

              if ( msgLevel( MSG::VERBOSE ) ) {
                verbose() << format( "size<=4  channel %4d frac %3d size %3d code %4.4x", channel, fraction, cSize, c )
                          << endmsg;
              }
            } else { // fragmented cluster, last edge found
              unsigned channel2  = c2 & 127;
              int      fraction2 = ( c2 >> 7 ) & 1;

              unsigned int diff = ( channel2 - channel );

              if ( diff > 128 ) {
                error() << "something went terribly wrong here first fragment: " << channel
                        << " second fragment: " << channel2 << endmsg;
                throw GaudiException( "There is an inconsistency between Encoder and Decoder!", "FTRawBankDecoder",
                                      StatusCode::FAILURE );
              }
              // fragemted clusters, size > 2*max size
              // only edges were saved, add middles now
              if ( diff > m_clusterMaxWidth ) {

                // add the first edge cluster
                clus.addHit(
                    std::forward_as_tuple(
                        LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel}, fraction, 0 ),
                    bank->sourceID() ); // pseudoSize=0

                if ( msgLevel( MSG::VERBOSE ) ) {
                  verbose() << format( "first edge cluster %4d frac %3d size %3d code %4.4x", channel, fraction, cSize,
                                       c )
                            << endmsg;
                }

                for ( unsigned int i = m_clusterMaxWidth; i < diff; i += m_clusterMaxWidth ) {
                  // all middle clusters will have same size as the first cluster,
                  // so use same fraction
                  clus.addHit( std::forward_as_tuple(
                                   LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel + i},
                                   fraction, 0 ),
                               bank->sourceID() );

                  if ( msgLevel( MSG::VERBOSE ) ) {
                    verbose() << format( "middle cluster %4d frac %3d size %3d code %4.4x", channel + i, fraction,
                                         cSize, c )
                              << " added " << diff << endmsg;
                  }
                }

                // add the last edge
                clus.addHit( std::forward_as_tuple(
                                 LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel2},
                                 fraction2, 0 ),
                             bank->sourceID() );

                if ( msgLevel( MSG::VERBOSE ) ) {
                  verbose() << format( "last edge cluster %4d frac %3d size %3d code %4.4x", channel2, fraction2,
                                       cSize2, c2 )
                            << endmsg;
                }
              } else { // big cluster size upto size 8
                unsigned int firstChan = channel - int( ( m_clusterMaxWidth - 1 ) / 2 );
                unsigned int widthClus = 2 * diff - 1 + fraction2;

                unsigned int clusChanPosition = firstChan + ( widthClus - 1 ) / 2;
                int          frac             = ( widthClus - 1 ) % 2;

                // add the new cluster = cluster1+cluster2
                clus.addHit( std::forward_as_tuple( LHCb::Detector::FTChannelID{station, layer, quarter, module, mat,
                                                                                sipm, clusChanPosition},
                                                    frac, widthClus ),
                             bank->sourceID() );

                if ( msgLevel( MSG::VERBOSE ) ) {
                  verbose() << format( "combined cluster %4d frac %3d size %3d code %4.4x", channel, fraction, cSize,
                                       c )
                            << endmsg;
                }
              } // end if adjacent clusters
              ++it;
            }    // last edge foud
          }      // not the last cluster
          else { // last cluster, so nothing we can do
            clus.addHit(
                std::forward_as_tuple( LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel},
                                       fraction, 4 ),
                bank->sourceID() );

            if ( msgLevel( MSG::VERBOSE ) ) {
              verbose() << format( "size<=4  channel %4d frac %3d size %3d code %4.4x", channel, fraction, cSize, c )
                        << endmsg;
            }
          }    // last cluster added
        }      // end loop over clusters in one sipm
      } else { // bank vrsn == 2
        static_assert( vrsn == 2 );
        // normal clustering without any modification to clusters, should work for encoder=2
        for ( auto it = first; it < first + nClus; ++it ) {
          short int c        = *it;
          unsigned  channel  = ( c >> 0 ) & 127;
          int       fraction = ( c >> 7 ) & 1;
          int       cSize    = ( c >> 8 ) & 1;
          clus.addHit(
              std::forward_as_tuple( LHCb::Detector::FTChannelID{station, layer, quarter, module, mat, sipm, channel},
                                     fraction, ( cSize ? 0 : 4 ) ),
              bank->sourceID() );
        }
      }
      first += nClus;
    } // end loop over sipms
  }   // end loop over rawbanks
  clus.setOffsets();
  return clus;
}

//=============================================================================
// Main execution
//=============================================================================
FTLiteClusters FTRawBankDecoder::operator()( const EventContext& evtCtx, const LHCb::RawBank::View& banks,
                                             const FTReadoutMap<FTRawBankDecoder>& readoutMap ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of raw banks " << banks.size() << endmsg;
  if ( banks.empty() ) return {};

  // Testing the bank version
  unsigned int vrsn    = banks[0]->version();
  auto const   version = ( vrsn == 5 && m_decodeV5AsV4.value() ) ? 4 : vrsn;
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Bank version=v" << vrsn << " with decoding version=v" << version << endmsg;

  // Check if decoding version corresponds with readout version.
  readoutMap.compatible( version );

  // Estimate total number of clusters from bank sizes
  auto clus = [&]( unsigned int nClusters ) {
    switch ( version ) {
    case 0:
      return decode<0>( evtCtx, banks, readoutMap, nClusters );
    case 2:
      return decode<2>( evtCtx, banks, readoutMap, nClusters );
    case 3:
      return decode<3>( evtCtx, banks, readoutMap, nClusters );
    case 4:
      return decode<4>( evtCtx, banks, readoutMap, nClusters );
    case 5:
      return decode<5>( evtCtx, banks, readoutMap, nClusters );
    case 6:
      return decode<6>( evtCtx, banks, readoutMap, nClusters );
    case 7:
      return decode<7>( evtCtx, banks, readoutMap, nClusters );
    default:
      throw GaudiException( "Unknown bank version: " + std::to_string( version ), __FILE__, StatusCode::FAILURE );
    };
  }( LHCb::FTDAQ::nbFTClusters( banks ) );

  if ( msgLevel( MSG::VERBOSE ) ) {
    for ( const auto& c : clus.range() )
      verbose() << format( " channel %4u frac %3f size %3u ", c.channelID(), c.fraction(), c.pseudoSize() ) << endmsg;
  }

  // Assert that clusters are sorted
  assert( std::is_sorted( clus.range().begin(), clus.range().end(),
                          []( const LHCb::FTLiteCluster& lhs, const LHCb::FTLiteCluster& rhs ) {
                            return lhs.channelID() < rhs.channelID();
                          } ) &&
          "Clusters from the RawBanks not sorted. Should be sorted by construction." );

  // sort clusters according to PrFTHits (loop over quadrants)
  for_each_quadrant( clus, []( auto first, auto last, auto iUQua ) {
    if ( first == last ) return;
    // Swap clusters within modules
    // if quadrant==0 or 3 for even layers or quadrant==1 or 2 for odd layers
    if ( ( ( ( iUQua >> 2 ) & 1 ) == 0 ) ^ ( ( iUQua & 3 ) >> 1 ) ^ ( iUQua & 1 ) ) {
      reverse_each_module( first, last );
    }
    // Swap clusters within full quadrant
    if ( ( iUQua & 1 ) == 0 ) {    // quadrant==0 or 2
      std::reverse( first, last ); // swap clusters in quadrant
    }
  } );
  return clus;
}
