/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifndef USE_DD4HEP
#  include "DetDesc/ParamValidDataObject.h"
#endif
#include "DetDesc/ConditionKey.h"
#include "Detector/FT/FTChannelID.h"
#include "GaudiKernel/MsgStream.h"
#include <Detector/FT/FTSourceID.h>
#include <Event/RawBank.h>

#include <FTDAQ/FTRawBankParams.h>
#include <FTDet/DeFTDetector.h>
#include <array>
#include <optional>
#include <string>
#include <utility>
#include <vector>
#include <yaml-cpp/yaml.h>

#include "assert.h"

struct SiPMLinkPair final {
  // Set default values to kInvalid
  unsigned int bankNumber = LHCb::Detector::FTChannelID::kInvalidChannel().channelID();
  unsigned int linkIndex  = LHCb::Detector::FTChannelID::kInvalidChannel().channelID();
};

template <typename PARENT>
struct FTReadoutMap final {
public:
  FTReadoutMap() = default;

  FTReadoutMap( const PARENT* parent );

  FTReadoutMap( const PARENT* parent, const YAML::Node& rInfo );

  std::set<unsigned int> compatibleVersions() const;

  void compatible( const unsigned bankVersion ) const;

  static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key ) {
    assert( parent );
#ifdef USE_DD4HEP
    const auto cond_paths = std::array{std::string{"/world/AfterMagnetRegion/T/FT:ReadoutMap"}};
    // dd4hep will always have the tag, so skip the test
    const bool hasCond = true;
#else
    const auto cond_paths = std::array{std::string{"/dd/Conditions/ReadoutConf/FT/ReadoutMap"}};
    const bool hasCond    = parent->template existDet<ParamValidDataObject>( cond_paths[0] );
#endif
    if ( hasCond ) {
      return parent->addConditionDerivation( std::move( cond_paths ), std::move( key ),
                                             [parent]( YAML::Node const& c ) {
                                               return FTReadoutMap{parent, c};
                                             } );
    } else {
      // Use the FT detector as a proxy
      return parent->addConditionDerivation( {DeFTDetectorLocation::Default}, std::move( key ),
                                             [parent]( DeFT const& ) { return FTReadoutMap{parent}; } );
    }
  }

  // Getters
  unsigned int               nBanks() const { return m_nBanks; }
  LHCb::Detector::FTSourceID sourceID( unsigned int iBank ) const { return m_sourceIDs[iBank]; }
  unsigned int               iSource( unsigned int sourceID ) const {
    auto i = std::find( m_sourceIDs.begin(), m_sourceIDs.end(), sourceID );
    return ( i - m_sourceIDs.begin() );
  }

  // First FTChannelID <-> banknumber
  LHCb::Detector::FTChannelID channelIDShift( unsigned int bankNumber ) const {
    return m_FTBankFirstChannel[bankNumber];
  }

  unsigned int bankNumber( LHCb::Detector::FTChannelID id ) const {
    auto it = std::find_if( m_FTBankFirstChannel.begin(), m_FTBankFirstChannel.end(),
                            [&id]( const auto& firstChan ) { return id < firstChan; } );
    return it - m_FTBankFirstChannel.begin() - 1u;
  }

  // SiPM <-> Link
  SiPMLinkPair findBankNumberAndIndex( LHCb::Detector::FTChannelID sipm ) const {
    for ( unsigned int bankNumber = 0; bankNumber < FTRawBank::BankProperties::NbBanks; ++bankNumber ) {
      for ( unsigned int linkIndex = 0; linkIndex < FTRawBank::BankProperties::NbLinksPerBank; ++linkIndex ) {
        if ( m_linkMap[bankNumber][linkIndex] == sipm ) return {bankNumber, linkIndex};
      }
    }
    assert( 0 && "Did not find the bank number and index for sipm I" );
    return {};
  };

  // Getter for index remapping array for SiPM -> linkID at specific bank number
  std::array<int, FTRawBank::BankProperties::NbLinksPerBank> getSiPMRemapping( unsigned int sourceID ) const {
    return m_SiPMMap[this->iSource( sourceID )];
  };

  // Getter for index remapping array for SiPM -> linkID at specific bank number
  std::array<int, FTRawBank::BankProperties::NbLinksPerBank> getSiPMRemappingFromIndex( unsigned int iRow ) const {
    return m_SiPMMap[iRow];
  };

  // Link <-> SiPM
  LHCb::Detector::FTChannelID getGlobalSiPMID( unsigned int sourceID, unsigned int link ) const {
    auto iRow = iSource( sourceID );
    assert( link < FTRawBank::BankProperties::NbLinksPerBank && "Link numbers should be smaller than 24." );
    return m_linkMap[iRow][link];
  };
  // Link <-> SiPM
  LHCb::Detector::FTChannelID getGlobalSiPMIDFromIndex( unsigned int iSource, unsigned int link ) const {
    return m_linkMap[iSource][link];
  };

  // Translation functions between v6 and v7, for testing purposes
  unsigned int linkMapRow( unsigned int iBank ) const {
    int iBankInQ       = iBank % 5;
    int iQuarter       = ( iBank / 5 ) % 4;
    int iLayer         = ( iBank / 5 / 4 ) % 4;
    int iStation       = ( iBank / 5 / 4 / 4 );
    int iQuarterInHalf = iQuarter / 2;
    int shift          = ( iQuarter == 1 || iQuarter == 3 ) ? 0 : 120;
    int output         = shift + iBankInQ + iQuarterInHalf * 5 + iLayer * 5 * 2 + iStation * 5 * 2 * 4;

    return output;
  }

private:
  void readFile0( const YAML::Node& rInfo );
  void readFile1( const YAML::Node& rInfo );

  unsigned int                             m_nBanks;
  std::optional<unsigned int>              m_version;
  std::vector<LHCb::Detector::FTChannelID> m_FTBankFirstChannel;
  std::vector<LHCb::Detector::FTSourceID>  m_sourceIDs;

  std::array<std::array<LHCb::Detector::FTChannelID, FTRawBank::BankProperties::NbLinksPerBank>,
             FTRawBank::BankProperties::NbBanks>
      m_linkMap; // m_linkMap[bankNumber][linkID] = siPMID if plugged,  FTChannelID(-1) if not.
  std::array<std::array<int, FTRawBank::BankProperties::NbLinksPerBank>,
             FTRawBank::BankProperties::NbBanks>
      m_SiPMMap; // Array of arrays with index order to map SiPMID->linkID

  // Precalculated link information
  std::vector<bool> m_linkOrdering;
};

template <typename PARENT>
FTReadoutMap<PARENT>::FTReadoutMap( const PARENT* parent ) {
  parent->debug() << "ReadoutMap condition absent from the conditions database." << endmsg;
  parent->info() << "Conditions DB is compatible with FT bank version 2, 3." << endmsg;
}

template <typename PARENT>
FTReadoutMap<PARENT>::FTReadoutMap( const PARENT* parent, const YAML::Node& rInfo ) {
  if ( rInfo["version"] ) {
    m_version = rInfo["version"].as<int>();
    parent->debug() << "Found ReadoutMap version " << *m_version << " in the conditions database." << endmsg;
    parent->info() << "Conditions DB is compatible with FT bank version 7." << endmsg;
  } else {
    m_version = 0;
    parent->debug() << "Did not find ReadoutMap version parameter in the conditions database. Assuming version 0."
                    << endmsg;
    parent->info() << "Conditions DB is compatible with FT bank version 4, 5, 6." << endmsg;
  }
  switch ( *m_version ) {
  case 0:
    this->readFile0( rInfo );
    break;
  case 1:
    this->readFile1( rInfo );
    break;
  default:
    throw GaudiException( "Unknown readout map version from conditions database : " + std::to_string( *m_version ),
                          __FILE__, StatusCode::FAILURE );
  }
}

template <typename PARENT>
std::set<unsigned int> FTReadoutMap<PARENT>::compatibleVersions() const {
  if ( !m_version ) {
    return {2u, 3u};
  } else if ( *m_version == 0 ) {
    return {0u, 4u, 5u, 6u};
  } else if ( *m_version == 1 ) {
    return {7u};
  } else {
    throw GaudiException( "Unknown conditions version: " + std::to_string( *m_version ), __FILE__,
                          StatusCode::FAILURE );
  }
}

template <typename PARENT>
void FTReadoutMap<PARENT>::compatible( const unsigned int bankVersion ) const {
  auto comp = compatibleVersions();
  if ( !comp.count( bankVersion ) ) {
    std::string s;
    if ( m_version ) {
      s = "Conditions version: " + std::to_string( *m_version );
    } else {
      s = "No readout condition";
    }
    throw GaudiException( s + " is incompatible with bank version " + std::to_string( bankVersion ), __FILE__,
                          StatusCode::FAILURE );
  }
}

// Due to different expected structures in the DDDB for the construction of the readoutmap that is needed,
// 2 different versions of the readFile function have been added:
//
// Decoding version <=6 uses readFile 0
// Decoding version  7  uses readFile 1
//
// Check have been added to the decoding to see if the correct version of the decoding is used with the
// correct readFile version.
// Version 1 for decoding version 7
template <typename PARENT>
void FTReadoutMap<PARENT>::readFile1( const YAML::Node& rInfo ) {

  // Source IDs
  const auto& sourceIDs = rInfo["sourceIDs"].as<std::vector<int>>();
  m_nBanks              = sourceIDs.size();
  m_sourceIDs.clear();
  m_sourceIDs.reserve( sourceIDs.size() );
  std::transform( sourceIDs.begin(), sourceIDs.end(), std::back_inserter( m_sourceIDs ),
                  []( unsigned int i ) { return LHCb::Detector::FTSourceID{i}; } );

  // Make the map of hardware links with switchings
  const auto& allLinks = rInfo["LinkMap"].as<std::vector<int>>();
  if ( allLinks.size() != m_nBanks * FTRawBank::BankProperties::NbLinksPerBank ) {
    throw GaudiException( "Readout map has a different number of links (" + std::to_string( allLinks.size() ) +
                              ") than expected (" +
                              std::to_string( m_nBanks * FTRawBank::BankProperties::NbLinksPerBank ) + ")",
                          __FILE__, StatusCode::FAILURE );
  }
  for ( unsigned int i = 0; i < sourceIDs.size(); i++ ) {
    // Start with linkID->SiPMID map
    std::array<LHCb::Detector::FTChannelID, FTRawBank::BankProperties::NbLinksPerBank> temp;
    for ( unsigned int j = 0; j < FTRawBank::BankProperties::NbLinksPerBank; j++ ) {
      temp[j] = ( LHCb::Detector::FTChannelID( allLinks[i * FTRawBank::BankProperties::NbLinksPerBank + j] ) );
      if ( temp[j] != LHCb::Detector::FTChannelID::kInvalidChannel() &&
           ( temp[j].station() != m_sourceIDs[i].station() || temp[j].layer() != m_sourceIDs[i].layer() ||
             temp[j].quarter() != m_sourceIDs[i].quarter() ) ) {
        std::cout << "ERROR: Link " << temp[j] << " cannot belong to sourceID " << m_sourceIDs[i] << "\n";
        throw GaudiException( "Incoherent readout map for bank n." + std::to_string( i ), __FILE__,
                              StatusCode::FAILURE );
      }
    };

    // Now create the SiPMID->linkID index array
    std::array<int, FTRawBank::BankProperties::NbLinksPerBank> temp_SiPM; // SiPMID->linkID
    auto                                                       sortedMap = temp;
    std::sort( sortedMap.begin(), sortedMap.end() );
    int place( 0 );
    int nInvalid( 0 );
    for ( int i_link = 0; i_link < FTRawBank::BankProperties::NbLinksPerBank; i_link++ ) {
      // Ignore invalids
      if ( sortedMap[i_link] == LHCb::Detector::FTChannelID::kInvalidChannel() ) {
        nInvalid++;
        continue;
      }
      // Look for the i_link-th elements
      int j( 0 );
      for ( ; j < FTRawBank::BankProperties::NbLinksPerBank; j++ ) {
        if ( sortedMap[i_link] == temp[j] ) break;
      }
      temp_SiPM[place] = j;
      place++;
    }
    // Now pad with -1 for invalid links
    for ( int i_invalid = 0; i_invalid < nInvalid; i_invalid++ ) {
      // Needs - 1 since FTRawBank::BankProperties::NbLinksPerBank is 1 bigger than array size
      temp_SiPM[FTRawBank::BankProperties::NbLinksPerBank - 1 - i_invalid] = -1;
    }
    m_linkMap[i] = temp;               // linkID->SiPMID
    m_SiPMMap[i] = temp_SiPM;          // SiPMID->linkID
    m_linkOrdering.push_back( false ); // TODO: check ordering there
  }
}

// Version 0 for decoding versions <=6
template <typename PARENT>
void FTReadoutMap<PARENT>::readFile0( const YAML::Node& rInfo ) {
  const auto& stations     = rInfo["FTBankStation"].as<std::vector<int>>();
  const auto& layers       = rInfo["FTBankLayer"].as<std::vector<int>>();
  const auto& quarters     = rInfo["FTBankQuarter"].as<std::vector<int>>();
  const auto& firstModules = rInfo["FTBankFirstModule"].as<std::vector<int>>();
  const auto& firstMats    = rInfo["FTBankFirstMat"].as<std::vector<int>>();

  // Construct the first channel attribute
  m_nBanks = stations.size();
  m_FTBankFirstChannel.reserve( m_nBanks );
  for ( unsigned int i = 0; i < m_nBanks; i++ ) {
    m_FTBankFirstChannel.emplace_back(
        LHCb::Detector::FTChannelID::StationID( stations[i] ), LHCb::Detector::FTChannelID::LayerID( layers[i] ),
        LHCb::Detector::FTChannelID::QuarterID( quarters[i] ), LHCb::Detector::FTChannelID::ModuleID( firstModules[i] ),
        LHCb::Detector::FTChannelID::MatID( firstMats[i] ), 0u, 0u );
  }
}
